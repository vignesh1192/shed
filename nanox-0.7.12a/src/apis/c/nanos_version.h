#ifndef  _NANOS_VERSION_H_
#define  _NANOS_VERSION_H_
#define NANOS_API_MASTER 5026
#define NANOS_API_WORKSHARING 1000
#define NANOS_API_DEPS_API 1001
#define NANOS_API_COPIES_API 1002
#define NANOS_API_OPENMP 8
#define NANOS_API_OPENCL 1000
#ifdef _MERCURIUM
#pragma nanos interface family(master) version(5026)
#pragma nanos interface family(worksharing) version(1000)
#pragma nanos interface family(deps_api) version(1001)
#pragma nanos interface family(copies_api) version(1002)
#pragma nanos interface family(openmp) version(8)
#pragma nanos interface family(opencl) version(1000)
#endif // _MERCURIUM
#endif // _NANOS_VERSION_H_
