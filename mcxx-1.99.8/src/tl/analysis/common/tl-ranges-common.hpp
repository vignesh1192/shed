

/*--------------------------------------------------------------------
 (C) Copyright 2006-2014 Barcelona Supercomputing Center             **
 Centro Nacional de Supercomputacion

 This file is part of Mercurium C/C++ source-to-source compiler.

 See AUTHORS file in the top level directory for information
 regarding developers and contributors.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 3 of the License, or (at your option) any later version.

 Mercurium C/C++ source-to-source compiler is distributed in the hope
 that it will be useful, but WITHOUT ANY WARRANTY; without even the
 implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU Lesser General Public License for more
 details.

 You should have received a copy of the GNU Lesser General Public
 License along with Mercurium C/C++ source-to-source compiler; if
 not, write to the Free Software Foundation, Inc., 675 Mass Ave,
 Cambridge, MA 02139, USA.
 --------------------------------------------------------------------*/

#ifndef TL_RANGE_ANALYSIS_UTILS_HPP
#define TL_RANGE_ANALYSIS_UTILS_HPP

#include "tl-induction-variables-data.hpp"
#include "tl-nodecl-visitor.hpp"

#define RANGES_DEBUG CURRENT_CONFIGURATION->debug_options.ranges_verbose

namespace TL {
namespace Analysis {
namespace Utils {
    
    // ******************************************************************************************* //
    // *********************************** Ranges arithmetic ************************************* //

    bool nodecl_is_Z_range(const NBase& n);

    NBase range_sub(const NBase& r1, const NBase& r2);

    NBase range_addition(const NBase& r1, const NBase& r2);
    NBase range_subtraction(const NBase& r1, const NBase& r2);
    NBase range_multiplication(const NBase& r1, const NBase& r2);
    NBase range_division(const NBase& r1, const NBase& r2);
    NBase range_intersection(const NBase& r, const NBase& r2);
    NBase range_union(const NBase& r1, const NBase& r2);
    Nodecl::Range range_value_add(const Nodecl::Range& r, const NBase& v);
    Nodecl::Range range_value_sub(const Nodecl::Range& r, const NBase& v);
    Nodecl::Range range_value_mul(const Nodecl::Range& r, const NBase& v);
    Nodecl::Range range_value_div(const Nodecl::Range& r, const NBase& v);

    // ********************************* END Ranges arithmetic *********************************** //
    // ******************************************************************************************* //



    // ******************************************************************************************* //
    // ******************************* Range Analysis Constraints ******************************** //

    /*! The possible constraints are:
     *  - Y = [lb, ub]
     *  - Y = X1 bin_op X2
     *  - Y = phi(X1, X2)
     *  - Y = X ∩ [lb, ub]
     */
    struct Constraint {
        TL::Symbol _ssa_sym;    /*!< symbol associated to a given variable at this point of the program */
        NBase _value;           /*!< value applying to the variable */

        // *** Constructors *** //
        Constraint();               // Required for std::map operations
        Constraint(const TL::Symbol& constr_sym, const NBase& value);

        // *** Getters and Setters *** //
        const TL::Symbol& get_symbol() const;
        void set_symbol(const TL::Symbol& s);
        const NBase& get_value() const;

        // *** Comparators *** //
        bool operator!=(const Constraint& c) const;
        bool operator==(const Constraint& c) const;
    };

    // ***************************** END Range Analysis Constraints ****************************** //
    // ******************************************************************************************* //
    
}
}
}

#endif          // TL_RANGE_ANALYSIS_UTILS_HPP
