/*--------------------------------------------------------------------
(C) Copyright 2006-2014 Barcelona Supercomputing Center             *
Centro Nacional de Supercomputacion

This file is part of Mercurium C/C++ source-to-source compiler.

See AUTHORS file in the top level directory for information
regarding developers and contributors.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

Mercurium C/C++ source-to-source compiler is distributed in the hope
that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public
License along with Mercurium C/C++ source-to-source compiler; if
not, write to the Free Software Foundation, Inc., 675 Mass Ave,
Cambridge, MA 02139, USA.
--------------------------------------------------------------------*/

#include "cxx-cexpr.h"
#include "cxx-process.h"
#include "tl-expression-reduction.hpp"
#include "tl-range-analysis.hpp"

#include <algorithm>
#include <limits.h>
#include <fstream>
#include <list>
#include <set>
#include <unistd.h>
#include <sys/stat.h>
#include <unistd.h>

namespace TL {
namespace Analysis {

namespace {
    // *** Variables and methods to simulate SSA during the Constraint Graph construction *** //
    unsigned int non_sym_constraint_id = 0;

    std::map<Symbol, NBase> ssa_to_original_var;

    //! This maps stores the relationship between each variable in a given node and 
    //! the last identifier used to create a constraint for that variable
    std::map<NBase, unsigned int, Nodecl::Utils::Nodecl_structural_less> var_to_last_constraint_id;

    unsigned int get_next_id(const NBase& n)
    {
        unsigned int next_id = 0;
        if (!n.is_null())
        {
            if(var_to_last_constraint_id.find(n) != var_to_last_constraint_id.end())
                next_id = var_to_last_constraint_id[n] + 1;
            var_to_last_constraint_id[n] = next_id;
        }
        else
        {
            next_id = ++non_sym_constraint_id;
        }
        return next_id;
    }

    // *** Convenient global constants to create the ranges *** //
    const_value_t* zero = const_value_get_zero(/*num_bytes*/ 4, /*sign*/1);
    const_value_t* one = const_value_get_one(/*num_bytes*/ 4, /*sign*/1);
    const_value_t* minus_one = const_value_get_minus_one(/*num_bytes*/ 4, /*sign*/1);
    const_value_t* int_max = const_value_get_integer(LONG_MAX, /*num_bytes*/4, /*sign*/1);
    const NBase& plus_inf = Nodecl::Analysis::PlusInfinity::make(Type::get_long_int_type(), int_max);
    const_value_t* int_min = const_value_get_integer(LONG_MIN, /*num_bytes*/4, /*sign*/1);
    const NBase& minus_inf = Nodecl::Analysis::MinusInfinity::make(Type::get_long_int_type(), int_min);
}

    // ***************************************************************************** //
    // ************ Class replacing original variables with ssa symbols ************ //

    ConstraintReplacement::ConstraintReplacement(
            VarToConstraintMap* input_constraints,
            Constraints *constraints,
            std::vector<Symbol> *ordered_constraints)
        : _input_constraints(input_constraints),
          _constraints(constraints),
          _ordered_constraints(ordered_constraints) // Attributes needed to create new constraints
    {}

    void ConstraintReplacement::visit(const Nodecl::ArraySubscript& n)
    {
        if (_input_constraints->find(n) != _input_constraints->end())
            n.replace((*_input_constraints)[n].get_symbol().make_nodecl(/*set_ref_type*/false));
        else
        {
            // 1. Build a symbol for the new constraint based on the name of the original variable
            std::stringstream ss; ss << get_next_id(n);
            Symbol s(Utils::get_nodecl_base(n).get_symbol());
            std::string subscripts_str;
            const Nodecl::List& subscripts = n.get_subscripts().as<Nodecl::List>();
            for (Nodecl::List::const_iterator it = subscripts.begin(); it != subscripts.end(); ++it)
            {
                if (_input_constraints->find(*it) != _input_constraints->end())
                    subscripts_str += (*_input_constraints)[*it].get_symbol().get_name();
                else    // The subscript is a global variable
                    subscripts_str += it->prettyprint();
                subscripts_str += "_";
            }
            std::string ssa_name = s.get_name() + "_" + subscripts_str + ss.str();
            Symbol ssa_sym(n.retrieve_context().new_symbol(ssa_name));
            Type t = s.get_type();
            ssa_sym.set_type(t);
            ssa_to_original_var[ssa_sym] = n;

            // 2. Build the value for the constraint
            NBase val = Nodecl::Range::make(minus_inf.shallow_copy(),
                                            plus_inf.shallow_copy(),
                                            const_value_to_nodecl(zero), t);

            // 3. Build the constraint and insert it in the constraints map
            ConstraintBuilder cbv(*_input_constraints, _constraints, _ordered_constraints);
            Utils::Constraint c = cbv.build_constraint(ssa_sym, val, t, __GlobalVar);
            (*_input_constraints)[n] = c;

            n.replace(ssa_sym.make_nodecl(/*set_ref_type*/false));
        }
    }

    void ConstraintReplacement::visit(const Nodecl::Cast& n)
    {   // Remove casts from the constraints
        n.replace(n.get_rhs());
        walk(n);
    }

    void ConstraintReplacement::visit(const Nodecl::ClassMemberAccess& n)
    {
        if (_input_constraints->find(n) != _input_constraints->end())
            n.replace((*_input_constraints)[n].get_symbol().make_nodecl(/*set_ref_type*/false));
        else
        {
            // Create a new ssa variable here to use it from now on in the current function
            // 1. Build a symbol for the new constraint based on the name of the original variable
            std::stringstream ss; ss << get_next_id(n);
            std::string ssa_name = n.prettyprint() + "_" + ss.str();
            Symbol ssa_sym(n.retrieve_context().new_symbol(ssa_name));
            Type t = n.get_type();
            ssa_sym.set_type(t);
            ssa_to_original_var[ssa_sym] = n;
            // 2. Build the value of the constraint
            NBase val = Nodecl::Range::make(minus_inf.shallow_copy(),
                                            plus_inf.shallow_copy(),
                                            const_value_to_nodecl(zero), t);
            // 3. Build the constraint and insert it in the constraints map
            ConstraintBuilder cbv(*_input_constraints, _constraints, _ordered_constraints);
            Utils::Constraint c = cbv.build_constraint(ssa_sym, val, t, __GlobalVar);
            (*_input_constraints)[n] = c;

            n.replace(ssa_sym.make_nodecl(/*set_ref_type*/false));
        }
    }

    void ConstraintReplacement::visit(const Nodecl::FunctionCall& n)
    {
        if (_input_constraints->find(n) != _input_constraints->end())
            n.replace((*_input_constraints)[n].get_symbol().make_nodecl(/*set_ref_type*/false));
        else
        {
            Type t(Type::get_long_int_type());
            NBase val = Nodecl::Range::make(
                    minus_inf.shallow_copy(),
                    plus_inf.shallow_copy(),
                    const_value_to_nodecl(zero), t);
            n.replace(val);
        }
    }

    void ConstraintReplacement::visit(const Nodecl::Symbol& n)
    {
        if (_input_constraints->find(n) == _input_constraints->end())
        {
            // FunctionCalls are replaced with the value [-inf, +inf]
            if (n.get_symbol().is_function())
            {
                // 1. Build a symbol for the new constraint based on the name of the original variable
                std::stringstream ss; ss << get_next_id(n);
                Symbol orig_s(Utils::get_nodecl_base(n).get_symbol());
                std::string constr_name = orig_s.get_name() + "_" + ss.str();
                Symbol s(n.retrieve_context().new_symbol(constr_name));
                Type t = orig_s.get_type();
                s.set_type(t);
                ssa_to_original_var[s] = n;
                // 2. Get the value for the constraint
                NBase val = Nodecl::Range::make(minus_inf.shallow_copy(),
                                                plus_inf.shallow_copy(),
                                                const_value_to_nodecl(zero), t);
                // 3. Build the constraint and insert it in the constraints map
                ConstraintBuilder cbv(*_input_constraints, _constraints, _ordered_constraints);
                Utils::Constraint c = cbv.build_constraint(s, val, t, __GlobalVar);
                (*_input_constraints)[n] = c;
                return;
            }

            // Check for global variables
            ERROR_CONDITION(n.get_symbol().get_scope().is_namespace_scope(),
                    "No constraints found for non-global symbol %s in locus %s."
                    "We should replace the variable with the corresponding constraint.",
                    n.prettyprint().c_str(), n.get_locus_str().c_str());

            // n is a global variable
            return;
        }

        n.replace((*_input_constraints)[n].get_symbol().make_nodecl(/*set_ref_type*/false));
    }

    // ************ Class replacing original variables with ssa symbols ************ //
    // ***************************************************************************** //



    // ***************************************************************************** //
    // **************** Visitor building constraints from statements *************** //

    ConstraintBuilder::ConstraintBuilder(
            const VarToConstraintMap& input_constraints_map,
            Constraints *constraints,
            std::vector<Symbol> *ordered_constraints)
        : _input_constraints(input_constraints_map), _output_constraints(), 
          _output_true_constraints(), _output_false_constraints(), 
          _constraints(constraints), _ordered_constraints(ordered_constraints)
    {}

    ConstraintBuilder::ConstraintBuilder(
            const VarToConstraintMap& input_constraints_map,
            const VarToConstraintMap& current_constraints,
            Constraints *constraints,
            std::vector<Symbol> *ordered_constraints)
        : _input_constraints(input_constraints_map), _output_constraints(current_constraints),
          _output_true_constraints(), _output_false_constraints(), 
          _constraints(constraints), _ordered_constraints(ordered_constraints)
    {}

    Utils::Constraint ConstraintBuilder::build_constraint(
            const Symbol& s,
            const NBase& val,
            const Type& t,
            ConstraintKind c_kind)
    {
        // Create the constraint
        Utils::Constraint c(s, val);

        // Insert the constraint in the global structures that will allow us building the Constraint Graph
        if (_constraints->find(s) == _constraints->end())
            _ordered_constraints->push_back(s);
        (*_constraints)[s] = val;

        // Print the constraint in the standard error
        print_constraint(c_kind, s, val, t);

        return c;
    }

    void ConstraintBuilder::compute_parameters_constraints(const ObjectList<Symbol>& params)
    {
        for (ObjectList<Symbol>::const_iterator it = params.begin(); it != params.end(); ++it)
        {
            Symbol param = *it;

            // Avoid function pointers
            if (param.get_type().is_pointer()
                    && param.get_type().points_to().is_function())
                continue;

            Nodecl::Symbol param_n = param.make_nodecl(/*set_ref_type*/false);
            Type t = param.get_type();

            // Build a symbol for the new constraint based on the name of the original variable
            std::stringstream ss; ss << get_next_id(param_n);
            std::string ssa_name = param.get_name() + "_" + ss.str();
            Symbol ssa_sym(param.get_scope().new_symbol(ssa_name));
            ssa_sym.set_type(t);
            ssa_to_original_var[ssa_sym] = param_n;

            // Get the value for the constraint
            NBase val = Nodecl::Range::make(minus_inf.shallow_copy(), 
                                            plus_inf.shallow_copy(), 
                                            const_value_to_nodecl(zero), t);

            // Build the constraint and insert it in the constraints map
            Utils::Constraint c = build_constraint(ssa_sym, val, t, __Parameter);
            _output_constraints[param_n] = c;
        }
    }

    void ConstraintBuilder::set_false_constraint_to_inf(const NBase& n)
    {
        if (n.is<Nodecl::Equal>()
                || n.is<Nodecl::LowerThan>() || n.is<Nodecl::LowerOrEqualThan>()
                || n.is<Nodecl::GreaterThan>() || n.is<Nodecl::GreaterOrEqualThan>())
        {
            // False constraints are not just the negation of the condition,
            // because LHS fulfilling is not enough for the whole condition to fulfill
            const NBase& lhs = n.as<Nodecl::Equal>().get_lhs();
            ERROR_CONDITION(_output_false_constraints.find(lhs) == _output_false_constraints.end(),
                            "Nodecl %s not found in the set of 'output false constraints' while replacing constraint",
                            lhs.prettyprint().c_str());
            Utils::Constraint old_c_false = _output_false_constraints[lhs];
            TL::Symbol old_s = old_c_false.get_symbol();
            const NBase& old_val = old_c_false.get_value();
            ERROR_CONDITION(!old_val.is<Nodecl::Analysis::RangeIntersection>(),
                            "Constraint value of a 'false' flow edge has type '%s' when RangeIntersection expected.\n",
                            ast_print_node_type(old_val.get_kind()));
            const NBase& old_val_input_ssa_var = old_val.as<Nodecl::Analysis::RangeIntersection>().get_lhs();
            ERROR_CONDITION(ssa_to_original_var.find(old_val_input_ssa_var.get_symbol()) == ssa_to_original_var.end(),
                            "Constraint value of a 'false' flow edge does not contain ssa variable, but '%s' instead",
                            old_val_input_ssa_var.prettyprint().c_str());
            Type t(Type::get_long_int_type());
            NBase val_false = Nodecl::Analysis::RangeIntersection::make(
                    old_val_input_ssa_var,
                    Nodecl::Range::make(
                            minus_inf.shallow_copy(),
                            plus_inf.shallow_copy(),
                            const_value_to_nodecl(zero), t),
                    t);
            Utils::Constraint new_c_false = build_constraint(old_s, val_false, t, __Replace);
            _output_false_constraints[lhs] = new_c_false;
        }
        else if (n.is<Nodecl::LogicalAnd>() || n.is<Nodecl::Different>())
        {}  // Nothing to be done because the infinite range is set to the edge recursively
        else
        {
            internal_error("Unexpected node of type %s while setting false branch to [-inf, +inf]\n",
                           ast_print_node_type(n.get_kind()));
        }
    }

    VarToConstraintMap ConstraintBuilder::get_output_constraints() const
    {
        return _output_constraints;
    }

    VarToConstraintMap ConstraintBuilder::get_output_true_constraints() const
    {
        return _output_true_constraints;
    }

    VarToConstraintMap ConstraintBuilder::get_output_false_constraints() const
    {
        return _output_false_constraints;
    }

    void ConstraintBuilder::join_list(TL::ObjectList<Utils::Constraint>& list)
    {
        WARNING_MESSAGE("join_list of a list of constraint is not yet supported. Doing nothing.", 0);
    }

    void ConstraintBuilder::visit_assignment(const NBase& lhs, const NBase& rhs)
    {
        // 1.- Build a symbol for the new constraint based on the name of the original variable
        std::stringstream ss; ss << get_next_id(lhs);
        Symbol s(Utils::get_nodecl_base(lhs).get_symbol());

        std::string subscripts_str;
        if (lhs.no_conv().is<Nodecl::ArraySubscript>())
        {
            const Nodecl::List& subscripts = lhs.no_conv().as<Nodecl::ArraySubscript>().get_subscripts().as<Nodecl::List>();
            for (Nodecl::List::const_iterator it = subscripts.begin(); it != subscripts.end(); ++it)
            {
                if (_input_constraints.find(*it) != _input_constraints.end())
                    subscripts_str += _input_constraints[*it].get_symbol().get_name();
                else    // The subscript is a global variable
                    subscripts_str += it->prettyprint();
                subscripts_str += "_";
            }
        }
        std::string ssa_name = s.get_name() + "_" + subscripts_str + ss.str();
        Symbol ssa_sym(lhs.retrieve_context().new_symbol(ssa_name));
        Type t = s.get_type();
        ssa_sym.set_type(t);
        ssa_to_original_var[ssa_sym] = lhs;

        // 2.- Build the value of the constraint
        NBase val;
        if (rhs.is_constant())       // x = c;    -->    X1 = c
            val = Nodecl::Range::make(rhs.shallow_copy(), rhs.shallow_copy(), const_value_to_nodecl(zero), t);
        else 
        {   // Replace all the memory accesses by the symbols of the constraints arriving to the current node
            val = rhs.no_conv().shallow_copy();
            ConstraintReplacement cr(&_input_constraints, _constraints, _ordered_constraints);
            cr.walk(val);
        }

        // 3.- Build the constraint and insert it in the corresponding maps
        Utils::Constraint c = build_constraint(ssa_sym, val, t, __BinaryOp);
        _input_constraints[lhs] = c;
        _output_constraints[lhs] = c;
    }
    
    
    void ConstraintBuilder::visit_increment(const NBase& rhs, bool positive)
    {
        // 1.- Check the integrity of the analysis
        ERROR_CONDITION(_input_constraints.find(rhs) == _input_constraints.end(),
                        "Some input constraint required for the increment's RHS '%s' (%s).\n",
                        rhs.prettyprint().c_str(),
                        ast_print_node_type(rhs.get_kind()));
        
        // 2.- Build a symbol for the new constraint based on the name of the original variable
        std::stringstream ss; ss << get_next_id(rhs);
        Symbol s(Utils::get_nodecl_base(rhs).get_symbol());
        Type t(s.get_type());
        std::string constr_name = s.get_name() + "_" + ss.str();
        Symbol ssa_sym(rhs.retrieve_context().new_symbol(constr_name));
        ssa_sym.set_type(t);
        ssa_to_original_var[ssa_sym] = rhs;

        NBase val;
        Symbol entry_ssa_sym = _input_constraints[rhs].get_symbol();
        if (positive)
        {
            val = Nodecl::Add::make(entry_ssa_sym.make_nodecl(/*set_ref_type*/false),
                                    const_value_to_nodecl(one), t);
        }
        else
        {
            val = Nodecl::Minus::make(entry_ssa_sym.make_nodecl(/*set_ref_type*/false),
                                      const_value_to_nodecl(one), t);
        }
        Utils::Constraint c = build_constraint(ssa_sym, val, t, __UnaryOp);
        _input_constraints[rhs] = c;
        _output_constraints[rhs] = c;
    }

    void ConstraintBuilder::visit_comparison_side(
            const NBase& n,
            const NBase& val,
            char side /*l:left, r:right*/,
            node_t comparison_kind)
    {
        // 1.- Check the input is something we expect: 'side' is a symbol
        ERROR_CONDITION(_input_constraints.find(n) == _input_constraints.end(),
                        "Some input constraint required for the variable '%s' when parsing a %s nodecl",
                        n.prettyprint().c_str(), ast_print_node_type(comparison_kind));
        if (!val.is_constant())
        {
            ERROR_CONDITION(_input_constraints.find(val) == _input_constraints.end(),
                            "Some input constraint required for the variable '%s' when parsing a %s nodecl",
                            val.prettyprint().c_str(), ast_print_node_type(comparison_kind));
        }

        // 2.- Get the original symbol of the lhs
        const NBase& base_n = Utils::get_nodecl_base(n);
        Symbol s(base_n.get_symbol());
        Type t = s.get_type();
        std::string s_name = s.get_name();

        // 3.- Get the last ssa symbol related to the original symbol
        std::string last_ssa_name = _input_constraints.find(n)->second.get_symbol().get_name();
        Scope ctx = n.retrieve_context();
        Symbol last_ssa = ctx.get_symbol_from_name(last_ssa_name);
        ERROR_CONDITION(!last_ssa.is_valid(),
                        "No symbol '%s' found while building constraint for variable '%s'",
                        last_ssa_name.c_str(), n.prettyprint().c_str());

        // 4.- Compute the constraints generated from the condition
        //     to the possible TRUE and FALSE exit edges
        // =========================================================

        // 4.1.- Replace, if necessary, all memory accesses
        //       by the corresponding SSA symbols arriving to the current node
        if (!val.is_constant())
        {
            ConstraintReplacement cr(&_input_constraints, _constraints, _ordered_constraints);
            cr.walk(val);
        }

        // 4.2.- Build the symbols created for the TRUE and FALSE edges
        // 4.2.1.- Build the TRUE constraint symbol
        std::stringstream ss_true; ss_true << get_next_id(n);
        Symbol s_true(ctx.new_symbol(s_name + "_" + ss_true.str()));
        s_true.set_type(t);
        ssa_to_original_var[s_true] = n;
        // 4.2.2.- Build the FALSE constraint symbol
        std::stringstream ss_false; ss_false << get_next_id(n);
        Symbol s_false(ctx.new_symbol(s_name + "_" + ss_false.str()));
        s_false.set_type(t);
        ssa_to_original_var[s_false] = n;

        // 4.3.- Build the values created for the TRUE and FALSE edges
        NBase val_true, val_false;
        switch (comparison_kind)
        {
            case NODECL_EQUAL:
            {   
                switch (side)
                {
                    case 'l':
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value
                        // v == x;   --TRUE--->   v1 = v0 ∩ [x, x]
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(val.shallow_copy(),
                                                    val.shallow_copy(), 
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value
                        // v == x;   --FALSE-->   v2 = v0 ∩ ([-∞, x-1] U [x+1, -∞])
                        NBase lb, ub;
                        if (val.is_constant())
                        {
                            lb = const_value_to_nodecl(const_value_add(val.get_constant(), one));
                            ub = const_value_to_nodecl(const_value_sub(val.get_constant(), one));
                        }
                        else
                        {
                            lb = Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t);
                            ub = Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t);
                        }
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Analysis::RangeUnion::make(
                                        Nodecl::Range::make(minus_inf.shallow_copy(), ub, 
                                                            const_value_to_nodecl(zero), t),
                                        Nodecl::Range::make(lb, plus_inf.shallow_copy(), 
                                                            const_value_to_nodecl(zero), t),
                                        t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                };
            }
            case NODECL_DIFFERENT:
            {
                switch (side)
                {
                    case 'l':
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value
                        // v != x;   --TRUE--->   v1 = v0 ∩ ([-∞, x-1] U [x+1, -∞])
                        NBase lb, ub;
                        if (val.is_constant())
                        {
                            lb = const_value_to_nodecl(const_value_add(val.get_constant(), one));
                            ub = const_value_to_nodecl(const_value_sub(val.get_constant(), one));
                        }
                        else
                        {
                            lb = Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t);
                            ub = Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t);
                        }
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Analysis::RangeUnion::make(
                                        Nodecl::Range::make(minus_inf.shallow_copy(), ub,
                                                            const_value_to_nodecl(zero), t),
                                        Nodecl::Range::make(lb, plus_inf.shallow_copy(),
                                                            const_value_to_nodecl(zero), t),
                                        t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value
                        // v != x;   --FALSE-->   v2 = v0 ∩ [x, x]
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(val.shallow_copy(),
                                                    val.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                };
            }
            case NODECL_LOWER_OR_EQUAL_THAN:
            {   
                switch (side)
                {
                    case 'l':
                    {
                        // 4.3.1.- Build the TRUE constraint value
                        // v <= x;   --TRUE--->   v1 = v0 ∩ [-∞, x]
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(minus_inf.shallow_copy(), 
                                                    val.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value
                        // v <= x;   --FALSE-->   v2 = v0 ∩ [x+1, +∞]
                        NBase lb = (val.is_constant() ? const_value_to_nodecl(const_value_add(val.get_constant(), one)) 
                                                      : Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(lb,
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value
                        // x <= v;   --TRUE--->   v1 = v0 ∩ [x, +∞]
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(val.shallow_copy(),
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value
                        // x <= v;   --FALSE-->   v2 = v0 ∩ [-∞, x-1]
                        NBase ub = (val.is_constant() ? const_value_to_nodecl(const_value_sub(val.get_constant(), one)) 
                                                      : Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(plus_inf.shallow_copy(),
                                                    ub,
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                }
            }
            case NODECL_LOWER_THAN:
            {
                switch (side)
                {
                    case 'l':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the LHS
                        // v < x;   --TRUE--->  v1 = v0 ∩ [-∞, x-1]
                        NBase ub = (val.is_constant() ? const_value_to_nodecl(const_value_sub(val.get_constant(), one)) 
                                                      : Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(minus_inf.shallow_copy(),
                                                    ub,
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the LHS
                        // v < x;   --FALSE-->  v2 = v0 ∩ [x, +∞]
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(val.shallow_copy(),
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the RHS
                        // x < v   --TRUE--->  v1 = v0 ∩ [x+1, +∞]
                        NBase lb = (val.is_constant() ? const_value_to_nodecl(const_value_add(val.get_constant(), one))
                                                      : Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(lb,
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the RHS
                        // x < v   --FALSE-->  v2 = v0 ∩ [-∞, x]
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(minus_inf.shallow_copy(),
                                                    val.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                };
                break;
            }
            case NODECL_GREATER_OR_EQUAL_THAN:
            {
                switch (side)
                {
                    case 'l':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the LHS
                        // v >= x;   --TRUE--->  v1 = v0 ∩ [x, +∞]
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(val.shallow_copy(),
                                                    plus_inf.shallow_copy(), 
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the LHS
                        // v >= x;   --FALSE-->  v2 = v0 ∩ [-∞, x-1]
                        NBase ub = (val.is_constant() ? const_value_to_nodecl(const_value_sub(val.get_constant(), one)) 
                                                      : Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(minus_inf.shallow_copy(), 
                                                    ub, 
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the RHS
                        // x >= v;   --TRUE--->  v1 = v0 ∩ [-∞, x]
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(minus_inf.shallow_copy(),
                                                    val.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the RHS
                        // x >= v;   --FALSE--->  v1 = v0 ∩ [x+1, +∞]
                        NBase lb = (val.is_constant() ? const_value_to_nodecl(const_value_add(val.get_constant(), one))
                                                      : Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(lb,
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                };
                break;
            }
            case NODECL_GREATER_THAN:
            {
                switch (side)
                {
                    case 'l':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the LHS
                        // v > x;   --TRUE--->  v1 = v0 ∩ [x+1, +∞]
                        NBase lb = (val.is_constant() ? const_value_to_nodecl(const_value_add(val.get_constant(), one)) 
                                                      : Nodecl::Add::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(lb,
                                                    plus_inf.shallow_copy(), 
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the LHS
                        // v > x;   --FALSE-->  v2 = v0 ∩ [-∞, x]
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(minus_inf.shallow_copy(), 
                                                    val.shallow_copy(), 
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    case 'r':
                    {
                        // 4.3.1.- Build the TRUE constraint value for the RHS
                        // x > v;   --TRUE--->  v1 = v0 ∩ [-∞, x-1]
                        NBase ub = (val.is_constant() ? const_value_to_nodecl(const_value_sub(val.get_constant(), one))
                                                      : Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t));
                        val_true = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false),
                                Nodecl::Range::make(minus_inf.shallow_copy(),
                                                    ub,
                                                    const_value_to_nodecl(zero), t),
                                t);

                        // 4.3.2.- Build the FALSE constraint value for the RHS
                        // x > v;   --FALSE--->  v1 = v0 ∩ [x, +∞]
                        val_false = Nodecl::Analysis::RangeIntersection::make(
                                last_ssa.make_nodecl(/*set_ref_type*/false), 
                                Nodecl::Range::make(val.shallow_copy(),
                                                    plus_inf.shallow_copy(),
                                                    const_value_to_nodecl(zero), t),
                                t);
                        break;
                    }
                    default:
                        internal_error("Unexpected side value '%s'. Expecting 'l' or 'r'.\n", side);
                };
                break;
            }
            default:
                internal_error("Unexpected node kind %s while building constraint for a comparison.\n",
                               ast_print_node_type(comparison_kind));
        };

        // 4.4.- Build the TRUE and FALSE constraints and store them
        Utils::Constraint c_true = build_constraint(s_true, val_true, t, __ComparatorTrue);
        _output_true_constraints[n] = c_true;
        Utils::Constraint c_false = build_constraint(s_false, val_false, t, __ComparatorFalse);
        _output_false_constraints[n] = c_false;
    }

    // This method assumes the lhs is always a variable and the rhs may be a constant or a variable
    void ConstraintBuilder::visit_comparison(
            const NBase& lhs,
            const NBase& rhs,
            node_t comparison_kind)
    {
        // Create the constraints for the LHS, if necessary
        if (!lhs.is_constant())
            visit_comparison_side(lhs, rhs.shallow_copy(), /*LHS*/'l', comparison_kind);

        // Create the constraints for the RHS, if necessary
        if (!rhs.is_constant())
            visit_comparison_side(rhs, lhs.shallow_copy(), /*RHS*/'r', comparison_kind);
    }

    void ConstraintBuilder::visit(const Nodecl::AddAssignment& n)
    {
        NBase lhs = n.get_lhs();
        NBase rhs = n.get_rhs();
        NBase new_rhs = Nodecl::Assignment::make(lhs.shallow_copy(), 
                                                 Nodecl::Add::make(lhs.shallow_copy(), rhs.shallow_copy(), rhs.get_type()), 
                                                 lhs.get_type());
        n.replace(new_rhs);
        visit_assignment(n.get_lhs().no_conv(), n.get_rhs().no_conv());
    }

    void ConstraintBuilder::visit(const Nodecl::Assignment& n)
    {
        visit_assignment(n.get_lhs().no_conv(), n.get_rhs().no_conv());
    }

    // x != c;   ---TRUE-->    X1 = X0 ∩ ([-∞, c-1] U [c+1, -∞])
    //           --FALSE-->    X1 = X0 ∩ [c, c]
    void ConstraintBuilder::visit(const Nodecl::Different& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_DIFFERENT); 
    }

    // x == c;   ---TRUE-->    X1 = X0 ∩ [c, c]
    //           --FALSE-->    X1 = X0 ∩ ([-∞, c-1] U [c+1, -∞])
    void ConstraintBuilder::visit(const Nodecl::Equal& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_EQUAL);
    }

    // x > c;   ---TRUE-->    X1 = X0 ∩ [ c+1, +∞ ]
    //          --FALSE-->    X1 = X0 ∩ [-∞, c]
    void ConstraintBuilder::visit(const Nodecl::GreaterThan& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_GREATER_THAN);
    }

    // x >= c;   ---TRUE-->    X1 = X0 ∩ [ c, +∞ ]
    //           --FALSE-->    X1 = X0 ∩ [-∞, c-1]
    void ConstraintBuilder::visit(const Nodecl::GreaterOrEqualThan& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_GREATER_OR_EQUAL_THAN);
    }

    void ConstraintBuilder::visit(const Nodecl::LogicalAnd& n)
    {
        // 1.- Compute the constraints for the LSH
        const NBase& lhs = n.get_lhs();
        walk(lhs);
        set_false_constraint_to_inf(lhs);

        // 2.- Compute the constraints for the RHS
        const NBase& rhs = n.get_rhs();
        walk(rhs);
        set_false_constraint_to_inf(rhs);
    }

    // x <= c;    ---TRUE-->    X1 = X0 ∩ [-∞, c]
    //            --FALSE-->    X1 = X0 ∩ [ c+1,  +∞]
    void ConstraintBuilder::visit(const Nodecl::LowerOrEqualThan& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_LOWER_OR_EQUAL_THAN);
    }

    // x < c;    ---TRUE-->    X1 = X0 ∩ [-∞, c-1]
    //           --FALSE-->    X1 = X0 ∩ [ c,  +∞]
    void ConstraintBuilder::visit(const Nodecl::LowerThan& n)
    {
        visit_comparison(n.get_lhs().no_conv(),
                         n.get_rhs().no_conv(),
                         NODECL_LOWER_THAN);
    }

    // x % c;   ---TRUE-->    X1 = X0 ∩ [0, c-1]
    //          --FALSE-->    X1 = X0 ∩ ([-∞, -1] U [c, -∞])
    void ConstraintBuilder::visit(const Nodecl::Mod& n)
    {
        NBase lhs = n.get_lhs().no_conv();
        NBase rhs = n.get_rhs().no_conv();

        // 1.- Check the input is something we expect: LHS has a constraint or is a parameter
        ERROR_CONDITION(_input_constraints.find(lhs) == _input_constraints.end(),
                        "Some input constraint required for the LHS when parsing a %s nodecl",
                        ast_print_node_type(n.get_kind()));

        Symbol s(Utils::get_nodecl_base(lhs).get_symbol());
        Type t = s.get_type();
        std::string s_name = s.get_name();
        // Get the last ssa symbol related to the original symbol
        std::string last_ssa_name = _input_constraints.find(n)->second.get_symbol().get_name();
        Symbol last_ssa_s = n.retrieve_context().get_symbol_from_name(last_ssa_name);
        ERROR_CONDITION(!s.is_valid(),
                        "No symbol '%s' found while building constraint for node '%s'",
                        s.get_name().c_str(), n.prettyprint().c_str());

        // 2.- Compute the constraints generated from the condition to the possible TRUE and FALSE exit edges
        NBase val = rhs.shallow_copy();
        if (!val.is_constant())
        {   // Replace all the memory accesses by the ssa symbols arriving to the current node
            ConstraintReplacement cr(&_input_constraints, _constraints, _ordered_constraints);
            cr.walk(val);
        }

        // 2.1.- Compute the constraint that corresponds to the true branch taken from this node
        // x < x;       --TRUE-->       X1 = X0 ∩ [0, c-1]
        // 2.1.1.- Build the TRUE constraint symbol
        std::stringstream ss_true; ss_true << get_next_id(lhs);
        Symbol s_true(n.retrieve_context().new_symbol(s_name + "_" + ss_true.str()));
        s_true.set_type(t);
        ssa_to_original_var[s_true] = lhs;
        // 2.1.2.- Build the TRUE constraint value
        NBase ub = (rhs.is_constant() ? const_value_to_nodecl(const_value_sub(rhs.get_constant(), one)) 
                                      : Nodecl::Minus::make(val.shallow_copy(), const_value_to_nodecl(one), t));
        NBase val_true = 
            Nodecl::Analysis::RangeIntersection::make(
                last_ssa_s.make_nodecl(/*set_ref_type*/false), 
                Nodecl::Range::make(const_value_to_nodecl(zero),
                                    ub, 
                                    const_value_to_nodecl(zero), t),
                t);
        // 2.1.3.- Build the TRUE constraint and store it
        Utils::Constraint c_true = build_constraint(s_true, val_true, t, __ModTrue);
        _output_true_constraints[lhs] = c_true;
        // 2.2.- Compute the constraint that corresponds to the false branch taken from this node
        // x < c;       --FALSE-->      X1 = X0 ∩ ([-∞, -1] U [c, -∞])
        // 2.2.1.- Build the FALSE constraint symbol
        std::stringstream ss_false; ss_false << get_next_id(lhs);
        Symbol s_false(n.retrieve_context().new_symbol(s_name + "_" + ss_false.str()));
        s_false.set_type(t);
        ssa_to_original_var[s_false] = lhs;
        // 2.2.2.- Build the FALSE constraint value
        NBase val_false = 
            Nodecl::Analysis::RangeIntersection::make(
                last_ssa_s.make_nodecl(/*set_ref_type*/false),
                Nodecl::Analysis::RangeUnion::make(
                    Nodecl::Range::make(minus_inf.shallow_copy(),
                                        const_value_to_nodecl(minus_one),
                                        const_value_to_nodecl(zero), t),
                    Nodecl::Range::make(val.shallow_copy(),
                                        plus_inf.shallow_copy(),
                                        const_value_to_nodecl(zero), t),
                    t),
                t);
        // 2.2.3.- Build the FALSE constraint and store it
        Utils::Constraint c_false = build_constraint(s_false, val_false, t, __ModFalse);
        _output_false_constraints[lhs] = c_false;
    }

    void ConstraintBuilder::visit(const Nodecl::ObjectInit& n)
    {
        Symbol s(n.get_symbol());
        Nodecl::Symbol lhs = s.make_nodecl(/*set_ref_type*/false);
        NBase rhs = s.get_value();
        visit_assignment(lhs, rhs);
    }

    // FIXME Check the order of creation of constraints depending on whether the op. is pre-in/decrement or post-in/decrement
    //       Example: x = y++;              Wrong: 
    //                X0 = Y0;                     Y1 = Y0 + 1;
    //                Y1 = Y0 + 1;                 X0 = Y1;

    // x--;    -->    X1 = X0 + 1
    void ConstraintBuilder::visit(const Nodecl::Postdecrement& n)
    {
        visit_increment(n.get_rhs(), /*positive*/ false);
    }

    // x++;    -->    X1 = X0 + 1
    void ConstraintBuilder::visit(const Nodecl::Postincrement& n)
    {
        visit_increment(n.get_rhs(), /*positive*/ true);
    }

    // --x;    -->    X1 = X0 - 1
    void ConstraintBuilder::visit(const Nodecl::Predecrement& n)
    {
        visit_increment(n.get_rhs(), /*positive*/ false);
    }

    // ++x;    -->    X1 = X0 + 1
    void ConstraintBuilder::visit(const Nodecl::Preincrement& n)
    {
        visit_increment(n.get_rhs(), /*positive*/ true);
    }

    // ************** END visitor building constraints from statements ************* //
    // ***************************************************************************** //



    // ***************************************************************************** //
    // ******************* Class implementing constraint graph ********************* //

    ConstraintGraph::ConstraintGraph(std::string name)
        : _name(name), _nodes(), _node_to_scc_map()
    {}

    CGNode* ConstraintGraph::get_node_from_ssa_var(const NBase& n)
    {
        CGValueToCGNode_map::iterator it = _nodes.find(n);
        ERROR_CONDITION(it == _nodes.end(),
                        "No SSA variable '%s' found in Constraint Graph '%s'",
                        n.prettyprint().c_str(), _name.c_str());
        return it->second;
    }

    CGNode* ConstraintGraph::insert_node(const NBase& value, CGNodeType type)
    {
        // If the node already existed, return it
        if (_nodes.find(value) != _nodes.end())
            return _nodes[value];

        // Otherwise, create the node and return it
        CGNode* node = new CGNode(type, value);
        _nodes[value] = node;
        return node;
    }

    CGNode* ConstraintGraph::insert_node(CGNodeType type)
    {
        CGNode* node = new CGNode(type, NBase::null());
        NBase value = Nodecl::IntegerLiteral::make(Type::get_long_int_type(), 
                                                   const_value_get_integer(node->get_id(), /*num_bytes*/4, /*sign*/1));
        _nodes[value] = node;
        return node;
    }

    void ConstraintGraph::connect_nodes(CGNode* source, CGNode* target, bool is_back_edge)
    {
        CGEdge* e = source->add_child(target, is_back_edge);
        target->add_entry(e);
    }

    static CGNodeType get_op_type_from_value(const NBase& val)
    {
        switch(val.get_kind())
        {
            case NODECL_ADD:                            return __Add;
            case NODECL_DIV:                            return __Div;
            case NODECL_ANALYSIS_RANGE_INTERSECTION:    return __Intersection;
            case NODECL_MUL:                            return __Mul;
            case NODECL_ANALYSIS_PHI:                   return __Phi;
            case NODECL_MINUS:                          return __Sub;
            case NODECL_SYMBOL:                         return __Sym;
            default:
                internal_error("Unexpected Constraint value %s.\n", val.prettyprint().c_str());
        }
    }

    static NBase generate_range_from_constant(const NBase& constant)
    {
        Optimizations::ReduceExpressionVisitor rev;
        rev.walk(constant);
        Nodecl::Range const_range = Nodecl::Range::make(
                constant.shallow_copy(),
                constant.shallow_copy(),
                const_value_to_nodecl(zero),
                Type::get_long_int_type());
        return const_range;
    }

    CGNode* ConstraintGraph::fill_cg_with_binary_op_rec(
            const NBase& val,
            CGNodeType n_type)
    {
        ERROR_CONDITION(!Nodecl::Utils::nodecl_is_arithmetic_op(val),
                        "Expected arithmetic operation in constraint, but found '%s'.\n",
                        val.prettyprint().c_str());

        // We take the liberty of casting to Nodecl::Add always because
        // all binary operation structurally have the same tree
        const NBase& lhs = val.as<Nodecl::Add>().get_lhs().no_conv();
        const NBase& rhs = val.as<Nodecl::Add>().get_rhs().no_conv();
        CGNodeType val_type = get_op_type_from_value(val);

        CGNode* target_op = NULL;
        if (lhs.is<Nodecl::Symbol>())
        {
            if (rhs.is<Nodecl::Symbol>())
            {   // var1 OP var2
                CGNode* source1 = insert_node(lhs);                 // var1
                CGNode* source2 = insert_node(rhs);                 // var2
                target_op = insert_node(val_type);                  // OP

                connect_nodes(source1, target_op);                  // var1 -> OP
                connect_nodes(source2, target_op);                  // var2 -> OP
            }
            else if (rhs.is_constant())
            {   // var OP const
                CGNode* source1 = insert_node(lhs);                 // var

                NBase const_range = generate_range_from_constant(rhs.shallow_copy());
                CGNode* source2 = insert_node(const_range, __Const);// const

                target_op = insert_node(n_type);                    // outerOP

                connect_nodes(source1, target_op);                  // var -> outerOP
                connect_nodes(source2, target_op);                  // const -> outerOP
            }
            else if (Nodecl::Utils::nodecl_is_arithmetic_op(rhs))
            {   // var OP (...)
                CGNode* source1 = insert_node(lhs);                             // var
                CGNode* source2 = fill_cg_with_binary_op_rec(rhs, val_type);    // (...)
                target_op = insert_node(val_type);                              // OP

                connect_nodes(source1, target_op);                              // var -> OP
                connect_nodes(source2, target_op);                              // (...) -> OP
            }
            else
            {
                internal_error("Unexpected value '%s' for a Constraint.\n", val.prettyprint().c_str());
            }
        }
        else if (lhs.is_constant())
        {
            if (rhs.is<Nodecl::Symbol>())
            {   // const OP var
                CGNode* source = insert_node(rhs);                  // var
                target_op = insert_node(n_type);                    // outerOP
                connect_nodes(source, target_op);                   // var -> outerOP
            }
            else if (rhs.is_constant())
            {
                // TODO
            }
            else if (Nodecl::Utils::nodecl_is_arithmetic_op(rhs))
            {   // const OP (...)
                CGNode* source1 = fill_cg_with_binary_op_rec(rhs, val_type);    // (...)

                NBase const_range = generate_range_from_constant(lhs.shallow_copy());
                CGNode* source2 = insert_node(const_range, __Const);            // const

                target_op = insert_node(n_type);                                // OP

                connect_nodes(source1, target_op);                              // (...) -> OP
                connect_nodes(source2, target_op);                              // const -> OP
            }
            else
            {
                internal_error("Unexpected value '%s' for a Constraint.\n", val.prettyprint().c_str());
            }
        }
        else if (Nodecl::Utils::nodecl_is_arithmetic_op(lhs))
        {
            CGNode* source1 = fill_cg_with_binary_op_rec(lhs, val_type);
            if (rhs.is<Nodecl::Symbol>())
            {   // (...) OP var
                CGNode* source2 = insert_node(rhs);             // var
                target_op = insert_node(val_type);              // OP
                connect_nodes(source1, target_op);              // var -> OP
                connect_nodes(source2, target_op);              // var -> OP
            }
            else if (rhs.is_constant())
            {   // var OP const
                NBase const_range = generate_range_from_constant(rhs.shallow_copy());
                CGNode* source2 = insert_node(const_range, __Const);    // const

                target_op = insert_node(n_type);                        // outerOP

                connect_nodes(source1, target_op);                      // var -> outerOP
                connect_nodes(source2, target_op);                      // const -> outerOP
            }
            else if (Nodecl::Utils::nodecl_is_arithmetic_op(rhs))
            {   // (.x.) OP (.y.)
                CGNode* source2 = fill_cg_with_binary_op_rec(rhs, val_type);    // (.y.)
                target_op = insert_node(val_type);                              // OP
                connect_nodes(source1, target_op);                              // (.x.) -> OP
                connect_nodes(source2, target_op);                              // (.y.) -> OP
            }
            else
            {
                internal_error("Unexpected value '%s' for a Constraint.\n", val.prettyprint().c_str());
            }
        }
        else
        {
            internal_error("Unexpected value '%s' for a Constraint.\n", val.prettyprint().c_str());
        }

        return target_op;
    }

    void ConstraintGraph::fill_cg_with_binary_op(
            const NBase& s,
            const NBase& val,
            CGNodeType op_type)
    {
        // res = (...) OP (...)
        CGNode* target_op = fill_cg_with_binary_op_rec(val, op_type);   // OP
        CGNode* target = insert_node(s);                                // res
        connect_nodes(target_op, target);                               // OP -> res
    }

    /*! Generate a constraint graph from the PCFG and the precomputed Constraints for each node
     *  A Constraint Graph is created as follows:
     *  - The nodes of the graphs are:
     *    - A. A node for each SSA variable from Constraints: c0 = ...              Node    c0
     *    - B. A node for each Constraint Value which is a Range: c1 = [lb, ub]     Node [lb, ub]
     *  - The edges are built following the rules below:
     *    - D. Constraint Phi: c2 = Phi(c0, c1)                                     Edge    c0 ---------------> c2
     *                                                                              Edge    c1 ---------------> c2
     *    - E. Constraint Intersection: c1 = c0 ∩ [lb, ub]                          Edge    c0 ----[lb, ub]---> c1
     *    - F. Constraint Range: c0 = [lb, ub]                                      Edge [lb, ub]-------------> c1
     *    - G. Constraint Arithmetic op: a. c1 = c0 + 1                             Edge    c1 ------ 1 ------> c0
     *                                   b. c1 = d + e                              Edge    d  ---------------> c1
     *                                                                              Edge    e  ---------------> c1
     *    - H. Constraint : c1 = c0                                                 Edge    c0 ---------------> c1
     */
    void ConstraintGraph::fill_constraint_graph(
        const Constraints& constraints,
        const std::vector<Symbol>& ordered_constraints)
    {
        std::map<NBase, CGNode*> back_edges;
        for (std::vector<Symbol>::const_iterator ito = ordered_constraints.begin();
             ito != ordered_constraints.end(); ++ito)
        {
            Symbol ssa_sym = *ito;
            Constraints::const_iterator it = constraints.find(ssa_sym);
            ERROR_CONDITION(it == constraints.end(), 
                            "SSA constraint symbol %s not found in the constraints' container.\n",
                            ssa_sym.get_name().c_str());
            const NBase& val = it->second;

            // Insert in the CG the edges corresponding to the current Constraint
            const NBase& ssa_var = ssa_sym.make_nodecl(/*set_ref_type*/false);
            if (val.is<Nodecl::Symbol>())
            {   // H. 
                CGNode* source = insert_node(val);
                CGNode* target = insert_node(ssa_var);  // A.
                connect_nodes(source, target);
            }
            else if (val.is<Nodecl::Analysis::Phi>())
            {   // D.
                // Create the target
                CGNode* target_phi = insert_node(__Phi);
                // Create the sources
                std::queue<CGNode*> sources;
                Nodecl::List expressions = val.as<Nodecl::Analysis::Phi>().get_expressions().as<Nodecl::List>();
                for (Nodecl::List::iterator ite = expressions.begin(); ite != expressions.end(); ++ite)
                {
                    NBase e = *ite;
                    if (_nodes.find(e) == _nodes.end())
                    {   // The expression inside the Phi node comes from a back edge
                        // We will print it later so the order of node creation respects the order in the sequential code
                        back_edges.insert(std::pair<NBase, CGNode*>(e, target_phi));
                    }
                    else
                    {
                        CGNode* source = insert_node(e);
                        sources.push(source);
                    }
                }
                CGNode* target = insert_node(ssa_var);  // A.
                // Connect them
                while (!sources.empty())
                {
                    CGNode* source = sources.front();
                    sources.pop();
                    connect_nodes(source, target_phi);
                }
                connect_nodes(target_phi, target);
            }
            else if (val.is<Nodecl::Analysis::RangeIntersection>())
            {   // E.
                const NBase& lhs = val.as<Nodecl::Analysis::RangeIntersection>().get_lhs().no_conv();
                const NBase& rhs = val.as<Nodecl::Analysis::RangeIntersection>().get_rhs().no_conv();
                
                CGNode* source = insert_node(lhs);
                CGNode* target = insert_node(ssa_var);  // A.
                NBase range = rhs.shallow_copy();
                Optimizations::ReduceExpressionVisitor rev;
                rev.walk(range);
                CGNode* range_node = insert_node(range, __Intersection);
                connect_nodes(source, range_node);
                connect_nodes(range_node, target);
            }
            else if (val.is<Nodecl::Range>())
            {
                // B. Create a new node if the Constraint Value is a Range
                CGNode* source = insert_node(val, __Const);
                CGNode* target = insert_node(ssa_var);  // A.
                // F. Create edge between the Range node and the Constraint node
                connect_nodes(source, target);
            }
            else if (Nodecl::Utils::nodecl_is_arithmetic_op(val))
            {
                CGNodeType type = get_op_type_from_value(val);
                fill_cg_with_binary_op(ssa_var, val, type);
            }
            else
            {
                internal_error("Unexpected type of Constraint value '%s' for constraint '%s'.\n", 
                               ast_print_node_type(val.get_kind()), val.prettyprint().c_str());
            }
        }

        // Connect now the back edges
        for (std::map<NBase, CGNode*>::iterator it = back_edges.begin(); it != back_edges.end(); ++it)
        {   // Both source and target must exist already
            CGNode* source = insert_node(it->first);
            CGNode* target = it->second;
            connect_nodes(source, target, /*is_back_edge*/true);
        }
    }

    void ConstraintGraph::print_graph() const
    {
        // Get a file to print a DOT with the Constraint Graph
        // Create the directory of dot files if it has not been previously created
        char buffer[1024];
        char* err = getcwd(buffer, 1024);
        if(err == NULL)
            internal_error ("An error occurred while getting the path of the current directory", 0);
        struct stat st;
        std::string directory_name = std::string(buffer) + "/dot/";
        if (stat(directory_name.c_str(), &st) != 0)
        {
            int dot_directory = mkdir(directory_name.c_str(), S_IRWXU);
            if (dot_directory != 0)
                internal_error ("An error occurred while creating the dot directory in '%s'", 
                                directory_name.c_str());
        }

        // Create the file where we will store the DOT CG
        std::string dot_file_name = directory_name + _name + "_cg.dot";
        std::ofstream dot_cg;
        dot_cg.open(dot_file_name.c_str());
        if (!dot_cg.good())
            internal_error ("Unable to open the file '%s' to store the CG.", dot_file_name.c_str());
        if (VERBOSE)
            std::cerr << "- CG DOT file '" << dot_file_name << "'" << std::endl;
        dot_cg << "digraph CG {\n";

        // 1.- Print the general information of the graph
        dot_cg << "\tcompound=true;\n";
        dot_cg << "\tlabel=\"Constraint Graph of function '" << _name << "'\"";
        dot_cg << "\tnode [shape=record, fontname=\"Times-Roman\", fontsize=14];\n";

        for (CGValueToCGNode_map::const_iterator it = _nodes.begin(); it != _nodes.end(); ++it)
        {
            CGNode* n = it->second;
            unsigned int source = n->get_id();

            // 2.- Print the Node
            CGNodeType t = n->get_type();
            if (t == __Sym || t == __Const || t == __Intersection)
            {   // Print nodes with a valuation associated
                NBase constraint = n->get_constraint();
                dot_cg << "\t" << source << " [label=\"[" << source << "] " << constraint.prettyprint() << "\", shape=\"polygon\"];\n";
                NBase val = n->get_valuation();
                if (!constraint.is<Nodecl::Range>() && !val.is_null())
                {
                    // Print a node containing the valuation
                    dot_cg << "\t0" << source << " [label=\"" << val.prettyprint() << "\", "
                           << "style=\"dashed\", color=\"gray55\", fontcolor=\"gray27\", shape=\"polygon\"];\n";
                    // Connect it to its constraint node
                    dot_cg << "\t" << source << "->" << "0" << source << " [label=\"\", style=\"dashed\", color=\"gray55\"];\n";
                    // set the same rank to the constraint node an its valuation node
                    dot_cg << "\t{rank=same; " << source << "; 0" << source << ";}";
                }
            }
            else
            {   // Print operation nodes
                dot_cg << "\t" << source << " [label=\"[" << source << "] " << n->get_type_as_string() << "\", shape=\"polygon\"];\n";
            }

            // Print the node relations
            const std::set<CGEdge*>& exits = n->get_exits();
            for (std::set<CGEdge*>::iterator ite = exits.begin(); ite != exits.end(); ++ite)
            {
                unsigned int target = (*ite)->get_target()->get_id();
                bool back_edge = (*ite)->is_back_edge();
                std::string attrs = " [label=\"\", style=\"" + std::string(back_edge ? "dotted" : "solid") + "\"]";
                dot_cg << "\t" << source << "->" << target << attrs << ";\n";
            }
        }
        dot_cg << "}\n";
        dot_cg.close();
        if (!dot_cg.good())
            internal_error ("Unable to close the file '%s' where CG has been stored.", dot_file_name.c_str());
    }

namespace {
    bool stack_contains_cgnode(const std::stack<CGNode*>& s, CGNode* n)
    {
        bool result = false;
        std::stack<CGNode*> tmp = s;
        while(!tmp.empty() && !result)
        {
            if(tmp.top()==n)
                result = true;
            tmp.pop();
        }
        return result;
    }
}
    
    void ConstraintGraph::strong_connect(CGNode* n, unsigned int& scc_current_index, 
            std::stack<CGNode*>& s, std::vector<SCC*>& scc_list, 
            std::map<CGNode*, int>& scc_lowlink_index,
            std::map<CGNode*, int>& scc_index)
    {
        // Set the depth index for 'n' to the smallest unused index
        scc_index[n] = scc_current_index;
        scc_lowlink_index[n] = scc_current_index;
        ++scc_current_index;
        s.push(n);

        // Consider the successors of 'n'
        std::set<CGNode*> succ = n->get_children();
        for (std::set<CGNode*>::iterator it = succ.begin(); it != succ.end(); ++it)
        {
            CGNode* m = *it;
            if (scc_index.find(m)==scc_index.end())
            {   // Initialize values for this node if it has not yet been initialized
                scc_index[m] = -1;
                scc_lowlink_index[m] = -1;
            }
            if (scc_index[m] == -1)
            {   // Successor 'm' has not yet been visited: recurse on it
                strong_connect(m, scc_current_index, s, scc_list, scc_lowlink_index, scc_index);
                scc_lowlink_index[n] = std::min(scc_lowlink_index[n], scc_lowlink_index[m]);
            }
            else if (stack_contains_cgnode(s, m))
            {   // Successor 'm' is in the current SCC
                scc_lowlink_index[n] = std::min(scc_lowlink_index[n], scc_index[m]);
            }
        }   

        // If 'n' is a root node, pop the set and generate an SCC
        if ((scc_lowlink_index[n] == scc_index[n]) && !s.empty())
        {
            SCC* scc = new SCC(&_node_to_scc_map);
            while (!s.empty() && s.top()!=n)
            {
                scc->add_node(s.top());
                s.pop();
            }
            if (!s.empty() && s.top()==n)
            {
                scc->add_node(s.top());
                s.pop();
            }
            scc_list.push_back(scc);
        }
    }
    
    // Implementation of the Tarjan's strongly connected components algorithm
    std::vector<SCC*> ConstraintGraph::topologically_compose_strongly_connected_components()
    {
        std::vector<SCC*> scc_list;
        std::stack<CGNode*> s;
        unsigned int scc_current_index = 0;

        // 1.- Collect each set of nodes that form a SCC
        std::map<CGNode*, int> scc_lowlink_index;
        std::map<CGNode*, int> scc_index;
        for (CGValueToCGNode_map::iterator it = _nodes.begin(); it != _nodes.end(); ++it)
        {
            CGNode* n = it->second;
            if ((scc_index.find(n) == scc_index.end()) || (scc_index[n] == -1))
                strong_connect(n, scc_current_index, s, scc_list, scc_lowlink_index, scc_index);
        }

        // 2.- Compute the directionality of each scc_current_index
        // 3.- Create a map between the Constraint Graph nodes and their SCC
        for (std::vector<SCC*>::iterator it = scc_list.begin(); it != scc_list.end(); ++it)
        {
            std::vector<CGNode*> scc_nodes = (*it)->get_nodes();
            for (std::vector<CGNode*>::iterator itt = scc_nodes.begin(); itt != scc_nodes.end(); ++itt)
            {
                _node_to_scc_map.insert(std::pair<CGNode*, SCC*>(*itt, *it));
            }
        }

        // Compute the root of each SCC
        for (std::vector<SCC*>::iterator it = scc_list.begin(); it != scc_list.end(); ++it)
        {
            SCC* scc = *it;
            const std::vector<CGNode*>& nodes = scc->get_nodes();
            if (scc->is_trivial())
            {
                scc->add_root(nodes[0]);
            }
            else
            {
                for (std::vector<CGNode*>::const_iterator itt = nodes.begin();
                     itt != nodes.end(); ++itt)
                {   // In a cycle, the root is the Phi node
                    if ((*itt)->get_type() == __Phi)
                    {
                        scc->add_root(*itt);
                    }
                }
            }
        }

        print_sccs(scc_list);

        // Collect the roots of each SCC tree
        std::vector<SCC*> roots;
        for (std::map<CGNode*, SCC*>::iterator it = _node_to_scc_map.begin();
             it != _node_to_scc_map.end(); ++it)
        {
            if (it->first->get_entries().empty())
                roots.push_back(it->second);
        }

        return roots;
    }

    void ConstraintGraph::evaluate_cgnode(CGNode* const n)
    {
        const NBase& old_valuation = n->get_valuation();
        NBase new_valuation;
        CGNodeType node_type = n->get_type();
        switch (node_type)
        {
            case __Const:
            {   // The node is a range => its valuation is the constraint itself
                new_valuation = n->get_constraint();
                break;
            }
            case __Sym:
            {   // The node is an SSA symbol => its valuation depend on the entry operation
                // 1.- Check the integrity of the Constraint Graph at this point
                const std::set<CGNode*>& parents = n->get_parents();
                ERROR_CONDITION(parents.size() != 1,
                                "A symbol node is expected to have one unique entry, "
                                "but node %d has %d entries.\n",
                                n->get_id(), parents.size());
                // 2.- Evaluate the node
                CGNode* parent = *parents.begin();
                const std::set<CGNode*>& grandparents = parent->get_parents();
                CGNodeType parent_type = parent->get_type();
                NBase op1, op2;     // Variables defined and used in different cases of the following switch case
                switch (parent_type)
                {
                    case __Const:
                    {   // For a __Const node already evaluated, the methods 'get_valuation' and
                        // 'get_constraint' return the same, the range
                        new_valuation = parent->get_valuation();
                        break;
                    }
                    case __Sym:
                    {   // FIXME We should try to avoid redounding SSA variables when they surely have the same valuation
                        // Two consecutive __Sym nodes will always have the same valuation
                        new_valuation = parent->get_valuation();
                        break;
                    }
                    case __Intersection:
                    {
                        // 2.1.- Check the integrity of the Constraint Graph at this point
                        ERROR_CONDITION(grandparents.size() != 1, 
                                        "An intersection node is expected to have exactly 1 entry, "
                                        "but node %d has %d entries.\n", 
                                        parent->get_id(), grandparents.size());

                        // 2.2.- Compute the intersection
                        op1 = (*grandparents.begin())->get_valuation();
                        op2 = parent->get_constraint();
                        new_valuation = Utils::range_intersection(op1, op2);
                        break;
                    }
                    case __Phi:
                    {
                        // 2.1.- Check the integrity of the Constraint Graph at this point
                        ERROR_CONDITION(grandparents.size() < 2, 
                                        "A phi node is expected to have at least 2 entries, "
                                        "but node %d has %d entries.\n", 
                                        parent->get_id(), grandparents.size());
                        
                        // 2.2.- Join all entry valuations into the phi node
                        ObjectList<NBase> entry_valuations;
                        std::set<CGNode*>::const_iterator it = grandparents.begin();
                        op1 = (*it)->get_valuation();
                        ++it;
                        for (; it != grandparents.end(); ++it)
                        {
                            op2 = (*it)->get_valuation();
                            op1 = Utils::range_union(op1, op2);
                        }
                        new_valuation = op1;
                        break;
                    }
                    // Any other operation must be a binary operation
                    default:
                    {
                        ERROR_CONDITION(grandparents.size() != 2, 
                                        "A binary operation node is expected to have exactly 2 entries, "
                                        "but node %d has %d entries.\n", 
                                        parent->get_id(), grandparents.size());
                        std::set<CGNode*>::iterator it = grandparents.begin();
                        op1 = (*it)->get_valuation(); ++it;
                        op2 = (*it)->get_valuation();
                        switch (parent_type)
                        {
                            case __Add:
                            {
                                new_valuation = Utils::range_addition(op1, op2);
                                break;
                            }
                            case __Sub:
                            {
                                new_valuation = Utils::range_subtraction(op1, op2);
                                break;
                            }
                            case __Mul:
                            {
                                new_valuation = Utils::range_multiplication(op1, op2);
                                break;
                            }
                            case __Div:
                            {
                                new_valuation = Utils::range_division(op1, op2);
                                break;
                            }
                            default:
                            {
                                internal_error("Unexpected parent type %d while evaluating node %d. "
                                               "Expecting binary operation.\n",
                                               parent->get_type_as_string().c_str(), n->get_id());
                            }
                        };
                        break;
                    }
                };

                // Check that we indeed compute some valuation
                ERROR_CONDITION(new_valuation.is_null(),
                                "Unexpected node %d of type %s.\n",
                                parent->get_id(), parent->get_type_as_string().c_str());
                break;
            }
            default:
                internal_error("Unexpected node type %d while evaluating node %d.\n",
                                n->get_type_as_string().c_str(), n->get_id());
        };

        if (RANGES_DEBUG)
            std::cerr << "    EVALUATE " << n->get_id()
                      << "  ::  " << n->get_constraint().prettyprint()
                      << "  ::  " << (old_valuation.is_null() ? "[⊥, ⊥]" : old_valuation.prettyprint())
                      << " -> " << new_valuation.prettyprint() << std::endl;
        n->set_valuation(new_valuation);
    }

    static NBase get_next_lower(const std::set<const_value_t*>& const_values, const_value_t* c)
    {
        for (std::set<const_value_t*>::const_reverse_iterator it = const_values.rbegin();
             it != const_values.rend(); ++it)
        {
            const_value_t* diff = const_value_sub(c, *it);
            if (const_value_is_positive(diff) || const_value_is_zero(diff))
                return const_value_to_nodecl(*it);
        }
        return minus_inf.shallow_copy();
    }

    static NBase get_next_greater(const std::set<const_value_t*>& const_values, const_value_t* c)
    {
        for (std::set<const_value_t*>::const_iterator it = const_values.begin();
             it != const_values.end(); ++it)
        {
            const_value_t* diff = const_value_sub(*it, c);
            if (const_value_is_positive(diff) || const_value_is_zero(diff))
                return const_value_to_nodecl(*it);
        }
        return plus_inf.shallow_copy();
    }

    static void gather_constants_from_const_node(CGNode* n, std::set<const_value_t*>& const_values)
    {
        const Nodecl::Range& range = n->get_constraint().as<Nodecl::Range>();
        const NBase& lb = range.get_lower(); 
        const NBase& ub = range.get_upper();
        if (lb.is_constant())
            const_values.insert(lb.get_constant());
        if (ub.is_constant())
            const_values.insert(ub.get_constant());
    }

    std::set<const_value_t*> ConstraintGraph::gather_scc_constants(SCC* scc)
    {
        std::set<const_value_t*> const_values;     // Result

        // 1.- Start looking for the values from the root of the component
        const std::list<CGNode*>& roots = scc->get_roots();
        std::queue<CGNode*,std::list<CGNode*> > worklist(roots);

        // 2.- Iterate until all nodes in the component have been visited
        while (!worklist.empty())
        {
            // 2.1.- Get the first node in the queue
            CGNode* n = worklist.front();
            worklist.pop();

            // 2.2.- Base case: we are exiting the component
            if (_node_to_scc_map[n] != scc)
                continue;

            // 2.3.- Evaluate the current node: if it contains a constant, store it
            CGNodeType type = n->get_type();
            if (type == __Const || type == __Intersection)
            {
                gather_constants_from_const_node(n, const_values);
            }
            // 2.4.- It may happen that an operation node receives a constant from outside the component
            else if (type != __Sym && type != __Intersection)
            {
                const std::set<CGNode*> parents = n->get_parents();
                for (std::set<CGNode*>::const_iterator it = parents.begin(); it != parents.end(); ++it)
                {
                    CGNode* p = *it;
                    // Only check node form outside the component
                    // since the ones inside are already checked in the normal work-flow
                    if (_node_to_scc_map[p] != scc
                            && (p->get_type() == __Const || type == __Intersection))
                    {
                        gather_constants_from_const_node(p, const_values);
                    }
                }
            }

            // 2.5.- Prepare the following iterations
            const std::set<CGEdge*>& exits = n->get_exits();
            for (std::set<CGEdge*>::const_iterator it = exits.begin(); it != exits.end(); ++it)
            {
                if (!(*it)->is_back_edge())     // Avoid cycles
                    worklist.push((*it)->get_target());
            }
        }
        return const_values;
    }

    // The widen operator used is a generalization of the Cousot and Cousot's widening operator:
    //     I[Y] = | [⊥, ⊥]                            -> e(Y)
    //            | e(Y)_ < I[Y]_ && e(Y)^ > I[Y]^   -> [-inf , +inf ]
    //            | e(Y)_ < I[Y]_                    -> [-inf , I[Y]^]
    //            | e(Y)^ > I[Y]^                    -> [I[Y]_, +inf ]
    void ConstraintGraph::widen(SCC* scc)
    {
        const std::list<CGNode*> roots = scc->get_roots();     // This is the phi node with an entry back edge

        // 1.- Gather all constants in this component
        std::set<const_value_t*> const_values = gather_scc_constants(scc);

        // 2.- Traverse the component applying the widen operation
        std::queue<CGNode*,std::list<CGNode*> > worklist(roots);
        while (!worklist.empty())
        {
            // 2.1.- Get the next node to be treated
            CGNode* n = worklist.front();
            worklist.pop();

            // 2.2.- Base case: if the node is not in the same SCC, then we will treat it later
            if (_node_to_scc_map[n] != scc)
                continue;

            // 2.3.- Keep the old valuation to be able to compare if there has been some change
            //     We make a copy because otherwise the pointer may be modified
            const NBase& old_valuation = n->get_valuation().shallow_copy();
            NBase widen_valuation = old_valuation;
            // 2.4.- Calculate the current node, only if it is a symbol, because:
            //        - __Const nodes will n ever change their valuation since it is the constraint itself
            //        - Operation nodes are never evaluated
            if (n->get_type() == __Sym)
            {
                // 2.4.1.- Compute the new valuation of the node
                evaluate_cgnode(n);

                // 2.4.2.- Apply the widen operation
                const NBase& new_valuation = n->get_valuation();
                ERROR_CONDITION(!new_valuation.is<Nodecl::Range>(),
                                "Non-range interval '%s' found for CG-Node %d. Range expected\n",
                                new_valuation.prettyprint().c_str(), n->get_id());
                widen_valuation = new_valuation;
                if (!old_valuation.is_null())
                {   // Note that I_old[Y] = [⊥, ⊥] -> I_new[Y] = e(Y)
                    const Nodecl::Range& last_range = old_valuation.as<Nodecl::Range>();
                    const NBase& old_lb = last_range.get_lower();
                    const NBase& old_ub = last_range.get_upper();
                    const Nodecl::Range& new_range = new_valuation.as<Nodecl::Range>();
                    const NBase& new_lb = new_range.get_lower();
                    const NBase& new_ub = new_range.get_upper();
                    if (old_lb.is_constant() && new_lb.is_constant())
                    {
                        const_value_t* old_lb_c = old_lb.get_constant();
                        const_value_t* new_lb_c = new_lb.get_constant();
                        const_value_t* lb_diff = const_value_sub(new_lb_c, old_lb_c);
                        if (const_value_is_negative(lb_diff))
                        {   // e(Y)_ < I[Y]_
                            if (old_ub.is_constant() && new_ub.is_constant())
                            {
                                const_value_t* old_ub_c = old_ub.get_constant();
                                const_value_t* new_ub_c = new_ub.get_constant();
                                const_value_t* ub_diff = const_value_sub(new_ub_c, old_ub_c);
                                if (const_value_is_positive(ub_diff))
                                {   // e(Y)^ > I[Y]^ -> [-inf , +inf]
                                    const NBase& next_lower = get_next_lower(const_values, new_lb_c);
                                    const NBase& next_upper = get_next_greater(const_values, new_ub_c);
                                    widen_valuation = Nodecl::Range::make(
                                            next_lower,
                                            next_upper,
                                            const_value_to_nodecl(zero),
                                            Type::get_long_int_type());
                                }
                                else
                                {   // e(Y)^ <= I[Y]^ -> [-inf , I[Y]^]
                                    const NBase& next_lower = get_next_lower(const_values, new_lb_c);
                                    widen_valuation = Nodecl::Range::make(
                                            next_lower,
                                            old_ub,
                                            const_value_to_nodecl(zero),
                                            Type::get_long_int_type());
                                }
                            }
                            else
                            {
                                WARNING_MESSAGE("Mixing UBs '%s' and '%s' is not implemented yet "
                                                "because they are not constant values.\n",
                                                old_ub.prettyprint().c_str(), new_ub.prettyprint().c_str());
                            }
                        }
                        else if (old_ub.is_constant() && new_ub.is_constant())
                        {   // e(Y)_ > I[Y]_
                            const_value_t* old_ub_c = old_ub.get_constant();
                            const_value_t* new_ub_c = new_ub.get_constant();
                            const_value_t* ub_diff = const_value_sub(new_ub_c, old_ub_c);
                            if (const_value_is_positive(ub_diff))
                            {
                                const NBase& next_upper = 
                                        (new_ub.is_constant() ? get_next_greater(const_values, new_ub.get_constant())
                                                              : plus_inf.shallow_copy());
                                widen_valuation = Nodecl::Range::make(
                                        old_lb,
                                        next_upper,
                                        const_value_to_nodecl(zero),
                                        Type::get_long_int_type());
                            }
                        }
                        else
                        {
                            WARNING_MESSAGE("Mixing UBs '%s' and '%s' is not implemented yet "
                                            "because they are not constant values.\n",
                                            old_ub.prettyprint().c_str(), new_ub.prettyprint().c_str());
                        }
                    }
                    else
                    {
                        WARNING_MESSAGE("Mixing valuations '%s' and '%s' is not implemented yet "
                                        "because they are not constant values.\n",
                                        old_lb.prettyprint().c_str(), new_lb.prettyprint().c_str());
                    }

                    // Set the new valuation after widening
                    if (RANGES_DEBUG)
                    {
                        const NBase& constraint = n->get_constraint();
                        std::cerr << "    WIDEN " << n->get_id()
                                  << "  ::  " << (constraint.is_null() ? n->get_type_as_string() : constraint.prettyprint())
                                  << " = " << widen_valuation.prettyprint() << std::endl;
                    }
                    n->set_valuation(widen_valuation);
                }
            }

            // 2.5.- Prepare next iteration by adding to the worklist the children nodes
            // only if the new valuation is different from the previous one
            // Since at the beginning all evaluation are null, we are sure we pass through all nodes
            // in the component at least once
            if (n->get_type() != __Sym      // Nothing can change
                || !Nodecl::Utils::structurally_equal_nodecls(old_valuation, widen_valuation,
                                                              /*skip_conversions*/true))
            {
                const std::set<CGNode*>& children = n->get_children();
                for (std::set<CGNode*>::const_iterator it = children.begin();
                     it != children.end(); ++it)
                    worklist.push(*it);
            }
        }
    }

    // The narrow operator used is the one proposed by Cousot and Cousot's:
    //     I[Y] = | I[Y]_ = -inf && e(Y)_ > -inf     -> [e(Y)_, I[Y]^]
    //            | I[Y]^ = +inf && e(Y)^ < +inf     -> [I[Y]_, e(Y)^]
    //            | I[Y]_ > e(Y)_                    -> [e(Y)_, I[Y]^]
    //            | I[Y]^ < e(Y)^                    -> [I[Y]_, e(Y)^]
    void ConstraintGraph::narrow(SCC* scc)
    {
        // 1.- Traverse the component applying the narrow operation
        const std::list<CGNode*> roots = scc->get_roots();     // This is the phi node with an entry back edge
        std::queue<CGNode*,std::list<CGNode*> > worklist(roots);
        std::set<CGNode*> visited;
        while (!worklist.empty())
        {
            // 1.1.- Get the next node to be treated
            CGNode* n = worklist.front();
            worklist.pop();

            // 1.2.- Base case: if the node is not in the same SCC, then we will treat it later
            if (_node_to_scc_map[n] != scc)
                continue;

            // 1.3.- Keep the old valuation to be able to compare if there has been some change
            //     We make a copy because otherwise the pointer may be modified
            const NBase& old_valuation = n->get_valuation().shallow_copy();
            NBase narrow_valuation = old_valuation;

            // 1.4.- Calculate the current node, only if it is a symbol, because:
            //        - __Const nodes will n ever change their valuation since it is the constraint itself
            //        - Operation nodes are never evaluated
            if (n->get_type() == __Sym)
            {
                // 1.4.1.- Compute the new valuation of the node
                evaluate_cgnode(n);

                // 1.4.2.- Apply the narrow operation
                const NBase& new_valuation = n->get_valuation();
                ERROR_CONDITION(!new_valuation.is<Nodecl::Range>(),
                                "Non-range interval '%s' found for CG-Node %d. Range expected\n",
                                new_valuation.prettyprint().c_str(), n->get_id());
                narrow_valuation = new_valuation;

                const Nodecl::Range& last_range = old_valuation.as<Nodecl::Range>();
                const NBase& old_lb = last_range.get_lower();
                const NBase& old_ub = last_range.get_upper();
                const Nodecl::Range& new_range = new_valuation.as<Nodecl::Range>();
                const NBase& new_lb = new_range.get_lower();
                const NBase& new_ub = new_range.get_upper();
                if (Nodecl::Utils::structurally_equal_nodecls(old_lb, minus_inf)
                        && new_lb.is_constant()
                        && const_value_is_positive(const_value_sub(new_lb.get_constant(),
                                                                   minus_inf.get_constant())))
                {
                    narrow_valuation = Nodecl::Range::make(new_lb, old_ub,
                                                           Nodecl::NodeclBase(const_value_to_nodecl(zero)),
                                                           Type::get_long_int_type());
                }
                else if (Nodecl::Utils::structurally_equal_nodecls(old_ub, plus_inf)
                        && new_ub.is_constant()
                        && const_value_is_positive(const_value_sub(plus_inf.get_constant(),
                                                                   new_ub.get_constant())))
                {
                    narrow_valuation = Nodecl::Range::make(old_lb, new_ub,
                                                           Nodecl::NodeclBase(const_value_to_nodecl(zero)),
                                                           Type::get_long_int_type());
                }
                else
                {
                    if (old_lb.is_constant() && new_lb.is_constant())
                    {
                        const_value_t* old_lb_c = old_lb.get_constant();
                        const_value_t* new_lb_c = new_lb.get_constant();
                        const_value_t* diff = const_value_sub(old_lb_c, new_lb_c);
                        if (const_value_is_positive(diff))
                        {
                            narrow_valuation = Nodecl::Range::make(new_lb, old_ub,
                                                                   Nodecl::NodeclBase(const_value_to_nodecl(zero)),
                                                                   Type::get_long_int_type());
                        }
                        else if (const_value_is_negative(diff))
                        {
                            narrow_valuation = Nodecl::Range::make(old_lb, new_ub,
                                                                   Nodecl::NodeclBase(const_value_to_nodecl(zero)),
                                                                   Type::get_long_int_type());
                        }
                    }
                    else
                    {
                        WARNING_MESSAGE("Mixing valuations '%s' and '%s' is not implemented yet "
                                        "because they contain non-constant values.\n",
                                        old_valuation.prettyprint().c_str(), new_valuation.prettyprint().c_str());
                    }
                }

                // Set the new valuation after narrowing
                if (RANGES_DEBUG)
                {
                    const NBase& constraint = n->get_constraint();
                    std::cerr << "    NARROW " << n->get_id()
                              << "  ::  " << (constraint.is_null() ? n->get_type_as_string() : constraint.prettyprint())
                              << " = " << narrow_valuation.prettyprint() << std::endl;
                }
            }

            // 1.5.- Prepare next iteration by adding to the worklist the children nodes
            // only if the new valuation is different from the previous one
            // or it is the first time we try to narrow this node
            if (n->get_type() != __Sym      // Nothing can change
                    || !Nodecl::Utils::structurally_equal_nodecls(old_valuation, narrow_valuation,
                                                                  /*skip_conversions*/true)
                    || visited.find(n) == visited.end())
            {
                const std::set<CGNode*>& children = n->get_children();
                for (std::set<CGNode*>::const_iterator it = children.begin();
                    it != children.end(); ++it)
                    worklist.push(*it);
            }

            // Mark the node as visited: from now on, if the valuation in this node does not change,
            // then we will not keep iterating over its children
            visited.insert(n);
        }
    }

    void ConstraintGraph::resolve_cycle(SCC* scc)
    {
        // Widening operation
        widen(scc);

        // Future resolution
        // TODO This part cannot be implemented until the CG support futures

        // Narrowing operation
        narrow(scc);
    }

    // Only __Sym nodes are evaluated!
    void ConstraintGraph::solve_constraints(const std::vector<SCC*>& root_sccs)
    {
        if (RANGES_DEBUG)
        {
            std::cerr << "________________________________________________" << std::endl;
            std::cerr << "SOLVE CONSTRAINTS:" << std::endl;
            std::cerr << "------------------" << std::endl;
        }

        // Are suitable to be solved those nodes that are roots
        // because they do not depend on the previous evaluation of any other node
        std::queue<SCC*> next_scc;
        for (std::vector<SCC*>::const_iterator it = root_sccs.begin(); it != root_sccs.end(); ++it)
            next_scc.push(*it);

        // Keep track of those SCC already solved, so we do not solve them again
        std::set<SCC*> visited;

        // Iterate until there is no SCC to be solved
        while (!next_scc.empty())
        {
            // 1.- Get the next SCC to be solved
            SCC* scc = next_scc.front();
            next_scc.pop();

            // 2.- Base case: the SCC has already been solved
            if (visited.find(scc) != visited.end())
                continue;

            // 2.- Check whether this SCC is ready to be solved:
            //     All its entries, but those coming from back edges, must have been already solved
            const std::list<CGNode*>& roots = scc->get_roots();
            std::set<CGEdge*> entries;
            for (std::list<CGNode*>::const_iterator it = roots.begin(); it != roots.end(); ++it)
            {
                const std::set<CGEdge*> tmp = (*it)->get_entries();
                entries.insert(tmp.begin(), tmp.end());
            }
            bool is_ready = true;
            for (std::set<CGEdge*>::const_iterator it = entries.begin(); it != entries.end(); ++it)
            {
                CGNode* parent = (*it)->get_source();
                SCC* parent_scc = _node_to_scc_map[parent];
                if (scc != parent_scc                           // This parent belongs to a different component
                        && visited.find(parent_scc) == visited.end())  // That other component has not been solved yet
                {
                    is_ready = false;
                    break;
                }
            }
            if (!is_ready)
            {
                next_scc.push(scc);
                continue;
            }

            // Solve the current SCC
            if (scc->is_trivial())
            {   // Evaluate the only node within the SCC, if necessary (operation nodes are not evaluated)
                CGNode* n = scc->get_nodes()[0];
                if (n->get_type() == __Sym || n->get_type() == __Const)
                {
                    if (RANGES_DEBUG)
                        std::cerr << "  SCC " << scc->get_id() << "  ->  TRIVIAL" << std::endl;
                    evaluate_cgnode(n);
                }
            }
            else
            {   // Cycle resolution
                if (RANGES_DEBUG)
                    std::cerr << "  SCC " << scc->get_id() << "  ->  RESOLVE CYCLE" << std::endl;
                resolve_cycle(scc);
            }
            visited.insert(scc);

            // Prepare next iterations, if there are
            // We will add more than one child here when a single node generates more than one constraint
            const ObjectList<SCC*>& scc_exits = scc->get_scc_exits();
            for (ObjectList<SCC*>::const_iterator it = scc_exits.begin();
                 it != scc_exits.end(); ++it)
                next_scc.push(*it);
        }
    }

    // ***************** END Class implementing constraint graph ******************* //
    // ***************************************************************************** //



    // ***************************************************************************** //
    // ********************* Class implementing range analysis ********************* //
    
    RangeAnalysis::RangeAnalysis(ExtensibleGraph* pcfg)
        : _pcfg(pcfg), _cg(new ConstraintGraph(pcfg->get_name())), 
          _constraints(), _ordered_constraints()
    {}
    
    void RangeAnalysis::compute_range_analysis()
    {   
        // 1.- Compute the constraints of the current PCFG
        std::map<Node*, VarToConstraintMap> pcfg_constraints;
        compute_constraints(pcfg_constraints);

        // 2.- Build the Constraint Graph (CG) from the computed constraints
        build_constraint_graph();
        _cg->print_graph();

        // 3.- Extract the Strongly Connected Components (SCC) of the graph
        //     And get the root of each topologically ordered subgraph
        std::vector<SCC*> roots = _cg->topologically_compose_strongly_connected_components();

        // 4.- Constraints evaluation
        _cg->solve_constraints(roots);
        if(VERBOSE)
            _cg->print_graph();

        // 5.- Insert computed ranges in the PCFG
        set_ranges_to_pcfg(pcfg_constraints);
    }
    
    void RangeAnalysis::compute_constraints(
        /*out*/ std::map<Node*, VarToConstraintMap>& pcfg_constraints)
    {
        // 1.- Create constraint [-∞, +∞] for each parameter
        compute_parameters_constraints(pcfg_constraints);

        // 2.- Compute constraints for the function statements
        Node* entry = _pcfg->get_graph()->get_graph_entry_node();
        std::queue<Node*> worklist; worklist.push(entry);
        std::set<Node*> treated;
        compute_constraints_rec(worklist, treated, pcfg_constraints);

        //3.- Print in std out the constraints, if requested
        if (RANGES_DEBUG)
            print_constraints();
    }
    
    void RangeAnalysis::build_constraint_graph()
    {
        _cg->fill_constraint_graph(_constraints, _ordered_constraints);
    }
    
    // Set an constraint to the graph entry node for each parameter of the function
    void RangeAnalysis::compute_parameters_constraints(
        /*out*/ std::map<Node*, VarToConstraintMap>& pcfg_constraints)
    {
        Symbol func_sym = _pcfg->get_function_symbol();
        if (!func_sym.is_valid())    // The PCFG have been built for something other than a FunctionCode
            return;

        Node* entry = _pcfg->get_graph()->get_graph_entry_node();
        const ObjectList<Symbol>& params = func_sym.get_function_parameters();
        ConstraintBuilder cbv(
                /*unnecessary for parameters*/pcfg_constraints[entry],
                &_constraints, &_ordered_constraints);
        cbv.compute_parameters_constraints(params);
        pcfg_constraints[entry] = cbv.get_output_constraints();
    }

namespace {

    void create_recomputed_constraint(
            const VarToConstraintMap& new_constrs,
            VarToConstraintMap& constrs,
            Constraints *constraints,
            std::vector<Symbol> *ordered_constraints,
            ConstraintBuilder& cbv)
    {
        // Example:
        //     we had:      i1 = i0
        //     we have:     i3 = phi(i1,i2)
        //     but we want: i3 = i0
        //                  i1 = phi(i3,i2)
        for (VarToConstraintMap::const_iterator it = new_constrs.begin();
             it != new_constrs.end(); ++it)
        {
            const NBase& orig_var = it->first;
            if ((constrs.find(orig_var) != constrs.end())
                    && (constrs[orig_var] != it->second))
            {
                // 1.- Get a new symbol for the new constraint
                std::stringstream ss; ss << get_next_id(orig_var);
                Symbol orig_sym(orig_var.get_symbol());
                std::string constr_name = orig_sym.get_name() + "_" + ss.str();
                Symbol ssa_var(orig_var.retrieve_context().new_symbol(constr_name));
                Type t(orig_sym.get_type());
                ssa_var.set_type(t);
                ssa_to_original_var[ssa_var] = orig_var;
                // 2.- Rebuild the old constraint               (i1 = i0   =>   i3 = i0)
                Utils::Constraint& old_c = constrs[orig_var];
                Symbol old_ssa_var = old_c.get_symbol();
                NBase old_val = old_c.get_value();
                (*constraints)[ssa_var] = old_val;
                // Look for the position to insert the new constraint
                std::vector<Symbol>::iterator ito = ordered_constraints->begin();
                while (*ito != old_ssa_var && ito != ordered_constraints->end())
                    ++ito;
                ERROR_CONDITION(ito == ordered_constraints->end(),
                                "SSA variable %s not found in the list of ordered constraints\n",
                                ssa_var.get_name().c_str());
                ordered_constraints->insert(ito, ssa_var);
                // 3.- Build the value of the new constraint    (i1 = phi(i3,i2))
                NBase e1 = ssa_var.make_nodecl(/*set_ref_type*/false);
                NBase e2 = it->second.get_symbol().make_nodecl(/*set_ref_type*/false);
                Nodecl::List exprs = Nodecl::List::make(e1, e2);
                NBase val = Nodecl::Analysis::Phi::make(exprs, t);
                // 4.- Build the new constraint and insert it in the proper list
                Utils::Constraint new_c = cbv.build_constraint(old_ssa_var, val, t, __BackEdge);
                constrs[orig_var] = new_c;
            }
        }
    }
    
    void compute_constraint_from_back_edge(
            Node* n,
            const VarToConstraintMap& new_constraint_map,
            Constraints *constraints,
            std::vector<Symbol> *ordered_constraints,
            std::map<Node*, VarToConstraintMap>& pcfg_constraints)
    {
        VarToConstraintMap& constrs = pcfg_constraints[n];
        ConstraintBuilder cbv(constrs, constraints, ordered_constraints);
        create_recomputed_constraint(
                new_constraint_map,
                constrs,
                constraints,
                ordered_constraints,
                cbv);

    }

    // Utility method: for printing the constraints map if debugging is necessary
    void print_var_to_constraint_map(const VarToConstraintMap& constrs)
    {
        for (VarToConstraintMap::const_iterator it = constrs.begin(); it != constrs.end(); ++it)
        {
            std::cerr << "      " << it->first.prettyprint() << "  ::  " << it->second.get_symbol().get_name() << " -> "
                      << it->second.get_value().prettyprint() << std::endl;
        }
    }
}

    // This is a breadth-first search because for a given node we need all its parents 
    // Since the graph has loops, we have to solve first the loops,
    // so we can properly propagate the inner loop constraints
    void RangeAnalysis::compute_constraints_rec(
        /*inout*/ std::queue<Node*>& worklist,
        /*inout*/ std::set<Node*>& treated,
        /*inout*/ std::map<Node*, VarToConstraintMap>& pcfg_constraints)
    {
        std::queue<Node*> next_worklist;

        while (!worklist.empty())
        {
            Node* n = worklist.front();
            worklist.pop();

            if (treated.find(n) != treated.end())
                continue;

            // 1.- Check whether all n parents (coming from non-back-edges) are already computed
            //     Also save whether the node has back-edges (needed to order the computation of the next steps)
            bool n_has_backedge = false;
            const ObjectList<Edge*>& entries = n->get_entry_edges();
            bool ready = true;
            for (ObjectList<Edge*>::const_iterator it = entries.begin(); it != entries.end(); ++it)
            {
                Edge* e = *it;
                if (e->is_back_edge())
                    n_has_backedge = true;
                if (!e->is_back_edge()                                      // *it is a dominator of n
                        && !e->get_source()->is_omp_task_node()             // *it is not a task node
                        && treated.find(e->get_source())==treated.end())    // *it is not yet visited
                {
                    ready = false;
                    break;
                }
            }
            if (!ready)
            {
                worklist.push(n);
                continue;
            }

            // 2.- The element is ready to be computed, let's do it
            if (n->is_graph_node())
            {
                std::queue<Node*> inner_worklist;
                inner_worklist.push(n->get_graph_entry_node());
                // 2.1.- For graph nodes, call recursively to compute inner nodes constraints
                // 2.1.1.- Recursive call with the graph entry node
                compute_constraints_rec(inner_worklist, treated, pcfg_constraints);
                // 2.1.2.- Propagate the information from the exit node to the graph node
                Node* graph_exit = n->get_graph_exit_node();
                pcfg_constraints[n] = pcfg_constraints[graph_exit];
            }
            else
            {
                // 2.2.- For the rest of nodes,
                //   - merge information from parents
                //   - compute current node information
                //   - merge parents' info with current node's info

                // 2.2.1.- Collect and join constraints computed for all the parents
                VarToConstraintMap input_constrs;           // Constraints coming directly from parents => they are propagated
                VarToConstraintMap merged_input_constrs;    // Constraints resulting from the join of parents' constraints
                                                            // These constraints belong to the current node => they are not propagated
                // ConstraintBuilder necessary for merging the constraints from the parents
                // The 'input_constrs' are not used here, so we use a random empty map
                ConstraintBuilder cbv_propagated(input_constrs, &_constraints, &_ordered_constraints);

                const ObjectList<Node*>& parents = (n->is_entry_node() ? n->get_outer_node()->get_parents()
                                                                       : n->get_parents());
                NodeclSet treated_omp_private_vars;
                for (ObjectList<Node*>::const_iterator itp = parents.begin(); itp != parents.end(); ++itp)
                {
                    VarToConstraintMap parent_constrs = pcfg_constraints[*itp];
                    for (VarToConstraintMap::iterator itc = parent_constrs.begin();
                         itc != parent_constrs.end(); ++itc)
                    {
                        const NBase& orig_var = itc->first;
                        // 2.2.1.1.- Treat omp nodes depending on the data-sharing of the variables
                        if (n->is_entry_node() && n->get_outer_node()->is_omp_task_node())
                        {
                            // If the variable is:
                            //    - firstprivate -> the only parent we have to take into account is the task creation node
                            //    - private      -> no propagation exists and the range of the variable is [-inf, +inf]
                            //    - shared       -> take into account all parents, and any possible modification done in the concurrent code
                            Node* task_node = n->get_outer_node();
                            const NodeclSet& fp_vars = task_node->get_firstprivate_vars();
                            const NodeclSet& p_vars = task_node->get_private_vars();
                            if (Utils::nodecl_set_contains_nodecl(orig_var, fp_vars))
                            {
                                if (!(*itp)->is_omp_task_creation_node())
                                    continue;   // Skip this parent because its values for #orig_var must not be propagated
                            }
                            else if (Utils::nodecl_set_contains_nodecl(orig_var, p_vars))
                            {
                                // Avoid propagating the range [-inf, +inf] for the same variable multiple times
                                if (Utils::nodecl_set_contains_nodecl(orig_var, treated_omp_private_vars))
                                    continue;
                                treated_omp_private_vars.insert(orig_var);
                                // Build a new constraint with value [-inf, +inf] because
                                // the initialization value of this variable is undefined
                                // 2.2.1.1.1.- Get a new symbol for the new constraint
                                std::stringstream ss; ss << get_next_id(orig_var);
                                Symbol orig_sym(orig_var.get_symbol());
                                std::string constr_name = orig_sym.get_name() + "_" + ss.str();
                                Symbol ssa_var(orig_var.retrieve_context().new_symbol(constr_name));
                                Type t(orig_sym.get_type());
                                ssa_var.set_type(t);
                                ssa_to_original_var[ssa_var] = orig_var;
                                // 2.2.1.1.2.- Build the value of the new constraint
                                NBase new_constraint_val = Nodecl::Range::make(minus_inf.shallow_copy(),
                                                                               plus_inf.shallow_copy(),
                                                                               const_value_to_nodecl(zero), t);
                                // 2.2.1.1.3.- Remove the old constraint from the input_constrs
                                //         If it was in the merged_input_constrs map, it will be deleted with the insertion
                                if (input_constrs.find(orig_var) != input_constrs.end())
                                    input_constrs.erase(orig_var);
                                // 2.2.1.1.4.- Build the current constraint and insert it in the proper list
                                Utils::Constraint new_c = cbv_propagated.build_constraint(ssa_var, new_constraint_val, t, __Propagated);
                                merged_input_constrs[orig_var] = new_c;
                            }
                            else
                            {   // FIXME
                                // The variable is shared: much more values could be propagated here (from all concurrent definitions)
                                // But, as an initial approach, we propagate the values of all parents
                                // so we do nothing special here, as it was a non-OpenMP node
                            }
                        }
                        // 2.2.1.2.- Treat non-OpenMP nodes
                        const Utils::Constraint& c = itc->second;
                        if (input_constrs.find(orig_var) == input_constrs.end() &&
                            merged_input_constrs.find(orig_var) == merged_input_constrs.end())
                        {   // No constraints found yet for variable orig_var
                            input_constrs[orig_var] = c;
                        }
                        else
                        {   // Constraints for variable orig_var already found: merge them with the new constraint
                            // 2.2.1.2.1.- Get the existing constraint
                            Utils::Constraint old_c =
                                    ((input_constrs.find(orig_var) != input_constrs.end()) ? input_constrs[orig_var]
                                                                                           : merged_input_constrs[orig_var]);
                            NBase old_value = old_c.get_value();

                            // 2.2.1.2.2.- If the new constraint is different from the old one, compute the combination of both
                            NBase current_value = c.get_value();
                            if (!Nodecl::Utils::structurally_equal_nodecls(old_value, current_value,
                                /*skip_conversion_nodes*/true))
                            {
                                // 2.2.1.2.2.1.- Get a new symbol for the new constraint
                                std::stringstream ss; ss << get_next_id(orig_var);
                                Symbol orig_sym(orig_var.get_symbol());
                                std::string constr_name = orig_sym.get_name() + "_" + ss.str();
                                Symbol ssa_var(orig_var.retrieve_context().new_symbol(constr_name));
                                Type t(orig_sym.get_type());
                                ssa_var.set_type(t);
                                ssa_to_original_var[ssa_var] = orig_var;

                                // 2.2.1.2.2.2.- Build the value of the new constraint
                                NBase new_value;
                                if (old_value.is<Nodecl::Analysis::Phi>())
                                {   // Attach a new element to the list inside the node Phi
                                    Nodecl::List expressions = old_value.as<Nodecl::Analysis::Phi>().get_expressions().as<Nodecl::List>();
                                    Nodecl::Symbol new_expr = c.get_symbol().make_nodecl(/*set_ref_type*/false);
                                    expressions.append(new_expr);
                                    new_value = Nodecl::Analysis::Phi::make(expressions, orig_var.get_type());
                                }
                                else
                                {   // Create a new node Phi with the combination of the old constraint and the new one
                                    Nodecl::Symbol tmp1 = old_c.get_symbol().make_nodecl(/*set_ref_type*/false);
                                    Nodecl::Symbol tmp2 = c.get_symbol().make_nodecl(/*set_ref_type*/false);
                                    Nodecl::List expressions = Nodecl::List::make(tmp1, tmp2);
                                    new_value = Nodecl::Analysis::Phi::make(expressions, current_value.get_type());
                                }

                                // 2.2.1.2.2.3.- Remove the old constraint from the input_constrs
                                // If it was in the merged_input_constrs map, it will be renewed with the insertion
                                if (input_constrs.find(orig_var) != input_constrs.end())
                                    input_constrs.erase(orig_var);
                                // 2.2.1.2.2.4.- Build the current constraint and insert it in the proper list
                                Utils::Constraint new_c = cbv_propagated.build_constraint(ssa_var, new_value, t, __Propagated);
                                merged_input_constrs[orig_var] = new_c;
                            }
                        }
                    }
                }

                // 3.- Propagate constraints from parent nodes to the current node
                pcfg_constraints[n].insert(merged_input_constrs.begin(), merged_input_constrs.end());

                // 4.- Compute the constraints generated in the current node
                VarToConstraintMap inner_constrs;
                if (n->has_statements())
                {
                    // 4.1.- Compute the constraints of the current node
                    // Note: take into account the constraints the node may already have (if it is the TRUE or FALSE child of a conditional)
                    VarToConstraintMap all_input_constrs = pcfg_constraints[n];
                    all_input_constrs.insert(merged_input_constrs.begin(), merged_input_constrs.end());
                    all_input_constrs.insert(input_constrs.begin(), input_constrs.end());
                    ConstraintBuilder cbv(all_input_constrs, &_constraints, &_ordered_constraints);
                    NodeclList stmts = n->get_statements();
                    for (NodeclList::iterator itt = stmts.begin(); itt != stmts.end(); ++itt)
                        cbv.walk(*itt);

                    inner_constrs = cbv.get_output_constraints();
                    pcfg_constraints[n].insert(inner_constrs.begin(), inner_constrs.end());

                    // 4.3.- Set true/false output constraints to current children, if applies
                    ObjectList<Edge*> exits = n->get_exit_edges();
                    if (exits.size()==2 &&
                        ((exits[0]->is_true_edge() && exits[1]->is_false_edge()) || (exits[1]->is_true_edge() && exits[0]->is_false_edge())))
                    {
                        VarToConstraintMap out_true_constrs = cbv.get_output_true_constraints();
                        VarToConstraintMap out_false_constrs = cbv.get_output_false_constraints();

                        // 4.3.1.- We always propagate to the TRUE edge
                        Node* true_node = (exits[0]->is_true_edge() ? exits[0]->get_target() : exits[1]->get_target());
                        Node* real_true_node = true_node;
                        while (true_node->is_exit_node())
                            true_node = true_node->get_outer_node()->get_children()[0];
                        if (true_node->is_graph_node())
                            true_node = true_node->get_graph_entry_node();
                        pcfg_constraints[true_node].insert(out_true_constrs.begin(), out_true_constrs.end());

                        // 4.3.2.- For the if_else cases, we only propagate to the FALSE edge when it contains statements ('else' statements)
                        Node* false_node = (exits[0]->is_true_edge() ? exits[1]->get_target() : exits[0]->get_target());
                        ObjectList<Node*> real_true_node_children = real_true_node->get_children();
                        if ((false_node->get_entry_edges().size() == 1) || !real_true_node_children.contains(false_node))
                        {   // If the true_node is a parent of the false_node, then there are no statements
                            // Avoid cases where the FALSE edge leads to the end of the graph
                            ObjectList<Node*> children;
                            while (false_node->is_exit_node())
                            {
                                children = false_node->get_outer_node()->get_children();
                                if (!children.empty())
                                    false_node = children[0];
                                else
                                {
                                    false_node = NULL;
                                    break;
                                }
                            }
                            if (false_node != NULL)
                            {
                                if (false_node->is_graph_node())
                                    false_node = false_node->get_graph_entry_node();
                                pcfg_constraints[false_node].insert(out_false_constrs.begin(), out_false_constrs.end());
                            }
                        }
                    }
                }

                // 5.- Purge propagated constraints:
                // When the node generates a constraint for a given variable
                // any propagated constraint from parents for that variable is deleted here
                for (VarToConstraintMap::iterator itt = inner_constrs.begin();
                     itt != inner_constrs.end(); ++itt)
                {
                    VarToConstraintMap::iterator ittt = input_constrs.find(itt->first);
                    if (ittt != input_constrs.end())
                        input_constrs.erase(ittt);
                }
                pcfg_constraints[n].insert(input_constrs.begin(), input_constrs.end());
            }

            treated.insert(n);

            if (!n->is_omp_task_node())
            {
                const ObjectList<Edge*>& exits = n->get_exit_edges();
                if (n_has_backedge)
                {   // This means the node is the condition of a loop
                    //     - first compute the loop constraints (the TRUE edge)
                    //     - then compute the other children (store the nodes in next_worklist)
                    Node* next_in_loop = NULL;
                    for (ObjectList<Edge*>::const_iterator it = exits.begin(); it != exits.end(); ++it)
                    {
                        Node* t = (*it)->get_target();
                        if (treated.find(t) == treated.end())
                        {
                            if ((*it)->is_true_edge())
                                next_in_loop = t;
                            else
                                next_worklist.push(t);
                        }
                    }

                    ERROR_CONDITION(next_in_loop==NULL,
                                    "Node %d has a back edge, but we have not found any 'true' edge.\n",
                                    n->get_id());

                    std::queue<Node*> loop_worklist;
                    loop_worklist.push(next_in_loop);
                    compute_constraints_rec(loop_worklist, treated, pcfg_constraints);
                }
                else
                {   // Otherwise,
                    //    - first propagate information to back edges, if there are
                    //    - then compute the other children (store the nodes in next_worklist)
                    for (ObjectList<Edge*>::const_iterator it = exits.begin(); it != exits.end(); ++it)
                    {
                        Node* t = (*it)->get_target();
                        if ((*it)->is_back_edge())
                        {   // Propagate here constraints from the back edge
                            compute_constraint_from_back_edge(
                                    t, pcfg_constraints[n],
                                    &_constraints, &_ordered_constraints,
                                    pcfg_constraints);
                        }

                        if (treated.find(t) == treated.end())
                            next_worklist.push(t);
                    }
                }
            }

            compute_constraints_rec(next_worklist, treated, pcfg_constraints);
        }
    }

    void RangeAnalysis::set_ranges_to_pcfg(
            const std::map<Node*, VarToConstraintMap>& pcfg_constraints)
    {
        for (std::map<Node*, VarToConstraintMap>::const_iterator it = pcfg_constraints.begin();
             it != pcfg_constraints.end(); ++it)
        {
            const VarToConstraintMap constraints = it->second;
            if(!constraints.empty())
            {
                for(VarToConstraintMap::const_iterator itt = constraints.begin(); itt != constraints.end(); ++itt)
                {
                    Symbol s(itt->second.get_symbol());
                    std::map<Symbol, NBase, Nodecl::Utils::Nodecl_structural_less>::iterator ssa_to_var_it;
                    ssa_to_var_it = ssa_to_original_var.find(s);
                    ERROR_CONDITION (ssa_to_var_it == ssa_to_original_var.end(), 
                                     "SSA symbol '%s' is not related to any variable of the original code\n", 
                                     s.get_name().c_str());
                    CGNode* n = _cg->get_node_from_ssa_var(itt->second.get_symbol().make_nodecl(/*set_ref_type*/false));
                    it->first->set_range(ssa_to_var_it->second, n->get_valuation());
                }
            }
        }
    }

    void RangeAnalysis::print_constraints()
    {
        std::cerr << "________________________________________________" << std::endl;
        std::cerr << "CONSTRAINT MAP:" << std::endl;
        std::cerr << "---------------" << std::endl;
        for (std::vector<Symbol>::iterator it = _ordered_constraints.begin();
             it != _ordered_constraints.end(); ++it)
        {
            std::pair<Symbol, NBase> c = *_constraints.find(*it);
            std::cerr << "    " << c.first.get_name() << "  ->  " << c.second.prettyprint() << std::endl;
        }
    }

    // ********************* END Class implementing range analysis ********************* //
    // ********************************************************************************* //
}
}
