/* Autogenerated file. DO NOT MODIFY. */
/* Changes in nodecl-generator.py or cxx-nodecl.def will overwrite this file */
#ifndef TL_NODECL_FWD_HPP
#define TL_NODECL_FWD_HPP

#include "tl-nodecl-base-fwd.hpp"
#include "mem.h"

namespace Nodecl {
class CxxNoexcept;
class CxxEqualInitializer;
class Preincrement;
class ClassMemberAccess;
namespace OpenMP { 
class DepInout;
}
class CxxFunctionFormUnaryPrefix;
class CxxDef;
namespace Analysis { 
class InductionVarExpr;
}
class FunctionCode;
class VectorLogicalAnd;
class VectorGreaterThan;
class VectorFmadd;
namespace Analysis { 
class Phi;
}
class StructuredValueParenthesized;
class StructuredValueCompoundLiteral;
class PreprocessorLine;
class RangeLoopControl;
class VectorDifferent;
class GxxTrait;
class VectorMaskAnd2Not;
class CxxArrowPtrMember;
namespace OpenMP { 
class Overlap;
}
class ContinueStatement;
class CxxDepFunctionCall;
class VectorBitwiseShr;
class FortranReadStatement;
class IfElseStatement;
namespace OpenMP { 
class ParallelSimdFor;
}
namespace OpenMP { 
class For;
}
class CompoundExpression;
class Power;
class VectorMaskOr;
namespace OpenMP { 
class FlushAtExit;
}
class MemberInit;
class VectorLogicalNot;
class PragmaClauseArg;
class GccAsmOperand;
class CaseStatement;
class PragmaCustomDirective;
class VectorFmminus;
class CxxInitializer;
namespace Analysis { 
class PlusInfinity;
}
namespace OpenMP { 
class Private;
}
namespace OpenMP { 
class TaskwaitShallow;
}
class LowerThan;
class CxxMemberInit;
class IntelAssume;
namespace Analysis { 
namespace AutoScope { 
class Firstprivate;
}
}
namespace OpenMP { 
class Commutative;
}
class GccBuiltinVaArg;
class FortranForall;
namespace OpenMP { 
class SimdReduction;
}
class CxxDepNameConversion;
class VectorPrefetch;
namespace OpenMP { 
class SimdFunction;
}
class VectorFunctionCode;
class PseudoDestructorName;
class PointerToMember;
class VectorSincos;
class FieldDesignator;
class FortranNotPresent;
class Conversion;
class VectorReductionMinus;
class Div;
class Minus;
namespace OpenMP { 
class DepInValue;
}
namespace OpenMP { 
class TaskLabel;
}
class CxxLambda;
class ForStatement;
namespace OpenMP { 
class TaskwaitDeep;
}
namespace Analysis { 
class RangeUnion;
}
namespace Analysis { 
namespace AutoScope { 
class Private;
}
}
namespace OpenMP { 
class DepInPrivate;
}
namespace Analysis { 
class LiveOut;
}
namespace OpenMP { 
class BarrierWait;
}
class Symbol;
class VectorReductionMul;
class Verbatim;
class VectorLoop;
class DoStatement;
namespace OpenMP { 
class FlushAtEntry;
}
namespace OpenMP { 
class If;
}
class ParenthesizedExpression;
class ErrStatement;
class Dummy;
class InPlaceFlag;
class CxxCaptureReference;
class AlignedFlag;
class BitwiseOr;
namespace OpenMP { 
class ForAppendix;
}
class PragmaCustomLine;
class BitwiseNot;
class BooleanLiteral;
class MulAssignment;
class Neg;
class CxxCaptureCopy;
namespace OpenMP { 
class Nontemporal;
}
class FortranNullifyStatement;
class CxxDepNameNested;
namespace Analysis { 
namespace Correctness { 
class Race;
}
}
namespace OpenMP { 
class Master;
}
class VectorSqrt;
class FortranAlternateReturnStatement;
class AddAssignment;
namespace Analysis { 
class LiveIn;
}
namespace Analysis { 
namespace AutoScope { 
class Shared;
}
}
namespace OpenMP { 
class Name;
}
class VectorFunctionCall;
namespace OpenMP { 
class TaskExpression;
}
namespace Analysis { 
class MinusInfinity;
}
namespace OpenMP { 
class Simd;
}
class LogicalNot;
class CxxDepNew;
namespace OpenMP { 
class TaskCall;
}
namespace OpenMP { 
class TaskLoop;
}
namespace OpenMP { 
class WaitOnDependences;
}
class PragmaCustomClause;
namespace Analysis { 
class Defined;
}
class CxxFunctionFormImplicit;
namespace OpenMP { 
class Atomic;
}
namespace Analysis { 
class AssertDecl;
}
class Cast;
class FortranHollerith;
namespace OpenMP { 
class Linear;
}
class FortranOpenStatement;
namespace OpenMP { 
class Untied;
}
class FortranBindC;
class CxxExplicitTypeCast;
class VectorSubscript;
class ArithmeticShrAssignment;
class Context;
class VectorAdd;
class GccAsmSpec;
class VectorBitwiseAnd;
namespace Analysis { 
namespace Correctness { 
class IncoherentFp;
}
}
class CxxExplicitInstantiationDef;
class PragmaContext;
class VectorMaskXor;
class EvictFlag;
class FunctionCall;
class ArraySubscript;
class DeleteArray;
class VectorBitwiseShl;
class VectorBitwiseOr;
class CxxUsingDecl;
namespace OpenMP { 
class Threadprivate;
}
class Plus;
class RelaxedFlag;
namespace OpenMP { 
class CopyOut;
}
namespace OpenMP { 
class SimdFor;
}
class ReturnStatement;
namespace OpenMP { 
class Onto;
}
namespace OpenMP { 
class NDRange;
}
class HasBeenDefinedFlag;
namespace Analysis { 
class Minimum;
}
class CxxUsingNamespace;
class GreaterOrEqualThan;
class FortranData;
class VectorRsqrt;
class VectorMinus;
class FortranComputedGotoStatement;
namespace Analysis { 
namespace Correctness { 
class IncoherentOutPointed;
}
}
class UnboundedLoopControl;
class TryBlock;
class VectorDiv;
class New;
namespace OpenMP { 
class NoMask;
}
namespace OpenMP { 
class DepIn;
}
namespace OpenMP { 
class Single;
}
class FortranActualArgument;
class CxxDepTemplateId;
class BitwiseOrAssignment;
class VectorPromotion;
namespace Analysis { 
class RangeIntersection;
}
class VlaWildcard;
class VectorLaneId;
class Concat;
class CxxClassMemberAccess;
namespace OpenMP { 
class Prefetch;
}
class ArithmeticShr;
namespace OpenMP { 
class CopyIn;
}
class CxxFunctionFormTemplateId;
class StructuredValueBracedImplicit;
class Comma;
class Typeid;
namespace OpenMP { 
class Firstprivate;
}
class VectorMaskAnd;
class Shaping;
class ObjectInit;
class ModAssignment;
class Equal;
class Offsetof;
class Delete;
namespace OpenMP { 
class VectorLengthFor;
}
namespace OpenMP { 
class File;
}
class CxxPostfixInitializer;
namespace OpenMP { 
class Alloca;
}
class Mod;
class FortranAlternateReturnArgument;
class VectorCast;
namespace OpenMP { 
class CriticalName;
}
namespace Analysis { 
class UpperExposed;
}
class CxxDepGlobalNameNested;
namespace OpenMP { 
class Concurrent;
}
class BitwiseAndAssignment;
class Offset;
namespace OpenMP { 
class Unroll;
}
namespace Analysis { 
class Undefined;
}
class IntegerLiteral;
namespace OpenMP { 
class TaskReduction;
}
namespace Analysis { 
class ReachingDefinitionOut;
}
class VectorMaskAssignment;
class VectorBitwiseXor;
class VectorScatter;
class GreaterThan;
class GccAsmDefinition;
class C99FieldDesignator;
class FortranDeallocateStatement;
namespace OpenMP { 
class Auto;
}
class BitwiseAnd;
class IntelAssumeAligned;
namespace OpenMP { 
class Shared;
}
namespace OpenMP { 
class Implements;
}
class VectorBitwiseNot;
namespace OpenMP { 
class NumTasks;
}
class Assignment;
namespace OpenMP { 
class TargetDeclaration;
}
namespace OpenMP { 
class Workshare;
}
class VectorConditionalExpression;
class SwitchStatement;
class VectorReductionAdd;
class FortranUse;
class FloatingLiteral;
namespace OpenMP { 
class Reduction;
}
namespace Analysis { 
class InductionVariable;
}
namespace OpenMP { 
class Register;
}
class VectorNeg;
class StringLiteral;
class StructuredValueBracedTypecast;
class FortranStopStatement;
class CxxSizeof;
namespace OpenMP { 
class Sections;
}
class VectorLoad;
class LoopControl;
class LogicalAnd;
namespace OpenMP { 
class Section;
}
class CxxAlignof;
class VectorFabs;
class GotoStatement;
class Postdecrement;
class VectorLogicalOr;
namespace OpenMP { 
class GrainSize;
}
class FortranBozLiteral;
class CudaKernelCall;
namespace Analysis { 
class EmptyRange;
}
class DefaultArgument;
class FortranWriteStatement;
namespace Analysis { 
namespace Correctness { 
class IncoherentP;
}
}
class CxxArrow;
class VectorLowerThan;
class VectorConversion;
class VectorAssignment;
class FortranUseOnly;
class VectorMod;
class VectorAlignRight;
namespace OpenMP { 
class Schedule;
}
class Range;
class VectorMaskAnd1Not;
class DefaultStatement;
namespace OpenMP { 
class ReductionItem;
}
namespace Analysis { 
class RangeSub;
}
class FortranLabelAssignStatement;
namespace OpenMP { 
class BarrierFull;
}
namespace OpenMP { 
class Priority;
}
class AlignmentInfo;
namespace Analysis { 
namespace Correctness { 
class Dead;
}
}
class FortranCloseStatement;
class IteratorLoopControl;
class Sizeof;
class UpcSyncStatement;
class PragmaCustomStatement;
class VectorEqual;
class UnknownPragma;
class MultiReference;
class C99IndexDesignator;
namespace OpenMP { 
class CopyInout;
}
class CxxValuePack;
class ComplexLiteral;
namespace OpenMP { 
class SharedAndAlloca;
}
class LabeledStatement;
class FortranImpliedDo;
namespace OpenMP { 
class ShMem;
}
class Alignof;
class VectorLowerOrEqualThan;
namespace OpenMP { 
class Final;
}
class Add;
class VectorArithmeticShr;
class FortranIoStatement;
namespace Analysis { 
namespace Correctness { 
class IncoherentOut;
}
}
class RealPart;
class ExpressionStatement;
class CxxForRanged;
class Different;
class NontemporalFlag;
class FortranArithmeticIfStatement;
class CxxParseLater;
namespace OpenMP { 
class BarrierAtEnd;
}
namespace OpenMP { 
class FirstLastprivate;
}
class Unknown;
class VectorGreaterOrEqualThan;
namespace OpenMP { 
class Critical;
}
class VirtualFunctionCall;
class FortranEntryStatement;
class Text;
class Throw;
class CxxExplicitInstantiationDecl;
namespace Analysis { 
class Assert;
}
class CxxValuePackExpanded;
class OnTopFlag;
class BitwiseXor;
class FortranAssignedGotoStatement;
class PragmaCustomDeclaration;
class CxxSizeofPack;
class CxxDecl;
namespace Analysis { 
class ReachDefExpr;
}
class Predecrement;
class VectorLiteral;
class FortranPrintStatement;
class CxxFunctionFormUnaryPostfix;
class BitwiseShr;
class Type;
class VectorMaskConversion;
class CatchHandler;
class CxxParenthesizedInitializer;
class Postincrement;
class ConditionalExpression;
class FortranAllocateStatement;
class Reference;
class LowerOrEqualThan;
class CxxDotPtrMember;
namespace Analysis { 
class Maximum;
}
class CxxDepNameSimple;
class CompoundStatement;
class FortranPauseStatement;
namespace OpenMP { 
class FunctionTaskParsingContext;
}
namespace OpenMP { 
class Aligned;
}
namespace OpenMP { 
class Mask;
}
class BreakStatement;
namespace OpenMP { 
class BarrierSignal;
}
class ValueInitialization;
namespace OpenMP { 
class NoPrefetch;
}
class CxxBracedInitializer;
namespace OpenMP { 
class Suitable;
}
namespace OpenMP { 
class Uniform;
}
class FortranEquivalence;
class AsmDefinition;
namespace OpenMP { 
class Target;
}
namespace Analysis { 
class Dead;
}
namespace OpenMP { 
class DepOut;
}
class ImagPart;
class ImplicitMemberInit;
class FortranIoSpec;
class VectorMaskNot;
namespace OpenMP { 
class SimdParallel;
}
class LogicalOr;
class TopLevel;
namespace OpenMP { 
class CombinedWorksharing;
}
namespace Analysis { 
namespace Correctness { 
class IncoherentIn;
}
}
class BitwiseShl;
class TemplateFunctionCode;
class FortranWhere;
namespace OpenMP { 
class Lastprivate;
}
namespace OpenMP { 
class UnrollAndJam;
}
class VectorGather;
class FortranWherePair;
namespace Analysis { 
namespace Correctness { 
class IncoherentInPointed;
}
}
class Dereference;
class StructuredValueFortranTypespecArrayConstructor;
class WhileStatement;
class BitwiseShrAssignment;
class CxxFunctionFormBinaryInfix;
class ErrExpr;
class VectorMul;
namespace OpenMP { 
class Parallel;
}
class Mul;
class CxxImplicitInstantiation;
namespace Analysis { 
class ReachingDefinitionIn;
}
class C99DesignatedInitializer;
class SourceComment;
class BitwiseShlAssignment;
namespace OpenMP { 
class Taskyield;
}
namespace Analysis { 
namespace Correctness { 
class AutoStorage;
}
}
class EmptyStatement;
class StructuredValue;
class VectorStore;
class MaskLiteral;
class BitwiseXorAssignment;
namespace OpenMP { 
class NoFlush;
}
namespace OpenMP { 
class FlushMemory;
}
namespace OpenMP { 
class Task;
}
class FortranBindOpencl;
class DivAssignment;
class MinusAssignment;
class IndexDesignator;

} // Nodecl
#endif // TL_NODECL_FWD_HPP
