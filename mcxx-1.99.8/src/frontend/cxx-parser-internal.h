
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton interface for Bison GLR parsers in C
   
      Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     END = 0,
     ADD_ASSIGN = 258,
     ANDAND = 259,
     AND_ASSIGN = 260,
     ASM = 261,
     TOKEN_AUTO_STORAGE = 262,
     TOKEN_AUTO_TYPE = 263,
     TOKEN_BOOL = 264,
     BOOLEAN_LITERAL = 265,
     BREAK = 266,
     CASE = 267,
     CATCH = 268,
     TOKEN_CHAR = 269,
     CHARACTER_LITERAL = 270,
     CLASS = 271,
     TOKEN_CONST = 272,
     CONST_CAST = 273,
     CONTINUE = 274,
     DECIMAL_LITERAL = 275,
     DEFAULT = 276,
     TOKEN_DELETE = 277,
     TOKEN_DELETE_ARRAY = 278,
     DIV_ASSIGN = 279,
     DO = 280,
     TWO_COLONS = 281,
     TOKEN_DOUBLE = 282,
     DYNAMIC_CAST = 283,
     ELSE = 284,
     ENUM = 285,
     EQUAL = 286,
     DECLTYPE = 287,
     EXPLICIT = 288,
     EXPORT = 289,
     EXTERN = 290,
     TOKEN_FLOAT = 291,
     FLOATING_LITERAL = 292,
     HEXADECIMAL_FLOAT = 293,
     FOR = 294,
     FRIEND = 295,
     GOTO = 296,
     BINARY_LITERAL = 297,
     HEXADECIMAL_LITERAL = 298,
     IDENTIFIER = 299,
     TOK_FINAL = 300,
     TOK_OVERRIDE = 301,
     IF = 302,
     INLINE = 303,
     TOKEN_INT = 304,
     TOKEN_INT128 = 305,
     TOKEN_FLOAT128 = 306,
     LEFT = 307,
     LEFT_ASSIGN = 308,
     LESS_OR_EQUAL = 309,
     TOKEN_LONG = 310,
     MINUSMINUS = 311,
     MOD_ASSIGN = 312,
     MUL_ASSIGN = 313,
     MUTABLE = 314,
     NAMESPACE = 315,
     TOKEN_NEW = 316,
     TOKEN_NEW_ARRAY = 317,
     NOT_EQUAL = 318,
     OCTAL_LITERAL = 319,
     OPERATOR = 320,
     OPERATOR_LITERAL = 321,
     OR_ASSIGN = 322,
     OROR = 323,
     PLUSPLUS = 324,
     PRIVATE = 325,
     PROTECTED = 326,
     PTR_OP = 327,
     PTR_OP_MUL = 328,
     PUBLIC = 329,
     REGISTER = 330,
     REINTERPRET_CAST = 331,
     RETURN = 332,
     TOKEN_SHORT = 333,
     TOKEN_SIGNED = 334,
     SIZEOF = 335,
     STATIC = 336,
     STATIC_CAST = 337,
     STRING_LITERAL = 338,
     STRUCT = 339,
     SUB_ASSIGN = 340,
     SWITCH = 341,
     TEMPLATE = 342,
     TOKEN_THIS = 343,
     THROW = 344,
     ELLIPSIS = 345,
     TRY = 346,
     TYPEDEF = 347,
     TYPEID = 348,
     TYPENAME = 349,
     UNION = 350,
     TOKEN_UNSIGNED = 351,
     USING = 352,
     VIRTUAL = 353,
     TOKEN_VOID = 354,
     TOKEN_VOLATILE = 355,
     TOKEN_WCHAR_T = 356,
     TOKEN_CHAR16_T = 357,
     TOKEN_CHAR32_T = 358,
     TOKEN_ALIGNOF = 359,
     TOKEN_ALIGNAS = 360,
     WHILE = 361,
     XOR_ASSIGN = 362,
     STATIC_ASSERT = 363,
     PP_COMMENT = 364,
     PP_TOKEN = 365,
     MCC_REBINDABLE_REFERENCE = 366,
     GXX_HAS_NOTHROW_ASSIGN = 367,
     GXX_HAS_NOTHROW_CONSTRUCTOR = 368,
     GXX_HAS_NOTHROW_COPY = 369,
     GXX_HAS_TRIVIAL_ASSIGN = 370,
     GXX_HAS_TRIVIAL_CONSTRUCTOR = 371,
     GXX_HAS_TRIVIAL_COPY = 372,
     GXX_HAS_TRIVIAL_DESTRUCTOR = 373,
     GXX_HAS_VIRTUAL_DESTRUCTOR = 374,
     GXX_IS_ABSTRACT = 375,
     GXX_IS_BASE_OF = 376,
     GXX_IS_CLASS = 377,
     GXX_IS_CONVERTIBLE_TO = 378,
     GXX_IS_EMPTY = 379,
     GXX_IS_ENUM = 380,
     GXX_IS_LITERAL_TYPE = 381,
     GXX_IS_POD = 382,
     GXX_IS_POLYMORPHIC = 383,
     GXX_IS_STANDARD_LAYOUT = 384,
     GXX_IS_TRIVIAL = 385,
     GXX_IS_UNION = 386,
     GXX_IS_FINAL = 387,
     GXX_UNDERLYING_TYPE = 388,
     RIGHT = 389,
     RIGHT_ASSIGN = 390,
     GREATER_OR_EQUAL = 391,
     AB1 = 392,
     AB2 = 393,
     STD_ATTRIBUTE_START = 394,
     STD_ATTRIBUTE_END = 395,
     STD_ATTRIBUTE_TEXT = 396,
     TOKEN_CONSTEXPR = 397,
     TOKEN_THREAD_LOCAL = 398,
     TOKEN_NULLPTR = 399,
     TOKEN_NOEXCEPT = 400,
     TOKEN_NOEXCEPT_ALONE = 401,
     BUILTIN_VA_ARG = 402,
     BUILTIN_OFFSETOF = 403,
     TOKEN_GCC_ALIGNOF = 404,
     EXTENSION = 405,
     REAL = 406,
     IMAG = 407,
     LABEL = 408,
     COMPLEX = 409,
     TYPEOF = 410,
     RESTRICT = 411,
     TOKEN_GCC_ATTRIBUTE = 412,
     THREAD = 413,
     SUBPARSE_EXPRESSION = 414,
     SUBPARSE_EXPRESSION_LIST = 415,
     SUBPARSE_STATEMENT = 416,
     SUBPARSE_DECLARATION = 417,
     SUBPARSE_MEMBER = 418,
     SUBPARSE_TYPE = 419,
     SUBPARSE_TYPE_LIST = 420,
     SUBPARSE_ID_EXPRESSION = 421,
     NODECL_LITERAL_EXPR = 422,
     NODECL_LITERAL_STMT = 423,
     SYMBOL_LITERAL_REF = 424,
     TYPE_LITERAL_REF = 425,
     MCC_BYTE = 426,
     MCC_BOOL = 427,
     MCC_MASK = 428,
     MCC_ARRAY_SUBSCRIPT_CHECK = 429,
     MCC_CONST_VALUE_CHECK = 430,
     C_FORTRAN_ALLOCATE = 431,
     STATEMENT_PLACEHOLDER = 432,
     UNKNOWN_PRAGMA = 433,
     VERBATIM_PRAGMA = 434,
     VERBATIM_CONSTRUCT = 435,
     VERBATIM_TYPE = 436,
     VERBATIM_TEXT = 437,
     PRAGMA_CUSTOM = 438,
     PRAGMA_CUSTOM_NEWLINE = 439,
     PRAGMA_CUSTOM_DIRECTIVE = 440,
     PRAGMA_CUSTOM_CONSTRUCT = 441,
     PRAGMA_CUSTOM_END_CONSTRUCT = 442,
     PRAGMA_CUSTOM_CONSTRUCT_NOEND = 443,
     PRAGMA_CUSTOM_END_CONSTRUCT_NOEND = 444,
     PRAGMA_CUSTOM_CLAUSE = 445,
     PRAGMA_CLAUSE_ARG_TEXT = 446,
     SUBPARSE_OPENMP_DECLARE_REDUCTION = 447,
     SUBPARSE_OPENMP_DEPEND_ITEM = 448,
     SUBPARSE_OMPSS_DEPENDENCY_EXPRESSION = 449,
     TWO_DOTS = 450,
     SUBPARSE_SUPERSCALAR_DECLARATOR = 451,
     SUBPARSE_SUPERSCALAR_DECLARATOR_LIST = 452,
     SUBPARSE_SUPERSCALAR_EXPRESSION = 453,
     CUDA_DEVICE = 454,
     CUDA_GLOBAL = 455,
     CUDA_HOST = 456,
     CUDA_CONSTANT = 457,
     CUDA_SHARED = 458,
     CUDA_KERNEL_LEFT = 459,
     CUDA_KERNEL_RIGHT = 460,
     OPENCL_GLOBAL = 461,
     OPENCL_KERNEL = 462,
     OPENCL_CONSTANT = 463,
     OPENCL_LOCAL = 464,
     XL_BUILTIN_SPEC = 465,
     TOKEN_DECLSPEC = 466,
     MS_INT8 = 467,
     MS_INT16 = 468,
     MS_INT32 = 469,
     MS_INT64 = 470,
     INTEL_ASSUME = 471,
     INTEL_ASSUME_ALIGNED = 472
   };
#endif


#ifndef YYSTYPE
typedef union YYSTYPE
{

/* Line 2638 of glr.c  */
#line 56 "src/frontend/cxx03.y"

	token_atrib_t token_atrib;
	AST ast;
	AST ast2[2];
	AST ast3[3];
	AST ast4[4];
	node_t node_type;
    const char *text;



/* Line 2638 of glr.c  */
#line 280 "src/frontend/cxx-parser-internal.h"
} YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
#endif

#if ! defined YYLTYPE && ! defined YYLTYPE_IS_DECLARED
typedef struct YYLTYPE
{

  int first_line;
  int first_column;
  int last_line;
  int last_column;

} YYLTYPE;
# define YYLTYPE_IS_DECLARED 1
# define YYLTYPE_IS_TRIVIAL 1
#endif



extern YYSTYPE mcxxlval;

extern YYLTYPE mcxxlloc;


