
/* A Bison parser, made by GNU Bison 2.4.1.  */

/* Skeleton implementation for Bison GLR parsers in C
   
      Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.
   
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.
   
   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C GLR parser skeleton written by Paul Hilfinger.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.4.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "glr.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 1


/* Substitute the variable and function names.  */
#define yyparse mc99parse
#define yylex   mc99lex
#define yyerror mc99error
#define yylval  mc99lval
#define yychar  mc99char
#define yydebug mc99debug
#define yynerrs mc99nerrs
#define yylloc  mc99lloc

/* Copy the first part of user declarations.  */

/* Line 172 of glr.c  */
#line 28 "src/frontend/c99.y"

/*
   Parser of ISO/IEC 9899:1999 - C

   It parses a superset of the language.

   Must be compiled with rofi-bison-2.1. 
   Ask for it at <rferrer@ac.upc.edu>
 */

#include "c99-parser.h"
#include "cxx-ast.h"
#include "cxx-lexer.h"
#include "cxx-utils.h"
#include "cxx-diagnostic.h"
#include "mem.h"

#define YYDEBUG 1
#define YYERROR_VERBOSE 1
// Sometimes we need lots of memory
#define YYMAXDEPTH (10000000)

#define YYMALLOC xmalloc
#define YYFREE DELETE
#define YYREALLOC xrealloc



/* Line 172 of glr.c  */
#line 96 "src/frontend/c99-parser.c"



#include "c99-parser-internal.h"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 1
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

/* Default (constant) value used for initialization for null
   right-hand sides.  Unlike the standard yacc.c template,
   here we set the default value of $$ to a zeroed-out value.
   Since the default value is undefined, this behavior is
   technically correct.  */
static YYSTYPE yyval_default;

/* Copy the second part of user declarations.  */

/* Line 243 of glr.c  */
#line 76 "src/frontend/c99.y"

extern int yylex(void);
static AST ambiguityHandler (YYSTYPE x0, YYSTYPE x1);
void yyerror(AST* parsed_tree UNUSED_PARAMETER, const char* c);


/* Line 243 of glr.c  */
#line 138 "src/frontend/c99-parser.c"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(e) ((void) (e))
#else
# define YYUSE(e) /* empty */
#endif

/* Identity function, used to suppress warnings about constant conditions.  */
#ifndef lint
# define YYID(n) (n)
#else
#if (defined __STDC__ || defined __C99__FUNC__ \
     || defined __cplusplus || defined _MSC_VER)
static int
YYID (int i)
#else
static int
YYID (i)
    int i;
#endif
{
  return i;
}
#endif

#ifndef YYFREE
# define YYFREE free
#endif
#ifndef YYMALLOC
# define YYMALLOC malloc
#endif
#ifndef YYREALLOC
# define YYREALLOC realloc
#endif

#define YYSIZEMAX ((size_t) -1)

#ifdef __cplusplus
   typedef bool yybool;
#else
   typedef unsigned char yybool;
#endif
#define yytrue 1
#define yyfalse 0

#ifndef YYSETJMP
# include <setjmp.h>
# define YYJMP_BUF jmp_buf
# define YYSETJMP(env) setjmp (env)
# define YYLONGJMP(env, val) longjmp (env, val)
#endif

/*-----------------.
| GCC extensions.  |
`-----------------*/

#ifndef __attribute__
/* This feature is available in gcc versions 2.5 and later.  */
# if (! defined __GNUC__ || __GNUC__ < 2 \
      || (__GNUC__ == 2 && __GNUC_MINOR__ < 5) || __STRICT_ANSI__)
#  define __attribute__(Spec) /* empty */
# endif
#endif

#define YYOPTIONAL_LOC(Name) Name

#ifndef YYASSERT
# define YYASSERT(condition) ((void) ((condition) || (abort (), 0)))
#endif

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  349
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   10713

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  193
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  224
/* YYNRULES -- Number of rules.  */
#define YYNRULES  630
/* YYNRULES -- Number of states.  */
#define YYNSTATES  1106
/* YYMAXRHS -- Maximum number of symbols on right-hand side of rule.  */
#define YYMAXRHS 12
/* YYMAXLEFT -- Maximum number of symbols to the left of a handle
   accessed by $0, $-1, etc., in any rule.  */
#define YYMAXLEFT 0

/* YYTRANSLATE(X) -- Bison symbol number corresponding to X.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   423

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    81,     2,     2,     2,    82,    83,     2,
      84,    85,    86,    87,    88,    89,    90,    91,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    92,    93,
      94,    95,    96,    97,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    98,     2,    99,   100,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,   101,   102,   103,   104,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,   105,   106,   107,   108,
     109,   110,   111,   112,   113,   114,   115,   116,   117,   118,
     119,   120,   121,   122,   123,   124,   125,   126,   127,   128,
     129,   130,   131,   132,   133,   134,   135,   136,   137,   138,
     139,   140,   141,   142,   143,   144,   145,   146,   147,   148,
     149,   150,   151,   152,   153,   154,   155,   156,   157,   158,
     159,   160,   161,   162,   163,   164,   165,   166,   167,   168,
     169,   170,   171,   172,   173,   174,   175,   176,   177,   178,
     179,   180,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     6,     8,    11,    13,    15,    17,
      19,    21,    29,    31,    33,    35,    38,    40,    42,    44,
      46,    49,    51,    53,    57,    59,    63,    65,    68,    70,
      72,    73,    80,    86,    88,    92,    94,    96,   101,   108,
     117,   128,   141,   142,   144,   146,   147,   149,   153,   158,
     166,   168,   170,   172,   176,   179,   182,   186,   190,   193,
     196,   198,   200,   202,   205,   209,   212,   215,   217,   219,
     223,   226,   229,   231,   233,   237,   240,   243,   245,   247,
     250,   252,   254,   256,   258,   260,   262,   264,   266,   268,
     270,   272,   274,   276,   278,   280,   282,   284,   286,   288,
     290,   292,   294,   296,   298,   300,   302,   304,   306,   310,
     313,   316,   318,   320,   322,   324,   326,   328,   333,   338,
     343,   345,   347,   349,   351,   353,   355,   357,   359,   361,
     363,   367,   371,   373,   377,   379,   383,   386,   390,   393,
     397,   398,   400,   402,   404,   407,   412,   415,   419,   421,
     424,   426,   429,   431,   433,   436,   440,   442,   445,   447,
     449,   453,   456,   458,   461,   466,   473,   480,   484,   487,
     489,   493,   494,   496,   498,   499,   501,   503,   504,   506,
     508,   510,   513,   515,   517,   519,   521,   523,   526,   531,
     538,   545,   549,   551,   555,   557,   558,   560,   565,   571,
     575,   577,   579,   583,   585,   588,   591,   595,   598,   600,
     603,   605,   609,   611,   614,   618,   624,   630,   635,   642,
     649,   651,   653,   657,   659,   663,   665,   669,   673,   676,
     680,   683,   685,   687,   689,   693,   696,   701,   705,   711,
     714,   716,   719,   723,   726,   729,   733,   736,   739,   741,
     743,   746,   748,   753,   757,   759,   762,   765,   769,   771,
     773,   775,   778,   782,   785,   787,   790,   792,   796,   798,
     802,   805,   809,   812,   817,   820,   824,   828,   833,   836,
     838,   840,   842,   844,   846,   848,   850,   852,   854,   858,
     863,   867,   874,   877,   879,   881,   885,   888,   890,   893,
     899,   907,   913,   915,   921,   929,   938,   940,   941,   943,
     944,   946,   948,   951,   954,   957,   961,   965,   970,   972,
     976,   978,   980,   984,   991,   998,  1007,  1014,  1021,  1023,
    1027,  1031,  1035,  1038,  1040,  1042,  1044,  1046,  1051,  1055,
    1060,  1064,  1068,  1071,  1074,  1079,  1083,  1088,  1091,  1093,
    1097,  1099,  1102,  1105,  1108,  1111,  1116,  1119,  1122,  1127,
    1130,  1133,  1136,  1138,  1140,  1142,  1144,  1146,  1148,  1150,
    1155,  1157,  1161,  1165,  1169,  1171,  1175,  1179,  1181,  1185,
    1189,  1191,  1195,  1199,  1203,  1207,  1209,  1213,  1217,  1219,
    1223,  1225,  1229,  1231,  1235,  1237,  1241,  1243,  1247,  1249,
    1255,  1260,  1262,  1266,  1268,  1272,  1274,  1276,  1278,  1280,
    1282,  1284,  1286,  1288,  1290,  1292,  1294,  1296,  1298,  1300,
    1302,  1304,  1306,  1308,  1310,  1312,  1314,  1316,  1319,  1321,
    1323,  1326,  1329,  1331,  1334,  1337,  1339,  1342,  1345,  1348,
    1351,  1353,  1357,  1362,  1367,  1371,  1373,  1378,  1383,  1385,
    1387,  1389,  1391,  1393,  1400,  1405,  1412,  1421,  1428,  1437,
    1439,  1444,  1447,  1450,  1452,  1456,  1468,  1470,  1472,  1474,
    1476,  1482,  1484,  1486,  1488,  1490,  1492,  1494,  1496,  1498,
    1500,  1502,  1504,  1507,  1511,  1515,  1519,  1523,  1530,  1532,
    1536,  1543,  1544,  1546,  1548,  1552,  1555,  1560,  1564,  1566,
    1568,  1570,  1573,  1575,  1577,  1584,  1587,  1589,  1591,  1594,
    1597,  1603,  1611,  1613,  1615,  1617,  1619,  1623,  1625,  1627,
    1629,  1631,  1633,  1635,  1637,  1639,  1641,  1643,  1646,  1650,
    1655,  1657,  1658,  1660,  1667,  1672,  1675,  1677,  1679,  1681,
    1685,  1689,  1693,  1697,  1699,  1701,  1705,  1711,  1715,  1721,
    1725,  1728,  1732,  1735,  1740,  1742,  1747,  1748,  1750,  1752,
    1755,  1758,  1762,  1768,  1774,  1777,  1782,  1785,  1790,  1793,
    1798,  1800,  1802,  1804,  1807,  1809,  1811,  1813,  1816,  1819,
    1823,  1827,  1829,  1833,  1837,  1841,  1844,  1846,  1847,  1858,
    1860,  1861,  1863,  1865,  1867,  1869,  1871,  1873,  1875,  1877,
    1879,  1884,  1890,  1894,  1902,  1908,  1912,  1914,  1916,  1918,
    1920,  1922,  1924,  1926,  1928,  1931,  1933,  1935,  1940,  1941,
    1943,  1945,  1949,  1951,  1956,  1958,  1960,  1962,  1964,  1966,
    1971
};

/* YYRHS -- A `-1'-separated list of the rules' RHS.  */
static const short int yyrhs[] =
{
     194,     0,    -1,   195,    -1,    -1,   196,    -1,   195,   196,
      -1,   197,    -1,   286,    -1,   216,    -1,   202,    -1,   198,
      -1,    79,    84,   342,    88,   344,    85,    93,    -1,   217,
      -1,   202,    -1,   200,    -1,   110,   200,    -1,   201,    -1,
     286,    -1,   211,    -1,   203,    -1,   110,   197,    -1,    72,
      -1,    73,    -1,   113,   204,    93,    -1,   345,    -1,   204,
      88,   345,    -1,   208,    -1,   206,   205,    -1,   205,    -1,
     206,    -1,    -1,   118,    84,    84,   209,    85,    85,    -1,
     118,    84,    84,    85,    85,    -1,   210,    -1,   209,    88,
     210,    -1,   345,    -1,    14,    -1,   345,    84,   324,    85,
      -1,     6,   212,    84,   344,    85,    93,    -1,     6,   212,
      84,   344,    92,   213,    85,    93,    -1,     6,   212,    84,
     344,    92,   213,    92,   213,    85,    93,    -1,     6,   212,
      84,   344,    92,   213,    92,   213,    92,   213,    85,    93,
      -1,    -1,    69,    -1,   214,    -1,    -1,   215,    -1,   214,
      88,   215,    -1,   344,    84,   340,    85,    -1,    98,   345,
      99,   344,    84,   340,    85,    -1,   344,    -1,   217,    -1,
      93,    -1,   221,   239,    93,    -1,   223,    93,    -1,   239,
      93,    -1,   219,   239,    93,    -1,   220,   229,   224,    -1,
     220,   229,    -1,   229,   224,    -1,   229,    -1,   220,    -1,
     226,    -1,   220,   226,    -1,   224,   229,   224,    -1,   224,
     229,    -1,   229,   224,    -1,   229,    -1,   224,    -1,   224,
     229,   224,    -1,   224,   229,    -1,   229,   224,    -1,   229,
      -1,   224,    -1,   224,   230,   224,    -1,   224,   230,    -1,
     230,   224,    -1,   230,    -1,   225,    -1,   224,   225,    -1,
     226,    -1,   208,    -1,   227,    -1,   228,    -1,    65,    -1,
     262,    -1,    57,    -1,    67,    -1,    42,    -1,    56,    -1,
     114,    -1,   115,    -1,     7,    -1,    52,    -1,    59,    -1,
      24,    -1,   119,    -1,    35,    -1,    78,    -1,   231,    -1,
     233,    -1,   231,    -1,   237,    -1,   234,    -1,   235,    -1,
     290,    -1,   268,    -1,   238,    -1,   224,   229,   224,    -1,
     224,   229,    -1,   229,   224,    -1,   229,    -1,   224,    -1,
     236,    -1,   237,    -1,   234,    -1,   235,    -1,   116,    84,
     340,    85,    -1,   116,    84,   271,    85,    -1,    76,    84,
     271,    85,    -1,   345,    -1,    12,    -1,     8,    -1,    36,
      -1,    25,    -1,    20,    -1,    68,    -1,    37,    -1,    38,
      -1,   132,    -1,   292,   207,   345,    -1,   267,   207,   345,
      -1,   242,    -1,   242,    88,   240,    -1,   241,    -1,   240,
      88,   241,    -1,   246,   243,    -1,   246,   243,   280,    -1,
     247,   243,    -1,   247,   243,   280,    -1,    -1,   244,    -1,
     245,    -1,   206,    -1,   245,   206,    -1,     6,    84,   344,
      85,    -1,   207,   263,    -1,   207,   248,   246,    -1,   263,
      -1,   248,   246,    -1,    86,    -1,    86,   261,    -1,    74,
      -1,    75,    -1,   207,   253,    -1,   207,   248,   249,    -1,
     253,    -1,   248,   249,    -1,   277,    -1,   275,    -1,    84,
     251,    85,    -1,    84,    85,    -1,   254,    -1,   253,   252,
      -1,   253,    98,   260,    99,    -1,   253,    98,   257,   258,
     260,    99,    -1,   253,    98,   259,   256,   260,    99,    -1,
      84,   249,    85,    -1,   255,   252,    -1,   264,    -1,    84,
     255,    85,    -1,    -1,   257,    -1,   261,    -1,    -1,   259,
      -1,    59,    -1,    -1,    86,    -1,   339,    -1,   262,    -1,
     261,   262,    -1,    14,    -1,    69,    -1,   117,    -1,    76,
      -1,   264,    -1,   263,   252,    -1,   263,    98,   260,    99,
      -1,   263,    98,   257,   258,   260,    99,    -1,   263,    98,
     259,   256,   260,    99,    -1,    84,   246,    85,    -1,   320,
      -1,   267,   207,   266,    -1,   345,    -1,    -1,    22,    -1,
     265,   101,   269,   103,    -1,   265,   101,   269,    88,   103,
      -1,   269,    88,   270,    -1,   270,    -1,   345,    -1,   345,
      95,   342,    -1,   232,    -1,   232,   273,    -1,   207,   248,
      -1,   207,   248,   272,    -1,   207,   274,    -1,   248,    -1,
     248,   272,    -1,   274,    -1,    84,   272,    85,    -1,   252,
      -1,   274,   252,    -1,    98,   260,    99,    -1,    98,   257,
     258,   260,    99,    -1,    98,   259,   256,   260,    99,    -1,
     274,    98,   260,    99,    -1,   274,    98,   257,   258,   260,
      99,    -1,   274,    98,   259,   256,   260,    99,    -1,   276,
      -1,   345,    -1,   276,    88,   345,    -1,   278,    -1,   278,
      88,    64,    -1,   279,    -1,   278,    88,   279,    -1,   221,
     247,   207,    -1,   222,   207,    -1,   221,   273,   207,    -1,
      95,   281,    -1,   339,    -1,   323,    -1,   281,    -1,   282,
      88,   281,    -1,   283,   281,    -1,   282,    88,   283,   281,
      -1,   345,    92,   281,    -1,   282,    88,   345,    92,   281,
      -1,   284,    95,    -1,   285,    -1,   284,   285,    -1,    98,
     342,    99,    -1,    90,   345,    -1,   287,   289,    -1,   287,
     288,   289,    -1,   110,   286,    -1,   221,   250,    -1,   250,
      -1,   218,    -1,   288,   218,    -1,   305,    -1,   291,   101,
     293,   103,    -1,   291,   101,   103,    -1,   292,    -1,   292,
     345,    -1,   292,   206,    -1,   292,   206,   345,    -1,    61,
      -1,    66,    -1,   294,    -1,   293,   294,    -1,   221,   295,
      93,    -1,   223,    93,    -1,    93,    -1,   110,   294,    -1,
     297,    -1,   297,    88,   296,    -1,   298,    -1,   296,    88,
     298,    -1,   247,   207,    -1,   247,   207,   299,    -1,    92,
     342,    -1,   345,   207,    92,   342,    -1,   246,   207,    -1,
     246,   207,   299,    -1,   207,    92,   342,    -1,   345,   207,
      92,   342,    -1,    95,   342,    -1,   301,    -1,   304,    -1,
     302,    -1,   303,    -1,   305,    -1,   308,    -1,   310,    -1,
     314,    -1,   307,    -1,   345,    92,   300,    -1,    11,   342,
      92,   300,    -1,    17,    92,   300,    -1,    11,   342,    64,
     342,    92,   300,    -1,   340,    93,    -1,    93,    -1,   199,
      -1,   101,   306,   103,    -1,   101,   103,    -1,   300,    -1,
     306,   300,    -1,    34,    84,   309,    85,   300,    -1,    34,
      84,   309,    85,   300,    21,   300,    -1,    63,    84,   309,
      85,   300,    -1,   340,    -1,    70,    84,   309,    85,   300,
      -1,    19,   300,    70,    84,   340,    85,    93,    -1,    28,
      84,   313,   312,    93,   311,    85,   300,    -1,   340,    -1,
      -1,   309,    -1,    -1,   303,    -1,   217,    -1,    10,    93,
      -1,    15,    93,    -1,    53,    93,    -1,    53,   340,    93,
      -1,    29,   345,    93,    -1,    29,    86,   340,    93,    -1,
     343,    -1,    84,   340,    85,    -1,   320,    -1,   316,    -1,
      84,   305,    85,    -1,   105,    84,   339,    88,   271,    85,
      -1,   106,    84,   271,    88,   319,    85,    -1,   107,    84,
     339,    88,   339,    88,   339,    85,    -1,   108,    84,   271,
      88,   271,    85,    -1,    77,    84,   339,    88,   317,    85,
      -1,   318,    -1,   317,    88,   318,    -1,   271,    92,   339,
      -1,    17,    92,   339,    -1,   345,   284,    -1,   345,    -1,
     321,    -1,   345,    -1,   315,    -1,   322,    98,   340,    99,
      -1,   322,    84,    85,    -1,   322,    84,   324,    85,    -1,
     322,    90,   320,    -1,   322,    51,   320,    -1,   322,    50,
      -1,   322,    43,    -1,    84,   271,    85,   323,    -1,   101,
     282,   103,    -1,   101,   282,    88,   103,    -1,   101,   103,
      -1,   339,    -1,   324,    88,   339,    -1,   322,    -1,    50,
     325,    -1,    43,   325,    -1,   326,   327,    -1,    58,   325,
      -1,    58,    84,   271,    85,    -1,   110,   327,    -1,   109,
     325,    -1,   109,    84,   271,    85,    -1,   111,   327,    -1,
     112,   327,    -1,     4,   345,    -1,    86,    -1,    83,    -1,
      87,    -1,    89,    -1,    81,    -1,   104,    -1,   325,    -1,
      84,   271,    85,   327,    -1,   327,    -1,   328,    86,   327,
      -1,   328,    91,   327,    -1,   328,    82,   327,    -1,   328,
      -1,   329,    87,   328,    -1,   329,    89,   328,    -1,   329,
      -1,   330,    39,   329,    -1,   330,    54,   329,    -1,   330,
      -1,   331,    94,   330,    -1,   331,    96,   330,    -1,   331,
      30,   330,    -1,   331,    41,   330,    -1,   331,    -1,   332,
      23,   331,    -1,   332,    46,   331,    -1,   332,    -1,   333,
      83,   332,    -1,   333,    -1,   334,   100,   333,    -1,   334,
      -1,   335,   102,   334,    -1,   335,    -1,   336,     4,   335,
      -1,   336,    -1,   337,    49,   336,    -1,   337,    -1,   337,
      97,   340,    92,   338,    -1,   337,    97,    92,   338,    -1,
     338,    -1,   337,   341,   339,    -1,   339,    -1,   340,    88,
     339,    -1,    95,    -1,    45,    -1,    18,    -1,     3,    -1,
      62,    -1,    40,    -1,    55,    -1,     5,    -1,    48,    -1,
      71,    -1,    44,    -1,   338,    -1,    16,    -1,    47,    -1,
      31,    -1,    32,    -1,    27,    -1,    26,    -1,     9,    -1,
      13,    -1,   344,    -1,    60,    -1,   344,    60,    -1,    33,
      -1,   346,    -1,   120,   340,    -1,   122,   306,    -1,   122,
      -1,   124,   293,    -1,   123,   195,    -1,   123,    -1,   125,
     271,    -1,   126,   347,    -1,   121,   324,    -1,   127,   320,
      -1,   232,    -1,   347,    88,   232,    -1,   128,    84,   350,
      85,    -1,   129,    84,   350,    85,    -1,   350,    88,   353,
      -1,   353,    -1,   130,    84,   353,    85,    -1,   131,    84,
     353,    85,    -1,    60,    -1,   348,    -1,   351,    -1,   349,
      -1,   352,    -1,   135,    84,   339,    88,   342,    85,    -1,
     136,    84,   339,    85,    -1,   322,    98,   311,    92,   311,
      99,    -1,   322,    98,   311,    92,   311,    92,   340,    99,
      -1,   322,    98,   340,    93,   340,    99,    -1,   322,    98,
     340,    93,   340,    92,   340,    99,    -1,   325,    -1,    84,
     271,    85,   327,    -1,   355,   354,    -1,   355,   356,    -1,
     356,    -1,    98,   340,    99,    -1,    28,    98,   351,    99,
      84,   313,   312,    93,   311,    85,   300,    -1,   357,    -1,
     133,    -1,   134,    -1,   358,    -1,   137,    84,   340,    85,
      93,    -1,   359,    -1,   138,    -1,   139,    -1,   360,    -1,
     360,    -1,   364,    -1,   361,    -1,   362,    -1,   361,    -1,
     363,    -1,   361,    -1,   144,   365,    -1,   144,   366,   196,
      -1,   144,   366,   294,    -1,   144,   366,   300,    -1,   146,
     367,   145,    -1,   146,    84,   370,    85,   367,   145,    -1,
     145,    -1,   147,   367,   145,    -1,   147,    84,   370,    85,
     367,   145,    -1,    -1,   368,    -1,   369,    -1,   368,    88,
     369,    -1,   368,   369,    -1,   151,    84,   370,    85,    -1,
     151,    84,    85,    -1,   151,    -1,   371,    -1,   372,    -1,
     371,   372,    -1,   373,    -1,   152,    -1,   140,   142,    84,
     345,    85,   143,    -1,   140,   143,    -1,   374,    -1,   374,
      -1,   153,   375,    -1,   154,   384,    -1,   376,    92,   378,
      92,   381,    -1,   376,    92,   378,    92,   381,    92,   382,
      -1,   379,    -1,   377,    -1,   345,    -1,   380,    -1,   378,
      88,   380,    -1,    87,    -1,    89,    -1,    86,    -1,    83,
      -1,   102,    -1,   100,    -1,     4,    -1,    49,    -1,   232,
      -1,   340,    -1,   320,   280,    -1,   322,    84,    85,    -1,
     322,    84,   324,    85,    -1,   340,    -1,    -1,   320,    -1,
     384,    98,   383,    92,   383,    99,    -1,   384,    98,   340,
      99,    -1,   155,   385,    -1,   386,    -1,   387,    -1,   339,
      -1,   101,   388,   103,    -1,   386,    88,   389,    -1,   388,
      88,   389,    -1,   345,    95,   390,    -1,   392,    -1,   391,
      -1,   339,    92,   339,    -1,   339,    92,   339,    92,   339,
      -1,   339,    93,   339,    -1,   339,    93,   339,    92,   339,
      -1,   157,   394,   395,    -1,   158,   393,    -1,   159,   340,
     395,    -1,   394,   395,    -1,   393,    88,   394,   395,    -1,
     264,    -1,   394,    98,   339,    99,    -1,    -1,   396,    -1,
     397,    -1,   396,   397,    -1,   101,   103,    -1,   101,   340,
     103,    -1,   101,   340,   156,   340,   103,    -1,   101,   340,
      92,   340,   103,    -1,   170,   325,    -1,   170,    84,   271,
      85,    -1,   166,   325,    -1,   166,    84,   271,    85,    -1,
     167,   325,    -1,   167,    84,   271,    85,    -1,   398,    -1,
     399,    -1,   162,    -1,   162,   400,    -1,   161,    -1,   163,
      -1,   401,    -1,   400,   401,    -1,    98,    99,    -1,    98,
     342,    99,    -1,    98,    86,    99,    -1,   402,    -1,   172,
     403,    93,    -1,   173,   403,    93,    -1,   165,   403,    93,
      -1,   168,    93,    -1,   340,    -1,    -1,   169,    84,   313,
     403,    93,   403,    93,   404,    85,   300,    -1,   405,    -1,
      -1,   340,    -1,    15,    -1,   406,    -1,   407,    -1,   174,
      -1,   175,    -1,   176,    -1,   177,    -1,   178,    -1,   322,
     408,    84,    85,    -1,   322,   408,    84,   324,    85,    -1,
     410,   409,   411,    -1,   339,    88,   339,    88,   339,    88,
     339,    -1,   339,    88,   339,    88,   339,    -1,   339,    88,
     339,    -1,   179,    -1,   180,    -1,   412,    -1,   181,    -1,
     182,    -1,   183,    -1,   184,    -1,   185,    -1,   360,   206,
      -1,   360,    -1,   413,    -1,   186,    84,   414,    85,    -1,
      -1,   415,    -1,   416,    -1,   415,    88,   416,    -1,   345,
      -1,   345,    84,   324,    85,    -1,   413,    -1,   187,    -1,
     188,    -1,   189,    -1,   190,    -1,   191,    84,   340,    85,
      -1,   192,    84,   339,    88,   339,    85,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,  2059,  2059,  2064,  2073,  2077,  2083,  2087,  2093,  2097,
    2101,  2108,  2114,  2118,  2122,  2128,  2132,  2138,  2144,  2149,
    2153,  2158,  2163,  2170,  2176,  2181,  2190,  2196,  2200,  2206,
    2211,  2216,  2220,  2226,  2230,  2237,  2243,  2249,  2258,  2263,
    2269,  2275,  2284,  2287,  2293,  2298,  2304,  2308,  2314,  2318,
    2324,  2332,  2336,  2343,  2347,  2351,  2357,  2363,  2367,  2371,
    2375,  2379,  2386,  2390,  2396,  2400,  2404,  2408,  2412,  2418,
    2422,  2426,  2430,  2434,  2446,  2450,  2454,  2458,  2464,  2468,
    2474,  2478,  2484,  2488,  2492,  2497,  2502,  2506,  2510,  2514,
    2519,  2523,  2529,  2533,  2537,  2541,  2546,  2552,  2556,  2562,
    2566,  2572,  2576,  2580,  2584,  2590,  2594,  2598,  2604,  2608,
    2612,  2616,  2620,  2626,  2630,  2634,  2638,  2645,  2649,  2655,
    2661,  2667,  2671,  2675,  2679,  2683,  2687,  2691,  2695,  2699,
    2705,  2711,  2722,  2726,  2732,  2736,  2742,  2746,  2752,  2756,
    2763,  2766,  2772,  2776,  2780,  2787,  2794,  2798,  2804,  2808,
    2814,  2818,  2822,  2826,  2836,  2840,  2846,  2850,  2857,  2861,
    2867,  2872,  2881,  2885,  2889,  2893,  2897,  2901,  2907,  2913,
    2918,  2926,  2929,  2935,  2943,  2946,  2952,  2960,  2963,  2967,
    2973,  2977,  2983,  2987,  2992,  2997,  3003,  3007,  3011,  3015,
    3019,  3023,  3029,  3035,  3041,  3046,  3051,  3057,  3061,  3067,
    3071,  3077,  3083,  3091,  3095,  3101,  3105,  3109,  3115,  3119,
    3123,  3129,  3133,  3137,  3141,  3145,  3149,  3153,  3157,  3161,
    3167,  3173,  3179,  3187,  3191,  3198,  3202,  3208,  3212,  3216,
    3222,  3228,  3232,  3238,  3242,  3246,  3252,  3259,  3267,  3277,
    3283,  3287,  3293,  3297,  3305,  3309,  3315,  3321,  3325,  3331,
    3335,  3341,  3351,  3355,  3361,  3365,  3372,  3378,  3387,  3391,
    3397,  3401,  3407,  3411,  3416,  3421,  3427,  3431,  3437,  3441,
    3447,  3451,  3455,  3459,  3468,  3472,  3476,  3480,  3489,  3499,
    3503,  3509,  3513,  3517,  3521,  3525,  3529,  3533,  3539,  3545,
    3549,  3554,  3560,  3564,  3571,  3577,  3581,  3587,  3591,  3606,
    3610,  3616,  3622,  3628,  3632,  3636,  3643,  3648,  3653,  3658,
    3664,  3668,  3674,  3678,  3682,  3686,  3690,  3697,  3707,  3711,
    3715,  3720,  3734,  3738,  3742,  3746,  3750,  3756,  3762,  3766,
    3772,  3776,  3790,  3796,  3804,  3810,  3816,  3820,  3824,  3828,
    3832,  3836,  3840,  3844,  3849,  3855,  3859,  3863,  3869,  3874,
    3881,  3885,  3889,  3893,  3897,  3901,  3906,  3910,  3914,  3918,
    3922,  3926,  3934,  3938,  3942,  3946,  3950,  3954,  3960,  3964,
    3970,  3974,  3978,  3982,  3988,  3992,  3996,  4002,  4006,  4010,
    4016,  4020,  4024,  4028,  4032,  4038,  4042,  4046,  4052,  4056,
    4062,  4066,  4072,  4076,  4082,  4086,  4092,  4096,  4102,  4106,
    4111,  4117,  4121,  4127,  4131,  4139,  4143,  4147,  4151,  4155,
    4159,  4163,  4167,  4171,  4175,  4179,  4185,  4195,  4199,  4203,
    4207,  4211,  4215,  4219,  4223,  4227,  4234,  4238,  4255,  4281,
    4287,  4291,  4295,  4299,  4303,  4307,  4311,  4315,  4319,  4323,
    4350,  4354,  4382,  4389,  4399,  4403,  4409,  4415,  4425,  4434,
    4440,  4454,  4461,  4501,  4505,  4511,  4515,  4519,  4523,  4529,
    4533,  4539,  4545,  4549,  4555,  4561,  4570,  4585,  4589,  4607,
    4613,  4634,  4642,  4734,  4740,  4746,  4776,  4780,  4786,  4790,
    4796,  4800,  4838,  4845,  4851,  4857,  4987,  4991,  4995,  5002,
    5006,  5013,  5016,  5022,  5026,  5030,  5036,  5040,  5044,  5050,
    5058,  5062,  5068,  5074,  5082,  5088,  5094,  5100,  5129,  5133,
    5139,  5143,  5149,  5153,  5159,  5165,  5169,  5175,  5176,  5177,
    5178,  5179,  5180,  5181,  5182,  5186,  5205,  5212,  5219,  5223,
    5250,  5255,  5260,  5264,  5270,  5295,  5301,  5305,  5311,  5317,
    5323,  5327,  5333,  5340,  5344,  5350,  5354,  5360,  5364,  5389,
    5393,  5397,  5403,  5408,  5415,  5419,  5427,  5430,  5433,  5437,
    5443,  5447,  5451,  5455,  5490,  5494,  5498,  5502,  5506,  5510,
    5516,  5520,  5526,  5530,  5536,  5540,  5547,  5551,  5557,  5561,
    5565,  5573,  5579,  5583,  5587,  5591,  5597,  5602,  5607,  5616,
    5621,  5625,  5629,  5656,  5662,  5668,  5672,  5676,  5680,  5684,
    5690,  5694,  5700,  5706,  5710,  5714,  5720,  5727,  5759,  5765,
    5769,  5773,  5777,  5790,  5797,  5811,  5846,  5852,  5859,  5862,
    5868,  5872,  5878,  5882,  5888,  5894,  5898,  5902,  5906,  5922,
    5926
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "\"end of input\"", "error", "$undefined", "\"+=\"", "\"&&\"", "\"&=\"",
  "\"__asm__\"", "\"auto storage-specifier\"", "\"_Bool\"",
  "\"boolean-literal\"", "\"break\"", "\"case\"", "\"char\"",
  "\"character-literal\"", "\"const\"", "\"continue\"",
  "\"decimal-literal\"", "\"default\"", "\"/=\"", "\"do\"", "\"double\"",
  "\"else\"", "\"enum\"", "\"==\"", "\"extern\"", "\"float\"",
  "\"floating-literal\"", "\"hexadecimal-floating-literal\"", "\"for\"",
  "\"goto\"", "\">=\"", "\"binary-integer-literal\"",
  "\"hexadecimal-integer-literal\"", "\"identifier\"", "\"if\"",
  "\"inline\"", "\"int\"", "\"__int128\"", "\"__float128\"", "\"<<\"",
  "\"<<=\"", "\"<=\"", "\"long\"", "\"--\"", "\"%=\"", "\"*=\"", "\"!=\"",
  "\"octal-integer-literal\"", "\"|=\"", "\"||\"", "\"++\"", "\"->\"",
  "\"register\"", "\"return\"", "\">>\"", "\">>=\"", "\"short\"",
  "\"signed\"", "\"sizeof\"", "\"static\"", "\"string-literal\"",
  "\"struct\"", "\"-=\"", "\"switch\"", "\"...\"", "\"typedef\"",
  "\"union\"", "\"unsigned\"", "\"void\"", "\"volatile\"", "\"while\"",
  "\"^=\"", "\"<preprocessor-comment>\"", "\"<preprocessor-token>\"",
  "\"<C-reference-specifier>\"", "\"<rebindable-reference-specifier>\"",
  "\"_Atomic\"", "\"_Generic\"", "\"_Noreturn\"", "\"_Static_assert\"",
  "\"_Thread_local\"", "'!'", "'%'", "'&'", "'('", "')'", "'*'", "'+'",
  "','", "'-'", "'.'", "'/'", "':'", "';'", "'<'", "'='", "'>'", "'?'",
  "'['", "']'", "'^'", "'{'", "'|'", "'}'", "'~'", "\"__builtin_va_arg\"",
  "\"__builtin_offsetof\"", "\"__builtin_choose_expr\"",
  "\"__builtin_types_compatible_p\"", "\"__alignof__\"",
  "\"__extension__\"", "\"__real__\"", "\"__imag__\"", "\"__label__\"",
  "\"__complex__\"", "\"__imaginary__\"", "\"__typeof__\"", "\"restrict\"",
  "\"__attribute__\"", "\"__thread\"", "\"<subparse-expression>\"",
  "\"<subparse-expression-list>\"", "\"<subparse-statement>\"",
  "\"<subparse-declaration>\"", "\"<subparse-member>\"",
  "\"<subparse-type>\"", "\"<subparse-type-list>\"",
  "\"<subparse-id-expression>\"", "\"<nodecl-literal-expression>\"",
  "\"<nodecl-literal-statement>\"", "\"<symbol-literal-reference>\"",
  "\"<type-literal-reference>\"", "\"<byte-type-spec>\"",
  "\"<bool-type-spec>\"", "\"<mask-type-spec>\"",
  "\"@array-subscript-check@\"", "\"@const-value-check@\"",
  "C_FORTRAN_ALLOCATE", "\"<statement-placeholder>\"",
  "\"<unknown-pragma>\"", "\"<verbatim pragma>\"",
  "\"<verbatim construct>\"", "\"<verbatim type clause>\"",
  "\"<verbatim text>\"", "\"<pragma-custom>\"",
  "\"<pragma-custom-newline>\"", "\"<pragma-custom-directive>\"",
  "\"<pragma-custom-construct>\"", "\"<pragma-custom-end-construct>\"",
  "\"<pragma-custom-construct-noend>\"",
  "\"<pragma-custom-end-construct-noend>\"", "\"<pragma-custom-clause>\"",
  "\"<pragma-clause-argument-text>\"", "\"<omp-declare-reduction>\"",
  "\"<omp-depend-item>\"", "\"<ompss-dependency-expression>\"", "\"..\"",
  "\"<subparse-superscalar-declarator>\"",
  "\"<subparse-superscalar-declarator-list>\"",
  "\"<subparse-superscalar-expression>\"", "\"MYTHREAD (UPC)\"",
  "\"relaxed (UPC)\"", "\"shared (UPC)\"", "\"strict (UPC)\"",
  "\"THREADS (UPC)\"", "\"upc_barrier\"", "\"upc_blocksizeof\"",
  "\"upc_elemsizeof\"", "\"upc_fence\"", "\"upc_forall\"",
  "\"upc_localsizeof\"", "\"UPC_MAX_BLOCKSIZE\"", "\"upc_notify\"",
  "\"upc_wait\"", "\"__device__\"", "\"__global__\"", "\"__host__\"",
  "\"__constant__\"", "\"__shared__\"", "\"<<<\"", "\">>>\"",
  "\"__global\"", "\"__kernel\"", "\"__constant\"", "\"__local\"",
  "\"_Builtin\"", "\"__declspec\"", "\"__int8\"", "\"__int16\"",
  "\"__int32\"", "\"__int64\"", "\"__assume\"", "\"__assume_aligned\"",
  "$accept", "translation_unit", "declaration_sequence", "declaration",
  "block_declaration", "static_assert_declaration",
  "non_empty_block_declaration", "nested_block_declaration",
  "nested_function_definition", "common_block_declaration",
  "label_declaration", "label_declarator_seq", "attribute_specifier",
  "attribute_specifier_seq", "attribute_specifier_seq_opt",
  "gcc_attribute", "gcc_attribute_list", "gcc_attribute_value",
  "asm_definition", "volatile_optional", "asm_operand_list",
  "asm_operand_list_nonempty", "asm_operand", "simple_declaration",
  "non_empty_simple_declaration", "old_style_parameter",
  "old_style_decl_specifier_seq",
  "nontype_specifier_without_attribute_seq", "decl_specifier_seq",
  "decl_specifier_seq_may_end_with_declarator", "decl_specifier_alone_seq",
  "nontype_specifier_seq", "nontype_specifier",
  "nontype_specifier_without_attribute", "storage_class_specifier",
  "function_specifier", "type_specifier", "type_specifier_alone",
  "declarating_type_specifier", "type_specifier_seq",
  "simple_type_specifier", "typeof_type_specifier",
  "atomic_type_specifier", "type_name", "builtin_types",
  "elaborated_type_specifier", "init_declarator_list",
  "init_declarator_list_nonempty", "init_declarator",
  "init_declarator_first", "gcc_extra_bits_init_declarator_opt",
  "gcc_extra_bits_init_declarator", "asm_specification", "declarator",
  "declarator_not_started_with_attributes", "ptr_operator",
  "functional_declarator",
  "functional_declarator_not_started_with_attributes",
  "parameter_or_kr_list", "parameters_and_qualifiers",
  "functional_direct_declarator", "functional_declarator_id",
  "functional_final_declarator_id", "optional_array_cv_qualifier_seq",
  "array_cv_qualifier_seq", "optional_array_static_qualif",
  "array_static_qualif", "optional_array_expression", "cv_qualifier_seq",
  "cv_qualifier", "direct_declarator", "declarator_id", "enum_head",
  "identifier_opt", "enum_key", "enum_specifier", "enumeration_list",
  "enumeration_definition", "type_id", "abstract_declarator",
  "abstract_declarator_not_started_with_attributes",
  "abstract_direct_declarator", "identifier_list", "identifier_list_kr",
  "parameter_type_list", "parameter_declaration_list",
  "parameter_declaration", "initializer", "initializer_clause",
  "initializer_list", "designation", "designator_list", "designator",
  "function_definition", "function_definition_header",
  "old_style_parameter_list", "function_body", "class_specifier",
  "class_head", "class_key", "member_specification_seq",
  "member_declaration", "member_declarator_list",
  "member_declarator_list_nonempty", "member_declarator_first",
  "member_declarator", "constant_initializer", "statement",
  "nondeclarating_statement", "labeled_statement", "expression_statement",
  "declaration_statement", "compound_statement", "statement_seq",
  "if_statement", "selection_statement", "condition",
  "iteration_statement", "expression_opt", "condition_opt",
  "for_init_statement", "jump_statement", "primary_expression",
  "generic_selection", "generic_assoc_list", "generic_association",
  "offsetof_member_designator", "id_expression", "unqualified_id",
  "postfix_expression", "braced_init_list", "expression_list",
  "unary_expression", "unary_operator", "cast_expression",
  "multiplicative_expression", "additive_expression", "shift_expression",
  "relational_expression", "equality_expression", "and_expression",
  "exclusive_or_expression", "inclusive_or_expression",
  "logical_and_expression", "logical_or_expression",
  "conditional_expression", "assignment_expression", "expression",
  "assignment_operator", "constant_expression", "literal",
  "string_literal", "identifier_token", "subparsing", "subparse_type_list",
  "nodecl_literal_expr", "nodecl_literal_stmt",
  "nodecl_literal_attribute_seq", "symbol_literal_ref", "type_literal_ref",
  "nodecl_string_literal", "noshape_cast_expression", "shape_seq", "shape",
  "mercurium_extended_type_specifiers", "fortran_allocate_statement",
  "statement_placeholder", "unknown_pragma", "pragma_custom_directive",
  "pragma_custom_construct_declaration",
  "pragma_custom_construct_member_declaration",
  "pragma_custom_construct_statement", "pragma_custom_line_directive",
  "pragma_custom_line_construct", "pragma_custom_clause_opt_seq",
  "pragma_custom_clause_seq", "pragma_custom_clause",
  "pragma_clause_arg_list", "pragma_clause_arg", "pragma_clause_arg_item",
  "pragma_clause_arg_text", "verbatim_construct", "omp_declare_reduction",
  "omp_dr_reduction_id", "omp_dr_identifier", "omp_dr_typename_list",
  "omp_dr_operator", "omp_dr_typename", "omp_dr_combiner",
  "omp_dr_initializer", "omp_expression_opt", "omp_depend_item",
  "ompss_dependency_expr", "ompss_single_dependency",
  "ompss_multi_dependency", "ompss_iterated_dep_body",
  "ompss_iterator_dep", "ompss_iterator_range",
  "ompss_iterator_range_section", "ompss_iterator_range_size",
  "superscalar_declarator_list", "superscalar_declarator",
  "opt_superscalar_region_spec_list", "superscalar_region_spec_list",
  "superscalar_region_spec", "upc_shared_type_qualifier",
  "upc_reference_type_qualifier", "upc_layout_qualifier",
  "upc_layout_qualifier_element", "upc_synchronization_statement",
  "upc_expression_opt", "upc_affinity_opt", "upc_affinity",
  "cuda_specifiers", "cuda_kernel_call", "cuda_kernel_arguments",
  "cuda_kernel_config_list", "cuda_kernel_config_left",
  "cuda_kernel_config_right", "opencl_specifiers", "declspec_specifier",
  "extended_decl_modifier_list", "extended_decl_modifier_list0",
  "extended_decl_modifier", 0
};
#endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned short int yyr1[] =
{
       0,   193,   194,   194,   195,   195,   196,   196,   197,   197,
     197,   198,   199,   199,   199,   200,   200,   201,   202,   202,
     202,   202,   202,   203,   204,   204,   205,   206,   206,   207,
     207,   208,   208,   209,   209,   210,   210,   210,   211,   211,
     211,   211,   212,   212,   213,   213,   214,   214,   215,   215,
     215,   216,   216,   217,   217,   217,   218,   219,   219,   219,
     219,   219,   220,   220,   221,   221,   221,   221,   221,   222,
     222,   222,   222,   222,   223,   223,   223,   223,   224,   224,
     225,   225,   226,   226,   226,   226,   226,   226,   226,   226,
     226,   226,   227,   227,   227,   227,   227,   228,   228,   229,
     229,   230,   230,   230,   230,   231,   231,   231,   232,   232,
     232,   232,   232,   233,   233,   233,   233,   234,   234,   235,
     236,   237,   237,   237,   237,   237,   237,   237,   237,   237,
     238,   238,   239,   239,   240,   240,   241,   241,   242,   242,
     243,   243,   244,   244,   244,   245,   246,   246,   247,   247,
     248,   248,   248,   248,   249,   249,   250,   250,   251,   251,
     252,   252,   253,   253,   253,   253,   253,   253,   254,   255,
     255,   256,   256,   257,   258,   258,   259,   260,   260,   260,
     261,   261,   262,   262,   262,   262,   263,   263,   263,   263,
     263,   263,   264,   265,   266,   266,   267,   268,   268,   269,
     269,   270,   270,   271,   271,   272,   272,   272,   273,   273,
     273,   274,   274,   274,   274,   274,   274,   274,   274,   274,
     275,   276,   276,   277,   277,   278,   278,   279,   279,   279,
     280,   281,   281,   282,   282,   282,   282,   282,   282,   283,
     284,   284,   285,   285,   286,   286,   286,   287,   287,   288,
     288,   289,   290,   290,   291,   291,   291,   291,   292,   292,
     293,   293,   294,   294,   294,   294,   295,   295,   296,   296,
     297,   297,   297,   297,   298,   298,   298,   298,   299,   300,
     300,   301,   301,   301,   301,   301,   301,   301,   302,   302,
     302,   302,   303,   303,   304,   305,   305,   306,   306,   307,
     307,   308,   309,   310,   310,   310,   311,   311,   312,   312,
     313,   313,   314,   314,   314,   314,   314,   314,   315,   315,
     315,   315,   315,   315,   315,   315,   315,   316,   317,   317,
     318,   318,   319,   319,   320,   321,   322,   322,   322,   322,
     322,   322,   322,   322,   322,   323,   323,   323,   324,   324,
     325,   325,   325,   325,   325,   325,   325,   325,   325,   325,
     325,   325,   326,   326,   326,   326,   326,   326,   327,   327,
     328,   328,   328,   328,   329,   329,   329,   330,   330,   330,
     331,   331,   331,   331,   331,   332,   332,   332,   333,   333,
     334,   334,   335,   335,   336,   336,   337,   337,   338,   338,
     338,   339,   339,   340,   340,   341,   341,   341,   341,   341,
     341,   341,   341,   341,   341,   341,   342,   343,   343,   343,
     343,   343,   343,   343,   343,   343,   344,   344,   345,   194,
     346,   346,   346,   346,   346,   346,   346,   346,   346,   346,
     347,   347,   348,   349,   350,   350,   351,   352,   353,   315,
     321,   301,   233,   315,   315,   322,   322,   322,   322,   354,
     354,   327,   355,   355,   356,   310,   226,   357,   357,   301,
     358,   300,   359,   360,   202,   294,   301,   301,   196,   196,
     294,   294,   361,   362,   363,   364,   365,   365,   365,   366,
     366,   367,   367,   368,   368,   368,   369,   369,   369,   370,
     371,   371,   372,   373,   374,   374,   202,   294,   346,   346,
     375,   375,   376,   376,   377,   378,   378,   379,   379,   379,
     379,   379,   379,   379,   379,   380,   381,   382,   382,   382,
     383,   383,   384,   384,   384,   346,   385,   385,   386,   387,
     388,   388,   389,   390,   390,   391,   391,   392,   392,   346,
     346,   346,   393,   393,   394,   394,   395,   395,   396,   396,
     397,   397,   397,   397,   325,   325,   325,   325,   325,   325,
     262,   262,   398,   398,   399,   399,   400,   400,   401,   401,
     401,   301,   402,   402,   402,   402,   403,   403,   310,   404,
     404,   405,   405,   226,   322,   406,   406,   406,   406,   406,
     407,   407,   408,   409,   409,   409,   410,   411,   226,   412,
     412,   412,   412,   226,   244,   244,   225,   413,   414,   414,
     415,   415,   416,   416,   205,   237,   237,   237,   237,   315,
     315
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     0,     1,     2,     1,     1,     1,     1,
       1,     7,     1,     1,     1,     2,     1,     1,     1,     1,
       2,     1,     1,     3,     1,     3,     1,     2,     1,     1,
       0,     6,     5,     1,     3,     1,     1,     4,     6,     8,
      10,    12,     0,     1,     1,     0,     1,     3,     4,     7,
       1,     1,     1,     3,     2,     2,     3,     3,     2,     2,
       1,     1,     1,     2,     3,     2,     2,     1,     1,     3,
       2,     2,     1,     1,     3,     2,     2,     1,     1,     2,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     2,
       2,     1,     1,     1,     1,     1,     1,     4,     4,     4,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       3,     3,     1,     3,     1,     3,     2,     3,     2,     3,
       0,     1,     1,     1,     2,     4,     2,     3,     1,     2,
       1,     2,     1,     1,     2,     3,     1,     2,     1,     1,
       3,     2,     1,     2,     4,     6,     6,     3,     2,     1,
       3,     0,     1,     1,     0,     1,     1,     0,     1,     1,
       1,     2,     1,     1,     1,     1,     1,     2,     4,     6,
       6,     3,     1,     3,     1,     0,     1,     4,     5,     3,
       1,     1,     3,     1,     2,     2,     3,     2,     1,     2,
       1,     3,     1,     2,     3,     5,     5,     4,     6,     6,
       1,     1,     3,     1,     3,     1,     3,     3,     2,     3,
       2,     1,     1,     1,     3,     2,     4,     3,     5,     2,
       1,     2,     3,     2,     2,     3,     2,     2,     1,     1,
       2,     1,     4,     3,     1,     2,     2,     3,     1,     1,
       1,     2,     3,     2,     1,     2,     1,     3,     1,     3,
       2,     3,     2,     4,     2,     3,     3,     4,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     3,     4,
       3,     6,     2,     1,     1,     3,     2,     1,     2,     5,
       7,     5,     1,     5,     7,     8,     1,     0,     1,     0,
       1,     1,     2,     2,     2,     3,     3,     4,     1,     3,
       1,     1,     3,     6,     6,     8,     6,     6,     1,     3,
       3,     3,     2,     1,     1,     1,     1,     4,     3,     4,
       3,     3,     2,     2,     4,     3,     4,     2,     1,     3,
       1,     2,     2,     2,     2,     4,     2,     2,     4,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     1,     4,
       1,     3,     3,     3,     1,     3,     3,     1,     3,     3,
       1,     3,     3,     3,     3,     1,     3,     3,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     1,     5,
       4,     1,     3,     1,     3,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     1,     1,
       2,     2,     1,     2,     2,     1,     2,     2,     2,     2,
       1,     3,     4,     4,     3,     1,     4,     4,     1,     1,
       1,     1,     1,     6,     4,     6,     8,     6,     8,     1,
       4,     2,     2,     1,     3,    11,     1,     1,     1,     1,
       5,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     3,     3,     3,     3,     6,     1,     3,
       6,     0,     1,     1,     3,     2,     4,     3,     1,     1,
       1,     2,     1,     1,     6,     2,     1,     1,     2,     2,
       5,     7,     1,     1,     1,     1,     3,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     3,     4,
       1,     0,     1,     6,     4,     2,     1,     1,     1,     3,
       3,     3,     3,     1,     1,     3,     5,     3,     5,     3,
       2,     3,     2,     4,     1,     4,     0,     1,     1,     2,
       2,     3,     5,     5,     2,     4,     2,     4,     2,     4,
       1,     1,     1,     2,     1,     1,     1,     2,     2,     3,
       3,     1,     3,     3,     3,     2,     1,     0,    10,     1,
       0,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       4,     5,     3,     7,     5,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     1,     1,     4,     0,     1,
       1,     3,     1,     4,     1,     1,     1,     1,     1,     4,
       6
};

/* YYDPREC[RULE-NUM] -- Dynamic precedence of rule #RULE-NUM (0 if none).  */
static const unsigned char yydprec[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     2,
       1,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     2,     1,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0
};

/* YYMERGER[RULE-NUM] -- Index of merging function for rule #RULE-NUM.  */
static const unsigned char yymerger[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     1,     1,     1,     0,     0,     0,     0,
       0,     0,     0,     0,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     1,     1,
       1,     1,     1,     0,     0,     0,     0,     1,     1,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     1,     1,
       0,     1,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     1,     1,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     1,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     1,     1,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     1,
       1,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       1,     1,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     1,     1,     0,     1,     1,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     1,
       1,     1,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     1,     1,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0
};

/* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
   doesn't specify something else to do.  Zero means the default is an
   error.  */
static const unsigned short int yydefact[] =
{
       3,    42,    92,   122,   121,   182,   125,   196,    95,   124,
     428,    97,   123,   127,   128,    88,    93,    89,    86,    94,
     258,    84,   259,    87,   126,   183,    21,    22,   152,   153,
     185,    98,     0,    30,   150,    52,     0,     0,    90,    91,
       0,   184,     0,    96,     0,     0,   432,   435,     0,     0,
       0,     0,     0,     0,   129,   467,   468,   473,     0,     0,
       0,     0,     0,     0,     0,     0,   574,   572,   575,   595,
     596,   597,   598,   599,   609,   610,   611,   612,   613,     0,
     625,   626,   627,   628,     0,     2,     4,     6,    10,     9,
      19,    81,    18,     8,    51,     0,     0,    68,    78,    80,
      82,    83,    67,    77,    99,   100,   103,   104,   113,   102,
     107,     0,   132,   140,    30,   248,   156,   162,     0,    85,
     148,   186,     0,    30,   106,     7,     0,   105,     0,   254,
     192,   334,   120,   429,   450,   452,   466,   474,   479,   478,
     506,   570,   571,   593,   608,   616,    43,     0,     0,     0,
       0,    28,    29,     0,    26,     0,     0,     0,   169,   335,
     624,   185,   151,   180,    20,   246,     0,    24,     0,     0,
       0,   423,   424,   417,   422,   421,   419,   420,     0,   418,
       0,     0,   426,     0,   366,   363,     0,   362,   364,   365,
       0,   367,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   336,   321,
     320,   350,   368,     0,   370,   374,   377,   380,   385,   388,
     390,   392,   394,   396,   398,   401,   403,   430,   318,   425,
     449,     0,   463,   594,   438,   348,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    30,   362,   293,
       0,     0,     0,     0,   472,     0,   587,     0,     0,   587,
     587,   294,    14,    16,    13,    12,    17,   297,   279,   281,
     282,   280,   283,   431,   287,   284,   285,   286,   320,     0,
     120,   451,   469,   471,   477,   476,   581,   434,   264,     0,
       0,     0,     0,   433,   260,   120,   475,   481,   480,   507,
     112,   111,    99,   203,   115,   116,   114,   436,   440,   437,
     439,     0,     0,     0,   505,   488,   491,   491,   482,     0,
     523,   524,   520,   519,   517,   518,   522,   521,   514,   508,
       0,   513,   512,   532,   509,     0,   538,   535,   536,   537,
     554,   556,   550,   556,   556,     0,   573,   576,   618,     1,
       5,     0,   247,    54,    79,    65,    75,    66,    76,    55,
      30,     0,   143,   138,   141,   142,   615,   149,   157,     0,
     177,   163,   168,   177,   187,     0,   195,   249,     0,    61,
      62,    60,     0,   244,   251,     0,   256,     0,   255,     0,
       0,   398,   416,     0,    27,    30,   154,   146,   191,   167,
     170,   181,     0,    23,     0,     0,     0,   361,     0,   352,
     351,     0,   354,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   357,   356,   359,   360,     0,     0,     0,
       0,   566,     0,   568,     0,   564,     0,     0,   343,   342,
       0,     0,     0,   307,   606,     0,     0,   353,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   408,   412,   407,   410,   415,
     406,   413,     0,   411,   409,   414,   405,     0,     0,     0,
     427,     0,   459,   461,   462,     0,   312,     0,   313,     0,
       0,     0,     0,     0,     0,     0,   314,     0,     0,     0,
       0,    81,   616,   296,     0,    15,    17,     0,     0,     0,
     586,     0,   585,     0,     0,     0,   298,   292,     0,   265,
       0,    30,     0,    30,    30,   186,     0,   266,   335,   263,
     261,   109,   110,    30,   177,    30,   212,   204,   210,     0,
     448,     0,     0,     0,     0,   498,     0,   492,   493,     0,
       0,   483,     0,   531,     0,     0,     0,     0,   549,   557,
     558,     0,   552,   551,   362,   578,     0,   577,   622,     0,
     619,   620,    53,    64,    74,     0,   133,   134,   140,     0,
       0,   139,   144,   614,   161,     0,    30,    68,    67,     0,
     159,   220,   158,   223,   225,   120,   176,   362,   174,   171,
       0,   173,   179,   174,   171,     0,     0,   200,   201,   193,
     131,     0,    63,    58,    59,   250,   245,   253,     0,   257,
     130,     0,   119,     0,   147,   155,    25,   118,   117,    36,
       0,     0,    33,    35,     0,     0,     0,     0,   322,   319,
     464,     0,     0,     0,     0,     0,     0,   445,     0,     0,
       0,     0,     0,     0,     0,   341,   338,     0,   340,     0,
     306,     0,     0,     0,   373,   371,   372,   375,   376,   378,
     379,   383,   384,   381,   382,   386,   387,   389,   391,   393,
     395,   397,     0,     0,   402,   404,     0,   349,     0,     0,
     290,     0,    30,   311,     0,   310,   309,     0,     0,   316,
       0,   302,   315,     0,     0,   295,     0,     0,   485,   584,
     587,   582,   583,   288,   484,   272,   270,   262,    30,     0,
     108,     0,     0,   174,   171,     0,   209,   177,   213,   441,
     446,   447,     0,   503,     0,   499,   500,   502,     0,   486,
       0,   495,     0,   489,   525,     0,   515,   530,     0,     0,
       0,   539,     0,   560,     0,   559,   556,   580,   579,     0,
     617,     0,    30,    30,   136,     0,     0,   230,   232,   231,
      30,    30,    30,    30,   228,    65,    66,   160,     0,     0,
     177,   175,   177,   172,   164,   177,   177,   188,     0,   197,
       0,    56,    57,   252,     0,    45,     0,    32,     0,     0,
       0,     0,   355,     0,   344,   369,     0,     0,     0,     0,
     358,   442,     0,     0,   454,   567,   569,   565,   629,     0,
     339,   307,     0,   337,   600,     0,     0,   607,   602,   400,
       0,     0,     0,   289,     0,   308,     0,     0,   317,     0,
       0,     0,   443,     0,     0,     0,   271,     0,    30,   267,
     268,    30,     0,    30,   207,   211,   177,   177,   214,   174,
     171,     0,     0,   491,   501,   497,     0,   494,   491,     0,
       0,   534,   531,     0,   540,   541,   555,     0,   561,     0,
     553,     0,   621,   135,   137,   145,     0,     0,   347,   233,
       0,     0,     0,   240,   335,     0,   227,   229,    64,   222,
     224,   226,     0,     0,     0,     0,   198,   199,   202,    38,
       0,     0,    44,    46,    50,     0,    31,    34,     0,     0,
       0,     0,   328,     0,     0,   333,     0,     0,   444,     0,
       0,     0,   306,     0,   601,   605,   399,   460,     0,     0,
     307,     0,   299,   301,   303,   470,   587,   278,     0,   274,
      30,     0,   273,   206,     0,     0,   177,   177,   217,   504,
       0,   496,     0,   516,   526,   510,   530,     0,     0,     0,
       0,   623,   243,   401,     0,     0,   345,   235,   239,     0,
     241,     0,    30,   165,   166,   189,   190,     0,     0,    45,
       0,     0,    11,    37,     0,     0,   327,     0,   323,   324,
     332,     0,   326,   453,   630,     0,   455,     0,   457,     0,
     291,     0,     0,   309,     0,     0,   276,   275,   269,     0,
     215,   216,     0,     0,   487,   490,     0,   533,     0,   542,
     544,   543,   563,   562,   242,   346,   234,     0,   335,   237,
       0,    39,     0,    47,     0,   331,   330,   329,     0,     0,
       0,   604,   304,     0,     0,   300,   590,   277,   218,   219,
     320,     0,   511,     0,     0,   236,     0,     0,     0,    45,
      48,   325,   456,   458,     0,   305,   307,   592,   591,     0,
     589,   527,     0,   545,   547,   238,     0,    40,     0,   603,
       0,     0,   338,     0,     0,     0,     0,     0,     0,   588,
     339,   546,   548,    49,    41,   465
};

/* YYPDEFGOTO[NTERM-NUM].  */
static const short int yydefgoto[] =
{
      -1,    84,    85,    86,    87,    88,   261,   262,   263,   264,
      90,   166,   151,   152,   575,    91,   631,   632,    92,   147,
     911,   912,   913,    93,   265,   377,   378,   379,    95,   586,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   303,
     105,   106,   107,   108,   109,   110,   111,   576,   577,   112,
     363,   364,   365,   155,   113,   114,   156,   115,   589,   536,
     116,   117,   118,   782,   783,   780,   781,   600,   601,   119,
     120,   121,   122,   609,   123,   124,   606,   607,   414,   722,
     537,   538,   590,   591,   592,   593,   594,   581,   767,   890,
     891,   892,   893,   266,   126,   382,   383,   127,   128,   129,
     293,   294,   526,   849,   527,   850,   846,   267,   268,   269,
     270,   271,   272,   273,   274,   275,   835,   276,   659,   836,
     696,   277,   208,   209,   921,   922,   924,   210,   131,   211,
     804,   234,   212,   213,   214,   215,   216,   217,   218,   219,
     220,   221,   222,   223,   224,   225,   226,   279,   478,   974,
     228,   229,   159,   133,   309,   230,   281,   646,   134,   135,
     647,   483,   231,   232,   136,   282,   283,   137,   284,   139,
     298,   285,   318,   319,   546,   547,   548,   734,   735,   736,
     737,   140,   329,   330,   331,   745,   332,   746,   965,  1062,
     748,   334,   337,   338,   339,   555,   874,  1029,  1030,  1031,
     342,   341,   558,   559,   560,   141,   142,   346,   347,   286,
     511,  1079,  1080,   143,   233,   445,   663,   446,   828,   144,
     145,   569,   570,   571
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -919
static const short int yypact[] =
{
    6809,    67,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
      83,  -919,   139,    60,   478,  -919,  7179,    97,  -919,  -919,
     174,  -919,   215,  -919,  5767,  5767,  2894,  6994,  7731,  8547,
    8547,    61,   240,   249,  -919,  -919,  -919,  -919,   212,   421,
     899,    61,  4632,    61,    61,  5767,  -919,   252,  -919,  -919,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,   280,
    -919,  -919,  -919,  -919,   418,  6994,  -919,  -919,  -919,  -919,
    -919,  -919,  -919,  -919,  -919,   411,   336,  8731,  -919,  -919,
    -919,  -919, 10527, 10527, 10439,  -919,  9384,  9549,  -919,  9714,
    -919,   342,   351,    94,   -44,  -919,   138,  -919,   363,  -919,
     218,   369,   374,   -44,  -919,  -919,  8915,  -919,   395,    62,
    -919,  -919,  9191,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,   414,  8547,  5767,
      54,  -919,   -44,   411,  -919,   416,   430,   432,  -919,  -919,
    -919,  -919,   478,  -919,  -919,  -919,   310,  -919,  4217,   438,
      97,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  6106,  -919,
    6106,  6219,  -919,   447,  -919,  -919,  3461,  -919,  -919,  -919,
    5767,  -919,   454,   456,   465,   477,  6332,  5767,  5767,  5767,
     479,   499,   507,  6445,  6558,  6671,   516,   523,  -919,  -919,
    -919,   643,  -919,  5767,  -919,   308,    35,   107,   264,    79,
     490,   522,   476,   619,  1795,  -919,  -919,   540,  -919,   574,
    -919,  5880,  -919,  -919,   547,  -919,   560,  5767,   568,   571,
    2894,   253,    76,   583,  4745,   584,   590,  3650,  1289,  -919,
    2516,  3083,   607,   620,  -919,   421,  5767,   612,   622,  5767,
    5767,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
    -919,  -919,  -919,  2894,  -919,  -919,  -919,  -919,   637,   324,
    9292,  -919,  -919,  -919,  -919,  -919,  -919,  6994,  -919,  7731,
     421,   645,   623,  7731,  -919,  -919,  -919,  -919,  -919,  -919,
    8547, 10527,  -919,   330,  -919,  -919,  -919,  -919,  -919,   595,
    -919,   655,   655,   639,  -919,  -919,    24,    26,  -919,  6994,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
     636,  -919,  -919,  -919,   640,  5767,  -919,  -919,  -919,  -919,
    -919,   134,   646,   134,   128,  4858,   649,  -919,    97,  -919,
    -919,   658,  -919,  -919,  -919, 10527, 10527, 10527, 10527,  -919,
     -44,   676,   -44,   669,  -919,   -44,   -44,  -919,  -919,  8003,
    2108,  -919,  -919,  2108,  -919,    97,    97,  -919,   580,  9003,
    -919, 10527,  8915,  -919,  -919,  7363,    63,    97,  -919,   707,
     684,    96,  -919,   683,  -919,   -44,   138,   218,  -919,  -919,
    -919,  -919,    97,  -919,   688,   436,    78,  -919,  3461,  -919,
    -919,  3461,  -919,  5767,   689,   692,   457,   208,  5767,  8547,
    5767,  8547,  3461,  -919,  -919,  -919,  -919,   655,  5767,  5767,
    3461,  -919,  3461,  -919,  3461,  -919,  5767,  5767,  -919,  -919,
      61,  4971,    61,  5767,  -919,   696,  5767,  -919,  5767,  5767,
    5767,  5767,  5767,  5767,  5767,  5767,  5767,  5767,  5767,  5767,
    5767,  5767,  5767,  5767,  5767,  -919,  -919,  -919,  -919,  -919,
    -919,  -919,  5767,  -919,  -919,  -919,  -919,  5084,  5767,  5767,
    -919,  3461,  -919,  -919,  -919,  5767,  -919,    68,  -919,  2894,
     712,  3272,   654,  5767,   697,  5767,  -919,   338,  5767,  5767,
    3839,   851,   957,  -919,  2705,  -919,  2326,   655,  5767,  2894,
     540,   698,  -919,  3272,   699,   701,  -919,  -919,  2894,  -919,
    7731,   -44,  5767,   -44,   -44,  -919,   702,   708,    -3,  -919,
    -919, 10527, 10527,  8003,  2108,   112,  -919,  -919,   259,  8547,
    -919,   700,   704,    97,   648,   713,   653,    -4,  -919,   648,
     657,  -919,  8547,  5767,   716,    91,  5767,  5197,  -919,   710,
    -919,    61,  -919,  -919,   706,  -919,   714,  -919,   730,   731,
     737,  -919,  -919, 10527, 10527,   580,   739,  -919,    94,   707,
    5310,  -919,   -44,   -44,  -919,   954,   -44,  7915,  9859,   734,
    -919,   740,  -919,   741,  -919,   468,  -919,   733,   771,   478,
     738,   478,  -919,   771,   478,   743,   122,  -919,   749,  -919,
     732,   753,  -919, 10527, 10527,  -919,  -919,  -919,  7547,  -919,
    -919,   114,  -919,   707,  -919,  -919,  -919,  -919,  -919,  -919,
     763,   472,  -919,   766,   769,   770,   773,  5310,  -919,  -919,
    -919,   775,   779,   783,   785,   789,   489,  -919,   787,   792,
     794,   795,   798,   494,   799,  -919,  -919,   508,  -919,   759,
     357,  5423,   801,   711,  -919,  -919,  -919,   308,   308,    35,
      35,   107,   107,   107,   107,   264,   264,    79,   490,   522,
     476,   619,  5767,   176,  -919,  -919,   807,  -919,  5767,  2894,
    -919,   809,  4028,  -919,   580,  -919,  5767,   796,   379,  -919,
     812,   540,  -919,   814,   824,  -919,   521,   529,  -919,  -919,
    5767,  -919,  -919,  -919,  -919,  -919,   821,  -919,    95,   825,
   10527,   330,   838,   771,   478,   829,  -919,  2108,  -919,  -919,
    -919,  -919,   839,  -919,   844,   648,  -919,  -919,    46,  -919,
     782,  -919,   845,  -919,  -919,   285,  -919,   275,   842,    97,
      97,  -919,   837,  -919,    49,  -919,   134,  -919,  -919,  5767,
    -919,    97,   -44,   -44,   669,    80,  4406,  -919,  -919,  -919,
    8187,   -44,     3,   -44,  -919, 10004, 10149,  -919,    97,  8275,
    5993,  -919,  5993,  -919,  -919,  5993,  5993,  -919,    70,  -919,
    5767,  -919, 10527,  -919,   846,    41,   141,  -919,   857,   181,
    5767,   843,   852,  8459,  -919,  -919,  8547,    97,  5767,  8547,
     853,  -919,   655,  5767,  -919,   854,   858,   860,  -919,  5767,
    -919,  5767,  5767,  -919,  -919,   531,  5767,  -919,  -919,  -919,
    5767,  5310,   855,  -919,  5767,  -919,   850,   861,  -919,  2894,
    2894,  2894,  -919,   859,   870,  5767,  -919,  1011,   -44,   877,
    -919,   -44,  5767,   123,   259,  -919,  5993,  5993,  -919,   771,
     478,   847,   830,   782,  -919,  -919,   882,  -919,   782,  8547,
    5767,  -919,  5767,   873,  -919,  -919,  -919,  5767,  -919,  5767,
    -919,   542,  -919,  -919,  -919,  -919,    97,  5767,  -919,  -919,
     140,  5310,   343,  -919,   884,   954,  -919,  -919, 10294,  -919,
    -919,  -919,   885,   894,   897,   903,  -919,  -919,  -919,  -919,
      97,    74,   895,  -919,    59,   905,  -919,  -919,   557,   912,
     913,   559,  -919,   921,   922,    66,   920,   924,  -919,   925,
     927,   198,   540,   273,  -919,   928,  -919,  -919,  2894,   591,
    5767,  3272,   992,  -919,  -919,  -919,  5767,  -919,  5767,   821,
     102,   929,  -919,  -919,   918,   923,  5993,  5993,  -919,  -919,
     880,  -919,   881,  -919,   540,   935,   540,   931,  5767,   183,
     246,  -919,  -919,   937,   940,  4519,  -919,  -919,  -919,  5767,
    -919,  5310,    32,  -919,  -919,  -919,  -919,   947,   941,    41,
      41,  5767,  -919,  -919,  5767,  5767,  -919,  8459,  -919,  -919,
      66,  5767,  -919,  -919,  -919,  5767,  -919,  5767,  -919,  5767,
    -919,   958,   965,  5767,  2894,   963,  -919,  -919,  -919,  5767,
    -919,  -919,   959,   962,  -919,  -919,  1778,  -919,   390,  -919,
    -919,  -919,  -919,  -919,  -919,  -919,  -919,  5310,   974,  -919,
     707,  -919,   377,  -919,   599,  -919,  -919,  -919,   982,   375,
     378,   980,  -919,  2894,   977,  -919,  5541,  -919,  -919,  -919,
     669,   703,  -919,  5767,  5767,  -919,  5310,   262,   981,    41,
    -919,  -919,  -919,  -919,  5767,  -919,  5767,  -919,   540,   988,
    -919,  -919,  5654,   984,   986,  -919,  5767,  -919,   994,  -919,
     995,  2894,  1081,   651,  5767,  5767,   671,   989,  2894,  -919,
    1088,  -919,  -919,  -919,  -919,  -919
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
    -919,  -919,  1042,    -8,    -7,  -919,  -919,   841,  -919,    69,
    -919,  -919,     6,   -62,   329,   536,  -919,   291,  -919,  -919,
    -918,  -919,   103,  -919,    21,   722,  -919,  -919,    38,  -919,
     -33,  1013,  1057,   -47,  -919,  -919,  1156,  1009,  1202,   -49,
    -919,  1269,  1285,  -919,  1611,  -919,   -83,  -919,   344,  -919,
     530,  -919,  -919,   -82,  -256,    23,   -53,  1016,  -919,  -114,
     960,  -919,   -16,  -542,  -332,  -531,  -317,  -331,     5,   -29,
    -150,   124,  -919,  -919,  -919,  -919,  -919,   332,   -41,  -517,
     527,  -657,  -919,  -919,  -919,  -919,   345,  -737,  -736,  -919,
     146,   200,  -829,   149,  -919,  -919,   744,  -919,  -919,  -919,
     742,  -235,  -919,  -919,  -919,   178,   180,   667,  -919,  -919,
    -468,  -919,   404,   883,  -919,  -919,   -19,  -919,  -731,   117,
    -493,  -919,  -919,  -919,  -919,   135,  -919,    19,  -919,   105,
    -482,  -434,  1014,  -919,  -173,    55,    17,   245,   142,   673,
     674,   672,   675,   665,    75,   190,   192,   413,  -919,  -138,
    -919,  -358,     0,  -919,  -919,  -919,  -919,   633,   650,  -919,
    -268,  -919,  -919,   914,  -919,  -919,  -919,   -32,    28,  -919,
    -919,  -919,  -919,  -137,  -304,  -919,  -488,  -476,  -919,   409,
    -919,   -26,  -919,  -919,  -919,  -919,  -919,   277,  -919,  -919,
     279,  -919,  -919,   820,  -919,  -919,   397,  -919,  -919,  -919,
    -919,   -50,  -310,  -919,   600,  -919,  -919,  -919,   817,  -919,
    -250,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,  -919,
     629,  -919,  -919,   405
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -617
static const short int yytable[] =
{
     132,   308,   371,   397,   372,   163,   374,   657,   307,   514,
     515,   393,   351,   550,   343,   292,   296,   157,   726,   130,
     710,    94,   299,   695,   424,   425,   426,   884,   138,   164,
     889,   621,   367,   562,   563,   523,   132,   167,   598,   162,
     447,   603,   605,   541,   542,   695,   280,   132,   295,   295,
     295,   362,   130,   599,   519,   130,   604,    94,   530,   741,
     328,   368,   786,   980,   854,   278,   130,   386,    94,    89,
     310,  1042,   785,   742,    42,   138,   297,   350,   424,   380,
     333,   366,   130,   130,   740,   132,   291,    10,  -208,   -30,
     931,  -208,   629,    10,    10,    10,    10,   295,   768,   487,
     361,   182,   459,    10,   130,    89,    94,   390,   544,    10,
     549,    10,  -208,   138,   130,    42,    89,  -205,   509,   480,
    -205,    42,   451,  -205,   452,   460,   295,   404,    10,   388,
      10,   865,   688,   401,   157,    10,   146,   479,   150,   910,
     480,   877,    79,   991,   150,   472,   453,   545,   295,   125,
      42,  1088,   878,   520,    89,   977,   886,   158,   394,   988,
     689,   454,   493,   630,   979,   885,   989,   148,   132,   130,
     407,   980,   130,   906,   480,   545,   395,   545,    42,   750,
      42,    42,   857,    79,    52,   165,   132,   340,   340,    79,
      52,    52,   856,   477,   751,   629,   125,  -208,   733,   794,
    -208,   480,   723,   725,  -208,   879,   795,   566,  -205,  1012,
     788,  -205,    42,    42,    10,  -205,   479,   724,    79,   163,
      42,   765,   369,   149,   391,   789,   915,   825,   975,   557,
      42,   157,   556,    57,   125,   557,   370,   235,   854,  1036,
     280,    42,   494,   976,   164,  1039,    79,   132,    79,    79,
     280,   132,   867,   162,   336,   726,   292,   296,   168,   278,
     292,   296,   866,   299,   479,   796,   278,   299,   830,   278,
     278,   479,    94,   280,   158,   664,   665,   666,   578,   350,
      79,    79,   371,   374,   768,   714,  1032,   132,    79,   295,
    1005,   528,   278,   295,   455,   611,   479,  1006,    79,   169,
     295,  1065,   369,   582,   583,   456,   130,   640,    94,    79,
     130,   551,   391,   624,   524,   138,   373,   297,   957,   132,
      89,   297,   480,  1081,   311,   881,   535,   291,   956,   771,
    1085,   291,   612,   312,   479,   380,   953,   491,   130,   392,
      94,   163,   625,   369,   163,  1090,  1086,   138,   568,  1033,
     345,   492,   292,   296,   313,   314,    89,   727,   457,   299,
     458,   479,   153,   479,   348,  1007,   918,   634,   394,   595,
     635,   158,  1008,   869,   871,   608,   610,   870,   642,   295,
     644,   645,   295,   530,   715,   295,   619,   620,    89,   650,
     448,   651,   394,   652,   449,   859,   861,   130,   402,   450,
     506,   524,   626,   403,    28,    29,   633,   585,   132,   768,
     860,   132,   479,   297,   533,   525,    34,   517,   349,   295,
     391,   295,   132,   291,   728,   397,   479,   392,   534,   353,
     132,   702,   132,   886,   132,   359,   125,   914,   978,   360,
     686,   979,   367,   153,    10,   479,   880,   369,  1013,   902,
     822,   903,   376,  -169,   904,   905,   823,   227,   387,   655,
     844,   658,  1068,   479,   805,   953,   479,   479,   125,  1069,
     669,   670,   838,   695,  1072,   375,   700,  1073,   344,   703,
     704,   132,  1063,  1064,   157,    28,    29,   292,   296,   280,
     729,   132,     5,   768,   299,    33,   385,    34,   389,   768,
     132,   398,   525,   744,   280,   163,   667,   668,   278,   280,
     278,   756,   693,   132,   524,   399,   362,   400,   280,   278,
     295,   628,   406,   278,   479,   954,   955,   336,   278,   694,
     384,   413,   278,   595,   693,   392,   524,   278,   418,   295,
     419,    52,   639,   732,   928,   479,   366,    25,   297,   420,
     832,   694,   295,  -120,   161,   768,  -120,   798,   291,   960,
     799,   421,   602,   427,   962,   602,   315,   316,   317,   154,
     163,   585,   401,   461,   811,   163,   153,   812,   463,   818,
     130,   405,   479,   428,   768,   292,   296,   295,   394,   394,
     415,   429,   299,   820,   130,    41,   485,   391,   762,   416,
     436,   675,   676,   417,   130,   636,   842,   437,   772,   812,
     641,   351,   643,    10,   843,   525,   934,   479,   295,   485,
     648,   649,   462,   464,   158,  1022,  1023,   971,   479,   654,
     485,   914,   914,   235,   480,   485,   848,   525,   662,    66,
      67,    68,   993,  -192,   996,   485,   297,   997,  1093,   154,
     154,   415,   908,   486,    28,    29,   291,   497,   937,   154,
     416,   488,   160,   489,   521,   154,    34,   495,   498,   510,
     684,   685,   510,   510,   499,   929,  1011,   687,    10,   479,
     624,   578,  1067,   539,  1070,   340,   438,   479,   154,   280,
     367,   507,   132,   439,   440,   163,  1015,   397,   163,   525,
     671,   672,   673,   674,   508,   512,   513,   947,   278,   525,
      52,   914,   392,   130,   952,   540,   529,   524,   851,    28,
      29,  -192,  -192,   543,   153,  -192,   602,   441,   552,   521,
    -192,    34,  -192,   442,   561,  -192,  1100,   522,   553,   485,
     728,   443,   160,   160,   853,   397,   438,   345,   752,   873,
     873,   572,   160,   439,   440,  -192,  1103,   391,   160,   479,
     579,   568,   920,   391,   580,   923,   894,   182,   927,   622,
     595,   623,   769,   627,   637,    52,  -192,   638,   899,   295,
     661,   160,   691,   501,    52,   730,   384,  1082,   608,   731,
     699,   709,   711,   442,   712,   717,   718,   738,   739,   633,
     733,   443,   743,   295,   749,   757,   295,   925,   585,   295,
    1016,   557,   415,   758,   759,   415,   760,   585,   525,   777,
     744,   416,   444,  -192,   416,   761,   415,   763,   778,   779,
     596,   163,  -178,  -194,   415,   416,   415,   784,   415,   280,
     280,   280,   787,   416,   790,   416,   791,   416,   797,   653,
     800,   821,   716,   235,   801,   802,   660,   719,   278,   278,
     278,   803,   721,   806,   721,   391,   130,   807,   848,   295,
     762,   808,   829,   809,   810,   813,   502,   814,   392,   815,
     816,  1057,   444,   817,   -26,   415,   972,   819,   391,   826,
     683,   827,   831,   834,   416,   837,   154,   839,   154,   840,
     624,   154,   154,   320,   415,   391,   698,   490,   701,   841,
     987,   701,   701,   416,   130,   774,   845,   852,   982,   602,
     391,   707,   154,   855,   862,   -26,   -26,   391,   858,   863,
     868,   154,    10,   545,   872,   -26,   876,   -26,   280,   909,
     516,   132,   916,   940,   766,   941,   958,   938,   321,   -26,
     851,   235,   945,   766,   766,   766,   920,   278,   769,   766,
     278,   766,   693,   946,   524,   950,   747,   961,   968,   -26,
     754,   525,   602,   959,   602,  1038,   981,   602,   602,   694,
     392,   -26,   322,   990,   983,   323,   324,    10,   325,   160,
    -616,   160,   235,   984,   160,   160,   985,   295,   992,   326,
     926,   327,   986,   392,   994,   995,   998,   999,  1001,  1002,
    1003,   930,  1004,  1014,   280,   160,  1009,  1020,   935,   525,
     936,  1019,  1021,   391,   160,  1024,  1025,  1026,    28,    29,
    1027,  -616,  -616,   278,  1041,   392,  -401,   -26,   770,  1034,
      34,  -616,   392,  -616,    10,  1060,  1040,   847,   602,   602,
    1053,  1052,   534,   280,   391,  -616,  1056,   154,  1058,   154,
     154,  1059,   300,   300,   154,   525,  1066,  1071,  1074,   501,
    1076,   154,   278,  1091,  1087,  -616,  1094,   973,  1095,  1097,
    1098,  -528,  1104,   769,    52,    28,    29,  -616,  -529,   287,
     917,   280,   505,  1043,   391,   521,   415,    34,   280,   895,
     896,   895,   897,   948,   615,   416,   356,   883,   764,   701,
     278,   352,   773,   396,   154,   357,   358,   278,   154,   154,
     907,  1037,   154,   510,   901,  1000,   616,   618,  1018,  1017,
    1054,  1061,  1047,   504,   677,   679,   678,   681,   392,   680,
     706,    52,   697,  -616,   864,   484,   963,   875,   602,   602,
     160,   967,   160,   160,   354,   554,   690,   160,     0,   755,
    1028,   300,   502,   567,   160,     0,   882,   769,     0,   392,
       0,   516,     0,   769,     0,     0,   708,   949,     0,     0,
     951,   300,   721,     0,     0,   713,  1045,  1046,     0,     0,
       0,     0,   409,  1048,   410,   412,     0,     0,     0,   300,
       0,  1051,     0,     0,     0,   301,   301,   160,     0,   392,
     423,   160,   160,     0,     0,   160,     0,   431,   433,   435,
       0,     0,     0,     0,     0,     0,     0,     0,   501,   769,
       0,     0,     0,     0,   932,   933,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   482,     0,   939,     0,     0,
       0,   302,   302,   355,   154,  1083,  1084,     0,   769,     0,
     300,     0,     0,     0,     0,     0,  1089,     0,     0,     0,
       0,     0,     0,     0,   235,     0,     0,     0,     0,   847,
       0,     0,   381,   964,     0,   966,  1101,  1102,     0,     0,
     969,     0,   970,     0,     0,     0,     0,     0,   154,   154,
     417,     0,     0,     5,   301,     0,   501,   154,   154,   154,
       0,   895,     0,     0,   532,     0,     0,     0,   304,   304,
       0,   502,  -150,     0,   301,     0,     0,     0,   302,     0,
       0,     0,     0,     0,   305,   305,     0,     0,     0,     0,
       0,     0,   301,     0,     0,     0,     0,   160,     0,     0,
     302,     0,     0,   932,     0,     0,   833,   354,    25,   510,
       0,     0,     0,  -150,  -150,   161,     0,     0,   573,   574,
     302,     0,     0,  -150,     0,  -150,     0,     0,     0,     0,
       0,     0,   587,     0,   154,     0,     0,   154,   302,   154,
       0,   160,   160,     0,   614,   304,     0,     0,     0,   502,
     160,   160,   160,   301,  1044,     0,    41,  -150,     0,     0,
       0,   305,     0,     0,   354,   354,     0,   304,  1049,  -150,
    1050,   300,     0,     0,   300,     0,   701,     0,     0,     0,
       0,     0,   300,   305,   300,   300,     0,   304,     0,     0,
       0,     0,     0,   300,     0,   300,     0,   300,     0,   302,
      66,    67,    68,   305,     0,   304,   531,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  1078,
       0,   305,     0,     0,     0,  -150,     0,   160,     0,     0,
     160,     0,   160,     0,     0,     0,   154,     0,     0,   932,
       0,     0,     0,     0,   300,     0,     0,     0,     0,  1096,
       0,     0,   302,     0,     0,     0,   942,   943,   944,     0,
       0,     0,     0,   300,     0,     0,   304,     0,   154,     0,
       0,     0,     0,     0,     0,   588,     0,     0,     0,     0,
       0,     0,   305,     0,     0,   613,     0,     0,   381,     0,
       0,     0,     0,     0,   720,     0,   587,     0,     0,     0,
       0,     0,   300,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   301,   300,     0,   301,     0,   304,
       0,   302,     0,     0,     0,   301,     0,   301,   301,   160,
       0,   302,     0,     0,   302,   305,   301,     0,   301,   354,
     301,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   776,     0,     0,     0,  1010,     0,     0,     0,     0,
     302,   160,     0,   302,     0,     0,     0,     0,     0,     0,
       0,   302,     0,   302,   302,     0,   792,     0,     0,     0,
     354,   354,   302,     0,   302,     0,   302,   301,   304,     0,
       0,     0,     0,     0,   354,     0,     0,     0,   304,     0,
       0,   304,     0,     0,   305,     0,   301,     0,     0,     0,
     306,   306,     0,     0,   305,     0,     0,   305,     0,     0,
       0,   354,     0,     0,     0,     0,     0,   304,     0,     0,
     304,  1055,     0,   302,     0,     0,     0,     0,   304,   588,
     304,   304,     0,   305,     0,   301,   305,     0,     0,   304,
       0,   304,   302,   304,   305,   300,   305,   305,   301,     0,
       0,     0,     0,     0,     0,   305,     0,   305,     0,   305,
    1075,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   302,     0,   306,     0,     0,
       0,   302,     0,   775,     0,     0,     0,     0,     0,     0,
     304,     0,     0,     0,   302,     0,     0,     0,  1099,   306,
       0,     0,     0,     0,     0,  1105,   305,     0,     0,   304,
       0,     0,     0,     0,     0,     0,     0,   354,     0,   306,
       0,     0,     0,   587,     0,   305,     0,   171,   898,   302,
       0,   172,   587,     0,   173,     0,     0,   306,   465,     0,
     466,     0,   304,     0,   174,   175,     0,     0,   304,   176,
     177,    10,     0,   467,     0,     0,   300,     0,   305,   300,
       0,   304,   300,     0,   305,   179,     0,     0,     0,     0,
       0,     0,     0,   354,     0,   468,     0,   305,   182,   469,
     470,     0,     0,   471,   472,     0,     0,     0,   301,   354,
     473,     0,     0,     0,     0,   183,   304,   474,   306,     0,
       0,     0,   408,     0,     0,     0,   475,     0,     0,     0,
       0,     0,   305,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   300,   192,   193,   194,   195,     0,     0,     0,
     476,     0,   477,     0,   302,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   200,     0,    52,     0,
       0,   306,     0,   201,   202,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   588,     0,     0,     0,
       0,     0,     0,     0,     0,   588,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   354,     0,     0,     0,   301,
       0,   304,   301,     0,     0,   301,     0,     0,     0,   206,
     207,     0,   302,     0,     0,     0,     0,   305,     0,     0,
     306,   302,     0,     0,     0,     0,     0,     0,     0,     0,
     306,     0,     0,   306,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   302,     0,     0,   302,     0,
     300,   302,     0,     0,     0,     0,     0,     0,     0,   306,
       0,     0,   306,     0,     0,   301,     0,     0,     0,     0,
     306,     0,   306,   306,     0,     0,     0,     0,     0,   304,
       0,   306,     0,   306,     0,   306,     0,     0,   304,     0,
       0,     0,     0,     0,     0,   305,     0,     0,     0,     0,
       0,     0,     0,     0,   305,     0,     0,     0,     0,     0,
       0,   302,   304,     0,     0,   304,     0,     0,   304,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   305,     0,
       0,   305,   306,     0,   305,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   306,   170,     0,     0,     0,     0,   171,     0,     0,
       0,   172,     5,     0,   173,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   174,   175,     0,     0,   304,   176,
     177,    10,     0,     0,   306,     0,     0,     0,     0,     0,
     306,   178,     0,   301,   305,   179,     0,     0,   180,     0,
       0,     0,     0,   306,     0,     0,   181,   596,   182,     0,
       0,     0,     0,     0,     0,     0,     0,    25,     0,     0,
       0,     0,     0,     0,   161,   183,     0,     0,     0,   184,
       0,   185,   186,     0,   597,   188,     0,   189,   306,   302,
       0,     0,     0,     0,     0,     0,   190,     0,     0,     0,
       0,     0,   191,   192,   193,   194,   195,   196,   197,   198,
     199,     0,     0,     0,     0,    41,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   200,     0,    52,     0,
       0,     0,     0,   201,   202,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   304,     0,     0,    66,
      67,    68,     0,     0,   203,   204,     0,     0,   205,     0,
       0,     0,   305,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   206,
     207,     0,     0,   306,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   -17,     0,     0,     0,
     -17,     0,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,     0,   -17,   -17,   -17,   -17,     0,
     -17,   -17,   -17,   -17,   -17,   -17,     0,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,     0,     0,     0,   -17,   -17,
       0,     0,     0,   -17,     0,     0,   -17,     0,   -17,   -17,
       0,   306,   -17,   -17,   -17,   -17,   -17,   -17,     0,   -17,
     306,   -17,   -17,   -17,   -17,   -17,   -17,     0,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,     0,     0,   -17,     0,   -17,
     -17,     0,   -17,   -17,   306,   -17,     0,   306,     0,   -17,
     306,     0,     0,     0,   -17,     0,     0,   -17,     0,   -17,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,   -17,     0,     0,     0,     0,
       0,     0,     0,     0,   -17,   -17,   -17,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,     0,     0,     0,
     -17,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     306,     0,     0,     0,     0,     0,     0,   -17,   -17,   -17,
       0,   -17,   -17,   -17,   -17,   -17,   -17,     0,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,     0,     0,   -17,   -17,   -17,
     -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,   -17,     0,
     170,     0,     1,     2,     3,   171,   236,   237,     4,   172,
       5,   238,   173,   239,     0,   240,     6,     0,     7,     0,
       8,     9,   174,   175,   241,   242,     0,   176,   177,    10,
     243,    11,    12,    13,    14,     0,     0,     0,    15,   178,
       0,     0,     0,   179,     0,     0,   180,     0,    16,   244,
       0,     0,    17,    18,   181,    19,   182,    20,     0,   245,
       0,    21,    22,    23,    24,    25,   246,     0,    26,    27,
      28,    29,    30,   183,    31,     0,     0,   184,     0,   185,
     247,     0,   248,   188,     0,   189,     0,     0,   306,   249,
       0,     0,     0,     0,   190,     0,     0,   250,     0,   503,
     191,   192,   193,   194,   195,   196,   251,   198,   199,    37,
      38,    39,    40,    41,    42,    43,     0,     0,     0,     0,
       0,     0,     0,     0,   200,   252,    52,    53,    54,    55,
      56,   201,   202,   253,   254,    57,    58,     0,     0,     0,
     255,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    66,    67,    68,
       0,   256,   203,   204,   257,   258,   205,     0,   259,   260,
      69,    70,    71,    72,    73,     0,     0,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,   206,   207,   170,
       0,     1,     2,     3,   171,   236,   237,     4,   172,     5,
     238,   173,   239,     0,   240,     6,     0,     7,     0,     8,
       9,   174,   175,   241,   242,     0,   176,   177,    10,   243,
      11,    12,    13,    14,     0,     0,     0,    15,   178,     0,
       0,     0,   179,     0,     0,   180,     0,    16,   244,     0,
       0,    17,    18,   181,    19,   182,    20,     0,   245,     0,
      21,    22,    23,    24,    25,   246,     0,    26,    27,    28,
      29,    30,   183,    31,     0,     0,   184,     0,   185,   247,
       0,   248,   188,     0,   189,     0,     0,     0,   249,     0,
       0,     0,     0,   190,     0,     0,   250,     0,   705,   191,
     192,   193,   194,   195,   196,   251,   198,   199,    37,    38,
      39,    40,    41,    42,    43,     0,     0,     0,     0,     0,
       0,     0,     0,   200,   252,    52,    53,    54,    55,    56,
     201,   202,   253,   254,    57,    58,     0,     0,     0,   255,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    66,    67,    68,     0,
     256,   203,   204,   257,   258,   205,     0,   259,   260,    69,
      70,    71,    72,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,   206,   207,   170,     0,
       1,     2,     3,   171,   236,   237,     4,   172,     5,   238,
     173,   239,     0,   240,     6,     0,     7,     0,     8,     9,
     174,   175,   241,   242,     0,   176,   177,    10,   243,    11,
      12,    13,    14,     0,     0,     0,    15,   178,     0,     0,
       0,   179,     0,     0,   180,     0,    16,   244,     0,     0,
      17,    18,   181,    19,   182,    20,     0,   245,     0,    21,
      22,    23,    24,    25,   246,     0,    26,    27,    28,    29,
      30,   183,    31,     0,     0,   184,     0,   185,   247,     0,
     248,   188,     0,   189,     0,     0,     0,   249,     0,     0,
       0,     0,   190,     0,     0,   250,     0,     0,   191,   192,
     193,   194,   195,   196,   251,   198,   199,    37,    38,    39,
      40,    41,    42,    43,     0,     0,     0,     0,     0,     0,
       0,     0,   200,   252,    52,    53,    54,    55,    56,   201,
     202,   253,   254,    57,    58,     0,     0,     0,   255,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    66,    67,    68,     0,   256,
     203,   204,   257,   258,   205,     0,   259,   260,    69,    70,
      71,    72,    73,     0,     0,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,   206,   207,   170,     0,     1,
       2,     3,   171,     0,     0,     4,   172,     5,     0,   173,
       0,     0,     0,     6,     0,     7,     0,     8,     9,   174,
     175,     0,     0,     0,   176,   177,    10,     0,    11,    12,
      13,    14,     0,     0,     0,    15,   178,     0,     0,     0,
     179,     0,     0,   180,     0,    16,     0,     0,     0,    17,
      18,   181,    19,   182,    20,     0,     0,     0,    21,    22,
      23,    24,    25,     0,     0,    26,    27,    28,    29,    30,
     183,    31,    32,     0,   184,     0,   185,   247,     0,   248,
     188,     0,   189,     0,     0,     0,    35,     0,     0,     0,
       0,   190,     0,     0,     0,     0,     0,   191,   192,   193,
     194,   195,   196,   251,   198,   199,    37,    38,    39,    40,
      41,    42,    43,     0,     0,     0,     0,     0,     0,     0,
       0,   200,     0,    52,    53,    54,    55,    56,   201,   202,
       0,     0,    57,    58,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    66,    67,    68,     0,     0,   203,
     204,     0,     0,   205,     0,     0,     0,    69,    70,    71,
      72,    73,     0,     0,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,   206,   207,   170,     0,     0,     2,
       3,   171,     0,     0,     4,   172,     5,     0,   173,     0,
       0,     0,     6,     0,     7,     0,     8,     9,   174,   175,
       0,     0,     0,   176,   177,    10,     0,    11,    12,    13,
      14,     0,     0,     0,    15,   178,     0,     0,     0,   179,
       0,     0,   180,     0,    16,     0,     0,     0,    17,    18,
     181,    19,   182,    20,     0,     0,     0,    21,    22,    23,
      24,    25,     0,     0,     0,     0,    28,    29,    30,   183,
      31,     0,     0,   184,     0,   185,   692,     0,   248,   188,
       0,   189,     0,     0,     0,   249,     0,     0,     0,     0,
     190,     0,     0,     0,     0,     0,   191,   192,   193,   194,
     195,   196,   197,   198,   199,     0,    38,    39,    40,    41,
      42,    43,     0,     0,     0,     0,     0,     0,     0,     0,
     200,     0,    52,    53,    54,    55,    56,   201,   202,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    66,    67,    68,     0,     0,   203,   204,
       0,     0,   205,     0,     0,     0,    69,    70,    71,    72,
      73,     0,     0,    74,    75,    76,    77,    78,    79,    80,
      81,    82,    83,   206,   207,   170,     0,     0,     2,     3,
     171,     0,     0,     4,   172,     5,     0,   173,     0,     0,
       0,     6,     0,     7,     0,     8,     9,   174,   175,     0,
       0,     0,   176,   177,    10,     0,    11,    12,    13,    14,
       0,     0,     0,    15,   178,     0,     0,     0,   179,     0,
       0,   180,     0,    16,     0,     0,     0,    17,    18,   181,
      19,   182,    20,     0,     0,     0,    21,    22,    23,    24,
      25,     0,     0,     0,     0,     0,     0,    30,   183,    31,
       0,     0,   184,     0,   185,   186,     0,   187,   188,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,   190,
       0,     0,   250,     0,     0,   191,   192,   193,   194,   195,
     196,   197,   198,   199,     0,    38,    39,    40,    41,    42,
      43,     0,     0,     0,     0,     0,     0,     0,     0,   200,
       0,    52,    53,    54,    55,    56,   201,   202,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    66,    67,    68,     0,     0,   203,   204,     0,
       0,   205,     0,     0,     0,    69,    70,    71,    72,    73,
       0,     0,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,   206,   207,   170,     0,     0,     2,     3,   171,
       0,     0,     4,   172,     5,     0,   173,     0,     0,     0,
       6,     0,     7,     0,     8,     9,   174,   175,     0,     0,
       0,   176,   177,    10,     0,    11,    12,    13,    14,     0,
       0,     0,    15,   178,     0,     0,     0,   179,     0,     0,
     180,     0,    16,     0,     0,     0,    17,    18,   181,    19,
     182,    20,     0,     0,     0,    21,    22,    23,    24,    25,
       0,     0,     0,     0,     0,     0,    30,   183,    31,     0,
       0,   184,     0,   185,   500,     0,   187,   188,     0,   189,
       0,     0,     0,     0,     0,     0,     0,     0,   190,     0,
       0,   250,     0,     0,   191,   192,   193,   194,   195,   196,
     197,   198,   199,     0,    38,    39,    40,    41,    42,    43,
       0,     0,     0,     0,     0,     0,     0,     0,   200,     0,
      52,    53,    54,    55,    56,   201,   202,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    66,    67,    68,     0,     0,   203,   204,     0,     0,
     205,     0,     0,     0,    69,    70,    71,    72,    73,     0,
       0,    74,    75,    76,    77,    78,    79,    80,    81,    82,
      83,   206,   207,   170,     0,     0,     2,     3,   171,     0,
       0,     4,   172,     5,     0,   173,     0,     0,     0,     6,
       0,     7,     0,     8,     9,   174,   175,     0,     0,     0,
     176,   177,    10,     0,    11,    12,    13,    14,     0,     0,
       0,    15,   178,     0,     0,     0,   179,     0,     0,   180,
       0,    16,     0,     0,     0,    17,    18,   181,    19,   182,
      20,     0,     0,     0,    21,    22,    23,    24,    25,     0,
       0,     0,     0,     0,     0,    30,   183,    31,     0,     0,
     184,     0,   185,   500,     0,   187,   188,     0,   189,     0,
       0,     0,     0,     0,     0,     0,     0,   190,     0,     0,
     250,     0,     0,   191,   192,   193,   194,   195,   196,   197,
     198,   199,     0,    38,    39,    40,    41,    42,    43,     0,
       0,     0,     0,     0,     0,     0,     0,   200,     0,    52,
      53,    54,    55,    56,   201,   202,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      66,    67,    68,     0,     0,   203,   204,     0,     0,   205,
       0,     0,     0,    69,    70,    71,    72,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
     206,   207,   170,     0,     0,     2,     3,   171,     0,     0,
       4,   172,     5,     0,   173,     0,     0,     0,     6,     0,
       7,     0,     8,     9,   174,   175,     0,     0,     0,   176,
     177,    10,     0,    11,    12,    13,    14,     0,     0,     0,
      15,   178,     0,     0,     0,   179,     0,     0,   180,     0,
      16,     0,     0,     0,    17,    18,   181,    19,   182,    20,
       0,     0,     0,    21,    22,    23,    24,    25,     0,     0,
       0,     0,     0,     0,    30,   183,    31,     0,     0,   184,
       0,   185,   186,     0,   187,   188,     0,   189,     0,     0,
       0,     0,     0,     0,     0,     0,   190,     0,     0,   250,
       0,     0,   191,   192,   193,   194,   195,   196,   197,   198,
     199,     0,    38,    39,    40,    41,    42,    43,     0,     0,
       0,     0,     0,     0,     0,     0,   200,     0,    52,    53,
      54,    55,    56,   201,   202,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    66,
      67,    68,     0,     0,   203,   204,     0,     0,   205,     0,
       0,     0,    69,    70,    71,    72,    73,     0,     0,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,   206,
     207,   170,     0,     0,     2,     3,   171,     0,     0,     4,
     172,     5,     0,   173,     0,     0,     0,     6,     0,     7,
       0,     8,     9,   174,   175,     0,     0,     0,   176,   177,
      10,     0,    11,    12,    13,    14,     0,     0,     0,    15,
     178,     0,     0,     0,   179,     0,     0,   180,     0,    16,
       0,     0,     0,    17,    18,   181,    19,   182,    20,     0,
       0,     0,    21,    22,    23,    24,    25,     0,     0,     0,
       0,     0,     0,    30,   183,    31,     0,     0,   184,     0,
     185,   186,     0,   187,   188,     0,   189,     0,     0,     0,
       0,     0,     0,     0,     0,   190,     0,     0,     0,     0,
       0,   191,   192,   193,   194,   195,   196,   197,   198,   199,
       0,    38,    39,    40,    41,    42,    43,     0,     0,     0,
       0,     0,     0,     0,     0,   200,     0,    52,    53,    54,
      55,    56,   201,   202,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    66,    67,
      68,     0,     0,   203,   204,     0,     0,   205,     0,     0,
       0,    69,    70,    71,    72,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,   206,   207,
     170,     0,     0,     0,     0,   171,     0,     0,     0,   172,
       0,     0,   173,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   174,   175,     0,     0,     0,   176,   177,    10,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   178,
       0,     0,     0,   179,     0,     0,   180,     0,     0,     0,
       0,     0,     0,     0,   181,     0,   182,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   183,     0,     0,     0,   184,     0,   185,
     186,     0,   187,   188,     0,   189,   886,     0,     0,     0,
       0,     0,     0,     0,   887,     0,     0,   766,     0,   888,
     191,   192,   193,   194,   195,   196,   197,   198,   199,     0,
       0,     0,     0,   170,     0,     0,     0,     0,   171,     0,
       0,     0,   172,     0,   200,   173,    52,     0,     0,     0,
       0,   201,   202,     0,     0,   174,   175,     0,     0,     0,
     176,   177,    10,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   178,     0,     0,     0,   179,     0,     0,   180,
       0,     0,   203,   204,     0,     0,   205,   181,     0,   182,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   183,   206,   207,     0,
     184,     0,   185,   186,     0,   187,   188,     0,   189,   886,
       0,     0,     0,     0,     0,     0,     0,   887,     0,     0,
     766,     0,  1035,   191,   192,   193,   194,   195,   196,   197,
     198,   199,     0,     0,     0,     0,   170,     0,     0,     0,
       0,   171,     0,     0,     0,   172,     0,   200,   173,    52,
       0,     0,     0,     0,   201,   202,     0,     0,   174,   175,
       0,     0,     0,   176,   177,    10,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   178,     0,     0,     0,   179,
       0,     0,   180,     0,     0,   203,   204,     0,     0,   205,
     181,     0,   182,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   183,
     206,   207,     0,   184,     0,   185,   186,     0,   187,   188,
       0,   189,     0,     0,     0,     0,     0,     0,     0,     0,
     190,     0,     0,   335,     0,     0,   191,   192,   193,   194,
     195,   196,   197,   198,   199,     0,     0,     0,     0,   170,
       0,     0,     0,     0,   171,     0,     0,     0,   172,     0,
     200,   173,    52,     0,     0,     0,     0,   201,   202,     0,
       0,   174,   175,     0,     0,     0,   176,   177,    10,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   178,     0,
       0,     0,   179,     0,     0,   180,     0,     0,   203,   204,
       0,     0,   205,   181,     0,   182,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   183,   206,   207,     0,   184,     0,   185,   186,
       0,   187,   188,     0,   189,     0,     0,     0,   496,     0,
       0,     0,     0,   190,     0,     0,     0,     0,     0,   191,
     192,   193,   194,   195,   196,   197,   198,   199,     0,     0,
       0,     0,   170,     0,     0,     0,     0,   171,     0,     0,
       0,   172,     0,   200,   173,    52,     0,     0,     0,     0,
     201,   202,     0,     0,   174,   175,     0,     0,     0,   176,
     177,    10,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   178,     0,     0,     0,   179,     0,     0,   180,     0,
       0,   203,   204,     0,     0,   205,   181,     0,   182,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   183,   206,   207,     0,   184,
       0,   185,   186,     0,   564,   188,     0,   189,     0,     0,
       0,     0,     0,     0,     0,     0,   190,   565,     0,     0,
       0,     0,   191,   192,   193,   194,   195,   196,   197,   198,
     199,     0,     0,     0,     0,   170,     0,     0,     0,     0,
     171,     0,     0,     0,   172,     0,   200,   173,    52,     0,
       0,     0,     0,   201,   202,     0,     0,   174,   175,     0,
       0,     0,   176,   177,    10,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   178,     0,     0,     0,   179,     0,
       0,   180,     0,     0,   203,   204,     0,     0,   205,   181,
       0,   182,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   183,   206,
     207,     0,   184,     0,   185,   186,   656,   187,   188,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,   190,
       0,     0,     0,     0,     0,   191,   192,   193,   194,   195,
     196,   197,   198,   199,     0,     0,     0,     0,   170,     0,
       0,     0,     0,   171,     0,     0,     0,   172,     0,   200,
     173,    52,     0,     0,     0,     0,   201,   202,     0,     0,
     174,   175,     0,     0,     0,   176,   177,    10,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   178,     0,     0,
       0,   179,     0,     0,   180,     0,     0,   203,   204,     0,
       0,   205,   181,     0,   182,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   183,   206,   207,     0,   184,     0,   185,   186,     0,
     187,   188,     0,   189,     0,     0,   682,     0,     0,     0,
       0,     0,   190,     0,     0,     0,     0,     0,   191,   192,
     193,   194,   195,   196,   197,   198,   199,     0,     0,     0,
       0,   170,     0,     0,     0,     0,   171,     0,     0,     0,
     172,     0,   200,   173,    52,     0,     0,     0,     0,   201,
     202,     0,     0,   174,   175,     0,     0,     0,   176,   177,
      10,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     178,     0,     0,     0,   179,     0,     0,   180,     0,     0,
     203,   204,     0,     0,   205,   181,     0,   182,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   183,   206,   207,     0,   184,     0,
     185,   186,     0,   187,   188,     0,   189,     0,     0,     0,
       0,     0,     0,     0,     0,   190,     0,     0,     0,     0,
     753,   191,   192,   193,   194,   195,   196,   197,   198,   199,
       0,     0,     0,     0,   170,     0,     0,     0,     0,   171,
       0,     0,     0,   172,     0,   200,   173,    52,     0,     0,
       0,     0,   201,   202,     0,     0,   174,   175,     0,     0,
       0,   176,   177,    10,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   178,     0,     0,     0,   179,     0,     0,
     180,     0,     0,   203,   204,     0,     0,   205,   181,     0,
     182,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   183,   206,   207,
       0,   184,     0,   185,   186,     0,   187,   188,     0,   189,
       0,     0,     0,     0,     0,     0,     0,     0,   190,     0,
       0,   766,     0,     0,   191,   192,   193,   194,   195,   196,
     197,   198,   199,     0,     0,     0,     0,   170,     0,     0,
       0,     0,   171,     0,     0,     0,   172,     0,   200,   173,
      52,     0,     0,     0,     0,   201,   202,     0,     0,   174,
     175,     0,     0,     0,   176,   177,    10,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   178,     0,     0,     0,
     179,     0,     0,   180,     0,     0,   203,   204,     0,     0,
     205,   181,     0,   182,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     183,   206,   207,     0,   184,     0,   185,   186,   824,   187,
     188,     0,   189,     0,     0,     0,     0,     0,     0,     0,
       0,   190,     0,     0,     0,     0,     0,   191,   192,   193,
     194,   195,   196,   197,   198,   199,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   170,     0,     0,     0,     0,
     171,   200,     0,    52,   172,     0,  1077,   173,   201,   202,
       0,     0,     0,     0,     0,     0,     0,   174,   175,     0,
       0,     0,   176,   177,    10,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   178,     0,     0,     0,   179,   203,
     204,   180,     0,   205,     0,     0,     0,     0,     0,   181,
       0,   182,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   206,   207,     0,     0,   183,     0,
       0,     0,   184,     0,   185,   186,     0,   187,   188,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,   190,
       0,     0,     0,     0,     0,   191,   192,   193,   194,   195,
     196,   197,   198,   199,     0,     0,     0,     0,   170,     0,
       0,     0,     0,   171,     0,     0,     0,   172,     0,   200,
     173,    52,     0,     0,     0,     0,   201,   202,     0,     0,
     174,   175,     0,     0,     0,   176,   177,    10,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   178,     0,     0,
       0,   179,     0,     0,   180,     0,     0,   203,   204,     0,
       0,   205,   181,     0,   182,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   183,   206,   207,     0,   184,     0,   185,   186,  1092,
     187,   188,     0,   189,     0,     0,     0,     0,     0,     0,
       0,     0,   190,     0,     0,     0,     0,     0,   191,   192,
     193,   194,   195,   196,   197,   198,   199,     0,     0,     0,
       0,   170,     0,     0,     0,     0,   171,     0,     0,     0,
     172,     0,   200,   173,    52,     0,     0,     0,     0,   201,
     202,     0,     0,   174,   175,     0,     0,     0,   176,   177,
      10,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     178,     0,     0,     0,   179,     0,     0,   180,     0,     0,
     203,   204,     0,     0,   205,   181,     0,   182,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   183,   206,   207,     0,   184,     0,
     185,   186,     0,   187,   188,     0,   189,     0,     0,     0,
       0,     0,     0,     0,     0,   190,     0,     0,     0,     0,
       0,   191,   192,   193,   194,   195,   196,   197,   198,   199,
       0,     0,     0,     0,   170,     0,     0,     0,     0,   171,
       0,     0,     0,   172,     0,   200,   173,    52,     0,     0,
       0,     0,   201,   202,     0,     0,   174,   175,     0,     0,
       0,   176,   177,    10,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   178,     0,     0,     0,   179,     0,     0,
     180,     0,     0,   203,   204,     0,     0,   205,   181,     0,
     182,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   183,   206,   207,
       0,   184,     0,   185,   481,     0,   187,   188,     0,   189,
       0,     0,     0,     0,     0,     0,     0,     0,   190,     0,
       0,     0,     0,     0,   191,   192,   193,   194,   195,   196,
     197,   198,   199,     0,     0,     0,     0,   170,     0,     0,
       0,     0,   171,     0,     0,     0,   172,     0,   200,   173,
      52,     0,     0,     0,     0,   201,   202,     0,     0,   174,
     175,     0,     0,     0,   176,   177,    10,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   178,     0,     0,     0,
     179,     0,     0,   180,     0,     0,   203,   204,     0,     0,
     205,   181,     0,   182,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     183,   206,   207,     0,   184,     0,   185,   186,     0,   597,
     188,     0,   189,     0,     0,     0,     0,     0,     0,     0,
       0,   190,     0,     0,     0,     0,     0,   191,   192,   193,
     194,   195,   196,   197,   198,   199,     0,     0,     0,     0,
     170,     0,     0,     0,     0,   171,     0,     0,     0,   172,
       0,   200,   173,    52,     0,     0,     0,     0,   201,   202,
       0,     0,   174,   175,     0,     0,     0,   176,   177,    10,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   178,
       0,     0,     0,   179,     0,     0,   180,     0,     0,   203,
     204,     0,     0,   205,   181,     0,   182,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   183,   206,   207,     0,   184,     0,   185,
     408,     0,   187,   188,     0,   189,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     191,   192,   193,   194,   195,   196,   197,   198,   199,     0,
       0,     0,     0,   170,     0,     0,     0,     0,   171,     0,
       0,     0,   172,     0,   200,   173,    52,     0,     0,     0,
       0,   201,   202,     0,     0,   174,   175,     0,     0,     0,
     176,   177,    10,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   178,     0,     0,     0,   179,     0,     0,   180,
       0,     0,   203,   204,     0,     0,   205,   181,     0,   182,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   183,   206,   207,     0,
     184,     0,   185,   411,     0,   187,   188,     0,   189,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   191,   192,   193,   194,   195,   196,   197,
     198,   199,     0,     0,     0,     0,   170,     0,     0,     0,
       0,   171,     0,     0,     0,   172,     0,   200,   173,    52,
       0,     0,     0,     0,   201,   202,     0,     0,   174,   175,
       0,     0,     0,   176,   177,    10,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   178,     0,     0,     0,   179,
       0,     0,   180,     0,     0,   203,   204,     0,     0,   205,
     181,     0,   182,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   183,
     206,   207,     0,   184,     0,   185,   422,     0,   187,   188,
       0,   189,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   191,   192,   193,   194,
     195,   196,   197,   198,   199,     0,     0,     0,     0,   170,
       0,     0,     0,     0,   171,     0,     0,     0,   172,     0,
     200,   173,    52,     0,     0,     0,     0,   201,   202,     0,
       0,   174,   175,     0,     0,     0,   176,   177,    10,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   178,     0,
       0,     0,   179,     0,     0,   180,     0,     0,   203,   204,
       0,     0,   205,   181,     0,   182,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   183,   206,   207,     0,   184,     0,   185,   430,
       0,   187,   188,     0,   189,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   191,
     192,   193,   194,   195,   196,   197,   198,   199,     0,     0,
       0,     0,   170,     0,     0,     0,     0,   171,     0,     0,
       0,   172,     0,   200,   173,    52,     0,     0,     0,     0,
     201,   202,     0,     0,   174,   175,     0,     0,     0,   176,
     177,    10,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   178,     0,     0,     0,   179,     0,     0,   180,     0,
       0,   203,   204,     0,     0,   205,   181,     0,   182,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   183,   206,   207,     0,   184,
       0,   185,   432,     0,   187,   188,     0,   189,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   191,   192,   193,   194,   195,   196,   197,   198,
     199,     0,     0,     0,     0,   170,     0,     0,     0,     0,
     171,     0,     0,     0,   172,     0,   200,   173,    52,     0,
       0,     0,     0,   201,   202,     0,     0,   174,   175,     0,
       0,     0,   176,   177,    10,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   178,     0,     0,     0,   179,     0,
       0,   180,     0,     0,   203,   204,     0,     0,   205,   181,
       0,   182,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   183,   206,
     207,     0,   184,     0,   185,   434,     0,   187,   188,     0,
     189,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   191,   192,   193,   194,   195,
     196,   197,   198,   199,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   200,
       0,    52,     0,     0,     0,     0,   201,   202,     0,     0,
       0,     0,     0,     0,     0,     1,     2,     3,     0,     0,
       0,     4,     0,     5,     0,     0,     0,     0,     0,     6,
       0,     7,     0,     8,     9,     0,     0,   203,   204,     0,
       0,   205,    10,     0,    11,    12,    13,    14,     0,     0,
       0,    15,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,   206,   207,     0,    17,    18,     0,    19,     0,
      20,     0,     0,     0,    21,    22,    23,    24,    25,     0,
       0,    26,    27,    28,    29,    30,     0,    31,    32,     0,
       0,     0,     0,    33,     0,    34,     0,     0,     0,     0,
       0,     0,    35,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    36,
       0,     0,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,     0,     0,    52,
      53,    54,    55,    56,     0,     0,     0,     0,    57,    58,
       0,     0,     0,    59,     0,     0,     0,     0,     0,     0,
       0,     0,    60,    61,    62,     0,    63,    64,    65,     0,
      66,    67,    68,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    69,    70,    71,    72,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
       1,     2,     3,     0,     0,     0,     4,     0,     5,     0,
       0,     0,     0,     0,     6,     0,     7,     0,     8,     9,
       0,     0,     0,     0,     0,     0,     0,    10,     0,    11,
      12,    13,    14,     0,     0,     0,    15,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,     0,
      17,    18,     0,    19,     0,    20,     0,     0,     0,    21,
      22,    23,    24,    25,     0,     0,    26,    27,    28,    29,
      30,     0,    31,    32,     0,     0,     0,     0,    33,     0,
      34,     0,     0,     0,     0,     0,     0,    35,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    36,     0,     0,    37,    38,    39,
      40,    41,    42,    43,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    52,    53,    54,    55,    56,     0,
       0,     0,     0,    57,    58,     0,     0,     0,    59,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    66,    67,    68,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    69,    70,
      71,    72,    73,     0,     0,    74,    75,    76,    77,    78,
      79,    80,    81,    82,    83,     1,     2,     3,     0,     0,
       0,     4,     0,     5,     0,     0,     0,     0,     0,     6,
       0,     7,     0,     8,     9,     0,     0,     0,     0,     0,
       0,     0,    10,     0,    11,    12,    13,    14,     0,     0,
       0,    15,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,     0,    17,    18,     0,    19,     0,
      20,     0,     0,     0,    21,    22,    23,    24,    25,     0,
       0,    26,    27,    28,    29,    30,     0,    31,    32,     0,
       0,     0,     0,    33,     0,    34,     0,     0,     0,     0,
       0,     0,    35,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    36,
       0,     0,    37,    38,    39,    40,    41,    42,    43,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    52,
      53,    54,    55,    56,     0,     0,     0,     0,    57,    58,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      66,    67,    68,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    69,    70,    71,    72,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
       2,     3,     0,     0,     0,     4,     0,     5,     0,     0,
       0,     0,     0,     6,     0,     7,     0,     8,     9,     0,
       0,     0,     0,     0,     0,     0,    10,     0,    11,    12,
      13,    14,     0,     0,     0,    15,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    16,     0,     0,     0,    17,
      18,     0,    19,     0,    20,     0,     0,     0,    21,    22,
      23,    24,    25,     0,     0,     0,     0,     0,     0,    30,
       0,    31,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   288,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   617,     0,     0,     0,
       0,     0,     0,   289,     0,     0,     0,    38,    39,    40,
      41,    42,    43,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,     0,    57,    58,     0,     0,     0,   290,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    66,    67,    68,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    69,    70,    71,
      72,    73,     0,     0,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,     2,     3,     0,     0,     0,     4,
       0,     5,     0,     0,     0,     0,     0,     6,     0,     7,
       0,     8,     9,     0,     0,     0,     0,     0,     0,     0,
      10,     0,    11,    12,    13,    14,     0,     0,     0,    15,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    16,
       0,     0,     0,    17,    18,     0,    19,     0,    20,     0,
       0,     0,    21,    22,    23,    24,    25,     0,     0,     0,
       0,     0,     0,    30,     0,    31,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     288,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     793,     0,     0,     0,     0,     0,     0,   289,     0,     0,
       0,    38,    39,    40,    41,    42,    43,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,     0,    57,    58,     0,     0,
       0,   290,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    66,    67,
      68,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    69,    70,    71,    72,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,     2,     3,
       0,     0,     0,     4,     0,     5,     0,     0,     0,     0,
       0,     6,     0,     7,     0,     8,     9,     0,     0,     0,
       0,     0,     0,     0,    10,     0,    11,    12,    13,    14,
       0,     0,     0,    15,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    16,     0,     0,     0,    17,    18,     0,
      19,     0,    20,     0,     0,     0,    21,    22,    23,    24,
      25,     0,     0,     0,     0,     0,     0,    30,     0,    31,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   288,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   289,     0,     0,     0,    38,    39,    40,    41,    42,
      43,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,     0,
      57,    58,     0,     0,     0,   290,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    66,    67,    68,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    69,    70,    71,    72,    73,
       0,     0,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,     2,     3,     0,     0,     0,     4,     0,     5,
       0,     0,     0,     0,     0,     6,     0,     7,     0,     8,
       9,     0,     0,     0,     0,     0,     0,     0,    10,     0,
      11,    12,    13,    14,     0,     0,     0,    15,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
       0,    17,    18,     0,    19,     0,    20,     0,     0,     0,
      21,    22,    23,    24,    25,     0,     0,     0,     0,     0,
       0,    30,     0,    31,     0,     0,     0,     0,     0,     0,
     -73,     0,     0,   -73,     0,     0,     0,     0,     0,     0,
       2,     3,     0,     0,     0,     4,     0,     5,     0,     0,
       0,     0,     0,     6,     0,     7,     0,     8,     9,    38,
      39,    40,    41,    42,    43,     0,    10,     0,    11,    12,
      13,    14,     0,     0,     0,    15,    53,    54,    55,    56,
       0,     0,     0,     0,     0,    16,     0,     0,     0,    17,
      18,     0,    19,     0,    20,     0,     0,     0,    21,    22,
      23,    24,    25,     0,     0,     0,    66,    67,    68,    30,
       0,    31,     0,     0,     0,     0,     0,     0,   584,    69,
      70,    71,    72,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    38,    39,    40,
      41,    42,    43,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    66,    67,    68,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    69,    70,    71,
      72,    73,     0,     0,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,     2,     3,     0,     0,     0,     4,
       0,     5,     0,     0,     0,     0,     0,     6,     0,     7,
       0,     8,     9,     0,     0,     0,     0,     0,     0,     0,
      10,     0,    11,    12,    13,    14,     0,     0,     0,    15,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    16,
       0,     0,     0,    17,    18,     0,    19,     0,    20,     0,
       0,     0,    21,    22,    23,    24,    25,     0,     0,     0,
       0,     0,     0,    30,     0,    31,     0,     0,     0,     0,
       0,     0,   584,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     2,     3,     0,     0,     0,     4,     0,     5,
       0,     0,     0,     0,     0,     6,     0,     7,     0,     8,
       9,    38,    39,    40,    41,    42,    43,     0,    10,     0,
      11,    12,    13,    14,     0,     0,     0,    15,    53,    54,
      55,    56,     0,     0,     0,     0,     0,    16,     0,     0,
       0,    17,    18,     0,    19,     0,    20,     0,     0,   900,
      21,    22,    23,    24,    25,     0,     0,     0,    66,    67,
      68,    30,     0,    31,     0,     0,     0,     0,     0,     0,
       0,    69,    70,    71,    72,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    38,
      39,    40,    41,    42,    43,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    53,    54,    55,    56,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    66,    67,    68,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,    69,
      70,    71,    72,    73,     0,     0,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,     2,     3,     0,     0,
       0,     4,     0,     5,     0,     0,   919,     0,     0,     6,
       0,     7,     0,     8,     9,     0,     0,     0,     0,     0,
       0,     0,    10,     0,    11,    12,    13,    14,     0,     0,
       0,    15,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,     0,    17,    18,     0,    19,     0,
      20,     0,     0,     0,    21,    22,    23,    24,    25,     0,
       0,     0,     0,     0,     0,    30,     0,    31,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     2,     3,     0,     0,     0,     4,
       0,     5,     0,     0,     0,     0,     0,     6,     0,     7,
       0,     8,     9,    38,    39,    40,    41,    42,    43,     0,
      10,     0,    11,    12,    13,    14,     0,     0,     0,    15,
      53,    54,    55,    56,     0,     0,     0,     0,     0,    16,
       0,     0,     0,    17,    18,     0,    19,     0,    20,     0,
       0,     0,    21,    22,    23,    24,    25,     0,     0,     0,
      66,    67,    68,    30,     0,    31,     0,     0,     0,     0,
       0,     0,     0,    69,    70,    71,    72,    73,     0,     0,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    38,    39,    40,    41,    42,    43,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    53,    54,
      55,    56,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    66,    67,
      68,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    69,    70,    71,    72,    73,     0,     0,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,     2,     3,
       0,     0,     0,     4,     0,     5,     0,     0,     0,     0,
       0,     6,     0,     7,     0,     8,     9,     0,     0,     0,
       0,     0,     0,     0,    10,     0,    11,    12,    13,    14,
       0,     0,     0,    15,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    16,     0,     0,     0,    17,    18,     0,
      19,     0,    20,     0,     0,     0,    21,    22,    23,    24,
      25,     0,     0,     0,     0,     0,     0,    30,     0,    31,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    38,    39,    40,    41,    42,
      43,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    53,    54,    55,    56,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    66,    67,    68,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    69,    70,    71,    72,    73,
       0,     0,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,     2,     3,     0,     0,     0,     4,     0,     5,
       0,     0,     0,     0,     0,     6,     0,     7,     0,     8,
       9,     0,     0,     0,     0,     0,     0,     0,    10,     0,
      11,    12,    13,    14,     0,     0,     0,    15,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    16,     0,     0,
       0,    17,    18,     0,    19,     0,    20,     0,     0,     0,
      21,    22,    23,    24,    25,     0,     0,     0,     0,     0,
       0,    30,     0,    31,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       2,     3,     0,     0,     0,     4,   250,     5,     0,     0,
       0,     0,     0,     6,     0,     7,     0,     8,     9,    38,
      39,    40,    41,     0,    43,     0,    10,     0,    11,    12,
      13,    14,     0,     0,     0,    15,    53,    54,    55,    56,
       0,     0,     0,     0,     0,    16,     0,     0,     0,    17,
      18,     0,    19,     0,    20,     0,     0,     0,    21,    22,
      23,    24,    25,     0,     0,     0,    66,    67,    68,    30,
       0,    31,     0,     0,     0,     0,     0,     0,     0,    69,
      70,    71,    72,    73,     0,     0,    74,    75,    76,    77,
      78,     0,    80,    81,    82,    83,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    38,    39,    40,
      41,     0,    43,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    53,    54,    55,    56,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    66,    67,    68,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    69,    70,    71,
      72,    73,     0,     0,    74,    75,    76,    77,    78,     0,
      80,    81,    82,    83,  -335,  -335,  -335,  -335,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -335,
       0,     0,     0,     0,  -335,     0,     0,     0,     0,     0,
       0,  -335,     0,     0,     0,     0,     0,     0,     0,     0,
    -335,  -335,  -335,     0,  -335,  -335,  -335,  -335,     0,  -335,
    -335,  -335,  -335,     0,     0,  -335,  -335,     0,     0,     0,
       0,     0,     0,  -335,     0,     0,     0,     0,     0,     0,
       0,     0,  -335,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  -335,  -335,  -120,  -120,  -120,  -335,  -335,
    -335,  -335,  -335,     0,  -335,  -335,  -335,  -335,  -335,  -120,
       0,  -335,     0,  -335,     0,  -335,  -335,  -335,  -335,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -120,
    -335,     0,     0,     0,     0,  -335,     0,     0,     0,     0,
       0,     0,  -335,     0,     0,     0,     0,     0,     0,     0,
    -335,  -335,  -335,  -335,     0,  -335,  -335,  -335,  -335,     0,
    -335,  -335,  -335,  -335,     0,     0,  -335,  -335,     0,     0,
       0,     0,     0,     0,  -335,     0,     0,     0,     0,     0,
       0,     0,     0,  -335,     0,     0,     0,     0,     0,     0,
    -335,     0,     0,     0,  -335,  -335,  -120,  -120,  -120,  -335,
    -335,  -335,  -335,  -335,   518,  -335,  -335,  -335,  -335,  -335,
    -335,  -103,  -335,     0,  -335,     0,     0,     0,  -103,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  -103,     0,
    -120,     0,     0,     0,     0,     0,     0,  -115,     0,  -103,
       0,     0,     0,     0,     0,     0,  -103,     0,     0,     0,
       0,  -335,     0,     0,     0,     0,  -103,     0,     0,     0,
    -103,  -103,     0,  -103,     0,     0,     0,     0,     0,  -103,
       0,  -103,     0,  -103,     0,     0,     0,     0,  -115,  -115,
    -103,     0,  -103,     0,     0,     0,     0,     0,  -115,     0,
    -115,  -335,     0,     0,     0,     0,  -115,     0,  -120,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  -103,  -103,
       0,  -103,  -103,  -103,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  -115,     0,     0,  -103,  -103,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,  -103,  -103,  -103,     0,     0,
       0,     0,     0,     0,     0,     0,  -104,     0,  -103,  -103,
    -103,  -103,  -103,  -104,     0,  -103,  -103,  -103,  -103,  -103,
    -103,     0,     0,  -104,     0,     0,     0,     0,     0,     0,
       0,     0,  -116,     0,  -104,     0,     0,     0,     0,     0,
       0,  -104,     0,     0,     0,     0,     0,     0,     0,     0,
       0,  -104,     0,     0,     0,  -104,  -104,     0,  -104,     0,
       0,     0,     0,     0,  -104,     0,  -104,     0,  -104,     0,
       0,     0,     0,  -116,  -116,  -104,     0,  -104,     0,     0,
       0,     0,     0,  -116,     0,  -116,     0,     0,     0,     0,
       0,  -116,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,  -104,  -104,     0,  -104,  -104,  -104,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,  -116,
       0,     0,  -104,  -104,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
    -104,  -104,  -104,     0,     0,     0,     0,     0,     0,     0,
       0,  -102,     0,  -104,  -104,  -104,  -104,  -104,  -102,     0,
    -104,  -104,  -104,  -104,  -104,  -104,     0,     0,  -102,     0,
       0,     0,     0,     0,     0,     0,     0,  -114,     0,  -102,
       0,     0,     0,     0,     0,     0,  -102,     0,     0,     0,
       0,     0,     0,     0,     0,     0,  -102,     0,     0,     0,
    -102,  -102,     0,  -102,     0,     0,     0,     0,     0,  -102,
       0,  -102,     0,  -102,     0,     0,     0,     0,  -114,  -114,
    -102,     0,  -102,     0,     0,     0,     0,     0,  -114,     0,
    -114,     0,     0,     0,     0,     0,  -114,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  -102,  -102,
       0,  -102,  -102,  -102,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,  -114,     0,     0,  -102,  -102,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     2,     0,     0,     0,
       0,     0,     0,     5,     0,  -102,  -102,  -102,     0,     0,
       0,     0,     0,     8,     0,     0,     0,     0,  -102,  -102,
    -102,  -102,  -102,     0,    11,  -102,  -102,  -102,  -102,  -102,
    -102,    15,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,     0,    17,    18,     0,    19,     0,
       0,     0,     0,     0,    21,     0,    23,     0,    25,     0,
       0,     0,     0,     0,     0,   161,     0,    31,     0,     0,
       0,     0,     0,     0,   -72,     0,     0,   -72,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    38,    39,     0,    41,    42,    43,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,    56,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     2,     0,     0,     0,     0,     0,     0,     5,     0,
      66,    67,    68,     0,     0,     0,     0,     0,     8,     0,
       0,     0,     0,    69,    70,    71,    72,    73,     0,    11,
      74,    75,    76,    77,    78,    79,    15,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,     0,
      17,    18,     0,    19,     0,     0,     0,     0,     0,    21,
       0,    23,     0,    25,     0,     0,     0,     0,     0,     0,
     161,     0,    31,     0,     0,     0,     0,     0,     0,   -70,
       0,     0,   -70,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    38,    39,
       0,    41,    42,    43,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    55,    56,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     2,     0,     0,     0,
       0,     0,     0,     5,     0,    66,    67,    68,     0,     0,
       0,     0,     0,     8,     0,     0,     0,     0,    69,    70,
      71,    72,    73,     0,    11,    74,    75,    76,    77,    78,
      79,    15,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    16,     0,     0,     0,    17,    18,     0,    19,     0,
       0,     0,     0,     0,    21,     0,    23,     0,    25,     0,
       0,     0,     0,     0,     0,   161,     0,    31,     0,     0,
       0,     0,     0,     0,   -71,     0,     0,   -71,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    38,    39,     0,    41,    42,    43,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    55,    56,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     2,     0,     0,     0,     0,     0,     0,     5,     0,
      66,    67,    68,     0,     0,     0,     0,     0,     8,     0,
       0,     0,     0,    69,    70,    71,    72,    73,     0,    11,
      74,    75,    76,    77,    78,    79,    15,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    16,     0,     0,     0,
      17,    18,     0,    19,     0,     0,     0,     0,     0,    21,
       0,    23,     0,    25,     0,     0,     0,     0,     0,     0,
     161,     0,    31,     0,     0,     0,     0,     0,     0,   -69,
       0,     0,   -69,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    38,    39,
       0,    41,    42,    43,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    55,    56,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   -99,     0,     0,     0,
       0,     0,     0,   -99,     0,    66,    67,    68,     0,     0,
       0,     0,     0,   -99,     0,     0,     0,     0,    69,    70,
      71,    72,    73,     0,   -99,    74,    75,    76,    77,    78,
      79,   -99,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   -99,     0,     0,     0,   -99,   -99,     0,   -99,     0,
       0,     0,     0,     0,   -99,     0,   -99,     0,   -99,     0,
       0,     0,     0,     0,     0,   -99,     0,   -99,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,  -101,     0,     2,     0,     0,     0,     0,     0,
       0,     5,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     8,     0,   -99,   -99,     0,   -99,   -99,   -99,     0,
       0,     0,    11,     0,     0,     0,     0,     0,     0,    15,
       0,     0,   -99,   -99,     0,     0,     0,     0,     0,    16,
       0,     0,     0,    17,    18,     0,    19,     0,     0,     0,
       0,     0,    21,     0,    23,     0,    25,     0,     0,     0,
     -99,   -99,   -99,   161,     0,    31,     0,     0,     0,     0,
       0,     0,     0,   -99,   -99,   -99,   -99,   -99,     0,     0,
     -99,   -99,   -99,   -99,   -99,   -99,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    38,    39,     0,    41,    42,    43,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      55,    56,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    66,    67,
      68,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    69,    70,    71,    72,    73,     0,     0,    74,    75,
      76,    77,    78,    79
};

/* YYCONFLP[YYPACT[STATE-NUM]] -- Pointer into YYCONFL of start of
   list of conflicting reductions corresponding to action entry for
   state STATE-NUM in yytable.  0 means no conflicts.  The list in
   yyconfl is terminated by a rule number of 0.  */
static const unsigned short int yyconflp[] =
{
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     3,     0,   295,   349,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   639,     0,     0,     0,     0,
       0,   669,     0,     0,     0,     0,     0,     0,   665,     0,
       0,     0,     0,     0,     0,   697,     0,     0,     0,     0,
       0,     0,     0,     0,     5,     0,     0,     0,     0,     0,
     701,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     1,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   641,     0,     0,     0,     0,     0,   671,
       7,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   703,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       9,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   293,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   653,     0,     0,   655,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   325,   327,     0,     0,   329,     0,     0,     0,     0,
     331,     0,   333,     0,     0,   335,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   345,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   351,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   353,   355,     0,     0,     0,
       0,     0,     0,     0,     0,   357,     0,   359,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   361,
       0,     0,     0,   681,   683,   685,     0,     0,     0,   687,
       0,   689,     0,     0,     0,     0,     0,     0,     0,   363,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   365,     0,     0,     0,     0,     0,     0,     0,     0,
     369,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   695,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   371,   373,     0,     0,     0,   699,   367,     0,     0,
       0,   375,     0,   377,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   379,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   381,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   383,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   385,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   317,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   319,     0,   321,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   323,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   387,     0,     0,     0,
     389,     0,   391,   393,   395,   397,   399,   401,   403,   405,
     407,   409,   411,   413,     0,   415,   417,   419,   421,     0,
     423,   425,   427,   429,   431,   433,     0,   435,   437,   439,
     441,   443,   445,   447,   449,     0,     0,     0,   451,   453,
       0,     0,     0,   455,     0,     0,   457,     0,   459,   461,
       0,     0,   463,   465,   467,   469,   471,   473,     0,   475,
       0,   477,   479,   481,   483,   485,   487,     0,   489,   491,
     493,   495,   497,   499,   501,     0,     0,   503,     0,   505,
     507,     0,   509,   511,     0,   513,     0,     0,     0,   515,
       0,     0,     0,     0,   517,     0,     0,   519,     0,   521,
     523,   525,   527,   529,   531,   533,   535,   537,   539,   541,
     543,   545,   547,   549,   551,   553,     0,     0,     0,     0,
       0,     0,     0,     0,   555,   557,   559,   561,   563,   565,
     567,   569,   571,   573,   575,   577,   579,     0,     0,     0,
     581,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   583,   585,   587,
       0,   589,   591,   593,   595,   597,   599,     0,   601,   603,
     605,   607,   609,   611,   613,     0,     0,   615,   617,   619,
     621,   623,   625,   627,   629,   631,   633,   635,   637,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   309,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   311,     0,   313,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     315,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   657,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   659,     0,   661,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   663,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   643,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   645,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   647,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     667,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    11,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   347,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   297,   299,   301,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   303,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   305,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   337,   307,   339,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    83,     0,     0,     0,     0,     0,     0,    85,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    87,     0,
     341,     0,     0,     0,     0,     0,     0,     0,     0,    89,
       0,     0,     0,     0,     0,     0,    91,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    93,     0,     0,     0,
      95,    97,     0,    99,     0,     0,     0,     0,     0,   101,
       0,   103,     0,   105,     0,     0,     0,     0,     0,     0,
     107,     0,   109,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   343,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   111,   113,
       0,   115,   117,   119,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   121,   123,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   125,   127,   129,     0,     0,
       0,     0,     0,     0,     0,     0,   153,     0,   131,   133,
     135,   137,   139,   155,     0,   141,   143,   145,   147,   149,
     151,     0,     0,   157,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   159,     0,     0,     0,     0,     0,
       0,   161,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   163,     0,     0,     0,   165,   167,     0,   169,     0,
       0,     0,     0,     0,   171,     0,   173,     0,   175,     0,
       0,     0,     0,     0,     0,   177,     0,   179,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   181,   183,     0,   185,   187,   189,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   191,   193,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     195,   197,   199,     0,     0,     0,     0,     0,     0,     0,
       0,   223,     0,   201,   203,   205,   207,   209,   225,     0,
     211,   213,   215,   217,   219,   221,     0,     0,   227,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   229,
       0,     0,     0,     0,     0,     0,   231,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   233,     0,     0,     0,
     235,   237,     0,   239,     0,     0,     0,     0,     0,   241,
       0,   243,     0,   245,     0,     0,     0,     0,     0,     0,
     247,     0,   249,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   251,   253,
       0,   255,   257,   259,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   261,   263,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   265,   267,   269,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   271,   273,
     275,   277,   279,     0,     0,   281,   283,   285,   287,   289,
     291,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   649,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   651,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   673,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     675,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   677,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   679,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   691,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    13,     0,     0,     0,
       0,     0,     0,    15,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    17,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    19,     0,     0,     0,     0,     0,
     693,    21,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    23,     0,     0,     0,    25,    27,     0,    29,     0,
       0,     0,     0,     0,    31,     0,    33,     0,    35,     0,
       0,     0,     0,     0,     0,    37,     0,    39,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    41,    43,     0,    45,    47,    49,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    51,    53,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      55,    57,    59,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    61,    63,    65,    67,    69,     0,     0,
      71,    73,    75,    77,    79,    81,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0
};

/* YYCONFL[I] -- lists of conflicting rule numbers, each terminated by
   0, pointed into by YYCONFLP.  */
static const short int yyconfl[] =
{
       0,   185,     0,    30,     0,    30,     0,    30,     0,   572,
       0,    68,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   101,     0,   101,     0,   101,     0,   101,
       0,   101,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   115,     0,   115,     0,   115,     0,   115,
       0,   115,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   116,     0,   116,     0,   116,     0,   116,
       0,   116,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   114,     0,   114,     0,   114,     0,   114,
       0,   114,     0,   186,     0,    30,     0,   335,     0,   335,
       0,   335,     0,   335,     0,   335,     0,   335,     0,    30,
       0,    30,     0,    30,     0,    30,     0,   362,     0,   362,
       0,   362,     0,   362,     0,   320,     0,   320,     0,   320,
       0,   320,     0,   320,     0,   320,     0,   335,     0,   335,
       0,   335,     0,   335,     0,   573,     0,    61,     0,    29,
       0,    81,     0,    81,     0,    81,     0,    81,     0,    81,
       0,    81,     0,    81,     0,    81,     0,    81,     0,   624,
       0,   624,     0,   624,     0,   624,     0,   624,     0,   624,
       0,   624,     0,   624,     0,   624,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   246,
       0,   246,     0,   246,     0,   246,     0,   246,     0,   335,
       0,   335,     0,    68,     0,    73,     0,    73,     0,    72,
       0,    72,     0,   221,     0,   221,     0,    30,     0,    30,
       0,    30,     0,    30,     0,    30,     0,    30,     0,   208,
       0,   208,     0,    70,     0,    70,     0,    71,     0,    71,
       0,   355,     0,   358,     0,   567,     0,   569,     0,   565,
       0,    69,     0,    69,     0,   299,     0,    30,     0,   416,
       0,   205,     0,   205,     0
};

static const short int yycheck[] =
{
       0,    50,   116,   153,   118,    34,   120,   441,    49,   259,
     260,   149,    95,   317,    64,    48,    48,    33,   535,     0,
     513,     0,    48,   491,   197,   198,   199,   764,     0,    36,
     766,   389,   114,   343,   344,   291,    36,    37,   370,    34,
     213,   373,   373,   311,   312,   513,    46,    47,    48,    49,
      50,   113,    33,   370,   289,    36,   373,    36,   293,   547,
      60,   114,   604,   892,   721,    46,    47,   129,    47,     0,
      51,   989,   603,   549,   118,    47,    48,    85,   251,   126,
      61,   113,    63,    64,    88,    85,    48,    33,    85,    92,
     821,    88,    14,    33,    33,    33,    33,    97,   580,   237,
       6,    60,    23,    33,    85,    36,    85,   148,    84,    33,
      84,    33,     0,    85,    95,   118,    47,    85,   255,    60,
      88,   118,    87,     0,    89,    46,   126,   168,    33,   129,
      33,    85,    64,   162,   150,    33,    69,    88,    84,    98,
      60,    92,   186,    84,    84,    49,    39,   151,   148,     0,
     118,  1069,   103,   290,    85,   891,    90,    33,   152,    85,
      92,    54,    86,    85,    98,    85,    92,    84,   168,   150,
     170,  1000,   153,   103,    60,   151,   153,   151,   118,    88,
     118,   118,   724,   186,   130,    36,   186,    63,    64,   186,
     130,   130,   723,    97,   103,    14,    47,    85,   152,    85,
      88,    60,   534,   534,    92,   156,    92,   345,    85,   940,
      88,    88,   118,   118,    33,    92,    88,   534,   186,   248,
     118,   579,    84,    84,   149,   103,    85,   661,    88,   101,
     118,   247,    98,   139,    85,   101,    98,    45,   895,   975,
     240,   118,   242,   103,   251,   981,   186,   247,   186,   186,
     250,   251,   740,   248,    62,   772,   289,   289,    84,   240,
     293,   293,   738,   289,    88,   623,   247,   293,    92,   250,
     251,    88,   251,   273,   150,   448,   449,   450,   360,   287,
     186,   186,   396,   397,   766,   520,   103,   287,   186,   289,
      92,   291,   273,   293,    30,   378,    88,    99,   186,    84,
     300,  1037,    84,   365,   366,    41,   287,    99,   287,   186,
     291,   319,   237,   395,   291,   287,    98,   289,   860,   319,
     251,   293,    60,  1060,    84,   759,   303,   289,   859,   585,
    1066,   293,   379,    84,    88,   382,   853,    84,   319,   149,
     319,   370,   395,    84,   373,  1076,    84,   319,   348,   103,
      98,    98,   385,   385,   142,   143,   287,    98,    94,   385,
      96,    88,    33,    88,    84,    92,   800,   408,   362,   369,
     411,   247,    99,    88,    99,   375,   376,    92,   419,   379,
     421,   422,   382,   618,   522,   385,   386,   387,   319,   430,
      82,   432,   386,   434,    86,   727,   727,   378,    88,    91,
     251,   378,   402,    93,    74,    75,   406,   369,   408,   891,
     727,   411,    88,   385,    84,   291,    86,    93,     0,   419,
     345,   421,   422,   385,   538,   575,    88,   237,    98,    93,
     430,    93,   432,    90,   434,    93,   287,   795,    95,    88,
     481,    98,   524,   114,    33,    88,   756,    84,   941,   780,
      93,   782,   123,    84,   785,   786,    99,    44,   129,   440,
     710,   442,    85,    88,   637,   982,    88,    88,   319,    92,
     453,   454,    93,   941,    99,   101,   495,    99,    65,   498,
     499,   481,    92,    93,   500,    74,    75,   520,   520,   489,
     539,   491,    14,   975,   520,    84,   101,    86,    84,   981,
     500,    85,   378,   552,   504,   534,   451,   452,   489,   509,
     491,   561,   491,   513,   491,    85,   578,    85,   518,   500,
     520,    85,    84,   504,    88,   856,   857,   335,   509,   491,
     126,    84,   513,   533,   513,   345,   513,   518,    84,   539,
      84,   130,    85,   543,   812,    88,   578,    69,   520,    84,
     688,   513,   552,    85,    76,  1037,    88,    85,   520,   863,
      88,    84,   370,    84,   868,   373,   145,   146,   147,    33,
     599,   533,   601,    83,    85,   604,   247,    88,   102,    85,
     561,   168,    88,    84,  1066,   618,   618,   587,   582,   583,
     186,    84,   618,    85,   575,   117,    88,   522,   575,   186,
      84,   459,   460,   190,   585,   413,    85,    84,   585,    88,
     418,   694,   420,    33,    85,   491,    85,    88,   618,    88,
     428,   429,   100,     4,   500,   956,   957,    85,    88,   437,
      88,   989,   990,   441,    60,    88,   718,   513,   446,   161,
     162,   163,    85,     6,    85,    88,   618,    88,  1082,   113,
     114,   247,   790,    93,    74,    75,   618,   244,   831,   123,
     247,    93,    33,    92,    84,   129,    86,    84,    84,   256,
     478,   479,   259,   260,    84,   813,    85,   485,    33,    88,
     762,   763,  1040,    88,    85,   561,    43,    88,   152,   689,
     772,    84,   692,    50,    51,   724,   946,   847,   727,   575,
     455,   456,   457,   458,    84,    93,    84,   845,   689,   585,
     130,  1069,   522,   694,   852,    60,    93,   694,   718,    74,
      75,    84,    85,    84,   395,    88,   534,    84,    92,    84,
      93,    86,    95,    90,    88,    98,    85,    92,    98,    88,
     854,    98,   113,   114,   721,   895,    43,    98,   556,   749,
     750,    93,   123,    50,    51,   118,    85,   682,   129,    88,
      84,   761,   803,   688,    95,   806,   766,    60,   809,    85,
     770,    88,   580,    85,    85,   130,   139,    85,   778,   779,
      84,   152,    70,   247,   130,    85,   382,    84,   788,    85,
      93,    93,    93,    90,    93,    93,    88,    84,   145,   799,
     152,    98,   145,   803,    88,    99,   806,   807,   770,   809,
     948,   101,   408,    99,    84,   411,    85,   779,   694,    85,
     869,   408,   179,   186,   411,    88,   422,    88,    88,    88,
      59,   860,    99,   101,   430,   422,   432,    99,   434,   839,
     840,   841,    99,   430,    95,   432,    93,   434,    85,   436,
      84,    92,   523,   661,    85,    85,   443,   528,   839,   840,
     841,    88,   533,    88,   535,   790,   847,    88,   950,   869,
     847,    88,   682,    88,    85,    88,   247,    85,   688,    85,
      85,  1019,   179,    85,    33,   481,   886,    88,   813,    88,
     477,   180,    85,    84,   481,    99,   360,    85,   362,    85,
     982,   365,   366,     4,   500,   830,   493,   240,   495,    85,
     910,   498,   499,   500,   895,   586,    95,    92,   895,   727,
     845,   508,   386,    85,    85,    74,    75,   852,    99,    85,
      85,   395,    33,   151,    92,    84,    99,    86,   938,    93,
     273,   941,    85,    93,   101,    84,    99,    92,    49,    98,
     950,   759,    93,   101,   101,   101,   997,   938,   766,   101,
     941,   101,   941,    93,   941,    88,   553,    85,    95,   118,
     557,   847,   780,   143,   782,   975,    92,   785,   786,   941,
     790,   130,    83,    88,    99,    86,    87,    33,    89,   360,
      33,   362,   800,    99,   365,   366,    99,   997,    93,   100,
     808,   102,    99,   813,    92,    92,    85,    85,    88,    85,
      85,   819,    85,    21,  1014,   386,    88,    99,   826,   895,
     830,    92,    99,   948,   395,   145,   145,    92,    74,    75,
      99,    74,    75,  1014,    93,   845,    99,   186,    84,    99,
      86,    84,   852,    86,    33,  1026,    99,   718,   856,   857,
      85,    93,    98,  1053,   979,    98,    93,   521,    99,   523,
     524,    99,    49,    50,   528,   941,    92,    85,    88,   533,
      93,   535,  1053,    85,    93,   118,    92,   887,    92,    85,
      85,     0,    93,   891,   130,    74,    75,   130,     0,    47,
     799,  1091,   251,   990,  1019,    84,   692,    86,  1098,   770,
     771,   772,   773,    92,   382,   692,    97,   763,   578,   696,
    1091,    95,   585,   153,   578,   102,   103,  1098,   582,   583,
     788,   975,   586,   710,   779,   925,   382,   385,   950,   949,
    1013,  1026,   997,   250,   461,   463,   462,   472,   948,   464,
     507,   130,   492,   186,   735,   231,   869,   750,   956,   957,
     521,   872,   523,   524,    97,   335,   489,   528,    -1,   559,
     968,   148,   533,   346,   535,    -1,   761,   975,    -1,   979,
      -1,   504,    -1,   981,    -1,    -1,   509,   848,    -1,    -1,
     851,   168,   853,    -1,    -1,   518,   994,   995,    -1,    -1,
      -1,    -1,   178,  1001,   180,   181,    -1,    -1,    -1,   186,
      -1,  1009,    -1,    -1,    -1,    49,    50,   578,    -1,  1019,
     196,   582,   583,    -1,    -1,   586,    -1,   203,   204,   205,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   692,  1037,
      -1,    -1,    -1,    -1,   821,   822,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   231,    -1,   834,    -1,    -1,
      -1,    49,    50,    97,   718,  1063,  1064,    -1,  1066,    -1,
     247,    -1,    -1,    -1,    -1,    -1,  1074,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,  1082,    -1,    -1,    -1,    -1,   950,
      -1,    -1,   126,   870,    -1,   872,  1094,  1095,    -1,    -1,
     877,    -1,   879,    -1,    -1,    -1,    -1,    -1,   762,   763,
     887,    -1,    -1,    14,   148,    -1,   770,   771,   772,   773,
      -1,   982,    -1,    -1,   301,    -1,    -1,    -1,    49,    50,
      -1,   692,    33,    -1,   168,    -1,    -1,    -1,   126,    -1,
      -1,    -1,    -1,    -1,    49,    50,    -1,    -1,    -1,    -1,
      -1,    -1,   186,    -1,    -1,    -1,    -1,   718,    -1,    -1,
     148,    -1,    -1,   940,    -1,    -1,   689,   300,    69,   946,
      -1,    -1,    -1,    74,    75,    76,    -1,    -1,   355,   356,
     168,    -1,    -1,    84,    -1,    86,    -1,    -1,    -1,    -1,
      -1,    -1,   369,    -1,   848,    -1,    -1,   851,   186,   853,
      -1,   762,   763,    -1,   381,   126,    -1,    -1,    -1,   770,
     771,   772,   773,   247,   991,    -1,   117,   118,    -1,    -1,
      -1,   126,    -1,    -1,   357,   358,    -1,   148,  1005,   130,
    1007,   408,    -1,    -1,   411,    -1,  1013,    -1,    -1,    -1,
      -1,    -1,   419,   148,   421,   422,    -1,   168,    -1,    -1,
      -1,    -1,    -1,   430,    -1,   432,    -1,   434,    -1,   247,
     161,   162,   163,   168,    -1,   186,   300,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,  1056,
      -1,   186,    -1,    -1,    -1,   186,    -1,   848,    -1,    -1,
     851,    -1,   853,    -1,    -1,    -1,   950,    -1,    -1,  1076,
      -1,    -1,    -1,    -1,   481,    -1,    -1,    -1,    -1,  1086,
      -1,    -1,   300,    -1,    -1,    -1,   839,   840,   841,    -1,
      -1,    -1,    -1,   500,    -1,    -1,   247,    -1,   982,    -1,
      -1,    -1,    -1,    -1,    -1,   369,    -1,    -1,    -1,    -1,
      -1,    -1,   247,    -1,    -1,   379,    -1,    -1,   382,    -1,
      -1,    -1,    -1,    -1,   531,    -1,   533,    -1,    -1,    -1,
      -1,    -1,   539,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   408,   552,    -1,   411,    -1,   300,
      -1,   369,    -1,    -1,    -1,   419,    -1,   421,   422,   950,
      -1,   379,    -1,    -1,   382,   300,   430,    -1,   432,   532,
     434,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   588,    -1,    -1,    -1,   938,    -1,    -1,    -1,    -1,
     408,   982,    -1,   411,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   419,    -1,   421,   422,    -1,   613,    -1,    -1,    -1,
     573,   574,   430,    -1,   432,    -1,   434,   481,   369,    -1,
      -1,    -1,    -1,    -1,   587,    -1,    -1,    -1,   379,    -1,
      -1,   382,    -1,    -1,   369,    -1,   500,    -1,    -1,    -1,
      49,    50,    -1,    -1,   379,    -1,    -1,   382,    -1,    -1,
      -1,   614,    -1,    -1,    -1,    -1,    -1,   408,    -1,    -1,
     411,  1014,    -1,   481,    -1,    -1,    -1,    -1,   419,   533,
     421,   422,    -1,   408,    -1,   539,   411,    -1,    -1,   430,
      -1,   432,   500,   434,   419,   692,   421,   422,   552,    -1,
      -1,    -1,    -1,    -1,    -1,   430,    -1,   432,    -1,   434,
    1053,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   533,    -1,   126,    -1,    -1,
      -1,   539,    -1,   587,    -1,    -1,    -1,    -1,    -1,    -1,
     481,    -1,    -1,    -1,   552,    -1,    -1,    -1,  1091,   148,
      -1,    -1,    -1,    -1,    -1,  1098,   481,    -1,    -1,   500,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   720,    -1,   168,
      -1,    -1,    -1,   770,    -1,   500,    -1,     9,   775,   587,
      -1,    13,   779,    -1,    16,    -1,    -1,   186,     3,    -1,
       5,    -1,   533,    -1,    26,    27,    -1,    -1,   539,    31,
      32,    33,    -1,    18,    -1,    -1,   803,    -1,   533,   806,
      -1,   552,   809,    -1,   539,    47,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   776,    -1,    40,    -1,   552,    60,    44,
      45,    -1,    -1,    48,    49,    -1,    -1,    -1,   692,   792,
      55,    -1,    -1,    -1,    -1,    77,   587,    62,   247,    -1,
      -1,    -1,    84,    -1,    -1,    -1,    71,    -1,    -1,    -1,
      -1,    -1,   587,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   869,   105,   106,   107,   108,    -1,    -1,    -1,
      95,    -1,    97,    -1,   692,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,   130,    -1,
      -1,   300,    -1,   135,   136,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   770,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   779,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   898,    -1,    -1,    -1,   803,
      -1,   692,   806,    -1,    -1,   809,    -1,    -1,    -1,   191,
     192,    -1,   770,    -1,    -1,    -1,    -1,   692,    -1,    -1,
     369,   779,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     379,    -1,    -1,   382,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   803,    -1,    -1,   806,    -1,
     997,   809,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   408,
      -1,    -1,   411,    -1,    -1,   869,    -1,    -1,    -1,    -1,
     419,    -1,   421,   422,    -1,    -1,    -1,    -1,    -1,   770,
      -1,   430,    -1,   432,    -1,   434,    -1,    -1,   779,    -1,
      -1,    -1,    -1,    -1,    -1,   770,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   779,    -1,    -1,    -1,    -1,    -1,
      -1,   869,   803,    -1,    -1,   806,    -1,    -1,   809,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   803,    -1,
      -1,   806,   481,    -1,   809,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   500,     4,    -1,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    13,    14,    -1,    16,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    26,    27,    -1,    -1,   869,    31,
      32,    33,    -1,    -1,   533,    -1,    -1,    -1,    -1,    -1,
     539,    43,    -1,   997,   869,    47,    -1,    -1,    50,    -1,
      -1,    -1,    -1,   552,    -1,    -1,    58,    59,    60,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    76,    77,    -1,    -1,    -1,    81,
      -1,    83,    84,    -1,    86,    87,    -1,    89,   587,   997,
      -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,    -1,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,    -1,    -1,    -1,    -1,   117,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,   130,    -1,
      -1,    -1,    -1,   135,   136,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   997,    -1,    -1,   161,
     162,   163,    -1,    -1,   166,   167,    -1,    -1,   170,    -1,
      -1,    -1,   997,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   191,
     192,    -1,    -1,   692,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     0,    -1,    -1,    -1,
       4,    -1,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    -1,    19,    20,    21,    22,    -1,
      24,    25,    26,    27,    28,    29,    -1,    31,    32,    33,
      34,    35,    36,    37,    38,    -1,    -1,    -1,    42,    43,
      -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    52,    53,
      -1,   770,    56,    57,    58,    59,    60,    61,    -1,    63,
     779,    65,    66,    67,    68,    69,    70,    -1,    72,    73,
      74,    75,    76,    77,    78,    -1,    -1,    81,    -1,    83,
      84,    -1,    86,    87,   803,    89,    -1,   806,    -1,    93,
     809,    -1,    -1,    -1,    98,    -1,    -1,   101,    -1,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,    -1,    -1,    -1,
     144,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     869,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,   163,
      -1,   165,   166,   167,   168,   169,   170,    -1,   172,   173,
     174,   175,   176,   177,   178,    -1,    -1,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,    -1,
       4,    -1,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    -1,    19,    20,    -1,    22,    -1,
      24,    25,    26,    27,    28,    29,    -1,    31,    32,    33,
      34,    35,    36,    37,    38,    -1,    -1,    -1,    42,    43,
      -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    52,    53,
      -1,    -1,    56,    57,    58,    59,    60,    61,    -1,    63,
      -1,    65,    66,    67,    68,    69,    70,    -1,    72,    73,
      74,    75,    76,    77,    78,    -1,    -1,    81,    -1,    83,
      84,    -1,    86,    87,    -1,    89,    -1,    -1,   997,    93,
      -1,    -1,    -1,    -1,    98,    -1,    -1,   101,    -1,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,   138,   139,   140,    -1,    -1,    -1,
     144,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,   163,
      -1,   165,   166,   167,   168,   169,   170,    -1,   172,   173,
     174,   175,   176,   177,   178,    -1,    -1,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,     4,
      -1,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    -1,    19,    20,    -1,    22,    -1,    24,
      25,    26,    27,    28,    29,    -1,    31,    32,    33,    34,
      35,    36,    37,    38,    -1,    -1,    -1,    42,    43,    -1,
      -1,    -1,    47,    -1,    -1,    50,    -1,    52,    53,    -1,
      -1,    56,    57,    58,    59,    60,    61,    -1,    63,    -1,
      65,    66,    67,    68,    69,    70,    -1,    72,    73,    74,
      75,    76,    77,    78,    -1,    -1,    81,    -1,    83,    84,
      -1,    86,    87,    -1,    89,    -1,    -1,    -1,    93,    -1,
      -1,    -1,    -1,    98,    -1,    -1,   101,    -1,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138,   139,   140,    -1,    -1,    -1,   144,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   161,   162,   163,    -1,
     165,   166,   167,   168,   169,   170,    -1,   172,   173,   174,
     175,   176,   177,   178,    -1,    -1,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,     4,    -1,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    -1,    19,    20,    -1,    22,    -1,    24,    25,
      26,    27,    28,    29,    -1,    31,    32,    33,    34,    35,
      36,    37,    38,    -1,    -1,    -1,    42,    43,    -1,    -1,
      -1,    47,    -1,    -1,    50,    -1,    52,    53,    -1,    -1,
      56,    57,    58,    59,    60,    61,    -1,    63,    -1,    65,
      66,    67,    68,    69,    70,    -1,    72,    73,    74,    75,
      76,    77,    78,    -1,    -1,    81,    -1,    83,    84,    -1,
      86,    87,    -1,    89,    -1,    -1,    -1,    93,    -1,    -1,
      -1,    -1,    98,    -1,    -1,   101,    -1,    -1,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,   138,   139,   140,    -1,    -1,    -1,   144,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   161,   162,   163,    -1,   165,
     166,   167,   168,   169,   170,    -1,   172,   173,   174,   175,
     176,   177,   178,    -1,    -1,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,   191,   192,     4,    -1,     6,
       7,     8,     9,    -1,    -1,    12,    13,    14,    -1,    16,
      -1,    -1,    -1,    20,    -1,    22,    -1,    24,    25,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    -1,    35,    36,
      37,    38,    -1,    -1,    -1,    42,    43,    -1,    -1,    -1,
      47,    -1,    -1,    50,    -1,    52,    -1,    -1,    -1,    56,
      57,    58,    59,    60,    61,    -1,    -1,    -1,    65,    66,
      67,    68,    69,    -1,    -1,    72,    73,    74,    75,    76,
      77,    78,    79,    -1,    81,    -1,    83,    84,    -1,    86,
      87,    -1,    89,    -1,    -1,    -1,    93,    -1,    -1,    -1,
      -1,    98,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   128,    -1,   130,   131,   132,   133,   134,   135,   136,
      -1,    -1,   139,   140,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,   166,
     167,    -1,    -1,   170,    -1,    -1,    -1,   174,   175,   176,
     177,   178,    -1,    -1,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   191,   192,     4,    -1,    -1,     7,
       8,     9,    -1,    -1,    12,    13,    14,    -1,    16,    -1,
      -1,    -1,    20,    -1,    22,    -1,    24,    25,    26,    27,
      -1,    -1,    -1,    31,    32,    33,    -1,    35,    36,    37,
      38,    -1,    -1,    -1,    42,    43,    -1,    -1,    -1,    47,
      -1,    -1,    50,    -1,    52,    -1,    -1,    -1,    56,    57,
      58,    59,    60,    61,    -1,    -1,    -1,    65,    66,    67,
      68,    69,    -1,    -1,    -1,    -1,    74,    75,    76,    77,
      78,    -1,    -1,    81,    -1,    83,    84,    -1,    86,    87,
      -1,    89,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,
      98,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,
     108,   109,   110,   111,   112,    -1,   114,   115,   116,   117,
     118,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     128,    -1,   130,   131,   132,   133,   134,   135,   136,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   161,   162,   163,    -1,    -1,   166,   167,
      -1,    -1,   170,    -1,    -1,    -1,   174,   175,   176,   177,
     178,    -1,    -1,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,     4,    -1,    -1,     7,     8,
       9,    -1,    -1,    12,    13,    14,    -1,    16,    -1,    -1,
      -1,    20,    -1,    22,    -1,    24,    25,    26,    27,    -1,
      -1,    -1,    31,    32,    33,    -1,    35,    36,    37,    38,
      -1,    -1,    -1,    42,    43,    -1,    -1,    -1,    47,    -1,
      -1,    50,    -1,    52,    -1,    -1,    -1,    56,    57,    58,
      59,    60,    61,    -1,    -1,    -1,    65,    66,    67,    68,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    76,    77,    78,
      -1,    -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,
      89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,
      -1,    -1,   101,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,    -1,   114,   115,   116,   117,   118,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,
      -1,   130,   131,   132,   133,   134,   135,   136,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   161,   162,   163,    -1,    -1,   166,   167,    -1,
      -1,   170,    -1,    -1,    -1,   174,   175,   176,   177,   178,
      -1,    -1,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,   191,   192,     4,    -1,    -1,     7,     8,     9,
      -1,    -1,    12,    13,    14,    -1,    16,    -1,    -1,    -1,
      20,    -1,    22,    -1,    24,    25,    26,    27,    -1,    -1,
      -1,    31,    32,    33,    -1,    35,    36,    37,    38,    -1,
      -1,    -1,    42,    43,    -1,    -1,    -1,    47,    -1,    -1,
      50,    -1,    52,    -1,    -1,    -1,    56,    57,    58,    59,
      60,    61,    -1,    -1,    -1,    65,    66,    67,    68,    69,
      -1,    -1,    -1,    -1,    -1,    -1,    76,    77,    78,    -1,
      -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,    89,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,    -1,   114,   115,   116,   117,   118,   119,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,
     130,   131,   132,   133,   134,   135,   136,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   161,   162,   163,    -1,    -1,   166,   167,    -1,    -1,
     170,    -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,
      -1,   181,   182,   183,   184,   185,   186,   187,   188,   189,
     190,   191,   192,     4,    -1,    -1,     7,     8,     9,    -1,
      -1,    12,    13,    14,    -1,    16,    -1,    -1,    -1,    20,
      -1,    22,    -1,    24,    25,    26,    27,    -1,    -1,    -1,
      31,    32,    33,    -1,    35,    36,    37,    38,    -1,    -1,
      -1,    42,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,
      -1,    52,    -1,    -1,    -1,    56,    57,    58,    59,    60,
      61,    -1,    -1,    -1,    65,    66,    67,    68,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    76,    77,    78,    -1,    -1,
      81,    -1,    83,    84,    -1,    86,    87,    -1,    89,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,
     101,    -1,    -1,   104,   105,   106,   107,   108,   109,   110,
     111,   112,    -1,   114,   115,   116,   117,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,   130,
     131,   132,   133,   134,   135,   136,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     161,   162,   163,    -1,    -1,   166,   167,    -1,    -1,   170,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
     191,   192,     4,    -1,    -1,     7,     8,     9,    -1,    -1,
      12,    13,    14,    -1,    16,    -1,    -1,    -1,    20,    -1,
      22,    -1,    24,    25,    26,    27,    -1,    -1,    -1,    31,
      32,    33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,
      42,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,
      52,    -1,    -1,    -1,    56,    57,    58,    59,    60,    61,
      -1,    -1,    -1,    65,    66,    67,    68,    69,    -1,    -1,
      -1,    -1,    -1,    -1,    76,    77,    78,    -1,    -1,    81,
      -1,    83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,   101,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,    -1,   114,   115,   116,   117,   118,   119,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   128,    -1,   130,   131,
     132,   133,   134,   135,   136,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,
     162,   163,    -1,    -1,   166,   167,    -1,    -1,   170,    -1,
      -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,     4,    -1,    -1,     7,     8,     9,    -1,    -1,    12,
      13,    14,    -1,    16,    -1,    -1,    -1,    20,    -1,    22,
      -1,    24,    25,    26,    27,    -1,    -1,    -1,    31,    32,
      33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    52,
      -1,    -1,    -1,    56,    57,    58,    59,    60,    61,    -1,
      -1,    -1,    65,    66,    67,    68,    69,    -1,    -1,    -1,
      -1,    -1,    -1,    76,    77,    78,    -1,    -1,    81,    -1,
      83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,    -1,    -1,
      -1,   104,   105,   106,   107,   108,   109,   110,   111,   112,
      -1,   114,   115,   116,   117,   118,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   128,    -1,   130,   131,   132,
     133,   134,   135,   136,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,
     163,    -1,    -1,   166,   167,    -1,    -1,   170,    -1,    -1,
      -1,   174,   175,   176,   177,   178,    -1,    -1,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,   191,   192,
       4,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,    13,
      -1,    -1,    16,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    26,    27,    -1,    -1,    -1,    31,    32,    33,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,
      -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    58,    -1,    60,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    77,    -1,    -1,    -1,    81,    -1,    83,
      84,    -1,    86,    87,    -1,    89,    90,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    98,    -1,    -1,   101,    -1,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,    -1,
      -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    13,    -1,   128,    16,   130,    -1,    -1,    -1,
      -1,   135,   136,    -1,    -1,    26,    27,    -1,    -1,    -1,
      31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,
      -1,    -1,   166,   167,    -1,    -1,   170,    58,    -1,    60,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    77,   191,   192,    -1,
      81,    -1,    83,    84,    -1,    86,    87,    -1,    89,    90,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,
     101,    -1,   103,   104,   105,   106,   107,   108,   109,   110,
     111,   112,    -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,
      -1,     9,    -1,    -1,    -1,    13,    -1,   128,    16,   130,
      -1,    -1,    -1,    -1,   135,   136,    -1,    -1,    26,    27,
      -1,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,
      -1,    -1,    50,    -1,    -1,   166,   167,    -1,    -1,   170,
      58,    -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,
     191,   192,    -1,    81,    -1,    83,    84,    -1,    86,    87,
      -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      98,    -1,    -1,   101,    -1,    -1,   104,   105,   106,   107,
     108,   109,   110,   111,   112,    -1,    -1,    -1,    -1,     4,
      -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,    13,    -1,
     128,    16,   130,    -1,    -1,    -1,    -1,   135,   136,    -1,
      -1,    26,    27,    -1,    -1,    -1,    31,    32,    33,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    47,    -1,    -1,    50,    -1,    -1,   166,   167,
      -1,    -1,   170,    58,    -1,    60,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    77,   191,   192,    -1,    81,    -1,    83,    84,
      -1,    86,    87,    -1,    89,    -1,    -1,    -1,    93,    -1,
      -1,    -1,    -1,    98,    -1,    -1,    -1,    -1,    -1,   104,
     105,   106,   107,   108,   109,   110,   111,   112,    -1,    -1,
      -1,    -1,     4,    -1,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    13,    -1,   128,    16,   130,    -1,    -1,    -1,    -1,
     135,   136,    -1,    -1,    26,    27,    -1,    -1,    -1,    31,
      32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,
      -1,   166,   167,    -1,    -1,   170,    58,    -1,    60,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    77,   191,   192,    -1,    81,
      -1,    83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    98,    99,    -1,    -1,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,    -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,
       9,    -1,    -1,    -1,    13,    -1,   128,    16,   130,    -1,
      -1,    -1,    -1,   135,   136,    -1,    -1,    26,    27,    -1,
      -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    -1,
      -1,    50,    -1,    -1,   166,   167,    -1,    -1,   170,    58,
      -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,   191,
     192,    -1,    81,    -1,    83,    84,    85,    86,    87,    -1,
      89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,
      -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,    -1,    -1,    -1,    -1,     4,    -1,
      -1,    -1,    -1,     9,    -1,    -1,    -1,    13,    -1,   128,
      16,   130,    -1,    -1,    -1,    -1,   135,   136,    -1,    -1,
      26,    27,    -1,    -1,    -1,    31,    32,    33,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,
      -1,    47,    -1,    -1,    50,    -1,    -1,   166,   167,    -1,
      -1,   170,    58,    -1,    60,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    77,   191,   192,    -1,    81,    -1,    83,    84,    -1,
      86,    87,    -1,    89,    -1,    -1,    92,    -1,    -1,    -1,
      -1,    -1,    98,    -1,    -1,    -1,    -1,    -1,   104,   105,
     106,   107,   108,   109,   110,   111,   112,    -1,    -1,    -1,
      -1,     4,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,
      13,    -1,   128,    16,   130,    -1,    -1,    -1,    -1,   135,
     136,    -1,    -1,    26,    27,    -1,    -1,    -1,    31,    32,
      33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    -1,
     166,   167,    -1,    -1,   170,    58,    -1,    60,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    77,   191,   192,    -1,    81,    -1,
      83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,    -1,    -1,
     103,   104,   105,   106,   107,   108,   109,   110,   111,   112,
      -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    13,    -1,   128,    16,   130,    -1,    -1,
      -1,    -1,   135,   136,    -1,    -1,    26,    27,    -1,    -1,
      -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    -1,    -1,
      50,    -1,    -1,   166,   167,    -1,    -1,   170,    58,    -1,
      60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,   191,   192,
      -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,    89,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,
      -1,   101,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,    -1,    -1,    -1,    -1,     4,    -1,    -1,
      -1,    -1,     9,    -1,    -1,    -1,    13,    -1,   128,    16,
     130,    -1,    -1,    -1,    -1,   135,   136,    -1,    -1,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,
      47,    -1,    -1,    50,    -1,    -1,   166,   167,    -1,    -1,
     170,    58,    -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      77,   191,   192,    -1,    81,    -1,    83,    84,    85,    86,
      87,    -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    98,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,
     107,   108,   109,   110,   111,   112,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,
       9,   128,    -1,   130,    13,    -1,    15,    16,   135,   136,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    26,    27,    -1,
      -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,   166,
     167,    50,    -1,   170,    -1,    -1,    -1,    -1,    -1,    58,
      -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   191,   192,    -1,    -1,    77,    -1,
      -1,    -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,
      89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,
      -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,    -1,    -1,    -1,    -1,     4,    -1,
      -1,    -1,    -1,     9,    -1,    -1,    -1,    13,    -1,   128,
      16,   130,    -1,    -1,    -1,    -1,   135,   136,    -1,    -1,
      26,    27,    -1,    -1,    -1,    31,    32,    33,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,
      -1,    47,    -1,    -1,    50,    -1,    -1,   166,   167,    -1,
      -1,   170,    58,    -1,    60,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    77,   191,   192,    -1,    81,    -1,    83,    84,    85,
      86,    87,    -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    98,    -1,    -1,    -1,    -1,    -1,   104,   105,
     106,   107,   108,   109,   110,   111,   112,    -1,    -1,    -1,
      -1,     4,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,
      13,    -1,   128,    16,   130,    -1,    -1,    -1,    -1,   135,
     136,    -1,    -1,    26,    27,    -1,    -1,    -1,    31,    32,
      33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    -1,
     166,   167,    -1,    -1,   170,    58,    -1,    60,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    77,   191,   192,    -1,    81,    -1,
      83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    98,    -1,    -1,    -1,    -1,
      -1,   104,   105,   106,   107,   108,   109,   110,   111,   112,
      -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,     9,
      -1,    -1,    -1,    13,    -1,   128,    16,   130,    -1,    -1,
      -1,    -1,   135,   136,    -1,    -1,    26,    27,    -1,    -1,
      -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    -1,    -1,
      50,    -1,    -1,   166,   167,    -1,    -1,   170,    58,    -1,
      60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,   191,   192,
      -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,    89,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    98,    -1,
      -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,   109,
     110,   111,   112,    -1,    -1,    -1,    -1,     4,    -1,    -1,
      -1,    -1,     9,    -1,    -1,    -1,    13,    -1,   128,    16,
     130,    -1,    -1,    -1,    -1,   135,   136,    -1,    -1,    26,
      27,    -1,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,
      47,    -1,    -1,    50,    -1,    -1,   166,   167,    -1,    -1,
     170,    58,    -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      77,   191,   192,    -1,    81,    -1,    83,    84,    -1,    86,
      87,    -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    98,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,
     107,   108,   109,   110,   111,   112,    -1,    -1,    -1,    -1,
       4,    -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,    13,
      -1,   128,    16,   130,    -1,    -1,    -1,    -1,   135,   136,
      -1,    -1,    26,    27,    -1,    -1,    -1,    31,    32,    33,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,
      -1,    -1,    -1,    47,    -1,    -1,    50,    -1,    -1,   166,
     167,    -1,    -1,   170,    58,    -1,    60,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    77,   191,   192,    -1,    81,    -1,    83,
      84,    -1,    86,    87,    -1,    89,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     104,   105,   106,   107,   108,   109,   110,   111,   112,    -1,
      -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,     9,    -1,
      -1,    -1,    13,    -1,   128,    16,   130,    -1,    -1,    -1,
      -1,   135,   136,    -1,    -1,    26,    27,    -1,    -1,    -1,
      31,    32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,
      -1,    -1,   166,   167,    -1,    -1,   170,    58,    -1,    60,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    77,   191,   192,    -1,
      81,    -1,    83,    84,    -1,    86,    87,    -1,    89,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   104,   105,   106,   107,   108,   109,   110,
     111,   112,    -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,
      -1,     9,    -1,    -1,    -1,    13,    -1,   128,    16,   130,
      -1,    -1,    -1,    -1,   135,   136,    -1,    -1,    26,    27,
      -1,    -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,
      -1,    -1,    50,    -1,    -1,   166,   167,    -1,    -1,   170,
      58,    -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,
     191,   192,    -1,    81,    -1,    83,    84,    -1,    86,    87,
      -1,    89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,
     108,   109,   110,   111,   112,    -1,    -1,    -1,    -1,     4,
      -1,    -1,    -1,    -1,     9,    -1,    -1,    -1,    13,    -1,
     128,    16,   130,    -1,    -1,    -1,    -1,   135,   136,    -1,
      -1,    26,    27,    -1,    -1,    -1,    31,    32,    33,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    43,    -1,
      -1,    -1,    47,    -1,    -1,    50,    -1,    -1,   166,   167,
      -1,    -1,   170,    58,    -1,    60,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    77,   191,   192,    -1,    81,    -1,    83,    84,
      -1,    86,    87,    -1,    89,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   104,
     105,   106,   107,   108,   109,   110,   111,   112,    -1,    -1,
      -1,    -1,     4,    -1,    -1,    -1,    -1,     9,    -1,    -1,
      -1,    13,    -1,   128,    16,   130,    -1,    -1,    -1,    -1,
     135,   136,    -1,    -1,    26,    27,    -1,    -1,    -1,    31,
      32,    33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    43,    -1,    -1,    -1,    47,    -1,    -1,    50,    -1,
      -1,   166,   167,    -1,    -1,   170,    58,    -1,    60,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    77,   191,   192,    -1,    81,
      -1,    83,    84,    -1,    86,    87,    -1,    89,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   104,   105,   106,   107,   108,   109,   110,   111,
     112,    -1,    -1,    -1,    -1,     4,    -1,    -1,    -1,    -1,
       9,    -1,    -1,    -1,    13,    -1,   128,    16,   130,    -1,
      -1,    -1,    -1,   135,   136,    -1,    -1,    26,    27,    -1,
      -1,    -1,    31,    32,    33,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    43,    -1,    -1,    -1,    47,    -1,
      -1,    50,    -1,    -1,   166,   167,    -1,    -1,   170,    58,
      -1,    60,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    77,   191,
     192,    -1,    81,    -1,    83,    84,    -1,    86,    87,    -1,
      89,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   104,   105,   106,   107,   108,
     109,   110,   111,   112,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   128,
      -1,   130,    -1,    -1,    -1,    -1,   135,   136,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,     6,     7,     8,    -1,    -1,
      -1,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    24,    25,    -1,    -1,   166,   167,    -1,
      -1,   170,    33,    -1,    35,    36,    37,    38,    -1,    -1,
      -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,   191,   192,    -1,    56,    57,    -1,    59,    -1,
      61,    -1,    -1,    -1,    65,    66,    67,    68,    69,    -1,
      -1,    72,    73,    74,    75,    76,    -1,    78,    79,    -1,
      -1,    -1,    -1,    84,    -1,    86,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   110,
      -1,    -1,   113,   114,   115,   116,   117,   118,   119,   120,
     121,   122,   123,   124,   125,   126,   127,    -1,    -1,   130,
     131,   132,   133,   134,    -1,    -1,    -1,    -1,   139,   140,
      -1,    -1,    -1,   144,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   153,   154,   155,    -1,   157,   158,   159,    -1,
     161,   162,   163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
       6,     7,     8,    -1,    -1,    -1,    12,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    24,    25,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,    35,
      36,    37,    38,    -1,    -1,    -1,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,
      56,    57,    -1,    59,    -1,    61,    -1,    -1,    -1,    65,
      66,    67,    68,    69,    -1,    -1,    72,    73,    74,    75,
      76,    -1,    78,    79,    -1,    -1,    -1,    -1,    84,    -1,
      86,    -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   110,    -1,    -1,   113,   114,   115,
     116,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   130,   131,   132,   133,   134,    -1,
      -1,    -1,    -1,   139,   140,    -1,    -1,    -1,   144,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,   175,
     176,   177,   178,    -1,    -1,   181,   182,   183,   184,   185,
     186,   187,   188,   189,   190,     6,     7,     8,    -1,    -1,
      -1,    12,    -1,    14,    -1,    -1,    -1,    -1,    -1,    20,
      -1,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    33,    -1,    35,    36,    37,    38,    -1,    -1,
      -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      61,    -1,    -1,    -1,    65,    66,    67,    68,    69,    -1,
      -1,    72,    73,    74,    75,    76,    -1,    78,    79,    -1,
      -1,    -1,    -1,    84,    -1,    86,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   110,
      -1,    -1,   113,   114,   115,   116,   117,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   130,
     131,   132,   133,   134,    -1,    -1,    -1,    -1,   139,   140,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     161,   162,   163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
       7,     8,    -1,    -1,    -1,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    24,    25,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,    35,    36,
      37,    38,    -1,    -1,    -1,    42,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,    56,
      57,    -1,    59,    -1,    61,    -1,    -1,    -1,    65,    66,
      67,    68,    69,    -1,    -1,    -1,    -1,    -1,    -1,    76,
      -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   103,    -1,    -1,    -1,
      -1,    -1,    -1,   110,    -1,    -1,    -1,   114,   115,   116,
     117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   131,   132,   133,   134,    -1,    -1,
      -1,    -1,   139,   140,    -1,    -1,    -1,   144,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,   175,   176,
     177,   178,    -1,    -1,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     7,     8,    -1,    -1,    -1,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    24,    25,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    52,
      -1,    -1,    -1,    56,    57,    -1,    59,    -1,    61,    -1,
      -1,    -1,    65,    66,    67,    68,    69,    -1,    -1,    -1,
      -1,    -1,    -1,    76,    -1,    78,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      93,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     103,    -1,    -1,    -1,    -1,    -1,    -1,   110,    -1,    -1,
      -1,   114,   115,   116,   117,   118,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   131,   132,
     133,   134,    -1,    -1,    -1,    -1,   139,   140,    -1,    -1,
      -1,   144,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,
     163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   174,   175,   176,   177,   178,    -1,    -1,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,     7,     8,
      -1,    -1,    -1,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    33,    -1,    35,    36,    37,    38,
      -1,    -1,    -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    52,    -1,    -1,    -1,    56,    57,    -1,
      59,    -1,    61,    -1,    -1,    -1,    65,    66,    67,    68,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    93,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   110,    -1,    -1,    -1,   114,   115,   116,   117,   118,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   131,   132,   133,   134,    -1,    -1,    -1,    -1,
     139,   140,    -1,    -1,    -1,   144,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   161,   162,   163,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   174,   175,   176,   177,   178,
      -1,    -1,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,     7,     8,    -1,    -1,    -1,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    24,
      25,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,
      35,    36,    37,    38,    -1,    -1,    -1,    42,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,
      -1,    56,    57,    -1,    59,    -1,    61,    -1,    -1,    -1,
      65,    66,    67,    68,    69,    -1,    -1,    -1,    -1,    -1,
      -1,    76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,
      85,    -1,    -1,    88,    -1,    -1,    -1,    -1,    -1,    -1,
       7,     8,    -1,    -1,    -1,    12,    -1,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    24,    25,   114,
     115,   116,   117,   118,   119,    -1,    33,    -1,    35,    36,
      37,    38,    -1,    -1,    -1,    42,   131,   132,   133,   134,
      -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,    56,
      57,    -1,    59,    -1,    61,    -1,    -1,    -1,    65,    66,
      67,    68,    69,    -1,    -1,    -1,   161,   162,   163,    76,
      -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,    85,   174,
     175,   176,   177,   178,    -1,    -1,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,   116,
     117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   131,   132,   133,   134,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,   175,   176,
     177,   178,    -1,    -1,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,     7,     8,    -1,    -1,    -1,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    24,    25,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,    42,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    52,
      -1,    -1,    -1,    56,    57,    -1,    59,    -1,    61,    -1,
      -1,    -1,    65,    66,    67,    68,    69,    -1,    -1,    -1,
      -1,    -1,    -1,    76,    -1,    78,    -1,    -1,    -1,    -1,
      -1,    -1,    85,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     7,     8,    -1,    -1,    -1,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    24,
      25,   114,   115,   116,   117,   118,   119,    -1,    33,    -1,
      35,    36,    37,    38,    -1,    -1,    -1,    42,   131,   132,
     133,   134,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,
      -1,    56,    57,    -1,    59,    -1,    61,    -1,    -1,    64,
      65,    66,    67,    68,    69,    -1,    -1,    -1,   161,   162,
     163,    76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   174,   175,   176,   177,   178,    -1,    -1,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,
     115,   116,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   131,   132,   133,   134,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   161,   162,   163,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,
     175,   176,   177,   178,    -1,    -1,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,     7,     8,    -1,    -1,
      -1,    12,    -1,    14,    -1,    -1,    17,    -1,    -1,    20,
      -1,    22,    -1,    24,    25,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    33,    -1,    35,    36,    37,    38,    -1,    -1,
      -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      61,    -1,    -1,    -1,    65,    66,    67,    68,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    76,    -1,    78,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,     7,     8,    -1,    -1,    -1,    12,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    20,    -1,    22,
      -1,    24,    25,   114,   115,   116,   117,   118,   119,    -1,
      33,    -1,    35,    36,    37,    38,    -1,    -1,    -1,    42,
     131,   132,   133,   134,    -1,    -1,    -1,    -1,    -1,    52,
      -1,    -1,    -1,    56,    57,    -1,    59,    -1,    61,    -1,
      -1,    -1,    65,    66,    67,    68,    69,    -1,    -1,    -1,
     161,   162,   163,    76,    -1,    78,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,
     181,   182,   183,   184,   185,   186,   187,   188,   189,   190,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   114,   115,   116,   117,   118,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   131,   132,
     133,   134,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,
     163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   174,   175,   176,   177,   178,    -1,    -1,   181,   182,
     183,   184,   185,   186,   187,   188,   189,   190,     7,     8,
      -1,    -1,    -1,    12,    -1,    14,    -1,    -1,    -1,    -1,
      -1,    20,    -1,    22,    -1,    24,    25,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    33,    -1,    35,    36,    37,    38,
      -1,    -1,    -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    52,    -1,    -1,    -1,    56,    57,    -1,
      59,    -1,    61,    -1,    -1,    -1,    65,    66,    67,    68,
      69,    -1,    -1,    -1,    -1,    -1,    -1,    76,    -1,    78,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   114,   115,   116,   117,   118,
     119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   131,   132,   133,   134,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   161,   162,   163,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   174,   175,   176,   177,   178,
      -1,    -1,   181,   182,   183,   184,   185,   186,   187,   188,
     189,   190,     7,     8,    -1,    -1,    -1,    12,    -1,    14,
      -1,    -1,    -1,    -1,    -1,    20,    -1,    22,    -1,    24,
      25,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,
      35,    36,    37,    38,    -1,    -1,    -1,    42,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,
      -1,    56,    57,    -1,    59,    -1,    61,    -1,    -1,    -1,
      65,    66,    67,    68,    69,    -1,    -1,    -1,    -1,    -1,
      -1,    76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
       7,     8,    -1,    -1,    -1,    12,   101,    14,    -1,    -1,
      -1,    -1,    -1,    20,    -1,    22,    -1,    24,    25,   114,
     115,   116,   117,    -1,   119,    -1,    33,    -1,    35,    36,
      37,    38,    -1,    -1,    -1,    42,   131,   132,   133,   134,
      -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,    56,
      57,    -1,    59,    -1,    61,    -1,    -1,    -1,    65,    66,
      67,    68,    69,    -1,    -1,    -1,   161,   162,   163,    76,
      -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,
     175,   176,   177,   178,    -1,    -1,   181,   182,   183,   184,
     185,    -1,   187,   188,   189,   190,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,   116,
     117,    -1,   119,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   131,   132,   133,   134,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   174,   175,   176,
     177,   178,    -1,    -1,   181,   182,   183,   184,   185,    -1,
     187,   188,   189,   190,     3,     4,     5,     6,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    18,
      -1,    -1,    -1,    -1,    23,    -1,    -1,    -1,    -1,    -1,
      -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      39,    40,    41,    -1,    43,    44,    45,    46,    -1,    48,
      49,    50,    51,    -1,    -1,    54,    55,    -1,    -1,    -1,
      -1,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    71,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    82,    83,    84,    85,    86,    87,    88,
      89,    90,    91,    -1,    93,    94,    95,    96,    97,    98,
      -1,   100,    -1,   102,    -1,     3,     4,     5,     6,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   118,
      18,    -1,    -1,    -1,    -1,    23,    -1,    -1,    -1,    -1,
      -1,    -1,    30,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     139,    39,    40,    41,    -1,    43,    44,    45,    46,    -1,
      48,    49,    50,    51,    -1,    -1,    54,    55,    -1,    -1,
      -1,    -1,    -1,    -1,    62,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    71,    -1,    -1,    -1,    -1,    -1,    -1,
     179,    -1,    -1,    -1,    82,    83,    84,   186,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,     7,   100,    -1,   102,    -1,    -1,    -1,    14,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    24,    -1,
     118,    -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,    35,
      -1,    -1,    -1,    -1,    -1,    -1,    42,    -1,    -1,    -1,
      -1,   139,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,
      56,    57,    -1,    59,    -1,    -1,    -1,    -1,    -1,    65,
      -1,    67,    -1,    69,    -1,    -1,    -1,    -1,    74,    75,
      76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    84,    -1,
      86,   179,    -1,    -1,    -1,    -1,    92,    -1,   186,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,
      -1,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   130,    -1,    -1,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     7,    -1,   174,   175,
     176,   177,   178,    14,    -1,   181,   182,   183,   184,   185,
     186,    -1,    -1,    24,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    33,    -1,    35,    -1,    -1,    -1,    -1,    -1,
      -1,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    65,    -1,    67,    -1,    69,    -1,
      -1,    -1,    -1,    74,    75,    76,    -1,    78,    -1,    -1,
      -1,    -1,    -1,    84,    -1,    86,    -1,    -1,    -1,    -1,
      -1,    92,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   114,   115,    -1,   117,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   130,
      -1,    -1,   133,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     161,   162,   163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     7,    -1,   174,   175,   176,   177,   178,    14,    -1,
     181,   182,   183,   184,   185,   186,    -1,    -1,    24,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    33,    -1,    35,
      -1,    -1,    -1,    -1,    -1,    -1,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,
      56,    57,    -1,    59,    -1,    -1,    -1,    -1,    -1,    65,
      -1,    67,    -1,    69,    -1,    -1,    -1,    -1,    74,    75,
      76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    84,    -1,
      86,    -1,    -1,    -1,    -1,    -1,    92,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,
      -1,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   130,    -1,    -1,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     7,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    24,    -1,    -1,    -1,    -1,   174,   175,
     176,   177,   178,    -1,    35,   181,   182,   183,   184,   185,
     186,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    65,    -1,    67,    -1,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    76,    -1,    78,    -1,    -1,
      -1,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   114,   115,    -1,   117,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   133,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     7,    -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,
     161,   162,   163,    -1,    -1,    -1,    -1,    -1,    24,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    35,
     181,   182,   183,   184,   185,   186,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,
      56,    57,    -1,    59,    -1,    -1,    -1,    -1,    -1,    65,
      -1,    67,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,
      76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,    85,
      -1,    -1,    88,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,
      -1,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     7,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    24,    -1,    -1,    -1,    -1,   174,   175,
     176,   177,   178,    -1,    35,   181,   182,   183,   184,   185,
     186,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    65,    -1,    67,    -1,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    76,    -1,    78,    -1,    -1,
      -1,    -1,    -1,    -1,    85,    -1,    -1,    88,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   114,   115,    -1,   117,   118,   119,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   133,   134,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,     7,    -1,    -1,    -1,    -1,    -1,    -1,    14,    -1,
     161,   162,   163,    -1,    -1,    -1,    -1,    -1,    24,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    35,
     181,   182,   183,   184,   185,   186,    42,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    52,    -1,    -1,    -1,
      56,    57,    -1,    59,    -1,    -1,    -1,    -1,    -1,    65,
      -1,    67,    -1,    69,    -1,    -1,    -1,    -1,    -1,    -1,
      76,    -1,    78,    -1,    -1,    -1,    -1,    -1,    -1,    85,
      -1,    -1,    88,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   114,   115,
      -1,   117,   118,   119,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   133,   134,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,     7,    -1,    -1,    -1,
      -1,    -1,    -1,    14,    -1,   161,   162,   163,    -1,    -1,
      -1,    -1,    -1,    24,    -1,    -1,    -1,    -1,   174,   175,
     176,   177,   178,    -1,    35,   181,   182,   183,   184,   185,
     186,    42,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    52,    -1,    -1,    -1,    56,    57,    -1,    59,    -1,
      -1,    -1,    -1,    -1,    65,    -1,    67,    -1,    69,    -1,
      -1,    -1,    -1,    -1,    -1,    76,    -1,    78,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    93,    -1,     7,    -1,    -1,    -1,    -1,    -1,
      -1,    14,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    24,    -1,   114,   115,    -1,   117,   118,   119,    -1,
      -1,    -1,    35,    -1,    -1,    -1,    -1,    -1,    -1,    42,
      -1,    -1,   133,   134,    -1,    -1,    -1,    -1,    -1,    52,
      -1,    -1,    -1,    56,    57,    -1,    59,    -1,    -1,    -1,
      -1,    -1,    65,    -1,    67,    -1,    69,    -1,    -1,    -1,
     161,   162,   163,    76,    -1,    78,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   174,   175,   176,   177,   178,    -1,    -1,
     181,   182,   183,   184,   185,   186,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   114,   115,    -1,   117,   118,   119,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     133,   134,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   161,   162,
     163,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   174,   175,   176,   177,   178,    -1,    -1,   181,   182,
     183,   184,   185,   186
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned short int yystos[] =
{
       0,     6,     7,     8,    12,    14,    20,    22,    24,    25,
      33,    35,    36,    37,    38,    42,    52,    56,    57,    59,
      61,    65,    66,    67,    68,    69,    72,    73,    74,    75,
      76,    78,    79,    84,    86,    93,   110,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   130,   131,   132,   133,   134,   139,   140,   144,
     153,   154,   155,   157,   158,   159,   161,   162,   163,   174,
     175,   176,   177,   178,   181,   182,   183,   184,   185,   186,
     187,   188,   189,   190,   194,   195,   196,   197,   198,   202,
     203,   208,   211,   216,   217,   221,   223,   224,   225,   226,
     227,   228,   229,   230,   231,   233,   234,   235,   236,   237,
     238,   239,   242,   247,   248,   250,   253,   254,   255,   262,
     263,   264,   265,   267,   268,   286,   287,   290,   291,   292,
     320,   321,   345,   346,   351,   352,   357,   360,   361,   362,
     374,   398,   399,   406,   412,   413,    69,   212,    84,    84,
      84,   205,   206,   207,   208,   246,   249,   255,   264,   345,
     413,    76,   261,   262,   197,   286,   204,   345,    84,    84,
       4,     9,    13,    16,    26,    27,    31,    32,    43,    47,
      50,    58,    60,    77,    81,    83,    84,    86,    87,    89,
      98,   104,   105,   106,   107,   108,   109,   110,   111,   112,
     128,   135,   136,   166,   167,   170,   191,   192,   315,   316,
     320,   322,   325,   326,   327,   328,   329,   330,   331,   332,
     333,   334,   335,   336,   337,   338,   339,   340,   343,   344,
     348,   355,   356,   407,   324,   339,    10,    11,    15,    17,
      19,    28,    29,    34,    53,    63,    70,    84,    86,    93,
     101,   110,   129,   137,   138,   144,   165,   168,   169,   172,
     173,   199,   200,   201,   202,   217,   286,   300,   301,   302,
     303,   304,   305,   306,   307,   308,   310,   314,   320,   340,
     345,   349,   358,   359,   361,   364,   402,   195,    93,   110,
     144,   221,   223,   293,   294,   345,   360,   361,   363,   374,
     224,   229,   231,   232,   234,   235,   237,   271,   232,   347,
     320,    84,    84,   142,   143,   145,   146,   147,   365,   366,
       4,    49,    83,    86,    87,    89,   100,   102,   345,   375,
     376,   377,   379,   320,   384,   101,   339,   385,   386,   387,
     264,   394,   393,   394,   340,    98,   400,   401,    84,     0,
     196,   239,   250,    93,   225,   229,   230,   224,   224,    93,
      88,     6,   206,   243,   244,   245,   360,   246,   249,    84,
      98,   252,   252,    98,   252,   101,   207,   218,   219,   220,
     226,   229,   288,   289,   305,   101,   206,   207,   345,    84,
     271,   337,   338,   342,   205,   248,   253,   263,    85,    85,
      85,   262,    88,    93,   271,   340,    84,   345,    84,   325,
     325,    84,   325,    84,   271,   305,   340,   340,    84,    84,
      84,    84,    84,   325,   327,   327,   327,    84,    84,    84,
      84,   325,    84,   325,    84,   325,    84,    84,    43,    50,
      51,    84,    90,    98,   179,   408,   410,   327,    82,    86,
      91,    87,    89,    39,    54,    30,    41,    94,    96,    23,
      46,    83,   100,   102,     4,     3,     5,    18,    40,    44,
      45,    48,    49,    55,    62,    71,    95,    97,   341,    88,
      60,    84,   325,   354,   356,    88,    93,   342,    93,    92,
     300,    84,    98,    86,   345,    84,    93,   340,    84,    84,
      84,   208,   413,   103,   306,   200,   286,    84,    84,   366,
     340,   403,    93,    84,   403,   403,   300,    93,    92,   294,
     366,    84,    92,   247,   248,   264,   295,   297,   345,    93,
     294,   229,   224,    84,    98,   248,   252,   273,   274,    88,
      60,   353,   353,    84,    84,   151,   367,   368,   369,    84,
     367,   196,    92,    98,   386,   388,    98,   101,   395,   396,
     397,    88,   395,   395,    86,    99,   342,   401,   345,   414,
     415,   416,    93,   224,   224,   207,   240,   241,   246,    84,
      95,   280,   206,   206,    85,   221,   222,   224,   229,   251,
     275,   276,   277,   278,   279,   345,    59,    86,   257,   259,
     260,   261,   339,   257,   259,   260,   269,   270,   345,   266,
     345,   239,   226,   229,   224,   218,   289,   103,   293,   345,
     345,   344,    85,    88,   246,   249,   345,    85,    85,    14,
      85,   209,   210,   345,   271,   271,   339,    85,    85,    85,
      99,   339,   271,   339,   271,   271,   350,   353,   339,   339,
     271,   271,   271,   340,   339,   320,    85,   324,   320,   311,
     340,    84,   339,   409,   327,   327,   327,   328,   328,   329,
     329,   330,   330,   330,   330,   331,   331,   332,   333,   334,
     335,   336,    92,   340,   339,   339,   271,   339,    64,    92,
     300,    70,    84,   217,   221,   303,   313,   351,   340,    93,
     309,   340,    93,   309,   309,   103,   350,   340,   300,    93,
     313,    93,    93,   300,   294,   342,   207,    93,    88,   207,
     224,   207,   272,   257,   259,   260,   272,    98,   252,   232,
      85,    85,   345,   152,   370,   371,   372,   373,    84,   145,
      88,   369,   370,   145,   232,   378,   380,   340,   383,    88,
      88,   103,   339,   103,   340,   397,   394,    99,    99,    84,
      85,    88,   248,    88,   243,   344,   101,   281,   323,   339,
      84,   247,   248,   273,   207,   229,   224,    85,    88,    88,
     258,   259,   256,   257,    99,   258,   256,    99,    88,   103,
      95,    93,   224,   103,    85,    92,   344,    85,    85,    88,
      84,    85,    85,    88,   323,   327,    88,    88,    88,    88,
      85,    85,    88,    88,    85,    85,    85,    85,    85,    88,
      85,    92,    93,    99,    85,   324,    88,   180,   411,   338,
      92,    85,   342,   300,    84,   309,   312,    99,    93,    85,
      85,    85,    85,    85,   403,    95,   299,   207,   246,   296,
     298,   345,    92,   248,   274,    85,   258,   256,    99,   257,
     259,   260,    85,    85,   372,    85,   370,   369,    85,    88,
      92,    99,    92,   345,   389,   389,    99,    92,   103,   156,
     395,   324,   416,   241,   280,    85,    90,    98,   103,   281,
     282,   283,   284,   285,   345,   207,   207,   207,   224,   345,
      64,   279,   260,   260,   260,   260,   103,   270,   342,    93,
      98,   213,   214,   215,   344,    85,    85,   210,   324,    17,
     271,   317,   318,   271,   319,   345,   339,   271,   353,   342,
     339,   311,   340,   340,    85,   339,   338,   327,    92,   340,
      93,    84,   300,   300,   300,    93,    93,   342,    92,   207,
      88,   207,   342,   272,   260,   260,   258,   256,    99,   143,
     367,    85,   367,   380,   340,   381,   340,   383,    95,   340,
     340,    85,   345,   338,   342,    88,   103,   281,    95,    98,
     285,    92,   248,    99,    99,    99,    99,   345,    85,    92,
      88,    84,    93,    85,    92,    92,    85,    88,    85,    85,
     284,    88,    85,    85,    85,    92,    99,    92,    99,    88,
     300,    85,   311,   313,    21,   403,   342,   299,   298,    92,
      99,    99,   260,   260,   145,   145,    92,    99,   339,   390,
     391,   392,   103,   103,    99,   103,   281,   283,   345,   281,
      99,    93,   213,   215,   340,   339,   339,   318,   339,   340,
     340,   339,    93,    85,   312,   300,    93,   342,    99,    99,
     320,   322,   382,    92,    93,   281,    92,   344,    85,    92,
      85,    85,    99,    99,    88,   300,    93,    15,   340,   404,
     405,   280,    84,   339,   339,   281,    84,    93,   213,   339,
     311,    85,    85,   324,    92,    92,   340,    85,    85,   300,
      85,   339,   339,    85,    93,   300
};


/* Prevent warning if -Wmissing-prototypes.  */
int yyparse (AST* parsed_tree);

/* Error token number */
#define YYTERROR 1

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */


#define YYRHSLOC(Rhs, K) ((Rhs)[K].yystate.yyloc)
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (YYID (N))							\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (YYID (0))

/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

# define YY_LOCATION_PRINT(File, Loc)			\
    fprintf (File, "%d.%d-%d.%d",			\
	     (Loc).first_line, (Loc).first_column,	\
	     (Loc).last_line,  (Loc).last_column)
#endif


#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */
#define YYLEX yylex ()

YYSTYPE yylval;

YYLTYPE yylloc;

int yynerrs;
int yychar;

static const int YYEOF = 0;
static const int YYEMPTY = -2;

typedef enum { yyok, yyaccept, yyabort, yyerr } YYRESULTTAG;

#define YYCHK(YYE)							     \
   do { YYRESULTTAG yyflag = YYE; if (yyflag != yyok) return yyflag; }	     \
   while (YYID (0))

#if YYDEBUG

# ifndef YYFPRINTF
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (YYID (0))


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

/*ARGSUSED*/
static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, AST* parsed_tree)
{
  if (!yyvaluep)
    return;
  YYUSE (yylocationp);
  YYUSE (parsed_tree);
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# else
  YYUSE (yyoutput);
# endif
  switch (yytype)
    {
      default:
	break;
    }
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep, YYLTYPE const * const yylocationp, AST* parsed_tree)
{
  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  YY_LOCATION_PRINT (yyoutput, *yylocationp);
  YYFPRINTF (yyoutput, ": ");
  yy_symbol_value_print (yyoutput, yytype, yyvaluep, yylocationp, parsed_tree);
  YYFPRINTF (yyoutput, ")");
}

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)			    \
do {									    \
  if (yydebug)								    \
    {									    \
      YYFPRINTF (stderr, "%s ", Title);					    \
      yy_symbol_print (stderr, Type,					    \
		       Value, Location, parsed_tree);  \
      YYFPRINTF (stderr, "\n");						    \
    }									    \
} while (YYID (0))

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;

#else /* !YYDEBUG */

# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)

#endif /* !YYDEBUG */

/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYMAXDEPTH * sizeof (GLRStackItem)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif

/* Minimum number of free items on the stack allowed after an
   allocation.  This is to allow allocation and initialization
   to be completed by functions that call yyexpandGLRStack before the
   stack is expanded, thus insuring that all necessary pointers get
   properly redirected to new data.  */
#define YYHEADROOM 2

#ifndef YYSTACKEXPANDABLE
# if (! defined __cplusplus \
      || (defined YYLTYPE_IS_TRIVIAL && YYLTYPE_IS_TRIVIAL \
	  && defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL))
#  define YYSTACKEXPANDABLE 1
# else
#  define YYSTACKEXPANDABLE 0
# endif
#endif

#if YYSTACKEXPANDABLE
# define YY_RESERVE_GLRSTACK(Yystack)			\
  do {							\
    if (Yystack->yyspaceLeft < YYHEADROOM)		\
      yyexpandGLRStack (Yystack);			\
  } while (YYID (0))
#else
# define YY_RESERVE_GLRSTACK(Yystack)			\
  do {							\
    if (Yystack->yyspaceLeft < YYHEADROOM)		\
      yyMemoryExhausted (Yystack);			\
  } while (YYID (0))
#endif


#if YYERROR_VERBOSE

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static size_t
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return strlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* !YYERROR_VERBOSE */

/** State numbers, as in LALR(1) machine */
typedef int yyStateNum;

/** Rule numbers, as in LALR(1) machine */
typedef int yyRuleNum;

/** Grammar symbol */
typedef short int yySymbol;

/** Item references, as in LALR(1) machine */
typedef short int yyItemNum;

typedef struct yyGLRState yyGLRState;
typedef struct yyGLRStateSet yyGLRStateSet;
typedef struct yySemanticOption yySemanticOption;
typedef union yyGLRStackItem yyGLRStackItem;
typedef struct yyGLRStack yyGLRStack;

struct yyGLRState {
  /** Type tag: always true.  */
  yybool yyisState;
  /** Type tag for yysemantics.  If true, yysval applies, otherwise
   *  yyfirstVal applies.  */
  yybool yyresolved;
  /** Number of corresponding LALR(1) machine state.  */
  yyStateNum yylrState;
  /** Preceding state in this stack */
  yyGLRState* yypred;
  /** Source position of the first token produced by my symbol */
  size_t yyposn;
  union {
    /** First in a chain of alternative reductions producing the
     *  non-terminal corresponding to this state, threaded through
     *  yynext.  */
    yySemanticOption* yyfirstVal;
    /** Semantic value for this state.  */
    YYSTYPE yysval;
  } yysemantics;
  /** Source location for this state.  */
  YYLTYPE yyloc;
};

struct yyGLRStateSet {
  yyGLRState** yystates;
  /** During nondeterministic operation, yylookaheadNeeds tracks which
   *  stacks have actually needed the current lookahead.  During deterministic
   *  operation, yylookaheadNeeds[0] is not maintained since it would merely
   *  duplicate yychar != YYEMPTY.  */
  yybool* yylookaheadNeeds;
  size_t yysize, yycapacity;
};

struct yySemanticOption {
  /** Type tag: always false.  */
  yybool yyisState;
  /** Rule number for this reduction */
  yyRuleNum yyrule;
  /** The last RHS state in the list of states to be reduced.  */
  yyGLRState* yystate;
  /** The lookahead for this reduction.  */
  int yyrawchar;
  YYSTYPE yyval;
  YYLTYPE yyloc;
  /** Next sibling in chain of options.  To facilitate merging,
   *  options are chained in decreasing order by address.  */
  yySemanticOption* yynext;
};

/** Type of the items in the GLR stack.  The yyisState field
 *  indicates which item of the union is valid.  */
union yyGLRStackItem {
  yyGLRState yystate;
  yySemanticOption yyoption;
};

struct yyGLRStack {
  int yyerrState;
  /* To compute the location of the error token.  */
  yyGLRStackItem yyerror_range[3];

  YYJMP_BUF yyexception_buffer;
  yyGLRStackItem* yyitems;
  yyGLRStackItem* yynextFree;
  size_t yyspaceLeft;
  yyGLRState* yysplitPoint;
  yyGLRState* yylastDeleted;
  yyGLRStateSet yytops;
};

#if YYSTACKEXPANDABLE
static void yyexpandGLRStack (yyGLRStack* yystackp);
#endif

static void yyFail (yyGLRStack* yystackp, AST* parsed_tree, const char* yymsg)
  __attribute__ ((__noreturn__));
static void
yyFail (yyGLRStack* yystackp, AST* parsed_tree, const char* yymsg)
{
  if (yymsg != NULL)
    yyerror (parsed_tree, yymsg);
  YYLONGJMP (yystackp->yyexception_buffer, 1);
}

static void yyMemoryExhausted (yyGLRStack* yystackp)
  __attribute__ ((__noreturn__));
static void
yyMemoryExhausted (yyGLRStack* yystackp)
{
  YYLONGJMP (yystackp->yyexception_buffer, 2);
}

#if YYDEBUG || YYERROR_VERBOSE
/** A printable representation of TOKEN.  */
static inline const char*
yytokenName (yySymbol yytoken)
{
  if (yytoken == YYEMPTY)
    return "";

  return yytname[yytoken];
}
#endif

/** Fill in YYVSP[YYLOW1 .. YYLOW0-1] from the chain of states starting
 *  at YYVSP[YYLOW0].yystate.yypred.  Leaves YYVSP[YYLOW1].yystate.yypred
 *  containing the pointer to the next state in the chain.  */
static void yyfillin (yyGLRStackItem *, int, int) __attribute__ ((__unused__));
static void
yyfillin (yyGLRStackItem *yyvsp, int yylow0, int yylow1)
{
  yyGLRState* s;
  int i;
  s = yyvsp[yylow0].yystate.yypred;
  for (i = yylow0-1; i >= yylow1; i -= 1)
    {
      YYASSERT (s->yyresolved);
      yyvsp[i].yystate.yyresolved = yytrue;
      yyvsp[i].yystate.yysemantics.yysval = s->yysemantics.yysval;
      yyvsp[i].yystate.yyloc = s->yyloc;
      s = yyvsp[i].yystate.yypred = s->yypred;
    }
}

/* Do nothing if YYNORMAL or if *YYLOW <= YYLOW1.  Otherwise, fill in
 * YYVSP[YYLOW1 .. *YYLOW-1] as in yyfillin and set *YYLOW = YYLOW1.
 * For convenience, always return YYLOW1.  */
static inline int yyfill (yyGLRStackItem *, int *, int, yybool)
     __attribute__ ((__unused__));
static inline int
yyfill (yyGLRStackItem *yyvsp, int *yylow, int yylow1, yybool yynormal)
{
  if (!yynormal && yylow1 < *yylow)
    {
      yyfillin (yyvsp, *yylow, yylow1);
      *yylow = yylow1;
    }
  return yylow1;
}

/** Perform user action for rule number YYN, with RHS length YYRHSLEN,
 *  and top stack item YYVSP.  YYLVALP points to place to put semantic
 *  value ($$), and yylocp points to place for location information
 *  (@$).  Returns yyok for normal return, yyaccept for YYACCEPT,
 *  yyerr for YYERROR, yyabort for YYABORT.  */
/*ARGSUSED*/ static YYRESULTTAG
yyuserAction (yyRuleNum yyn, int yyrhslen, yyGLRStackItem* yyvsp,
	      YYSTYPE* yyvalp,
	      YYLTYPE* YYOPTIONAL_LOC (yylocp),
	      yyGLRStack* yystackp
	      , AST* parsed_tree)
{
  yybool yynormal __attribute__ ((__unused__)) =
    (yystackp->yysplitPoint == NULL);
  int yylow;
  YYUSE (parsed_tree);
# undef yyerrok
# define yyerrok (yystackp->yyerrState = 0)
# undef YYACCEPT
# define YYACCEPT return yyaccept
# undef YYABORT
# define YYABORT return yyabort
# undef YYERROR
# define YYERROR return yyerrok, yyerr
# undef YYRECOVERING
# define YYRECOVERING() (yystackp->yyerrState != 0)
# undef yyclearin
# define yyclearin (yychar = YYEMPTY)
# undef YYFILL
# define YYFILL(N) yyfill (yyvsp, &yylow, N, yynormal)
# undef YYBACKUP
# define YYBACKUP(Token, Value)						     \
  return yyerror (parsed_tree, YY_("syntax error: cannot back up")),     \
	 yyerrok, yyerr

  yylow = 1;
  if (yyrhslen == 0)
    *yyvalp = yyval_default;
  else
    *yyvalp = yyvsp[YYFILL (1-yyrhslen)].yystate.yysemantics.yysval;
  YYLLOC_DEFAULT ((*yylocp), (yyvsp - yyrhslen), yyrhslen);
  yystackp->yyerror_range[1].yystate.yyloc = *yylocp;

  switch (yyn)
    {
        case 2:

/* Line 936 of glr.c  */
#line 2060 "src/frontend/c99.y"
    {
	*parsed_tree = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 3:

/* Line 936 of glr.c  */
#line 2064 "src/frontend/c99.y"
    {
	*parsed_tree = NULL;
;}
    break;

  case 4:

/* Line 936 of glr.c  */
#line 2074 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 5:

/* Line 936 of glr.c  */
#line 2078 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 6:

/* Line 936 of glr.c  */
#line 2084 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 7:

/* Line 936 of glr.c  */
#line 2088 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 8:

/* Line 936 of glr.c  */
#line 2094 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 9:

/* Line 936 of glr.c  */
#line 2098 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 10:

/* Line 936 of glr.c  */
#line 2102 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 11:

/* Line 936 of glr.c  */
#line 2109 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_STATIC_ASSERT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (7))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 12:

/* Line 936 of glr.c  */
#line 2115 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 13:

/* Line 936 of glr.c  */
#line 2119 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 14:

/* Line 936 of glr.c  */
#line 2123 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 15:

/* Line 936 of glr.c  */
#line 2129 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_EXTENSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 16:

/* Line 936 of glr.c  */
#line 2133 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 17:

/* Line 936 of glr.c  */
#line 2139 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 18:

/* Line 936 of glr.c  */
#line 2145 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 19:

/* Line 936 of glr.c  */
#line 2150 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 20:

/* Line 936 of glr.c  */
#line 2154 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_EXTENSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 21:

/* Line 936 of glr.c  */
#line 2159 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_PP_COMMENT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 22:

/* Line 936 of glr.c  */
#line 2164 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_PP_TOKEN, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 23:

/* Line 936 of glr.c  */
#line 2171 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_LABEL_DECL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 24:

/* Line 936 of glr.c  */
#line 2177 "src/frontend/c99.y"
    {
    AST symbol_holder = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
	((*yyvalp).ast) = ASTListLeaf(symbol_holder);
;}
    break;

  case 25:

/* Line 936 of glr.c  */
#line 2182 "src/frontend/c99.y"
    {
	AST label = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), label);
;}
    break;

  case 26:

/* Line 936 of glr.c  */
#line 2191 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 27:

/* Line 936 of glr.c  */
#line 2197 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 28:

/* Line 936 of glr.c  */
#line 2201 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 29:

/* Line 936 of glr.c  */
#line 2207 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 30:

/* Line 936 of glr.c  */
#line 2211 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 31:

/* Line 936 of glr.c  */
#line 2217 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_ATTRIBUTE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 32:

/* Line 936 of glr.c  */
#line 2221 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_ATTRIBUTE, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 33:

/* Line 936 of glr.c  */
#line 2227 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 34:

/* Line 936 of glr.c  */
#line 2231 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 35:

/* Line 936 of glr.c  */
#line 2238 "src/frontend/c99.y"
    {
	AST identif = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake2(AST_GCC_ATTRIBUTE_EXPR, identif, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 36:

/* Line 936 of glr.c  */
#line 2244 "src/frontend/c99.y"
    {
	AST identif = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake2(AST_GCC_ATTRIBUTE_EXPR, identif, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 37:

/* Line 936 of glr.c  */
#line 2250 "src/frontend/c99.y"
    {
	AST identif1 = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
	
	((*yyvalp).ast) = ASTMake2(AST_GCC_ATTRIBUTE_EXPR, identif1, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 38:

/* Line 936 of glr.c  */
#line 2259 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_ASM_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 39:

/* Line 936 of glr.c  */
#line 2264 "src/frontend/c99.y"
    {
	AST asm_parms = ASTMake4(AST_GCC_ASM_DEF_PARMS, 
			(((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (8))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (8))].yystate.yysemantics.yysval.ast)), NULL);
	((*yyvalp).ast) = ASTMake2(AST_GCC_ASM_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (8))].yystate.yysemantics.yysval.ast), asm_parms, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 40:

/* Line 936 of glr.c  */
#line 2270 "src/frontend/c99.y"
    {
	AST asm_parms = ASTMake4(AST_GCC_ASM_DEF_PARMS, 
			(((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (10))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (10))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((8) - (10))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (10))].yystate.yysemantics.yysval.ast)), NULL);
	((*yyvalp).ast) = ASTMake2(AST_GCC_ASM_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (10))].yystate.yysemantics.yysval.ast), asm_parms, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 41:

/* Line 936 of glr.c  */
#line 2276 "src/frontend/c99.y"
    {
	AST asm_parms = ASTMake4(AST_GCC_ASM_DEF_PARMS, 
			(((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (12))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (12))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((8) - (12))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((10) - (12))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (12))].yystate.yysemantics.yysval.ast)), NULL);
	((*yyvalp).ast) = ASTMake2(AST_GCC_ASM_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (12))].yystate.yysemantics.yysval.ast), asm_parms, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (12))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (12))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (12))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (12))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 42:

/* Line 936 of glr.c  */
#line 2284 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = NULL;
;}
    break;

  case 43:

/* Line 936 of glr.c  */
#line 2288 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_VOLATILE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 44:

/* Line 936 of glr.c  */
#line 2294 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 45:

/* Line 936 of glr.c  */
#line 2298 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = NULL;
;}
    break;

  case 46:

/* Line 936 of glr.c  */
#line 2305 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 47:

/* Line 936 of glr.c  */
#line 2309 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 48:

/* Line 936 of glr.c  */
#line 2315 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_GCC_ASM_OPERAND, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 49:

/* Line 936 of glr.c  */
#line 2319 "src/frontend/c99.y"
    {
    AST symbol_tree = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (7))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (7))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (7))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (7))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake3(AST_GCC_ASM_OPERAND, symbol_tree, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (7))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 50:

/* Line 936 of glr.c  */
#line 2325 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_GCC_ASM_OPERAND, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 51:

/* Line 936 of glr.c  */
#line 2333 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 52:

/* Line 936 of glr.c  */
#line 2337 "src/frontend/c99.y"
    {
    // This is an error but also a common extension
    ((*yyvalp).ast) = ASTLeaf(AST_EMPTY_DECL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 53:

/* Line 936 of glr.c  */
#line 2344 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SIMPLE_DECLARATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 54:

/* Line 936 of glr.c  */
#line 2348 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SIMPLE_DECLARATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 55:

/* Line 936 of glr.c  */
#line 2352 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SIMPLE_DECLARATION, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 56:

/* Line 936 of glr.c  */
#line 2358 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SIMPLE_DECLARATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 57:

/* Line 936 of glr.c  */
#line 2364 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 58:

/* Line 936 of glr.c  */
#line 2368 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 59:

/* Line 936 of glr.c  */
#line 2372 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 60:

/* Line 936 of glr.c  */
#line 2376 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 61:

/* Line 936 of glr.c  */
#line 2380 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 62:

/* Line 936 of glr.c  */
#line 2387 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 63:

/* Line 936 of glr.c  */
#line 2391 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 64:

/* Line 936 of glr.c  */
#line 2397 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 65:

/* Line 936 of glr.c  */
#line 2401 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 66:

/* Line 936 of glr.c  */
#line 2405 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 67:

/* Line 936 of glr.c  */
#line 2409 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 68:

/* Line 936 of glr.c  */
#line 2413 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 69:

/* Line 936 of glr.c  */
#line 2419 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 70:

/* Line 936 of glr.c  */
#line 2423 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 71:

/* Line 936 of glr.c  */
#line 2427 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 72:

/* Line 936 of glr.c  */
#line 2431 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 73:

/* Line 936 of glr.c  */
#line 2435 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 74:

/* Line 936 of glr.c  */
#line 2447 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 75:

/* Line 936 of glr.c  */
#line 2451 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 76:

/* Line 936 of glr.c  */
#line 2455 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 77:

/* Line 936 of glr.c  */
#line 2459 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 78:

/* Line 936 of glr.c  */
#line 2465 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 79:

/* Line 936 of glr.c  */
#line 2469 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 80:

/* Line 936 of glr.c  */
#line 2475 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 81:

/* Line 936 of glr.c  */
#line 2479 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 82:

/* Line 936 of glr.c  */
#line 2485 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 83:

/* Line 936 of glr.c  */
#line 2489 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 84:

/* Line 936 of glr.c  */
#line 2493 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_TYPEDEF_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 85:

/* Line 936 of glr.c  */
#line 2498 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 86:

/* Line 936 of glr.c  */
#line 2503 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SIGNED_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 87:

/* Line 936 of glr.c  */
#line 2507 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_UNSIGNED_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 88:

/* Line 936 of glr.c  */
#line 2511 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_LONG_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 89:

/* Line 936 of glr.c  */
#line 2515 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SHORT_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 90:

/* Line 936 of glr.c  */
#line 2520 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_GCC_COMPLEX_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 91:

/* Line 936 of glr.c  */
#line 2524 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_GCC_IMAGINARY_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 92:

/* Line 936 of glr.c  */
#line 2530 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_AUTO_STORAGE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 93:

/* Line 936 of glr.c  */
#line 2534 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_REGISTER_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 94:

/* Line 936 of glr.c  */
#line 2538 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_STATIC_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 95:

/* Line 936 of glr.c  */
#line 2542 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_EXTERN_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 96:

/* Line 936 of glr.c  */
#line 2547 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_THREAD_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 97:

/* Line 936 of glr.c  */
#line 2553 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_INLINE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 98:

/* Line 936 of glr.c  */
#line 2557 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_NORETURN_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 99:

/* Line 936 of glr.c  */
#line 2563 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 100:

/* Line 936 of glr.c  */
#line 2567 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 101:

/* Line 936 of glr.c  */
#line 2573 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 102:

/* Line 936 of glr.c  */
#line 2577 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 103:

/* Line 936 of glr.c  */
#line 2581 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 104:

/* Line 936 of glr.c  */
#line 2585 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 105:

/* Line 936 of glr.c  */
#line 2591 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 106:

/* Line 936 of glr.c  */
#line 2595 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 107:

/* Line 936 of glr.c  */
#line 2599 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 108:

/* Line 936 of glr.c  */
#line 2605 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 109:

/* Line 936 of glr.c  */
#line 2609 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 110:

/* Line 936 of glr.c  */
#line 2613 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 111:

/* Line 936 of glr.c  */
#line 2617 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 112:

/* Line 936 of glr.c  */
#line 2621 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_TYPE_SPECIFIER_SEQ, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 113:

/* Line 936 of glr.c  */
#line 2627 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_SIMPLE_TYPE_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 114:

/* Line 936 of glr.c  */
#line 2631 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 115:

/* Line 936 of glr.c  */
#line 2635 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 116:

/* Line 936 of glr.c  */
#line 2639 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 117:

/* Line 936 of glr.c  */
#line 2646 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_TYPEOF_EXPR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 118:

/* Line 936 of glr.c  */
#line 2650 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_TYPEOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 119:

/* Line 936 of glr.c  */
#line 2656 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_ATOMIC_TYPE_SPECIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 120:

/* Line 936 of glr.c  */
#line 2662 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 121:

/* Line 936 of glr.c  */
#line 2668 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CHAR_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 122:

/* Line 936 of glr.c  */
#line 2672 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_BOOL_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 123:

/* Line 936 of glr.c  */
#line 2676 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_INT_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 124:

/* Line 936 of glr.c  */
#line 2680 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_FLOAT_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 125:

/* Line 936 of glr.c  */
#line 2684 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_DOUBLE_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 126:

/* Line 936 of glr.c  */
#line 2688 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_VOID_TYPE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 127:

/* Line 936 of glr.c  */
#line 2692 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_GCC_INT128, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 128:

/* Line 936 of glr.c  */
#line 2696 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_GCC_FLOAT128, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 129:

/* Line 936 of glr.c  */
#line 2700 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_MCC_BYTE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 130:

/* Line 936 of glr.c  */
#line 2706 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake3(AST_ELABORATED_TYPE_CLASS_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 131:

/* Line 936 of glr.c  */
#line 2712 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake4(AST_ELABORATED_TYPE_ENUM_SPEC, identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 132:

/* Line 936 of glr.c  */
#line 2723 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 133:

/* Line 936 of glr.c  */
#line 2727 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ast_list_concat(ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 134:

/* Line 936 of glr.c  */
#line 2733 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 135:

/* Line 936 of glr.c  */
#line 2737 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 136:

/* Line 936 of glr.c  */
#line 2743 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_INIT_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 137:

/* Line 936 of glr.c  */
#line 2747 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_INIT_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 138:

/* Line 936 of glr.c  */
#line 2753 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_INIT_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 139:

/* Line 936 of glr.c  */
#line 2757 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_INIT_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 140:

/* Line 936 of glr.c  */
#line 2763 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 141:

/* Line 936 of glr.c  */
#line 2767 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 142:

/* Line 936 of glr.c  */
#line 2773 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 143:

/* Line 936 of glr.c  */
#line 2777 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 144:

/* Line 936 of glr.c  */
#line 2781 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ast_list_concat(ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 145:

/* Line 936 of glr.c  */
#line 2788 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_ASM_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 146:

/* Line 936 of glr.c  */
#line 2795 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 147:

/* Line 936 of glr.c  */
#line 2799 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 148:

/* Line 936 of glr.c  */
#line 2805 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 149:

/* Line 936 of glr.c  */
#line 2809 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 150:

/* Line 936 of glr.c  */
#line 2815 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_POINTER_SPEC, NULL, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 151:

/* Line 936 of glr.c  */
#line 2819 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_POINTER_SPEC, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 152:

/* Line 936 of glr.c  */
#line 2823 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_REFERENCE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 153:

/* Line 936 of glr.c  */
#line 2827 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_REBINDABLE_REFERENCE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 154:

/* Line 936 of glr.c  */
#line 2837 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 155:

/* Line 936 of glr.c  */
#line 2841 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 156:

/* Line 936 of glr.c  */
#line 2847 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 157:

/* Line 936 of glr.c  */
#line 2851 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 158:

/* Line 936 of glr.c  */
#line 2858 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 159:

/* Line 936 of glr.c  */
#line 2862 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 160:

/* Line 936 of glr.c  */
#line 2868 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_PARAMETERS_AND_QUALIFIERS,
        (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, NULL, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 161:

/* Line 936 of glr.c  */
#line 2873 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_PARAMETERS_AND_QUALIFIERS,
        ASTLeaf(AST_EMPTY_PARAMETER_DECLARATION_CLAUSE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL),
        NULL, NULL, NULL,
        make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 162:

/* Line 936 of glr.c  */
#line 2882 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 163:

/* Line 936 of glr.c  */
#line 2886 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR_FUNC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 164:

/* Line 936 of glr.c  */
#line 2890 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), NULL, NULL,  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 165:

/* Line 936 of glr.c  */
#line 2894 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 166:

/* Line 936 of glr.c  */
#line 2898 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 167:

/* Line 936 of glr.c  */
#line 2902 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PARENTHESIZED_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 168:

/* Line 936 of glr.c  */
#line 2908 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR_FUNC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 169:

/* Line 936 of glr.c  */
#line 2914 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 170:

/* Line 936 of glr.c  */
#line 2919 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_PARENTHESIZED_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 171:

/* Line 936 of glr.c  */
#line 2926 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 172:

/* Line 936 of glr.c  */
#line 2930 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 173:

/* Line 936 of glr.c  */
#line 2936 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 174:

/* Line 936 of glr.c  */
#line 2943 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 175:

/* Line 936 of glr.c  */
#line 2947 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 176:

/* Line 936 of glr.c  */
#line 2953 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_STATIC_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 177:

/* Line 936 of glr.c  */
#line 2960 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 178:

/* Line 936 of glr.c  */
#line 2964 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_VLA_EXPRESSION, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 179:

/* Line 936 of glr.c  */
#line 2968 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 180:

/* Line 936 of glr.c  */
#line 2974 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 181:

/* Line 936 of glr.c  */
#line 2978 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 182:

/* Line 936 of glr.c  */
#line 2984 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CONST_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 183:

/* Line 936 of glr.c  */
#line 2988 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_VOLATILE_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 184:

/* Line 936 of glr.c  */
#line 2993 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_GCC_RESTRICT_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 185:

/* Line 936 of glr.c  */
#line 2998 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_ATOMIC_TYPE_QUALIFIER, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 186:

/* Line 936 of glr.c  */
#line 3004 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 187:

/* Line 936 of glr.c  */
#line 3008 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR_FUNC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 188:

/* Line 936 of glr.c  */
#line 3012 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), NULL, NULL,  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 189:

/* Line 936 of glr.c  */
#line 3016 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 190:

/* Line 936 of glr.c  */
#line 3020 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 191:

/* Line 936 of glr.c  */
#line 3024 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PARENTHESIZED_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 192:

/* Line 936 of glr.c  */
#line 3030 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_DECLARATOR_ID_EXPR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 193:

/* Line 936 of glr.c  */
#line 3036 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_ENUM_HEAD, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 194:

/* Line 936 of glr.c  */
#line 3042 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 195:

/* Line 936 of glr.c  */
#line 3046 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 196:

/* Line 936 of glr.c  */
#line 3052 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_UNSCOPED_ENUM_KEY, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 197:

/* Line 936 of glr.c  */
#line 3058 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_ENUM_SPECIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 198:

/* Line 936 of glr.c  */
#line 3062 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_ENUM_SPECIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 199:

/* Line 936 of glr.c  */
#line 3068 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 200:

/* Line 936 of glr.c  */
#line 3072 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 201:

/* Line 936 of glr.c  */
#line 3078 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake2(AST_ENUMERATOR_DEF, identifier, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 202:

/* Line 936 of glr.c  */
#line 3084 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake2(AST_ENUMERATOR_DEF, identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 203:

/* Line 936 of glr.c  */
#line 3092 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_TYPE_ID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 204:

/* Line 936 of glr.c  */
#line 3096 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_TYPE_ID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 205:

/* Line 936 of glr.c  */
#line 3102 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 206:

/* Line 936 of glr.c  */
#line 3106 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 207:

/* Line 936 of glr.c  */
#line 3110 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 208:

/* Line 936 of glr.c  */
#line 3116 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 209:

/* Line 936 of glr.c  */
#line 3120 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_POINTER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 210:

/* Line 936 of glr.c  */
#line 3124 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 211:

/* Line 936 of glr.c  */
#line 3130 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PARENTHESIZED_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 212:

/* Line 936 of glr.c  */
#line 3134 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR_FUNC, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 213:

/* Line 936 of glr.c  */
#line 3138 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DECLARATOR_FUNC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 214:

/* Line 936 of glr.c  */
#line 3142 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 215:

/* Line 936 of glr.c  */
#line 3146 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 216:

/* Line 936 of glr.c  */
#line 3150 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 217:

/* Line 936 of glr.c  */
#line 3154 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), NULL, NULL,  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 218:

/* Line 936 of glr.c  */
#line 3158 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 219:

/* Line 936 of glr.c  */
#line 3162 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast),  ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 220:

/* Line 936 of glr.c  */
#line 3168 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_KR_PARAMETER_LIST, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 221:

/* Line 936 of glr.c  */
#line 3174 "src/frontend/c99.y"
    {
    AST symbol = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);

    ((*yyvalp).ast) = ASTListLeaf(symbol);
;}
    break;

  case 222:

/* Line 936 of glr.c  */
#line 3180 "src/frontend/c99.y"
    {
    AST symbol = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);

    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), symbol);
;}
    break;

  case 223:

/* Line 936 of glr.c  */
#line 3188 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 224:

/* Line 936 of glr.c  */
#line 3192 "src/frontend/c99.y"
    {
    AST variadic_arg = ASTLeaf(AST_VARIADIC_ARG, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), variadic_arg);
;}
    break;

  case 225:

/* Line 936 of glr.c  */
#line 3199 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 226:

/* Line 936 of glr.c  */
#line 3203 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 227:

/* Line 936 of glr.c  */
#line 3209 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_PARAMETER_DECL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 228:

/* Line 936 of glr.c  */
#line 3213 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_PARAMETER_DECL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 229:

/* Line 936 of glr.c  */
#line 3217 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_PARAMETER_DECL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 230:

/* Line 936 of glr.c  */
#line 3223 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_EQUAL_INITIALIZER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 231:

/* Line 936 of glr.c  */
#line 3229 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 232:

/* Line 936 of glr.c  */
#line 3233 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 233:

/* Line 936 of glr.c  */
#line 3239 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 234:

/* Line 936 of glr.c  */
#line 3243 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 235:

/* Line 936 of glr.c  */
#line 3247 "src/frontend/c99.y"
    {
    AST designated_initializer = ASTMake2(AST_DESIGNATED_INITIALIZER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);

    ((*yyvalp).ast) = ASTListLeaf(designated_initializer);
;}
    break;

  case 236:

/* Line 936 of glr.c  */
#line 3253 "src/frontend/c99.y"
    {
    AST designated_initializer = ASTMake2(AST_DESIGNATED_INITIALIZER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast)), NULL);

    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), designated_initializer);
;}
    break;

  case 237:

/* Line 936 of glr.c  */
#line 3260 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);

	AST gcc_initializer_clause = ASTMake2(AST_GCC_INITIALIZER_CLAUSE, identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);

	((*yyvalp).ast) = ASTListLeaf(gcc_initializer_clause);
;}
    break;

  case 238:

/* Line 936 of glr.c  */
#line 3268 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.token_atrib).token_text);

	AST gcc_initializer_clause = ASTMake2(AST_GCC_INITIALIZER_CLAUSE, identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);

	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), gcc_initializer_clause);
;}
    break;

  case 239:

/* Line 936 of glr.c  */
#line 3278 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_DESIGNATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 240:

/* Line 936 of glr.c  */
#line 3284 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 241:

/* Line 936 of glr.c  */
#line 3288 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 242:

/* Line 936 of glr.c  */
#line 3294 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_INDEX_DESIGNATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 243:

/* Line 936 of glr.c  */
#line 3298 "src/frontend/c99.y"
    {
    AST symbol = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);

    ((*yyvalp).ast) = ASTMake1(AST_FIELD_DESIGNATOR, symbol, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 244:

/* Line 936 of glr.c  */
#line 3306 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_FUNCTION_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 245:

/* Line 936 of glr.c  */
#line 3310 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_FUNCTION_DEFINITION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 246:

/* Line 936 of glr.c  */
#line 3316 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_EXTENSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 247:

/* Line 936 of glr.c  */
#line 3322 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_FUNCTION_DEFINITION_HEADER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 248:

/* Line 936 of glr.c  */
#line 3326 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_FUNCTION_DEFINITION_HEADER, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 249:

/* Line 936 of glr.c  */
#line 3332 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 250:

/* Line 936 of glr.c  */
#line 3336 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 251:

/* Line 936 of glr.c  */
#line 3342 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_FUNCTION_BODY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 252:

/* Line 936 of glr.c  */
#line 3352 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CLASS_SPECIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 253:

/* Line 936 of glr.c  */
#line 3356 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CLASS_SPECIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 254:

/* Line 936 of glr.c  */
#line 3362 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_CLASS_HEAD_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), NULL, NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 255:

/* Line 936 of glr.c  */
#line 3366 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake4(AST_CLASS_HEAD_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), identifier, NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 256:

/* Line 936 of glr.c  */
#line 3373 "src/frontend/c99.y"
    {
    AST class_head_extra = ASTMake3(AST_CLASS_HEAD_EXTRA, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);

	((*yyvalp).ast) = ASTMake4(AST_CLASS_HEAD_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, NULL, class_head_extra, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 257:

/* Line 936 of glr.c  */
#line 3379 "src/frontend/c99.y"
    {
    AST class_head_extra = ASTMake3(AST_CLASS_HEAD_EXTRA, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);

	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
	((*yyvalp).ast) = ASTMake4(AST_CLASS_HEAD_SPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), identifier, NULL, class_head_extra, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 258:

/* Line 936 of glr.c  */
#line 3388 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CLASS_KEY_STRUCT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 259:

/* Line 936 of glr.c  */
#line 3392 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CLASS_KEY_UNION, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 260:

/* Line 936 of glr.c  */
#line 3398 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 261:

/* Line 936 of glr.c  */
#line 3402 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 262:

/* Line 936 of glr.c  */
#line 3408 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_MEMBER_DECLARATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 263:

/* Line 936 of glr.c  */
#line 3412 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_MEMBER_DECLARATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 264:

/* Line 936 of glr.c  */
#line 3417 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_EMPTY_DECL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 265:

/* Line 936 of glr.c  */
#line 3422 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_EXTENSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 266:

/* Line 936 of glr.c  */
#line 3428 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 267:

/* Line 936 of glr.c  */
#line 3432 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ast_list_concat(ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 268:

/* Line 936 of glr.c  */
#line 3438 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 269:

/* Line 936 of glr.c  */
#line 3442 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 270:

/* Line 936 of glr.c  */
#line 3448 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_MEMBER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 271:

/* Line 936 of glr.c  */
#line 3452 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_MEMBER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 272:

/* Line 936 of glr.c  */
#line 3456 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_BITFIELD_DECLARATOR, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 273:

/* Line 936 of glr.c  */
#line 3460 "src/frontend/c99.y"
    {
    AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
    AST declarator_id_expr = ASTMake1(AST_DECLARATOR_ID_EXPR, identifier, ast_get_locus(identifier), NULL);

    ((*yyvalp).ast) = ASTMake3(AST_BITFIELD_DECLARATOR, declarator_id_expr, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 274:

/* Line 936 of glr.c  */
#line 3469 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_MEMBER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 275:

/* Line 936 of glr.c  */
#line 3473 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_MEMBER_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 276:

/* Line 936 of glr.c  */
#line 3477 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_BITFIELD_DECLARATOR, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 277:

/* Line 936 of glr.c  */
#line 3481 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
	AST declarator_id_expr = ASTMake1(AST_DECLARATOR_ID_EXPR, identifier, ast_get_locus(identifier), NULL);

	((*yyvalp).ast) = ASTMake3(AST_BITFIELD_DECLARATOR, declarator_id_expr, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 278:

/* Line 936 of glr.c  */
#line 3490 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 279:

/* Line 936 of glr.c  */
#line 3500 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 280:

/* Line 936 of glr.c  */
#line 3504 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 281:

/* Line 936 of glr.c  */
#line 3510 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 282:

/* Line 936 of glr.c  */
#line 3514 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 283:

/* Line 936 of glr.c  */
#line 3518 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 284:

/* Line 936 of glr.c  */
#line 3522 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 285:

/* Line 936 of glr.c  */
#line 3526 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 286:

/* Line 936 of glr.c  */
#line 3530 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 287:

/* Line 936 of glr.c  */
#line 3534 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 288:

/* Line 936 of glr.c  */
#line 3540 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
	
	((*yyvalp).ast) = ASTMake2(AST_LABELED_STATEMENT, identifier, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 289:

/* Line 936 of glr.c  */
#line 3546 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CASE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 290:

/* Line 936 of glr.c  */
#line 3550 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_DEFAULT_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 291:

/* Line 936 of glr.c  */
#line 3555 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_GCC_CASE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 292:

/* Line 936 of glr.c  */
#line 3561 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_EXPRESSION_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 293:

/* Line 936 of glr.c  */
#line 3565 "src/frontend/c99.y"
    {
	// Empty statement ...
	((*yyvalp).ast) = ASTLeaf(AST_EMPTY_STATEMENT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 294:

/* Line 936 of glr.c  */
#line 3572 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_DECLARATION_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 295:

/* Line 936 of glr.c  */
#line 3578 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_COMPOUND_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 296:

/* Line 936 of glr.c  */
#line 3582 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_COMPOUND_STATEMENT, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 297:

/* Line 936 of glr.c  */
#line 3588 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 298:

/* Line 936 of glr.c  */
#line 3592 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 299:

/* Line 936 of glr.c  */
#line 3607 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_IF_ELSE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), NULL, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 300:

/* Line 936 of glr.c  */
#line 3611 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_IF_ELSE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (7))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 301:

/* Line 936 of glr.c  */
#line 3617 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_SWITCH_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 302:

/* Line 936 of glr.c  */
#line 3623 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_CONDITION, NULL, NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 303:

/* Line 936 of glr.c  */
#line 3629 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_WHILE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 304:

/* Line 936 of glr.c  */
#line 3633 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DO_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (7))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 305:

/* Line 936 of glr.c  */
#line 3637 "src/frontend/c99.y"
    {
    AST loop_control = ASTMake3(AST_LOOP_CONTROL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (8))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_column), NULL);
	((*yyvalp).ast) = ASTMake3(AST_FOR_STATEMENT, loop_control, (((yyGLRStackItem const *)yyvsp)[YYFILL ((8) - (8))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 306:

/* Line 936 of glr.c  */
#line 3644 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 307:

/* Line 936 of glr.c  */
#line 3648 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 308:

/* Line 936 of glr.c  */
#line 3654 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 309:

/* Line 936 of glr.c  */
#line 3658 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 310:

/* Line 936 of glr.c  */
#line 3665 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 311:

/* Line 936 of glr.c  */
#line 3669 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 312:

/* Line 936 of glr.c  */
#line 3675 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_BREAK_STATEMENT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 313:

/* Line 936 of glr.c  */
#line 3679 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CONTINUE_STATEMENT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 314:

/* Line 936 of glr.c  */
#line 3683 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_RETURN_STATEMENT, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 315:

/* Line 936 of glr.c  */
#line 3687 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_RETURN_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 316:

/* Line 936 of glr.c  */
#line 3691 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
	
	((*yyvalp).ast) = ASTMake1(AST_GOTO_STATEMENT, identifier, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 317:

/* Line 936 of glr.c  */
#line 3698 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_GOTO_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 318:

/* Line 936 of glr.c  */
#line 3708 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 319:

/* Line 936 of glr.c  */
#line 3712 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PARENTHESIZED_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 320:

/* Line 936 of glr.c  */
#line 3716 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 321:

/* Line 936 of glr.c  */
#line 3721 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 322:

/* Line 936 of glr.c  */
#line 3735 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_PARENTHESIZED_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 323:

/* Line 936 of glr.c  */
#line 3739 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_GCC_BUILTIN_VA_ARG, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 324:

/* Line 936 of glr.c  */
#line 3743 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GCC_BUILTIN_OFFSETOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 325:

/* Line 936 of glr.c  */
#line 3747 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_GCC_BUILTIN_CHOOSE_EXPR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (8))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 326:

/* Line 936 of glr.c  */
#line 3751 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GCC_BUILTIN_TYPES_COMPATIBLE_P, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 327:

/* Line 936 of glr.c  */
#line 3757 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GENERIC_SELECTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 328:

/* Line 936 of glr.c  */
#line 3763 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 329:

/* Line 936 of glr.c  */
#line 3767 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 330:

/* Line 936 of glr.c  */
#line 3773 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GENERIC_ASSOCIATION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 331:

/* Line 936 of glr.c  */
#line 3777 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_GENERIC_ASSOCIATION_DEFAULT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast),
        make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 332:

/* Line 936 of glr.c  */
#line 3791 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GCC_OFFSETOF_MEMBER_DESIGNATOR,
            ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text),
            (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 333:

/* Line 936 of glr.c  */
#line 3797 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_GCC_OFFSETOF_MEMBER_DESIGNATOR,
            ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text),
            NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 334:

/* Line 936 of glr.c  */
#line 3805 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 335:

/* Line 936 of glr.c  */
#line 3811 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 336:

/* Line 936 of glr.c  */
#line 3817 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 337:

/* Line 936 of glr.c  */
#line 3821 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_ARRAY_SUBSCRIPT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 338:

/* Line 936 of glr.c  */
#line 3825 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_FUNCTION_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 339:

/* Line 936 of glr.c  */
#line 3829 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_FUNCTION_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 340:

/* Line 936 of glr.c  */
#line 3833 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CLASS_MEMBER_ACCESS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 341:

/* Line 936 of glr.c  */
#line 3837 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_POINTER_CLASS_MEMBER_ACCESS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 342:

/* Line 936 of glr.c  */
#line 3841 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_POSTINCREMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 343:

/* Line 936 of glr.c  */
#line 3845 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_POSTDECREMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 344:

/* Line 936 of glr.c  */
#line 3850 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_GCC_POSTFIX_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 345:

/* Line 936 of glr.c  */
#line 3856 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_INITIALIZER_BRACES, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 346:

/* Line 936 of glr.c  */
#line 3860 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_INITIALIZER_BRACES, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 347:

/* Line 936 of glr.c  */
#line 3864 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_INITIALIZER_BRACES, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 348:

/* Line 936 of glr.c  */
#line 3870 "src/frontend/c99.y"
    {
    AST expression_holder = ASTMake1(AST_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
	((*yyvalp).ast) = ASTListLeaf(expression_holder);
;}
    break;

  case 349:

/* Line 936 of glr.c  */
#line 3875 "src/frontend/c99.y"
    {
    AST expression_holder = ASTMake1(AST_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast)), NULL);
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), expression_holder);
;}
    break;

  case 350:

/* Line 936 of glr.c  */
#line 3882 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 351:

/* Line 936 of glr.c  */
#line 3886 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PREINCREMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 352:

/* Line 936 of glr.c  */
#line 3890 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PREDECREMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 353:

/* Line 936 of glr.c  */
#line 3894 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.node_type), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 354:

/* Line 936 of glr.c  */
#line 3898 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_SIZEOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 355:

/* Line 936 of glr.c  */
#line 3902 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_SIZEOF_TYPEID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 356:

/* Line 936 of glr.c  */
#line 3907 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_EXTENSION_EXPR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 357:

/* Line 936 of glr.c  */
#line 3911 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_ALIGNOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 358:

/* Line 936 of glr.c  */
#line 3915 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_ALIGNOF_TYPE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 359:

/* Line 936 of glr.c  */
#line 3919 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_REAL_PART, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 360:

/* Line 936 of glr.c  */
#line 3923 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_GCC_IMAG_PART, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 361:

/* Line 936 of glr.c  */
#line 3927 "src/frontend/c99.y"
    {
	AST identifier = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);

	((*yyvalp).ast) = ASTMake1(AST_GCC_LABEL_ADDR, identifier, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 362:

/* Line 936 of glr.c  */
#line 3935 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_DERREFERENCE;
;}
    break;

  case 363:

/* Line 936 of glr.c  */
#line 3939 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_REFERENCE;
;}
    break;

  case 364:

/* Line 936 of glr.c  */
#line 3943 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_PLUS;
;}
    break;

  case 365:

/* Line 936 of glr.c  */
#line 3947 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_NEG;
;}
    break;

  case 366:

/* Line 936 of glr.c  */
#line 3951 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_LOGICAL_NOT;
;}
    break;

  case 367:

/* Line 936 of glr.c  */
#line 3955 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_BITWISE_NOT;
;}
    break;

  case 368:

/* Line 936 of glr.c  */
#line 3961 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 369:

/* Line 936 of glr.c  */
#line 3965 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CAST, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast)), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 370:

/* Line 936 of glr.c  */
#line 3971 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 371:

/* Line 936 of glr.c  */
#line 3975 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_MUL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 372:

/* Line 936 of glr.c  */
#line 3979 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DIV, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 373:

/* Line 936 of glr.c  */
#line 3983 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_MOD, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 374:

/* Line 936 of glr.c  */
#line 3989 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 375:

/* Line 936 of glr.c  */
#line 3993 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_ADD, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 376:

/* Line 936 of glr.c  */
#line 3997 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_MINUS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 377:

/* Line 936 of glr.c  */
#line 4003 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 378:

/* Line 936 of glr.c  */
#line 4007 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_BITWISE_SHL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 379:

/* Line 936 of glr.c  */
#line 4011 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SHR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 380:

/* Line 936 of glr.c  */
#line 4017 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 381:

/* Line 936 of glr.c  */
#line 4021 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_LOWER_THAN, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 382:

/* Line 936 of glr.c  */
#line 4025 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_GREATER_THAN, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 383:

/* Line 936 of glr.c  */
#line 4029 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_GREATER_OR_EQUAL_THAN, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 384:

/* Line 936 of glr.c  */
#line 4033 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_LOWER_OR_EQUAL_THAN, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 385:

/* Line 936 of glr.c  */
#line 4039 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 386:

/* Line 936 of glr.c  */
#line 4043 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_EQUAL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 387:

/* Line 936 of glr.c  */
#line 4047 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_DIFFERENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 388:

/* Line 936 of glr.c  */
#line 4053 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 389:

/* Line 936 of glr.c  */
#line 4057 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_BITWISE_AND, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 390:

/* Line 936 of glr.c  */
#line 4063 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 391:

/* Line 936 of glr.c  */
#line 4067 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_BITWISE_XOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 392:

/* Line 936 of glr.c  */
#line 4073 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 393:

/* Line 936 of glr.c  */
#line 4077 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_BITWISE_OR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 394:

/* Line 936 of glr.c  */
#line 4083 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 395:

/* Line 936 of glr.c  */
#line 4087 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_LOGICAL_AND, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 396:

/* Line 936 of glr.c  */
#line 4093 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 397:

/* Line 936 of glr.c  */
#line 4097 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_LOGICAL_OR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 398:

/* Line 936 of glr.c  */
#line 4103 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 399:

/* Line 936 of glr.c  */
#line 4107 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_CONDITIONAL_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 400:

/* Line 936 of glr.c  */
#line 4112 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_GCC_CONDITIONAL_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 401:

/* Line 936 of glr.c  */
#line 4118 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 402:

/* Line 936 of glr.c  */
#line 4122 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.node_type), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 403:

/* Line 936 of glr.c  */
#line 4128 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 404:

/* Line 936 of glr.c  */
#line 4132 "src/frontend/c99.y"
    {
	AST comma_expression = ASTMake2(AST_COMMA, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);

	((*yyvalp).ast) = ASTMake1(AST_EXPRESSION, comma_expression, ast_get_locus(comma_expression), NULL);
;}
    break;

  case 405:

/* Line 936 of glr.c  */
#line 4140 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_ASSIGNMENT;
;}
    break;

  case 406:

/* Line 936 of glr.c  */
#line 4144 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_MUL_ASSIGNMENT;
;}
    break;

  case 407:

/* Line 936 of glr.c  */
#line 4148 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_DIV_ASSIGNMENT;
;}
    break;

  case 408:

/* Line 936 of glr.c  */
#line 4152 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_ADD_ASSIGNMENT;
;}
    break;

  case 409:

/* Line 936 of glr.c  */
#line 4156 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_SUB_ASSIGNMENT;
;}
    break;

  case 410:

/* Line 936 of glr.c  */
#line 4160 "src/frontend/c99.y"
    {
    ((*yyvalp).node_type) = AST_BITWISE_SHL_ASSIGNMENT;
;}
    break;

  case 411:

/* Line 936 of glr.c  */
#line 4164 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_SHR_ASSIGNMENT;
;}
    break;

  case 412:

/* Line 936 of glr.c  */
#line 4168 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_BITWISE_AND_ASSIGNMENT;
;}
    break;

  case 413:

/* Line 936 of glr.c  */
#line 4172 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_BITWISE_OR_ASSIGNMENT;
;}
    break;

  case 414:

/* Line 936 of glr.c  */
#line 4176 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_BITWISE_XOR_ASSIGNMENT;
;}
    break;

  case 415:

/* Line 936 of glr.c  */
#line 4180 "src/frontend/c99.y"
    {
	((*yyvalp).node_type) = AST_MOD_ASSIGNMENT;
;}
    break;

  case 416:

/* Line 936 of glr.c  */
#line 4186 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_CONSTANT_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 417:

/* Line 936 of glr.c  */
#line 4196 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_DECIMAL_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 418:

/* Line 936 of glr.c  */
#line 4200 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_OCTAL_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 419:

/* Line 936 of glr.c  */
#line 4204 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_BINARY_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 420:

/* Line 936 of glr.c  */
#line 4208 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_HEXADECIMAL_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 421:

/* Line 936 of glr.c  */
#line 4212 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_HEXADECIMAL_FLOAT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 422:

/* Line 936 of glr.c  */
#line 4216 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_FLOATING_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 423:

/* Line 936 of glr.c  */
#line 4220 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_BOOLEAN_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 424:

/* Line 936 of glr.c  */
#line 4224 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_CHARACTER_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 425:

/* Line 936 of glr.c  */
#line 4228 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 426:

/* Line 936 of glr.c  */
#line 4235 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_STRING_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 427:

/* Line 936 of glr.c  */
#line 4239 "src/frontend/c99.y"
    {
	// Let's concatenate here, it will ease everything

	const char* str1 = ASTText((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast));
	const char* str2 = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text;
	char* text = NEW_VEC0(char, strlen(str1) + strlen(str2) + 1);

	strcat(text, str1);

	// Append the second string
	strcat(text, str2);

	((*yyvalp).ast) = ASTLeaf(AST_STRING_LITERAL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), text);
;}
    break;

  case 428:

/* Line 936 of glr.c  */
#line 4256 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 429:

/* Line 936 of glr.c  */
#line 4282 "src/frontend/c99.y"
    {
	*parsed_tree = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 430:

/* Line 936 of glr.c  */
#line 4288 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 431:

/* Line 936 of glr.c  */
#line 4292 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 432:

/* Line 936 of glr.c  */
#line 4296 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = NULL;
;}
    break;

  case 433:

/* Line 936 of glr.c  */
#line 4300 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 434:

/* Line 936 of glr.c  */
#line 4304 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 435:

/* Line 936 of glr.c  */
#line 4308 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 436:

/* Line 936 of glr.c  */
#line 4312 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 437:

/* Line 936 of glr.c  */
#line 4316 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 438:

/* Line 936 of glr.c  */
#line 4320 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 439:

/* Line 936 of glr.c  */
#line 4324 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 440:

/* Line 936 of glr.c  */
#line 4351 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 441:

/* Line 936 of glr.c  */
#line 4355 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 442:

/* Line 936 of glr.c  */
#line 4383 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_NODECL_LITERAL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 443:

/* Line 936 of glr.c  */
#line 4394 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_NODECL_LITERAL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 444:

/* Line 936 of glr.c  */
#line 4400 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 445:

/* Line 936 of glr.c  */
#line 4404 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 446:

/* Line 936 of glr.c  */
#line 4410 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_SYMBOL_LITERAL_REF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 447:

/* Line 936 of glr.c  */
#line 4416 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_TYPE_LITERAL_REF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 448:

/* Line 936 of glr.c  */
#line 4427 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_STRING_LITERAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 449:

/* Line 936 of glr.c  */
#line 4435 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 450:

/* Line 936 of glr.c  */
#line 4441 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 451:

/* Line 936 of glr.c  */
#line 4455 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 452:

/* Line 936 of glr.c  */
#line 4466 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 453:

/* Line 936 of glr.c  */
#line 4502 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_MCC_ARRAY_SUBSCRIPT_CHECK, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 454:

/* Line 936 of glr.c  */
#line 4506 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_MCC_CONSTANT_VALUE_CHECK, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 455:

/* Line 936 of glr.c  */
#line 4512 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_ARRAY_SECTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 456:

/* Line 936 of glr.c  */
#line 4516 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_ARRAY_SECTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (8))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 457:

/* Line 936 of glr.c  */
#line 4520 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_ARRAY_SECTION_SIZE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 458:

/* Line 936 of glr.c  */
#line 4524 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_ARRAY_SECTION_SIZE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (8))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (8))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (8))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 459:

/* Line 936 of glr.c  */
#line 4530 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 460:

/* Line 936 of glr.c  */
#line 4534 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_CAST, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 461:

/* Line 936 of glr.c  */
#line 4540 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_SHAPING_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 462:

/* Line 936 of glr.c  */
#line 4546 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 463:

/* Line 936 of glr.c  */
#line 4550 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 464:

/* Line 936 of glr.c  */
#line 4556 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 465:

/* Line 936 of glr.c  */
#line 4562 "src/frontend/c99.y"
    {
    AST loop_control = ASTMake3(AST_LOOP_CONTROL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (11))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (11))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((9) - (11))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_column), NULL);
	((*yyvalp).ast) = ASTMake4(AST_FOR_STATEMENT, loop_control, (((yyGLRStackItem const *)yyvsp)[YYFILL ((11) - (11))].yystate.yysemantics.yysval.ast), NULL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (11))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (11))].yystate.yyloc).first_column), NULL);

;}
    break;

  case 466:

/* Line 936 of glr.c  */
#line 4571 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 467:

/* Line 936 of glr.c  */
#line 4586 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_MCC_BOOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 468:

/* Line 936 of glr.c  */
#line 4590 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_MCC_MASK, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 469:

/* Line 936 of glr.c  */
#line 4608 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 470:

/* Line 936 of glr.c  */
#line 4614 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_FORTRAN_ALLOCATE_STATEMENT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 471:

/* Line 936 of glr.c  */
#line 4635 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 472:

/* Line 936 of glr.c  */
#line 4643 "src/frontend/c99.y"
    {
    // This is an empty statement
    ((*yyvalp).ast) = ASTLeaf(AST_STATEMENT_PLACEHOLDER, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 473:

/* Line 936 of glr.c  */
#line 4735 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_UNKNOWN_PRAGMA, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 474:

/* Line 936 of glr.c  */
#line 4741 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 475:

/* Line 936 of glr.c  */
#line 4747 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 476:

/* Line 936 of glr.c  */
#line 4777 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 477:

/* Line 936 of glr.c  */
#line 4781 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 478:

/* Line 936 of glr.c  */
#line 4787 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 479:

/* Line 936 of glr.c  */
#line 4791 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 480:

/* Line 936 of glr.c  */
#line 4797 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 481:

/* Line 936 of glr.c  */
#line 4801 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 482:

/* Line 936 of glr.c  */
#line 4839 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_DIRECTIVE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 483:

/* Line 936 of glr.c  */
#line 4846 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_PRAGMA_CUSTOM_CONSTRUCT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 484:

/* Line 936 of glr.c  */
#line 4852 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_PRAGMA_CUSTOM_CONSTRUCT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 485:

/* Line 936 of glr.c  */
#line 4858 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake3(AST_PRAGMA_CUSTOM_CONSTRUCT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 486:

/* Line 936 of glr.c  */
#line 4988 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_LINE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 487:

/* Line 936 of glr.c  */
#line 4992 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_LINE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 488:

/* Line 936 of glr.c  */
#line 4996 "src/frontend/c99.y"
    {
    // This is a degenerated case caused by wrong designed pragmas
    ((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_LINE, NULL, NULL, make_locus("", 0, 0), NULL);
;}
    break;

  case 489:

/* Line 936 of glr.c  */
#line 5003 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_LINE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 490:

/* Line 936 of glr.c  */
#line 5007 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_PRAGMA_CUSTOM_LINE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 491:

/* Line 936 of glr.c  */
#line 5013 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = NULL;
;}
    break;

  case 492:

/* Line 936 of glr.c  */
#line 5017 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 493:

/* Line 936 of glr.c  */
#line 5023 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 494:

/* Line 936 of glr.c  */
#line 5027 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 495:

/* Line 936 of glr.c  */
#line 5031 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 496:

/* Line 936 of glr.c  */
#line 5037 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PRAGMA_CUSTOM_CLAUSE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 497:

/* Line 936 of glr.c  */
#line 5041 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PRAGMA_CUSTOM_CLAUSE, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 498:

/* Line 936 of glr.c  */
#line 5045 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_PRAGMA_CUSTOM_CLAUSE, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 499:

/* Line 936 of glr.c  */
#line 5051 "src/frontend/c99.y"
    {
    AST node = ASTLeaf(AST_PRAGMA_CLAUSE_ARG, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);

    ((*yyvalp).ast) = ASTListLeaf(node);
;}
    break;

  case 500:

/* Line 936 of glr.c  */
#line 5059 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 501:

/* Line 936 of glr.c  */
#line 5063 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib).token_text = strappend((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.token_atrib).token_text, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 502:

/* Line 936 of glr.c  */
#line 5069 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 503:

/* Line 936 of glr.c  */
#line 5075 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 504:

/* Line 936 of glr.c  */
#line 5083 "src/frontend/c99.y"
    {
    AST ident = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);

    ((*yyvalp).ast) = ASTMake1(AST_VERBATIM, ident, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (6))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 505:

/* Line 936 of glr.c  */
#line 5089 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_VERBATIM, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 506:

/* Line 936 of glr.c  */
#line 5095 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 507:

/* Line 936 of glr.c  */
#line 5101 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 508:

/* Line 936 of glr.c  */
#line 5130 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 509:

/* Line 936 of glr.c  */
#line 5134 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 510:

/* Line 936 of glr.c  */
#line 5140 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_OMP_DECLARE_REDUCTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 511:

/* Line 936 of glr.c  */
#line 5144 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_OMP_DECLARE_REDUCTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (7))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 512:

/* Line 936 of glr.c  */
#line 5150 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OMP_DR_OPERATOR, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 513:

/* Line 936 of glr.c  */
#line 5154 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 514:

/* Line 936 of glr.c  */
#line 5160 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OMP_DR_IDENTIFIER, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 515:

/* Line 936 of glr.c  */
#line 5166 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 516:

/* Line 936 of glr.c  */
#line 5170 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 525:

/* Line 936 of glr.c  */
#line 5187 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 526:

/* Line 936 of glr.c  */
#line 5206 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 527:

/* Line 936 of glr.c  */
#line 5213 "src/frontend/c99.y"
    {
    AST declarator_id = ASTMake1(AST_DECLARATOR_ID_EXPR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
    AST declarator = ASTMake1(AST_DECLARATOR, declarator_id, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);

    ((*yyvalp).ast) = ASTMake2(AST_INIT_DECLARATOR, declarator, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 528:

/* Line 936 of glr.c  */
#line 5220 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_FUNCTION_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 529:

/* Line 936 of glr.c  */
#line 5224 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_FUNCTION_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 530:

/* Line 936 of glr.c  */
#line 5251 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 531:

/* Line 936 of glr.c  */
#line 5255 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 532:

/* Line 936 of glr.c  */
#line 5261 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 533:

/* Line 936 of glr.c  */
#line 5265 "src/frontend/c99.y"
    {
    // Note that this is to be interpreted as a [lower:size] (not [lower:upper]),
    // so we create an AST_ARRAY_SECTION_SIZE here
    ((*yyvalp).ast) = ASTMake4(AST_ARRAY_SECTION_SIZE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 534:

/* Line 936 of glr.c  */
#line 5271 "src/frontend/c99.y"
    {
    // Note that this is to be interpreted as a [lower:size] (not [lower:upper]),
    // so we create an AST_ARRAY_SECTION_SIZE here
    ((*yyvalp).ast) = ASTMake2(AST_ARRAY_SUBSCRIPT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 535:

/* Line 936 of glr.c  */
#line 5296 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 536:

/* Line 936 of glr.c  */
#line 5302 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 537:

/* Line 936 of glr.c  */
#line 5306 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 538:

/* Line 936 of glr.c  */
#line 5312 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 539:

/* Line 936 of glr.c  */
#line 5318 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 540:

/* Line 936 of glr.c  */
#line 5324 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_OMPSS_MULTI_DEPENDENCY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 541:

/* Line 936 of glr.c  */
#line 5328 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_OMPSS_MULTI_DEPENDENCY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 542:

/* Line 936 of glr.c  */
#line 5334 "src/frontend/c99.y"
    {
    AST symbol = ASTLeaf(AST_SYMBOL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.token_atrib).token_text);
    ((*yyvalp).ast) = ASTMake2(AST_OMPSS_ITERATOR, symbol, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus(symbol), NULL);
;}
    break;

  case 543:

/* Line 936 of glr.c  */
#line 5341 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 544:

/* Line 936 of glr.c  */
#line 5345 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 545:

/* Line 936 of glr.c  */
#line 5351 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_OMPSS_ITERATOR_RANGE_SECTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 546:

/* Line 936 of glr.c  */
#line 5355 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_OMPSS_ITERATOR_RANGE_SECTION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 547:

/* Line 936 of glr.c  */
#line 5361 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_OMPSS_ITERATOR_RANGE_SIZE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 548:

/* Line 936 of glr.c  */
#line 5365 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_OMPSS_ITERATOR_RANGE_SIZE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 549:

/* Line 936 of glr.c  */
#line 5390 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SUPERSCALAR_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 550:

/* Line 936 of glr.c  */
#line 5394 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 551:

/* Line 936 of glr.c  */
#line 5398 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SUPERSCALAR_EXPRESSION, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 552:

/* Line 936 of glr.c  */
#line 5404 "src/frontend/c99.y"
    {
	AST ss_decl = ASTMake2(AST_SUPERSCALAR_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), NULL);
    ((*yyvalp).ast) = ASTListLeaf(ss_decl);
;}
    break;

  case 553:

/* Line 936 of glr.c  */
#line 5409 "src/frontend/c99.y"
    {
	AST ss_decl = ASTMake2(AST_SUPERSCALAR_DECLARATOR, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (4))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast)), NULL);
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), ss_decl);
;}
    break;

  case 554:

/* Line 936 of glr.c  */
#line 5416 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 555:

/* Line 936 of glr.c  */
#line 5420 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake4(AST_DECLARATOR_ARRAY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 556:

/* Line 936 of glr.c  */
#line 5427 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = NULL;
;}
    break;

  case 558:

/* Line 936 of glr.c  */
#line 5434 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 559:

/* Line 936 of glr.c  */
#line 5438 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 560:

/* Line 936 of glr.c  */
#line 5444 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTLeaf(AST_SUPERSCALAR_REGION_SPEC_FULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 561:

/* Line 936 of glr.c  */
#line 5448 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake1(AST_SUPERSCALAR_REGION_SPEC_SINGLE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 562:

/* Line 936 of glr.c  */
#line 5452 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SUPERSCALAR_REGION_SPEC_RANGE, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 563:

/* Line 936 of glr.c  */
#line 5456 "src/frontend/c99.y"
    {
	((*yyvalp).ast) = ASTMake2(AST_SUPERSCALAR_REGION_SPEC_LENGTH, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (5))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 564:

/* Line 936 of glr.c  */
#line 5491 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_LOCALSIZEOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 565:

/* Line 936 of glr.c  */
#line 5495 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_LOCALSIZEOF_TYPEID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 566:

/* Line 936 of glr.c  */
#line 5499 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_BLOCKSIZEOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 567:

/* Line 936 of glr.c  */
#line 5503 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_BLOCKSIZEOF_TYPEID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 568:

/* Line 936 of glr.c  */
#line 5507 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_ELEMSIZEOF, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 569:

/* Line 936 of glr.c  */
#line 5511 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_ELEMSIZEOF_TYPEID, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 570:

/* Line 936 of glr.c  */
#line 5517 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 571:

/* Line 936 of glr.c  */
#line 5521 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 572:

/* Line 936 of glr.c  */
#line 5527 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_SHARED, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 573:

/* Line 936 of glr.c  */
#line 5531 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_SHARED, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 574:

/* Line 936 of glr.c  */
#line 5537 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_UPC_RELAXED, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 575:

/* Line 936 of glr.c  */
#line 5541 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_UPC_STRICT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 576:

/* Line 936 of glr.c  */
#line 5548 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 577:

/* Line 936 of glr.c  */
#line 5552 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 578:

/* Line 936 of glr.c  */
#line 5558 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_LAYOUT_QUALIFIER, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 579:

/* Line 936 of glr.c  */
#line 5562 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_LAYOUT_QUALIFIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 580:

/* Line 936 of glr.c  */
#line 5566 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_LAYOUT_QUALIFIER, 
            ASTLeaf(AST_UPC_LAYOUT_UNDEF, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yyloc).first_column), NULL), 
            make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 581:

/* Line 936 of glr.c  */
#line 5574 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 582:

/* Line 936 of glr.c  */
#line 5580 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_NOTIFY, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 583:

/* Line 936 of glr.c  */
#line 5584 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_WAIT, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 584:

/* Line 936 of glr.c  */
#line 5588 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_UPC_BARRIER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 585:

/* Line 936 of glr.c  */
#line 5592 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_UPC_FENCE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 586:

/* Line 936 of glr.c  */
#line 5598 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 587:

/* Line 936 of glr.c  */
#line 5602 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 588:

/* Line 936 of glr.c  */
#line 5608 "src/frontend/c99.y"
    {
    AST upc_forall_header =
        ASTMake4(AST_UPC_FORALL_HEADER, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (10))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (10))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((6) - (10))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((8) - (10))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_column), NULL);

    ((*yyvalp).ast) = ASTMake2(AST_UPC_FORALL, upc_forall_header, (((yyGLRStackItem const *)yyvsp)[YYFILL ((10) - (10))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (10))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 589:

/* Line 936 of glr.c  */
#line 5617 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 590:

/* Line 936 of glr.c  */
#line 5621 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 591:

/* Line 936 of glr.c  */
#line 5626 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 592:

/* Line 936 of glr.c  */
#line 5630 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_UPC_CONTINUE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 593:

/* Line 936 of glr.c  */
#line 5657 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 594:

/* Line 936 of glr.c  */
#line 5663 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 595:

/* Line 936 of glr.c  */
#line 5669 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_CUDA_DEVICE, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 596:

/* Line 936 of glr.c  */
#line 5673 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_CUDA_GLOBAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 597:

/* Line 936 of glr.c  */
#line 5677 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_CUDA_HOST, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 598:

/* Line 936 of glr.c  */
#line 5681 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_CUDA_CONSTANT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 599:

/* Line 936 of glr.c  */
#line 5685 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_CUDA_SHARED, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 600:

/* Line 936 of glr.c  */
#line 5691 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_CUDA_KERNEL_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (4))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 601:

/* Line 936 of glr.c  */
#line 5695 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake3(AST_CUDA_KERNEL_CALL, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((4) - (5))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 602:

/* Line 936 of glr.c  */
#line 5701 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (3))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 603:

/* Line 936 of glr.c  */
#line 5707 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_CUDA_KERNEL_ARGUMENTS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (7))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((7) - (7))].yystate.yysemantics.yysval.ast), ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (7))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 604:

/* Line 936 of glr.c  */
#line 5711 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_CUDA_KERNEL_ARGUMENTS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (5))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (5))].yystate.yysemantics.yysval.ast), NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (5))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 605:

/* Line 936 of glr.c  */
#line 5715 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake4(AST_CUDA_KERNEL_ARGUMENTS, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast), NULL, NULL, ast_get_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast)), NULL);
;}
    break;

  case 606:

/* Line 936 of glr.c  */
#line 5721 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 607:

/* Line 936 of glr.c  */
#line 5728 "src/frontend/c99.y"
    {
    ((*yyvalp).token_atrib) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib);
;}
    break;

  case 608:

/* Line 936 of glr.c  */
#line 5760 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 609:

/* Line 936 of glr.c  */
#line 5766 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OPENCL_GLOBAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 610:

/* Line 936 of glr.c  */
#line 5770 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OPENCL_KERNEL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 611:

/* Line 936 of glr.c  */
#line 5774 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OPENCL_CONSTANT, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 612:

/* Line 936 of glr.c  */
#line 5778 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_OPENCL_LOCAL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 613:

/* Line 936 of glr.c  */
#line 5791 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_XL_BUILTIN_SPEC, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, 0), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 614:

/* Line 936 of glr.c  */
#line 5798 "src/frontend/c99.y"
    {
    if (CURRENT_CONFIGURATION->xl_compatibility)
    {
        ((*yyvalp).ast) = ast_list_concat(ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)), (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast));
    }
    else
    {
        warn_printf("%s: warning: ignoring '#pragma %s' after the declarator\n",
                 ast_location((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)),
                 ast_get_text((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (2))].yystate.yysemantics.yysval.ast)));
        ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((2) - (2))].yystate.yysemantics.yysval.ast);
    }
;}
    break;

  case 615:

/* Line 936 of glr.c  */
#line 5812 "src/frontend/c99.y"
    {
    if (CURRENT_CONFIGURATION->xl_compatibility)
    {
        ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
    }
    else
    {
        warn_printf("%s: warning: ignoring '#pragma %s' after the declarator\n",
                 ast_location((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)),
                 ast_get_text((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast)));
        ((*yyvalp).ast) = NULL;
    }
;}
    break;

  case 616:

/* Line 936 of glr.c  */
#line 5847 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 617:

/* Line 936 of glr.c  */
#line 5853 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_MS_DECLSPEC, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 618:

/* Line 936 of glr.c  */
#line 5859 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = NULL;
;}
    break;

  case 619:

/* Line 936 of glr.c  */
#line 5863 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 620:

/* Line 936 of glr.c  */
#line 5869 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTListLeaf((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 621:

/* Line 936 of glr.c  */
#line 5873 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTList((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (3))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (3))].yystate.yysemantics.yysval.ast));
;}
    break;

  case 622:

/* Line 936 of glr.c  */
#line 5879 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_MS_DECLSPEC_ITEM, NULL, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 623:

/* Line 936 of glr.c  */
#line 5883 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_MS_DECLSPEC_ITEM, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yysemantics.yysval.token_atrib).token_text);
;}
    break;

  case 624:

/* Line 936 of glr.c  */
#line 5889 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yysemantics.yysval.ast);
;}
    break;

  case 625:

/* Line 936 of glr.c  */
#line 5895 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_MS_INT8, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 626:

/* Line 936 of glr.c  */
#line 5899 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_MS_INT16, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 627:

/* Line 936 of glr.c  */
#line 5903 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_MS_INT32, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 628:

/* Line 936 of glr.c  */
#line 5907 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTLeaf(AST_MS_INT64, make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (1))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 629:

/* Line 936 of glr.c  */
#line 5923 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake1(AST_INTEL_ASSUME, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (4))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (4))].yystate.yyloc).first_column), NULL);
;}
    break;

  case 630:

/* Line 936 of glr.c  */
#line 5927 "src/frontend/c99.y"
    {
    ((*yyvalp).ast) = ASTMake2(AST_INTEL_ASSUME_ALIGNED, (((yyGLRStackItem const *)yyvsp)[YYFILL ((3) - (6))].yystate.yysemantics.yysval.ast), (((yyGLRStackItem const *)yyvsp)[YYFILL ((5) - (6))].yystate.yysemantics.yysval.ast), make_locus((((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_filename, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_line, (((yyGLRStackItem const *)yyvsp)[YYFILL ((1) - (6))].yystate.yyloc).first_column), NULL);
;}
    break;



/* Line 936 of glr.c  */
#line 10984 "src/frontend/c99-parser.c"
      default: break;
    }

  return yyok;
# undef yyerrok
# undef YYABORT
# undef YYACCEPT
# undef YYERROR
# undef YYBACKUP
# undef yyclearin
# undef YYRECOVERING
}


/*ARGSUSED*/ static void
yyuserMerge (int yyn, YYSTYPE* yy0, YYSTYPE* yy1)
{
  YYUSE (yy0);
  YYUSE (yy1);

  switch (yyn)
    {
        case 1: yy0->ast = ambiguityHandler (*yy0, *yy1); break;

      default: break;
    }
}

			      /* Bison grammar-table manipulation.  */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

/*ARGSUSED*/
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep, YYLTYPE *yylocationp, AST* parsed_tree)
{
  YYUSE (yyvaluep);
  YYUSE (yylocationp);
  YYUSE (parsed_tree);

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
	break;
    }
}

/** Number of symbols composing the right hand side of rule #RULE.  */
static inline int
yyrhsLength (yyRuleNum yyrule)
{
  return yyr2[yyrule];
}

static void
yydestroyGLRState (char const *yymsg, yyGLRState *yys, AST* parsed_tree)
{
  if (yys->yyresolved)
    yydestruct (yymsg, yystos[yys->yylrState],
		&yys->yysemantics.yysval, &yys->yyloc, parsed_tree);
  else
    {
#if YYDEBUG
      if (yydebug)
	{
	  if (yys->yysemantics.yyfirstVal)
	    YYFPRINTF (stderr, "%s unresolved ", yymsg);
	  else
	    YYFPRINTF (stderr, "%s incomplete ", yymsg);
	  yy_symbol_print (stderr, yystos[yys->yylrState],
			   NULL, &yys->yyloc, parsed_tree);
	  YYFPRINTF (stderr, "\n");
	}
#endif

      if (yys->yysemantics.yyfirstVal)
	{
	  yySemanticOption *yyoption = yys->yysemantics.yyfirstVal;
	  yyGLRState *yyrh;
	  int yyn;
	  for (yyrh = yyoption->yystate, yyn = yyrhsLength (yyoption->yyrule);
	       yyn > 0;
	       yyrh = yyrh->yypred, yyn -= 1)
	    yydestroyGLRState (yymsg, yyrh, parsed_tree);
	}
    }
}

/** Left-hand-side symbol for rule #RULE.  */
static inline yySymbol
yylhsNonterm (yyRuleNum yyrule)
{
  return yyr1[yyrule];
}

#define yyis_pact_ninf(yystate) \
  ((yystate) == YYPACT_NINF)

/** True iff LR state STATE has only a default reduction (regardless
 *  of token).  */
static inline yybool
yyisDefaultedState (yyStateNum yystate)
{
  return yyis_pact_ninf (yypact[yystate]);
}

/** The default reduction for STATE, assuming it has one.  */
static inline yyRuleNum
yydefaultAction (yyStateNum yystate)
{
  return yydefact[yystate];
}

#define yyis_table_ninf(yytable_value) \
  YYID (0)

/** Set *YYACTION to the action to take in YYSTATE on seeing YYTOKEN.
 *  Result R means
 *    R < 0:  Reduce on rule -R.
 *    R = 0:  Error.
 *    R > 0:  Shift to state R.
 *  Set *CONFLICTS to a pointer into yyconfl to 0-terminated list of
 *  conflicting reductions.
 */
static inline void
yygetLRActions (yyStateNum yystate, int yytoken,
		int* yyaction, const short int** yyconflicts)
{
  int yyindex = yypact[yystate] + yytoken;
  if (yyindex < 0 || YYLAST < yyindex || yycheck[yyindex] != yytoken)
    {
      *yyaction = -yydefact[yystate];
      *yyconflicts = yyconfl;
    }
  else if (! yyis_table_ninf (yytable[yyindex]))
    {
      *yyaction = yytable[yyindex];
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
  else
    {
      *yyaction = 0;
      *yyconflicts = yyconfl + yyconflp[yyindex];
    }
}

static inline yyStateNum
yyLRgotoState (yyStateNum yystate, yySymbol yylhs)
{
  int yyr;
  yyr = yypgoto[yylhs - YYNTOKENS] + yystate;
  if (0 <= yyr && yyr <= YYLAST && yycheck[yyr] == yystate)
    return yytable[yyr];
  else
    return yydefgoto[yylhs - YYNTOKENS];
}

static inline yybool
yyisShiftAction (int yyaction)
{
  return 0 < yyaction;
}

static inline yybool
yyisErrorAction (int yyaction)
{
  return yyaction == 0;
}

				/* GLRStates */

/** Return a fresh GLRStackItem.  Callers should call
 * YY_RESERVE_GLRSTACK afterwards to make sure there is sufficient
 * headroom.  */

static inline yyGLRStackItem*
yynewGLRStackItem (yyGLRStack* yystackp, yybool yyisState)
{
  yyGLRStackItem* yynewItem = yystackp->yynextFree;
  yystackp->yyspaceLeft -= 1;
  yystackp->yynextFree += 1;
  yynewItem->yystate.yyisState = yyisState;
  return yynewItem;
}

/** Add a new semantic action that will execute the action for rule
 *  RULENUM on the semantic values in RHS to the list of
 *  alternative actions for STATE.  Assumes that RHS comes from
 *  stack #K of *STACKP. */
static void
yyaddDeferredAction (yyGLRStack* yystackp, size_t yyk, yyGLRState* yystate,
		     yyGLRState* rhs, yyRuleNum yyrule)
{
  yySemanticOption* yynewOption =
    &yynewGLRStackItem (yystackp, yyfalse)->yyoption;
  yynewOption->yystate = rhs;
  yynewOption->yyrule = yyrule;
  if (yystackp->yytops.yylookaheadNeeds[yyk])
    {
      yynewOption->yyrawchar = yychar;
      yynewOption->yyval = yylval;
      yynewOption->yyloc = yylloc;
    }
  else
    yynewOption->yyrawchar = YYEMPTY;
  yynewOption->yynext = yystate->yysemantics.yyfirstVal;
  yystate->yysemantics.yyfirstVal = yynewOption;

  YY_RESERVE_GLRSTACK (yystackp);
}

				/* GLRStacks */

/** Initialize SET to a singleton set containing an empty stack.  */
static yybool
yyinitStateSet (yyGLRStateSet* yyset)
{
  yyset->yysize = 1;
  yyset->yycapacity = 16;
  yyset->yystates = (yyGLRState**) YYMALLOC (16 * sizeof yyset->yystates[0]);
  if (! yyset->yystates)
    return yyfalse;
  yyset->yystates[0] = NULL;
  yyset->yylookaheadNeeds =
    (yybool*) YYMALLOC (16 * sizeof yyset->yylookaheadNeeds[0]);
  if (! yyset->yylookaheadNeeds)
    {
      YYFREE (yyset->yystates);
      return yyfalse;
    }
  return yytrue;
}

static void yyfreeStateSet (yyGLRStateSet* yyset)
{
  YYFREE (yyset->yystates);
  YYFREE (yyset->yylookaheadNeeds);
}

/** Initialize STACK to a single empty stack, with total maximum
 *  capacity for all stacks of SIZE.  */
static yybool
yyinitGLRStack (yyGLRStack* yystackp, size_t yysize)
{
  yystackp->yyerrState = 0;
  yynerrs = 0;
  yystackp->yyspaceLeft = yysize;
  yystackp->yyitems =
    (yyGLRStackItem*) YYMALLOC (yysize * sizeof yystackp->yynextFree[0]);
  if (!yystackp->yyitems)
    return yyfalse;
  yystackp->yynextFree = yystackp->yyitems;
  yystackp->yysplitPoint = NULL;
  yystackp->yylastDeleted = NULL;
  return yyinitStateSet (&yystackp->yytops);
}


#if YYSTACKEXPANDABLE
# define YYRELOC(YYFROMITEMS,YYTOITEMS,YYX,YYTYPE) \
  &((YYTOITEMS) - ((YYFROMITEMS) - (yyGLRStackItem*) (YYX)))->YYTYPE

/** If STACK is expandable, extend it.  WARNING: Pointers into the
    stack from outside should be considered invalid after this call.
    We always expand when there are 1 or fewer items left AFTER an
    allocation, so that we can avoid having external pointers exist
    across an allocation.  */
static void
yyexpandGLRStack (yyGLRStack* yystackp)
{
  yyGLRStackItem* yynewItems;
  yyGLRStackItem* yyp0, *yyp1;
  size_t yysize, yynewSize;
  size_t yyn;
  yysize = yystackp->yynextFree - yystackp->yyitems;
  if (YYMAXDEPTH - YYHEADROOM < yysize)
    yyMemoryExhausted (yystackp);
  yynewSize = 2*yysize;
  if (YYMAXDEPTH < yynewSize)
    yynewSize = YYMAXDEPTH;
  yynewItems = (yyGLRStackItem*) YYMALLOC (yynewSize * sizeof yynewItems[0]);
  if (! yynewItems)
    yyMemoryExhausted (yystackp);
  for (yyp0 = yystackp->yyitems, yyp1 = yynewItems, yyn = yysize;
       0 < yyn;
       yyn -= 1, yyp0 += 1, yyp1 += 1)
    {
      *yyp1 = *yyp0;
      if (*(yybool *) yyp0)
	{
	  yyGLRState* yys0 = &yyp0->yystate;
	  yyGLRState* yys1 = &yyp1->yystate;
	  if (yys0->yypred != NULL)
	    yys1->yypred =
	      YYRELOC (yyp0, yyp1, yys0->yypred, yystate);
	  if (! yys0->yyresolved && yys0->yysemantics.yyfirstVal != NULL)
	    yys1->yysemantics.yyfirstVal =
	      YYRELOC(yyp0, yyp1, yys0->yysemantics.yyfirstVal, yyoption);
	}
      else
	{
	  yySemanticOption* yyv0 = &yyp0->yyoption;
	  yySemanticOption* yyv1 = &yyp1->yyoption;
	  if (yyv0->yystate != NULL)
	    yyv1->yystate = YYRELOC (yyp0, yyp1, yyv0->yystate, yystate);
	  if (yyv0->yynext != NULL)
	    yyv1->yynext = YYRELOC (yyp0, yyp1, yyv0->yynext, yyoption);
	}
    }
  if (yystackp->yysplitPoint != NULL)
    yystackp->yysplitPoint = YYRELOC (yystackp->yyitems, yynewItems,
				 yystackp->yysplitPoint, yystate);

  for (yyn = 0; yyn < yystackp->yytops.yysize; yyn += 1)
    if (yystackp->yytops.yystates[yyn] != NULL)
      yystackp->yytops.yystates[yyn] =
	YYRELOC (yystackp->yyitems, yynewItems,
		 yystackp->yytops.yystates[yyn], yystate);
  YYFREE (yystackp->yyitems);
  yystackp->yyitems = yynewItems;
  yystackp->yynextFree = yynewItems + yysize;
  yystackp->yyspaceLeft = yynewSize - yysize;
}
#endif

static void
yyfreeGLRStack (yyGLRStack* yystackp)
{
  YYFREE (yystackp->yyitems);
  yyfreeStateSet (&yystackp->yytops);
}

/** Assuming that S is a GLRState somewhere on STACK, update the
 *  splitpoint of STACK, if needed, so that it is at least as deep as
 *  S.  */
static inline void
yyupdateSplit (yyGLRStack* yystackp, yyGLRState* yys)
{
  if (yystackp->yysplitPoint != NULL && yystackp->yysplitPoint > yys)
    yystackp->yysplitPoint = yys;
}

/** Invalidate stack #K in STACK.  */
static inline void
yymarkStackDeleted (yyGLRStack* yystackp, size_t yyk)
{
  if (yystackp->yytops.yystates[yyk] != NULL)
    yystackp->yylastDeleted = yystackp->yytops.yystates[yyk];
  yystackp->yytops.yystates[yyk] = NULL;
}

/** Undelete the last stack that was marked as deleted.  Can only be
    done once after a deletion, and only when all other stacks have
    been deleted.  */
static void
yyundeleteLastStack (yyGLRStack* yystackp)
{
  if (yystackp->yylastDeleted == NULL || yystackp->yytops.yysize != 0)
    return;
  yystackp->yytops.yystates[0] = yystackp->yylastDeleted;
  yystackp->yytops.yysize = 1;
  YYDPRINTF ((stderr, "Restoring last deleted stack as stack #0.\n"));
  yystackp->yylastDeleted = NULL;
}

static inline void
yyremoveDeletes (yyGLRStack* yystackp)
{
  size_t yyi, yyj;
  yyi = yyj = 0;
  while (yyj < yystackp->yytops.yysize)
    {
      if (yystackp->yytops.yystates[yyi] == NULL)
	{
	  if (yyi == yyj)
	    {
	      YYDPRINTF ((stderr, "Removing dead stacks.\n"));
	    }
	  yystackp->yytops.yysize -= 1;
	}
      else
	{
	  yystackp->yytops.yystates[yyj] = yystackp->yytops.yystates[yyi];
	  /* In the current implementation, it's unnecessary to copy
	     yystackp->yytops.yylookaheadNeeds[yyi] since, after
	     yyremoveDeletes returns, the parser immediately either enters
	     deterministic operation or shifts a token.  However, it doesn't
	     hurt, and the code might evolve to need it.  */
	  yystackp->yytops.yylookaheadNeeds[yyj] =
	    yystackp->yytops.yylookaheadNeeds[yyi];
	  if (yyj != yyi)
	    {
	      YYDPRINTF ((stderr, "Rename stack %lu -> %lu.\n",
			  (unsigned long int) yyi, (unsigned long int) yyj));
	    }
	  yyj += 1;
	}
      yyi += 1;
    }
}

/** Shift to a new state on stack #K of STACK, corresponding to LR state
 * LRSTATE, at input position POSN, with (resolved) semantic value SVAL.  */
static inline void
yyglrShift (yyGLRStack* yystackp, size_t yyk, yyStateNum yylrState,
	    size_t yyposn,
	    YYSTYPE* yyvalp, YYLTYPE* yylocp)
{
  yyGLRState* yynewState = &yynewGLRStackItem (yystackp, yytrue)->yystate;

  yynewState->yylrState = yylrState;
  yynewState->yyposn = yyposn;
  yynewState->yyresolved = yytrue;
  yynewState->yypred = yystackp->yytops.yystates[yyk];
  yynewState->yysemantics.yysval = *yyvalp;
  yynewState->yyloc = *yylocp;
  yystackp->yytops.yystates[yyk] = yynewState;

  YY_RESERVE_GLRSTACK (yystackp);
}

/** Shift stack #K of YYSTACK, to a new state corresponding to LR
 *  state YYLRSTATE, at input position YYPOSN, with the (unresolved)
 *  semantic value of YYRHS under the action for YYRULE.  */
static inline void
yyglrShiftDefer (yyGLRStack* yystackp, size_t yyk, yyStateNum yylrState,
		 size_t yyposn, yyGLRState* rhs, yyRuleNum yyrule)
{
  yyGLRState* yynewState = &yynewGLRStackItem (yystackp, yytrue)->yystate;

  yynewState->yylrState = yylrState;
  yynewState->yyposn = yyposn;
  yynewState->yyresolved = yyfalse;
  yynewState->yypred = yystackp->yytops.yystates[yyk];
  yynewState->yysemantics.yyfirstVal = NULL;
  yystackp->yytops.yystates[yyk] = yynewState;

  /* Invokes YY_RESERVE_GLRSTACK.  */
  yyaddDeferredAction (yystackp, yyk, yynewState, rhs, yyrule);
}

/** Pop the symbols consumed by reduction #RULE from the top of stack
 *  #K of STACK, and perform the appropriate semantic action on their
 *  semantic values.  Assumes that all ambiguities in semantic values
 *  have been previously resolved.  Set *VALP to the resulting value,
 *  and *LOCP to the computed location (if any).  Return value is as
 *  for userAction.  */
static inline YYRESULTTAG
yydoAction (yyGLRStack* yystackp, size_t yyk, yyRuleNum yyrule,
	    YYSTYPE* yyvalp, YYLTYPE* yylocp, AST* parsed_tree)
{
  int yynrhs = yyrhsLength (yyrule);

  if (yystackp->yysplitPoint == NULL)
    {
      /* Standard special case: single stack.  */
      yyGLRStackItem* rhs = (yyGLRStackItem*) yystackp->yytops.yystates[yyk];
      YYASSERT (yyk == 0);
      yystackp->yynextFree -= yynrhs;
      yystackp->yyspaceLeft += yynrhs;
      yystackp->yytops.yystates[0] = & yystackp->yynextFree[-1].yystate;
      return yyuserAction (yyrule, yynrhs, rhs,
			   yyvalp, yylocp, yystackp, parsed_tree);
    }
  else
    {
      /* At present, doAction is never called in nondeterministic
       * mode, so this branch is never taken.  It is here in
       * anticipation of a future feature that will allow immediate
       * evaluation of selected actions in nondeterministic mode.  */
      int yyi;
      yyGLRState* yys;
      yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
      yys = yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred
	= yystackp->yytops.yystates[yyk];
      if (yynrhs == 0)
	/* Set default location.  */
	yyrhsVals[YYMAXRHS + YYMAXLEFT - 1].yystate.yyloc = yys->yyloc;
      for (yyi = 0; yyi < yynrhs; yyi += 1)
	{
	  yys = yys->yypred;
	  YYASSERT (yys);
	}
      yyupdateSplit (yystackp, yys);
      yystackp->yytops.yystates[yyk] = yys;
      return yyuserAction (yyrule, yynrhs, yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
			   yyvalp, yylocp, yystackp, parsed_tree);
    }
}

#if !YYDEBUG
# define YY_REDUCE_PRINT(Args)
#else
# define YY_REDUCE_PRINT(Args)		\
do {					\
  if (yydebug)				\
    yy_reduce_print Args;		\
} while (YYID (0))

/*----------------------------------------------------------.
| Report that the RULE is going to be reduced on stack #K.  |
`----------------------------------------------------------*/

/*ARGSUSED*/ static inline void
yy_reduce_print (yyGLRStack* yystackp, size_t yyk, yyRuleNum yyrule,
		 YYSTYPE* yyvalp, YYLTYPE* yylocp, AST* parsed_tree)
{
  int yynrhs = yyrhsLength (yyrule);
  yybool yynormal __attribute__ ((__unused__)) =
    (yystackp->yysplitPoint == NULL);
  yyGLRStackItem* yyvsp = (yyGLRStackItem*) yystackp->yytops.yystates[yyk];
  int yylow = 1;
  int yyi;
  YYUSE (yyvalp);
  YYUSE (yylocp);
  YYUSE (parsed_tree);
  YYFPRINTF (stderr, "Reducing stack %lu by rule %d (line %lu):\n",
	     (unsigned long int) yyk, yyrule - 1,
	     (unsigned long int) yyrline[yyrule]);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr, yyrhs[yyprhs[yyrule] + yyi],
		       &(((yyGLRStackItem const *)yyvsp)[YYFILL ((yyi + 1) - (yynrhs))].yystate.yysemantics.yysval)
		       , &(((yyGLRStackItem const *)yyvsp)[YYFILL ((yyi + 1) - (yynrhs))].yystate.yyloc)		       , parsed_tree);
      YYFPRINTF (stderr, "\n");
    }
}
#endif

/** Pop items off stack #K of STACK according to grammar rule RULE,
 *  and push back on the resulting nonterminal symbol.  Perform the
 *  semantic action associated with RULE and store its value with the
 *  newly pushed state, if FORCEEVAL or if STACK is currently
 *  unambiguous.  Otherwise, store the deferred semantic action with
 *  the new state.  If the new state would have an identical input
 *  position, LR state, and predecessor to an existing state on the stack,
 *  it is identified with that existing state, eliminating stack #K from
 *  the STACK.  In this case, the (necessarily deferred) semantic value is
 *  added to the options for the existing state's semantic value.
 */
static inline YYRESULTTAG
yyglrReduce (yyGLRStack* yystackp, size_t yyk, yyRuleNum yyrule,
	     yybool yyforceEval, AST* parsed_tree)
{
  size_t yyposn = yystackp->yytops.yystates[yyk]->yyposn;

  if (yyforceEval || yystackp->yysplitPoint == NULL)
    {
      YYSTYPE yysval;
      YYLTYPE yyloc;

      YY_REDUCE_PRINT ((yystackp, yyk, yyrule, &yysval, &yyloc, parsed_tree));
      YYCHK (yydoAction (yystackp, yyk, yyrule, &yysval,
			 &yyloc, parsed_tree));
      YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyrule], &yysval, &yyloc);
      yyglrShift (yystackp, yyk,
		  yyLRgotoState (yystackp->yytops.yystates[yyk]->yylrState,
				 yylhsNonterm (yyrule)),
		  yyposn, &yysval, &yyloc);
    }
  else
    {
      size_t yyi;
      int yyn;
      yyGLRState* yys, *yys0 = yystackp->yytops.yystates[yyk];
      yyStateNum yynewLRState;

      for (yys = yystackp->yytops.yystates[yyk], yyn = yyrhsLength (yyrule);
	   0 < yyn; yyn -= 1)
	{
	  yys = yys->yypred;
	  YYASSERT (yys);
	}
      yyupdateSplit (yystackp, yys);
      yynewLRState = yyLRgotoState (yys->yylrState, yylhsNonterm (yyrule));
      YYDPRINTF ((stderr,
		  "Reduced stack %lu by rule #%d; action deferred.  Now in state %d.\n",
		  (unsigned long int) yyk, yyrule - 1, yynewLRState));
      for (yyi = 0; yyi < yystackp->yytops.yysize; yyi += 1)
	if (yyi != yyk && yystackp->yytops.yystates[yyi] != NULL)
	  {
	    yyGLRState* yyp, *yysplit = yystackp->yysplitPoint;
	    yyp = yystackp->yytops.yystates[yyi];
	    while (yyp != yys && yyp != yysplit && yyp->yyposn >= yyposn)
	      {
		if (yyp->yylrState == yynewLRState && yyp->yypred == yys)
		  {
		    yyaddDeferredAction (yystackp, yyk, yyp, yys0, yyrule);
		    yymarkStackDeleted (yystackp, yyk);
		    YYDPRINTF ((stderr, "Merging stack %lu into stack %lu.\n",
				(unsigned long int) yyk,
				(unsigned long int) yyi));
		    return yyok;
		  }
		yyp = yyp->yypred;
	      }
	  }
      yystackp->yytops.yystates[yyk] = yys;
      yyglrShiftDefer (yystackp, yyk, yynewLRState, yyposn, yys0, yyrule);
    }
  return yyok;
}

static size_t
yysplitStack (yyGLRStack* yystackp, size_t yyk)
{
  if (yystackp->yysplitPoint == NULL)
    {
      YYASSERT (yyk == 0);
      yystackp->yysplitPoint = yystackp->yytops.yystates[yyk];
    }
  if (yystackp->yytops.yysize >= yystackp->yytops.yycapacity)
    {
      yyGLRState** yynewStates;
      yybool* yynewLookaheadNeeds;

      yynewStates = NULL;

      if (yystackp->yytops.yycapacity
	  > (YYSIZEMAX / (2 * sizeof yynewStates[0])))
	yyMemoryExhausted (yystackp);
      yystackp->yytops.yycapacity *= 2;

      yynewStates =
	(yyGLRState**) YYREALLOC (yystackp->yytops.yystates,
				  (yystackp->yytops.yycapacity
				   * sizeof yynewStates[0]));
      if (yynewStates == NULL)
	yyMemoryExhausted (yystackp);
      yystackp->yytops.yystates = yynewStates;

      yynewLookaheadNeeds =
	(yybool*) YYREALLOC (yystackp->yytops.yylookaheadNeeds,
			     (yystackp->yytops.yycapacity
			      * sizeof yynewLookaheadNeeds[0]));
      if (yynewLookaheadNeeds == NULL)
	yyMemoryExhausted (yystackp);
      yystackp->yytops.yylookaheadNeeds = yynewLookaheadNeeds;
    }
  yystackp->yytops.yystates[yystackp->yytops.yysize]
    = yystackp->yytops.yystates[yyk];
  yystackp->yytops.yylookaheadNeeds[yystackp->yytops.yysize]
    = yystackp->yytops.yylookaheadNeeds[yyk];
  yystackp->yytops.yysize += 1;
  return yystackp->yytops.yysize-1;
}

/** True iff Y0 and Y1 represent identical options at the top level.
 *  That is, they represent the same rule applied to RHS symbols
 *  that produce the same terminal symbols.  */
static yybool
yyidenticalOptions (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  if (yyy0->yyrule == yyy1->yyrule)
    {
      yyGLRState *yys0, *yys1;
      int yyn;
      for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
	   yyn = yyrhsLength (yyy0->yyrule);
	   yyn > 0;
	   yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
	if (yys0->yyposn != yys1->yyposn)
	  return yyfalse;
      return yytrue;
    }
  else
    return yyfalse;
}

/** Assuming identicalOptions (Y0,Y1), destructively merge the
 *  alternative semantic values for the RHS-symbols of Y1 and Y0.  */
static void
yymergeOptionSets (yySemanticOption* yyy0, yySemanticOption* yyy1)
{
  yyGLRState *yys0, *yys1;
  int yyn;
  for (yys0 = yyy0->yystate, yys1 = yyy1->yystate,
       yyn = yyrhsLength (yyy0->yyrule);
       yyn > 0;
       yys0 = yys0->yypred, yys1 = yys1->yypred, yyn -= 1)
    {
      if (yys0 == yys1)
	break;
      else if (yys0->yyresolved)
	{
	  yys1->yyresolved = yytrue;
	  yys1->yysemantics.yysval = yys0->yysemantics.yysval;
	}
      else if (yys1->yyresolved)
	{
	  yys0->yyresolved = yytrue;
	  yys0->yysemantics.yysval = yys1->yysemantics.yysval;
	}
      else
	{
	  yySemanticOption** yyz0p;
	  yySemanticOption* yyz1;
	  yyz0p = &yys0->yysemantics.yyfirstVal;
	  yyz1 = yys1->yysemantics.yyfirstVal;
	  while (YYID (yytrue))
	    {
	      if (yyz1 == *yyz0p || yyz1 == NULL)
		break;
	      else if (*yyz0p == NULL)
		{
		  *yyz0p = yyz1;
		  break;
		}
	      else if (*yyz0p < yyz1)
		{
		  yySemanticOption* yyz = *yyz0p;
		  *yyz0p = yyz1;
		  yyz1 = yyz1->yynext;
		  (*yyz0p)->yynext = yyz;
		}
	      yyz0p = &(*yyz0p)->yynext;
	    }
	  yys1->yysemantics.yyfirstVal = yys0->yysemantics.yyfirstVal;
	}
    }
}

/** Y0 and Y1 represent two possible actions to take in a given
 *  parsing state; return 0 if no combination is possible,
 *  1 if user-mergeable, 2 if Y0 is preferred, 3 if Y1 is preferred.  */
static int
yypreference (yySemanticOption* y0, yySemanticOption* y1)
{
  yyRuleNum r0 = y0->yyrule, r1 = y1->yyrule;
  int p0 = yydprec[r0], p1 = yydprec[r1];

  if (p0 == p1)
    {
      if (yymerger[r0] == 0 || yymerger[r0] != yymerger[r1])
	return 0;
      else
	return 1;
    }
  if (p0 == 0 || p1 == 0)
    return 0;
  if (p0 < p1)
    return 3;
  if (p1 < p0)
    return 2;
  return 0;
}

static YYRESULTTAG yyresolveValue (yyGLRState* yys,
				   yyGLRStack* yystackp, AST* parsed_tree);


/** Resolve the previous N states starting at and including state S.  If result
 *  != yyok, some states may have been left unresolved possibly with empty
 *  semantic option chains.  Regardless of whether result = yyok, each state
 *  has been left with consistent data so that yydestroyGLRState can be invoked
 *  if necessary.  */
static YYRESULTTAG
yyresolveStates (yyGLRState* yys, int yyn,
		 yyGLRStack* yystackp, AST* parsed_tree)
{
  if (0 < yyn)
    {
      YYASSERT (yys->yypred);
      YYCHK (yyresolveStates (yys->yypred, yyn-1, yystackp, parsed_tree));
      if (! yys->yyresolved)
	YYCHK (yyresolveValue (yys, yystackp, parsed_tree));
    }
  return yyok;
}

/** Resolve the states for the RHS of OPT, perform its user action, and return
 *  the semantic value and location.  Regardless of whether result = yyok, all
 *  RHS states have been destroyed (assuming the user action destroys all RHS
 *  semantic values if invoked).  */
static YYRESULTTAG
yyresolveAction (yySemanticOption* yyopt, yyGLRStack* yystackp,
		 YYSTYPE* yyvalp, YYLTYPE* yylocp, AST* parsed_tree)
{
  yyGLRStackItem yyrhsVals[YYMAXRHS + YYMAXLEFT + 1];
  int yynrhs;
  int yychar_current;
  YYSTYPE yylval_current;
  YYLTYPE yylloc_current;
  YYRESULTTAG yyflag;

  yynrhs = yyrhsLength (yyopt->yyrule);
  yyflag = yyresolveStates (yyopt->yystate, yynrhs, yystackp, parsed_tree);
  if (yyflag != yyok)
    {
      yyGLRState *yys;
      for (yys = yyopt->yystate; yynrhs > 0; yys = yys->yypred, yynrhs -= 1)
	yydestroyGLRState ("Cleanup: popping", yys, parsed_tree);
      return yyflag;
    }

  yyrhsVals[YYMAXRHS + YYMAXLEFT].yystate.yypred = yyopt->yystate;
  if (yynrhs == 0)
    /* Set default location.  */
    yyrhsVals[YYMAXRHS + YYMAXLEFT - 1].yystate.yyloc = yyopt->yystate->yyloc;
  yychar_current = yychar;
  yylval_current = yylval;
  yylloc_current = yylloc;
  yychar = yyopt->yyrawchar;
  yylval = yyopt->yyval;
  yylloc = yyopt->yyloc;
  yyflag = yyuserAction (yyopt->yyrule, yynrhs,
			   yyrhsVals + YYMAXRHS + YYMAXLEFT - 1,
			   yyvalp, yylocp, yystackp, parsed_tree);
  yychar = yychar_current;
  yylval = yylval_current;
  yylloc = yylloc_current;
  return yyflag;
}

#if YYDEBUG
static void
yyreportTree (yySemanticOption* yyx, int yyindent)
{
  int yynrhs = yyrhsLength (yyx->yyrule);
  int yyi;
  yyGLRState* yys;
  yyGLRState* yystates[1 + YYMAXRHS];
  yyGLRState yyleftmost_state;

  for (yyi = yynrhs, yys = yyx->yystate; 0 < yyi; yyi -= 1, yys = yys->yypred)
    yystates[yyi] = yys;
  if (yys == NULL)
    {
      yyleftmost_state.yyposn = 0;
      yystates[0] = &yyleftmost_state;
    }
  else
    yystates[0] = yys;

  if (yyx->yystate->yyposn < yys->yyposn + 1)
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, empty>\n",
	       yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
	       yyx->yyrule - 1);
  else
    YYFPRINTF (stderr, "%*s%s -> <Rule %d, tokens %lu .. %lu>\n",
	       yyindent, "", yytokenName (yylhsNonterm (yyx->yyrule)),
	       yyx->yyrule - 1, (unsigned long int) (yys->yyposn + 1),
	       (unsigned long int) yyx->yystate->yyposn);
  for (yyi = 1; yyi <= yynrhs; yyi += 1)
    {
      if (yystates[yyi]->yyresolved)
	{
	  if (yystates[yyi-1]->yyposn+1 > yystates[yyi]->yyposn)
	    YYFPRINTF (stderr, "%*s%s <empty>\n", yyindent+2, "",
		       yytokenName (yyrhs[yyprhs[yyx->yyrule]+yyi-1]));
	  else
	    YYFPRINTF (stderr, "%*s%s <tokens %lu .. %lu>\n", yyindent+2, "",
		       yytokenName (yyrhs[yyprhs[yyx->yyrule]+yyi-1]),
		       (unsigned long int) (yystates[yyi - 1]->yyposn + 1),
		       (unsigned long int) yystates[yyi]->yyposn);
	}
      else
	yyreportTree (yystates[yyi]->yysemantics.yyfirstVal, yyindent+2);
    }
}
#endif

/*ARGSUSED*/ static YYRESULTTAG
yyreportAmbiguity (yySemanticOption* yyx0,
		   yySemanticOption* yyx1, AST* parsed_tree)
{
  YYUSE (yyx0);
  YYUSE (yyx1);

#if YYDEBUG
  YYFPRINTF (stderr, "Ambiguity detected.\n");
  YYFPRINTF (stderr, "Option 1,\n");
  yyreportTree (yyx0, 2);
  YYFPRINTF (stderr, "\nOption 2,\n");
  yyreportTree (yyx1, 2);
  YYFPRINTF (stderr, "\n");
#endif

  yyerror (parsed_tree, YY_("syntax is ambiguous"));
  return yyabort;
}

/** Starting at and including state S1, resolve the location for each of the
 *  previous N1 states that is unresolved.  The first semantic option of a state
 *  is always chosen.  */
static void
yyresolveLocations (yyGLRState* yys1, int yyn1,
		    yyGLRStack *yystackp, AST* parsed_tree)
{
  if (0 < yyn1)
    {
      yyresolveLocations (yys1->yypred, yyn1 - 1, yystackp, parsed_tree);
      if (!yys1->yyresolved)
	{
	  yySemanticOption *yyoption;
	  yyGLRStackItem yyrhsloc[1 + YYMAXRHS];
	  int yynrhs;
	  int yychar_current;
	  YYSTYPE yylval_current;
	  YYLTYPE yylloc_current;
	  yyoption = yys1->yysemantics.yyfirstVal;
	  YYASSERT (yyoption != NULL);
	  yynrhs = yyrhsLength (yyoption->yyrule);
	  if (yynrhs > 0)
	    {
	      yyGLRState *yys;
	      int yyn;
	      yyresolveLocations (yyoption->yystate, yynrhs,
				  yystackp, parsed_tree);
	      for (yys = yyoption->yystate, yyn = yynrhs;
		   yyn > 0;
		   yys = yys->yypred, yyn -= 1)
		yyrhsloc[yyn].yystate.yyloc = yys->yyloc;
	    }
	  else
	    {
	      /* Both yyresolveAction and yyresolveLocations traverse the GSS
		 in reverse rightmost order.  It is only necessary to invoke
		 yyresolveLocations on a subforest for which yyresolveAction
		 would have been invoked next had an ambiguity not been
		 detected.  Thus the location of the previous state (but not
		 necessarily the previous state itself) is guaranteed to be
		 resolved already.  */
	      yyGLRState *yyprevious = yyoption->yystate;
	      yyrhsloc[0].yystate.yyloc = yyprevious->yyloc;
	    }
	  yychar_current = yychar;
	  yylval_current = yylval;
	  yylloc_current = yylloc;
	  yychar = yyoption->yyrawchar;
	  yylval = yyoption->yyval;
	  yylloc = yyoption->yyloc;
	  YYLLOC_DEFAULT ((yys1->yyloc), yyrhsloc, yynrhs);
	  yychar = yychar_current;
	  yylval = yylval_current;
	  yylloc = yylloc_current;
	}
    }
}

/** Resolve the ambiguity represented in state S, perform the indicated
 *  actions, and set the semantic value of S.  If result != yyok, the chain of
 *  semantic options in S has been cleared instead or it has been left
 *  unmodified except that redundant options may have been removed.  Regardless
 *  of whether result = yyok, S has been left with consistent data so that
 *  yydestroyGLRState can be invoked if necessary.  */
static YYRESULTTAG
yyresolveValue (yyGLRState* yys, yyGLRStack* yystackp, AST* parsed_tree)
{
  yySemanticOption* yyoptionList = yys->yysemantics.yyfirstVal;
  yySemanticOption* yybest;
  yySemanticOption** yypp;
  yybool yymerge;
  YYSTYPE yysval;
  YYRESULTTAG yyflag;
  YYLTYPE *yylocp = &yys->yyloc;

  yybest = yyoptionList;
  yymerge = yyfalse;
  for (yypp = &yyoptionList->yynext; *yypp != NULL; )
    {
      yySemanticOption* yyp = *yypp;

      if (yyidenticalOptions (yybest, yyp))
	{
	  yymergeOptionSets (yybest, yyp);
	  *yypp = yyp->yynext;
	}
      else
	{
	  switch (yypreference (yybest, yyp))
	    {
	    case 0:
	      yyresolveLocations (yys, 1, yystackp, parsed_tree);
	      return yyreportAmbiguity (yybest, yyp, parsed_tree);
	      break;
	    case 1:
	      yymerge = yytrue;
	      break;
	    case 2:
	      break;
	    case 3:
	      yybest = yyp;
	      yymerge = yyfalse;
	      break;
	    default:
	      /* This cannot happen so it is not worth a YYASSERT (yyfalse),
		 but some compilers complain if the default case is
		 omitted.  */
	      break;
	    }
	  yypp = &yyp->yynext;
	}
    }

  if (yymerge)
    {
      yySemanticOption* yyp;
      int yyprec = yydprec[yybest->yyrule];
      yyflag = yyresolveAction (yybest, yystackp, &yysval,
				yylocp, parsed_tree);
      if (yyflag == yyok)
	for (yyp = yybest->yynext; yyp != NULL; yyp = yyp->yynext)
	  {
	    if (yyprec == yydprec[yyp->yyrule])
	      {
		YYSTYPE yysval_other;
		YYLTYPE yydummy;
		yyflag = yyresolveAction (yyp, yystackp, &yysval_other,
					  &yydummy, parsed_tree);
		if (yyflag != yyok)
		  {
		    yydestruct ("Cleanup: discarding incompletely merged value for",
				yystos[yys->yylrState],
				&yysval, yylocp, parsed_tree);
		    break;
		  }
		yyuserMerge (yymerger[yyp->yyrule], &yysval, &yysval_other);
	      }
	  }
    }
  else
    yyflag = yyresolveAction (yybest, yystackp, &yysval, yylocp, parsed_tree);

  if (yyflag == yyok)
    {
      yys->yyresolved = yytrue;
      yys->yysemantics.yysval = yysval;
    }
  else
    yys->yysemantics.yyfirstVal = NULL;
  return yyflag;
}

static YYRESULTTAG
yyresolveStack (yyGLRStack* yystackp, AST* parsed_tree)
{
  if (yystackp->yysplitPoint != NULL)
    {
      yyGLRState* yys;
      int yyn;

      for (yyn = 0, yys = yystackp->yytops.yystates[0];
	   yys != yystackp->yysplitPoint;
	   yys = yys->yypred, yyn += 1)
	continue;
      YYCHK (yyresolveStates (yystackp->yytops.yystates[0], yyn, yystackp
			     , parsed_tree));
    }
  return yyok;
}

static void
yycompressStack (yyGLRStack* yystackp)
{
  yyGLRState* yyp, *yyq, *yyr;

  if (yystackp->yytops.yysize != 1 || yystackp->yysplitPoint == NULL)
    return;

  for (yyp = yystackp->yytops.yystates[0], yyq = yyp->yypred, yyr = NULL;
       yyp != yystackp->yysplitPoint;
       yyr = yyp, yyp = yyq, yyq = yyp->yypred)
    yyp->yypred = yyr;

  yystackp->yyspaceLeft += yystackp->yynextFree - yystackp->yyitems;
  yystackp->yynextFree = ((yyGLRStackItem*) yystackp->yysplitPoint) + 1;
  yystackp->yyspaceLeft -= yystackp->yynextFree - yystackp->yyitems;
  yystackp->yysplitPoint = NULL;
  yystackp->yylastDeleted = NULL;

  while (yyr != NULL)
    {
      yystackp->yynextFree->yystate = *yyr;
      yyr = yyr->yypred;
      yystackp->yynextFree->yystate.yypred = &yystackp->yynextFree[-1].yystate;
      yystackp->yytops.yystates[0] = &yystackp->yynextFree->yystate;
      yystackp->yynextFree += 1;
      yystackp->yyspaceLeft -= 1;
    }
}

static YYRESULTTAG
yyprocessOneStack (yyGLRStack* yystackp, size_t yyk,
		   size_t yyposn, AST* parsed_tree)
{
  int yyaction;
  const short int* yyconflicts;
  yyRuleNum yyrule;

  while (yystackp->yytops.yystates[yyk] != NULL)
    {
      yyStateNum yystate = yystackp->yytops.yystates[yyk]->yylrState;
      YYDPRINTF ((stderr, "Stack %lu Entering state %d\n",
		  (unsigned long int) yyk, yystate));

      YYASSERT (yystate != YYFINAL);

      if (yyisDefaultedState (yystate))
	{
	  yyrule = yydefaultAction (yystate);
	  if (yyrule == 0)
	    {
	      YYDPRINTF ((stderr, "Stack %lu dies.\n",
			  (unsigned long int) yyk));
	      yymarkStackDeleted (yystackp, yyk);
	      return yyok;
	    }
	  YYCHK (yyglrReduce (yystackp, yyk, yyrule, yyfalse, parsed_tree));
	}
      else
	{
	  yySymbol yytoken;
	  yystackp->yytops.yylookaheadNeeds[yyk] = yytrue;
	  if (yychar == YYEMPTY)
	    {
	      YYDPRINTF ((stderr, "Reading a token: "));
	      yychar = YYLEX;
	    }

	  if (yychar <= YYEOF)
	    {
	      yychar = yytoken = YYEOF;
	      YYDPRINTF ((stderr, "Now at end of input.\n"));
	    }
	  else
	    {
	      yytoken = YYTRANSLATE (yychar);
	      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
	    }

	  yygetLRActions (yystate, yytoken, &yyaction, &yyconflicts);

	  while (*yyconflicts != 0)
	    {
	      size_t yynewStack = yysplitStack (yystackp, yyk);
	      YYDPRINTF ((stderr, "Splitting off stack %lu from %lu.\n",
			  (unsigned long int) yynewStack,
			  (unsigned long int) yyk));
	      YYCHK (yyglrReduce (yystackp, yynewStack,
				  *yyconflicts, yyfalse, parsed_tree));
	      YYCHK (yyprocessOneStack (yystackp, yynewStack,
					yyposn, parsed_tree));
	      yyconflicts += 1;
	    }

	  if (yyisShiftAction (yyaction))
	    break;
	  else if (yyisErrorAction (yyaction))
	    {
	      YYDPRINTF ((stderr, "Stack %lu dies.\n",
			  (unsigned long int) yyk));
	      yymarkStackDeleted (yystackp, yyk);
	      break;
	    }
	  else
	    YYCHK (yyglrReduce (yystackp, yyk, -yyaction,
				yyfalse, parsed_tree));
	}
    }
  return yyok;
}

/*ARGSUSED*/ static void
yyreportSyntaxError (yyGLRStack* yystackp, AST* parsed_tree)
{
  if (yystackp->yyerrState == 0)
    {
#if YYERROR_VERBOSE
      int yyn;
      yyn = yypact[yystackp->yytops.yystates[0]->yylrState];
      if (YYPACT_NINF < yyn && yyn <= YYLAST)
	{
	  yySymbol yytoken = YYTRANSLATE (yychar);
	  size_t yysize0 = yytnamerr (NULL, yytokenName (yytoken));
	  size_t yysize = yysize0;
	  size_t yysize1;
	  yybool yysize_overflow = yyfalse;
	  char* yymsg = NULL;
	  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn + 1;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytokenName (yytoken);
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytokenName (yyx);
		yysize1 = yysize + yytnamerr (NULL, yytokenName (yyx));
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + strlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow)
	    yymsg = (char *) YYMALLOC (yysize);

	  if (yymsg)
	    {
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (parsed_tree, yymsg);
	      YYFREE (yymsg);
	    }
	  else
	    {
	      yyerror (parsed_tree, YY_("syntax error"));
	      yyMemoryExhausted (yystackp);
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (parsed_tree, YY_("syntax error"));
      yynerrs += 1;
    }
}

/* Recover from a syntax error on *YYSTACKP, assuming that *YYSTACKP->YYTOKENP,
   yylval, and yylloc are the syntactic category, semantic value, and location
   of the lookahead.  */
/*ARGSUSED*/ static void
yyrecoverSyntaxError (yyGLRStack* yystackp, AST* parsed_tree)
{
  size_t yyk;
  int yyj;

  if (yystackp->yyerrState == 3)
    /* We just shifted the error token and (perhaps) took some
       reductions.  Skip tokens until we can proceed.  */
    while (YYID (yytrue))
      {
	yySymbol yytoken;
	if (yychar == YYEOF)
	  yyFail (yystackp, parsed_tree, NULL);
	if (yychar != YYEMPTY)
	  {
	    /* We throw away the lookahead, but the error range
	       of the shifted error token must take it into account.  */
	    yyGLRState *yys = yystackp->yytops.yystates[0];
	    yyGLRStackItem yyerror_range[3];
	    yyerror_range[1].yystate.yyloc = yys->yyloc;
	    yyerror_range[2].yystate.yyloc = yylloc;
	    YYLLOC_DEFAULT ((yys->yyloc), yyerror_range, 2);
	    yytoken = YYTRANSLATE (yychar);
	    yydestruct ("Error: discarding",
			yytoken, &yylval, &yylloc, parsed_tree);
	  }
	YYDPRINTF ((stderr, "Reading a token: "));
	yychar = YYLEX;
	if (yychar <= YYEOF)
	  {
	    yychar = yytoken = YYEOF;
	    YYDPRINTF ((stderr, "Now at end of input.\n"));
	  }
	else
	  {
	    yytoken = YYTRANSLATE (yychar);
	    YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
	  }
	yyj = yypact[yystackp->yytops.yystates[0]->yylrState];
	if (yyis_pact_ninf (yyj))
	  return;
	yyj += yytoken;
	if (yyj < 0 || YYLAST < yyj || yycheck[yyj] != yytoken)
	  {
	    if (yydefact[yystackp->yytops.yystates[0]->yylrState] != 0)
	      return;
	  }
	else if (yytable[yyj] != 0 && ! yyis_table_ninf (yytable[yyj]))
	  return;
      }

  /* Reduce to one stack.  */
  for (yyk = 0; yyk < yystackp->yytops.yysize; yyk += 1)
    if (yystackp->yytops.yystates[yyk] != NULL)
      break;
  if (yyk >= yystackp->yytops.yysize)
    yyFail (yystackp, parsed_tree, NULL);
  for (yyk += 1; yyk < yystackp->yytops.yysize; yyk += 1)
    yymarkStackDeleted (yystackp, yyk);
  yyremoveDeletes (yystackp);
  yycompressStack (yystackp);

  /* Now pop stack until we find a state that shifts the error token.  */
  yystackp->yyerrState = 3;
  while (yystackp->yytops.yystates[0] != NULL)
    {
      yyGLRState *yys = yystackp->yytops.yystates[0];
      yyj = yypact[yys->yylrState];
      if (! yyis_pact_ninf (yyj))
	{
	  yyj += YYTERROR;
	  if (0 <= yyj && yyj <= YYLAST && yycheck[yyj] == YYTERROR
	      && yyisShiftAction (yytable[yyj]))
	    {
	      /* Shift the error token having adjusted its location.  */
	      YYLTYPE yyerrloc;
	      yystackp->yyerror_range[2].yystate.yyloc = yylloc;
	      YYLLOC_DEFAULT (yyerrloc, (yystackp->yyerror_range), 2);
	      YY_SYMBOL_PRINT ("Shifting", yystos[yytable[yyj]],
			       &yylval, &yyerrloc);
	      yyglrShift (yystackp, 0, yytable[yyj],
			  yys->yyposn, &yylval, &yyerrloc);
	      yys = yystackp->yytops.yystates[0];
	      break;
	    }
	}
      yystackp->yyerror_range[1].yystate.yyloc = yys->yyloc;
      if (yys->yypred != NULL)
	yydestroyGLRState ("Error: popping", yys, parsed_tree);
      yystackp->yytops.yystates[0] = yys->yypred;
      yystackp->yynextFree -= 1;
      yystackp->yyspaceLeft += 1;
    }
  if (yystackp->yytops.yystates[0] == NULL)
    yyFail (yystackp, parsed_tree, NULL);
}

#define YYCHK1(YYE)							     \
  do {									     \
    switch (YYE) {							     \
    case yyok:								     \
      break;								     \
    case yyabort:							     \
      goto yyabortlab;							     \
    case yyaccept:							     \
      goto yyacceptlab;							     \
    case yyerr:								     \
      goto yyuser_error;						     \
    default:								     \
      goto yybuglab;							     \
    }									     \
  } while (YYID (0))


/*----------.
| yyparse.  |
`----------*/

int
yyparse (AST* parsed_tree)
{
  int yyresult;
  yyGLRStack yystack;
  yyGLRStack* const yystackp = &yystack;
  size_t yyposn;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yychar = YYEMPTY;
  yylval = yyval_default;

#if YYLTYPE_IS_TRIVIAL
  yylloc.first_line   = yylloc.last_line   = 1;
  yylloc.first_column = yylloc.last_column = 1;
#endif


  if (! yyinitGLRStack (yystackp, YYINITDEPTH))
    goto yyexhaustedlab;
  switch (YYSETJMP (yystack.yyexception_buffer))
    {
    case 0: break;
    case 1: goto yyabortlab;
    case 2: goto yyexhaustedlab;
    default: goto yybuglab;
    }
  yyglrShift (&yystack, 0, 0, 0, &yylval, &yylloc);
  yyposn = 0;

  while (YYID (yytrue))
    {
      /* For efficiency, we have two loops, the first of which is
	 specialized to deterministic operation (single stack, no
	 potential ambiguity).  */
      /* Standard mode */
      while (YYID (yytrue))
	{
	  yyRuleNum yyrule;
	  int yyaction;
	  const short int* yyconflicts;

	  yyStateNum yystate = yystack.yytops.yystates[0]->yylrState;
	  YYDPRINTF ((stderr, "Entering state %d\n", yystate));
	  if (yystate == YYFINAL)
	    goto yyacceptlab;
	  if (yyisDefaultedState (yystate))
	    {
	      yyrule = yydefaultAction (yystate);
	      if (yyrule == 0)
		{
		  yystack.yyerror_range[1].yystate.yyloc = yylloc;
		  yyreportSyntaxError (&yystack, parsed_tree);
		  goto yyuser_error;
		}
	      YYCHK1 (yyglrReduce (&yystack, 0, yyrule, yytrue, parsed_tree));
	    }
	  else
	    {
	      yySymbol yytoken;
	      if (yychar == YYEMPTY)
		{
		  YYDPRINTF ((stderr, "Reading a token: "));
		  yychar = YYLEX;
		}

	      if (yychar <= YYEOF)
		{
		  yychar = yytoken = YYEOF;
		  YYDPRINTF ((stderr, "Now at end of input.\n"));
		}
	      else
		{
		  yytoken = YYTRANSLATE (yychar);
		  YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
		}

	      yygetLRActions (yystate, yytoken, &yyaction, &yyconflicts);
	      if (*yyconflicts != 0)
		break;
	      if (yyisShiftAction (yyaction))
		{
		  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);
		  yychar = YYEMPTY;
		  yyposn += 1;
		  yyglrShift (&yystack, 0, yyaction, yyposn, &yylval, &yylloc);
		  if (0 < yystack.yyerrState)
		    yystack.yyerrState -= 1;
		}
	      else if (yyisErrorAction (yyaction))
		{
		  yystack.yyerror_range[1].yystate.yyloc = yylloc;
		  yyreportSyntaxError (&yystack, parsed_tree);
		  goto yyuser_error;
		}
	      else
		YYCHK1 (yyglrReduce (&yystack, 0, -yyaction, yytrue, parsed_tree));
	    }
	}

      while (YYID (yytrue))
	{
	  yySymbol yytoken_to_shift;
	  size_t yys;

	  for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
	    yystackp->yytops.yylookaheadNeeds[yys] = yychar != YYEMPTY;

	  /* yyprocessOneStack returns one of three things:

	      - An error flag.  If the caller is yyprocessOneStack, it
		immediately returns as well.  When the caller is finally
		yyparse, it jumps to an error label via YYCHK1.

	      - yyok, but yyprocessOneStack has invoked yymarkStackDeleted
		(&yystack, yys), which sets the top state of yys to NULL.  Thus,
		yyparse's following invocation of yyremoveDeletes will remove
		the stack.

	      - yyok, when ready to shift a token.

	     Except in the first case, yyparse will invoke yyremoveDeletes and
	     then shift the next token onto all remaining stacks.  This
	     synchronization of the shift (that is, after all preceding
	     reductions on all stacks) helps prevent double destructor calls
	     on yylval in the event of memory exhaustion.  */

	  for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
	    YYCHK1 (yyprocessOneStack (&yystack, yys, yyposn, parsed_tree));
	  yyremoveDeletes (&yystack);
	  if (yystack.yytops.yysize == 0)
	    {
	      yyundeleteLastStack (&yystack);
	      if (yystack.yytops.yysize == 0)
		yyFail (&yystack, parsed_tree, YY_("syntax error"));
	      YYCHK1 (yyresolveStack (&yystack, parsed_tree));
	      YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));
	      yystack.yyerror_range[1].yystate.yyloc = yylloc;
	      yyreportSyntaxError (&yystack, parsed_tree);
	      goto yyuser_error;
	    }

	  /* If any yyglrShift call fails, it will fail after shifting.  Thus,
	     a copy of yylval will already be on stack 0 in the event of a
	     failure in the following loop.  Thus, yychar is set to YYEMPTY
	     before the loop to make sure the user destructor for yylval isn't
	     called twice.  */
	  yytoken_to_shift = YYTRANSLATE (yychar);
	  yychar = YYEMPTY;
	  yyposn += 1;
	  for (yys = 0; yys < yystack.yytops.yysize; yys += 1)
	    {
	      int yyaction;
	      const short int* yyconflicts;
	      yyStateNum yystate = yystack.yytops.yystates[yys]->yylrState;
	      yygetLRActions (yystate, yytoken_to_shift, &yyaction,
			      &yyconflicts);
	      /* Note that yyconflicts were handled by yyprocessOneStack.  */
	      YYDPRINTF ((stderr, "On stack %lu, ", (unsigned long int) yys));
	      YY_SYMBOL_PRINT ("shifting", yytoken_to_shift, &yylval, &yylloc);
	      yyglrShift (&yystack, yys, yyaction, yyposn,
			  &yylval, &yylloc);
	      YYDPRINTF ((stderr, "Stack %lu now in state #%d\n",
			  (unsigned long int) yys,
			  yystack.yytops.yystates[yys]->yylrState));
	    }

	  if (yystack.yytops.yysize == 1)
	    {
	      YYCHK1 (yyresolveStack (&yystack, parsed_tree));
	      YYDPRINTF ((stderr, "Returning to deterministic operation.\n"));
	      yycompressStack (&yystack);
	      break;
	    }
	}
      continue;
    yyuser_error:
      yyrecoverSyntaxError (&yystack, parsed_tree);
      yyposn = yystack.yytops.yystates[0]->yyposn;
    }

 yyacceptlab:
  yyresult = 0;
  goto yyreturn;

 yybuglab:
  YYASSERT (yyfalse);
  goto yyabortlab;

 yyabortlab:
  yyresult = 1;
  goto yyreturn;

 yyexhaustedlab:
  yyerror (parsed_tree, YY_("memory exhausted"));
  yyresult = 2;
  goto yyreturn;

 yyreturn:
  if (yychar != YYEMPTY)
    yydestruct ("Cleanup: discarding lookahead",
		YYTRANSLATE (yychar),
		&yylval, &yylloc, parsed_tree);

  /* If the stack is well-formed, pop the stack until it is empty,
     destroying its entries as we go.  But free the stack regardless
     of whether it is well-formed.  */
  if (yystack.yyitems)
    {
      yyGLRState** yystates = yystack.yytops.yystates;
      if (yystates)
	{
	  size_t yysize = yystack.yytops.yysize;
	  size_t yyk;
	  for (yyk = 0; yyk < yysize; yyk += 1)
	    if (yystates[yyk])
	      {
		while (yystates[yyk])
		  {
		    yyGLRState *yys = yystates[yyk];
		    yystack.yyerror_range[1].yystate.yyloc = yys->yyloc;
		    if (yys->yypred != NULL)
		      yydestroyGLRState ("Cleanup: popping", yys, parsed_tree);
		    yystates[yyk] = yys->yypred;
		    yystack.yynextFree -= 1;
		    yystack.yyspaceLeft += 1;
		  }
		break;
	      }
	}
      yyfreeGLRStack (&yystack);
    }

  /* Make sure YYID is used.  */
  return YYID (yyresult);
}

/* DEBUGGING ONLY */
#if YYDEBUG
static void yypstack (yyGLRStack* yystackp, size_t yyk)
  __attribute__ ((__unused__));
static void yypdumpstack (yyGLRStack* yystackp) __attribute__ ((__unused__));

static void
yy_yypstack (yyGLRState* yys)
{
  if (yys->yypred)
    {
      yy_yypstack (yys->yypred);
      YYFPRINTF (stderr, " -> ");
    }
  YYFPRINTF (stderr, "%d@%lu", yys->yylrState,
             (unsigned long int) yys->yyposn);
}

static void
yypstates (yyGLRState* yyst)
{
  if (yyst == NULL)
    YYFPRINTF (stderr, "<null>");
  else
    yy_yypstack (yyst);
  YYFPRINTF (stderr, "\n");
}

static void
yypstack (yyGLRStack* yystackp, size_t yyk)
{
  yypstates (yystackp->yytops.yystates[yyk]);
}

#define YYINDEX(YYX)							     \
    ((YYX) == NULL ? -1 : (yyGLRStackItem*) (YYX) - yystackp->yyitems)


static void
yypdumpstack (yyGLRStack* yystackp)
{
  yyGLRStackItem* yyp;
  size_t yyi;
  for (yyp = yystackp->yyitems; yyp < yystackp->yynextFree; yyp += 1)
    {
      YYFPRINTF (stderr, "%3lu. ",
                 (unsigned long int) (yyp - yystackp->yyitems));
      if (*(yybool *) yyp)
	{
	  YYFPRINTF (stderr, "Res: %d, LR State: %d, posn: %lu, pred: %ld",
		     yyp->yystate.yyresolved, yyp->yystate.yylrState,
		     (unsigned long int) yyp->yystate.yyposn,
		     (long int) YYINDEX (yyp->yystate.yypred));
	  if (! yyp->yystate.yyresolved)
	    YYFPRINTF (stderr, ", firstVal: %ld",
		       (long int) YYINDEX (yyp->yystate
                                             .yysemantics.yyfirstVal));
	}
      else
	{
	  YYFPRINTF (stderr, "Option. rule: %d, state: %ld, next: %ld",
		     yyp->yyoption.yyrule - 1,
		     (long int) YYINDEX (yyp->yyoption.yystate),
		     (long int) YYINDEX (yyp->yyoption.yynext));
	}
      YYFPRINTF (stderr, "\n");
    }
  YYFPRINTF (stderr, "Tops:");
  for (yyi = 0; yyi < yystackp->yytops.yysize; yyi += 1)
    YYFPRINTF (stderr, "%lu: %ld; ", (unsigned long int) yyi,
	       (long int) YYINDEX (yystackp->yytops.yystates[yyi]));
  YYFPRINTF (stderr, "\n");
}
#endif



/* Line 2634 of glr.c  */
#line 5935 "src/frontend/c99.y"











































// This is code



#include "cxx-utils.h"

static AST ambiguityHandler (YYSTYPE x0, YYSTYPE x1)
{
	AST son0 = x0.ast;
	AST son1 = x1.ast;

	if (son0 == son1) 
	{
        return son1;
	}

    return ast_make_ambiguous(son0, son1);
}



void yyerror(AST* parsed_tree UNUSED_PARAMETER, const char* c)
{
	fprintf(stderr, "%s:%d:%d: error: %s\n",
            mc99lloc.first_filename,
            mc99lloc.first_line,
            mc99lloc.first_column,
            c);
}

