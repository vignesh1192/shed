/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define to 1 if Analysis is enabled */
#define ANALYSIS_ENABLED 1

/* CUDA installation path */
/* #undef CUDA_DIR */

/* Default type environment */
#define DEFAULT_TYPE_ENVIRONMENT "linux-arm"

/* Define to enable instrumentation of Mercurium using Extrae */
/* #undef EXTRAE_ENABLED */

/* Define to 1 if binaries are installed in pkglibdir */
#define FORTRAN_NEW_SCANNER 1

/* Define to 1 if backtrace is available */
#define HAVE_BACKTRACE 1

/* Define to 1 if you have the `cacoshl' function. */
#define HAVE_CACOSHL 1

/* Define to 1 if you have the `cacosl' function. */
#define HAVE_CACOSL 1

/* Define to 1 if you have the `casinhl' function. */
#define HAVE_CASINHL 1

/* Define to 1 if you have the `casinl' function. */
#define HAVE_CASINL 1

/* Define to 1 if you have the `catanhl' function. */
#define HAVE_CATANHL 1

/* Define to 1 if you have the `catanl' function. */
#define HAVE_CATANL 1

/* Define to 1 if you have the `ccoshl' function. */
#define HAVE_CCOSHL 1

/* Define to 1 if you have the `ccosl' function. */
#define HAVE_CCOSL 1

/* Define to 1 if you have the `cpow' function. */
#define HAVE_CPOW 1

/* Define to 1 if you have the `cpowf' function. */
#define HAVE_CPOWF 1

/* Define to 1 if you have the `cpowl' function. */
#define HAVE_CPOWL 1

/* Define to 1 if you have the `csinhl' function. */
#define HAVE_CSINHL 1

/* Define to 1 if you have the `csinl' function. */
#define HAVE_CSINL 1

/* Define to 1 if you have the `ctanhl' function. */
#define HAVE_CTANHL 1

/* Define to 1 if you have the `ctanl' function. */
#define HAVE_CTANL 1

/* define if the compiler supports basic C++11 syntax */
#define HAVE_CXX11 1

/* Define to 1 if you have the declaration of `cygwin_conv_path', and to 0 if
   you don't. */
/* #undef HAVE_DECL_CYGWIN_CONV_PATH */

/* Define if you have the GNU dld library. */
/* #undef HAVE_DLD */

/* Define to 1 if you have the `dlerror' function. */
#define HAVE_DLERROR 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define if you have the _dyld_func_lookup function. */
/* #undef HAVE_DYLD */

/* Define to 1 if Fortran KIND=16 is supported */
/* #undef HAVE_FORTRAN_KIND16 */

/* Define if you have the iconv() function and it works. */
#define HAVE_ICONV 1

/* Define to 1 if __int128 is available */
/* #undef HAVE_INT128 */

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define if you have the libdl library or equivalent. */
#define HAVE_LIBDL 1

/* Define if libdlloader will be built on this platform */
#define HAVE_LIBDLLOADER 1

/* Define to 1 if mallinfo is available */
#define HAVE_MALLINFO 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if mkdtemp is available */
#define HAVE_MKDTEMP 1

/* Define to 1 if open_memstream is available */
#define HAVE_OPEN_MEMSTREAM 1

/* Define to 1 if quadmath.h is fully useable */
/* #undef HAVE_QUADMATH_H */

/* Define if you have the shl_load function. */
/* #undef HAVE_SHL_LOAD */

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if strsignal is available */
#define HAVE_STRSIGNAL 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if __builtin_exp10 is available */
#define HAVE__BUILTIN_EXP10 1

/* Define to 1 if __builtin_exp10f is available */
#define HAVE__BUILTIN_EXP10F 1

/* Define to 1 if __builtin_exp10l is available */
#define HAVE__BUILTIN_EXP10L 1

/* Define to 1 if __builtin_fpclassify is available */
#define HAVE__BUILTIN_FPCLASSIFY 1

/* Define to 1 if __builtin_huge_valq is available */
/* #undef HAVE__BUILTIN_HUGE_VALQ */

/* Define to 1 if __builtin_isinff is available */
#define HAVE__BUILTIN_ISINFF 1

/* Define to 1 if __builtin_isinfl is available */
#define HAVE__BUILTIN_ISINFL 1

/* Define to 1 if __builtin_isnanf is available */
#define HAVE__BUILTIN_ISNANF 1

/* Define to 1 if __builtin_isnanl is available */
#define HAVE__BUILTIN_ISNANL 1

/* Define to 1 if __builtin_nan is available */
#define HAVE__BUILTIN_NAN 1

/* Define to 1 if __builtin_nanf is available */
#define HAVE__BUILTIN_NANF 1

/* Define to 1 if __builtin_nanl is available */
#define HAVE__BUILTIN_NANL 1

/* Define to 1 if __builtin_nexttoward is available */
#define HAVE__BUILTIN_NEXTTOWARD 1

/* Define to 1 if __builtin_nexttowardf is available */
#define HAVE__BUILTIN_NEXTTOWARDF 1

/* Define to 1 if __builtin_popcount is available */
#define HAVE__BUILTIN_POPCOUNT 1

/* Define to 1 if __builtin_signbit is available */
#define HAVE__BUILTIN_SIGNBIT 1

/* Define as const if the declaration of iconv() needs const. */
#define ICONV_CONST 

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* Build version */
#define MCXX_BUILD_VERSION "unknown revision"

/* Configure line */
#define MCXX_CONFIGURE_ARGS "./configure --prefix=/home/vignesh/workspace/codesign/sched/ompss/ompss-15.06/bin/ --enable-ompss --with-nanox=/home/vignesh/workspace/codesign/sched/ompss/ompss-15.06/bin/ --disable-nanox-opencl-device --host=arm-linux-gnueabihf --disable-fortran-tests --disable-ifort"

/* Define to 1 if your C compiler doesn't accept -c and -o together. */
/* #undef NO_MINUS_C_MINUS_O */

/* Name of package */
#define PACKAGE "mcxx"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "roger.ferrer@bsc.es"

/* Define to the full name of this package. */
#define PACKAGE_NAME "mcxx"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "mcxx 1.99.8"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "mcxx"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.99.8"

/* Define to 1 if binaries are installed in pkglibdir */
/* #undef PKGLIB_INSTALL */

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Disables vectorization */
/* #undef VECTORIZATION_DISABLED */

/* Version number of package */
#define VERSION "1.99.8"

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */
